//
//  ATTouchIDObject.m
//  ATYunPay
//
//  Created by 艾藤软件 on 16/9/7.
//  Copyright © 2016年 艾腾软件. All rights reserved.
//

#import "ATTouchIDObject.h"
#import <LocalAuthentication/LocalAuthentication.h>

@implementation ATTouchIDObject

- (void)start_TouchID_WithMessage:(NSString *)message FallBackTitle:(NSString *)fallBackTitle Delegate:(id<TouchID_Delegate>)delegate
{
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = fallBackTitle;

    NSError *error = nil;
    self.delegate = delegate;
    //判断代理人是否为空
    if (self.delegate != nil) {
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            //使用context对象对识别的情况进行评估
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:message reply:^(BOOL success, NSError * _Nullable error) {
                //识别成功:
                if (success) {
                    if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeSuccess)]) {
                        //必须回到主线程执行,否则在更新UI时会出错！以下相同
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [self.delegate TouchID_AuthorizeSuccess];
                        }];
                    }
                }
                //识别失败（对应代理方法的每种情况,不实现对应方法就没有反应）
                else if (error)
                {
                    NSLog(@"error = %ld",error.code);
                    switch (error.code) {
                        case LAErrorAuthenticationFailed:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeFailure)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeFailure];
                                }];
                            }
                            break;
                        }
                        case LAErrorUserCancel:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeUserCancel)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeUserCancel];
                                }];
                            }
                            break;
                        }
                        case LAErrorUserFallback:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeUserFallBack)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeUserFallBack];
                                }];
                            }
                            break;
                        }
                        case LAErrorSystemCancel:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeSystemCancel)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeSystemCancel];
                                }];
                            }
                            break;
                        }
                        case LAErrorTouchIDNotEnrolled:
                        {
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDNotSet)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeTouchIDNotSet];
                                }];
                            }
                            break;
                        }
                        case LAErrorPasscodeNotSet:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizePasswordNotSet)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizePasswordNotSet];
                                }];
                            }
                            break;
                        }
                        case LAErrorTouchIDNotAvailable:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDNotAvailable)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeTouchIDNotAvailable];
                                }];
                            }
                            break;
                        }
                        case LAErrorTouchIDLockout:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDNotLockOut)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeTouchIDNotLockOut];
                                }];
                            }
                            break;
                        }
                        case LAErrorAppCancel:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDAppCancel)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeTouchIDAppCancel];
                                }];
                            }
                            break;
                        }
                        case LAErrorInvalidContext:{
                            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDInvalidContext)]) {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    [self.delegate TouchID_AuthorizeTouchIDInvalidContext];
                                }];
                            }
                            break;
                        }
                        default:
                            break;
                    }
                }
            }];
        }
        else if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error])
        {
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:message reply:^(BOOL success, NSError * _Nullable error) {
                //识别成功:
                if (success) {
                    if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeOpenLockSuccess)]) {
                        //必须回到主线程执行,否则在更新UI时会出错！以下相同
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [self.delegate TouchID_AuthorizeOpenLockSuccess];
                        }];
                    }
                }
                //识别失败（对应代理方法的每种情况,不实现对应方法就没有反应）
                else if (error)
                {
                    NSLog(@"error = %ld",error.code);
                    if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeOpenLockFail)]) {
                        //必须回到主线程执行,否则在更新UI时会出错！以下相同
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [self.delegate TouchID_AuthorizeOpenLockFail];
                        }];
                    }
                }
            }];
        }
        else
        {
            if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeTouchIDInvalidContext)]) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.delegate TouchID_AuthorizeNotSupport];
                }];
            }
            
        }
    }
    //设备不支持指纹识别
    else
    {
        if ([self.delegate respondsToSelector:@selector(TouchID_AuthorizeNotSupport)]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.delegate TouchID_AuthorizeNotSupport];
            }];
        }
    }
}

@end

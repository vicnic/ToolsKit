//
//  ATPersonModel.m
//  ATAddressBook
//
//  Created by 陈俏俊 on 2017/8/11.
//  Copyright © 2017年 陈俏俊. All rights reserved.
//

#import "ATPersonModel.h"

@implementation ATPersonModel

- (NSMutableArray *)mobileArray
{
    if(!_mobileArray)
    {
        _mobileArray = [NSMutableArray array];
    }
    return _mobileArray;
}

@end

//
//  ATPersonModel.h
//  ATAddressBook
//
//  Created by 陈俏俊 on 2017/8/11.
//  Copyright © 2017年 陈俏俊. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ATServerBookModel.h"
@interface ATPersonModel : NSObject

/** 联系人姓名*/
@property (nonatomic, copy) NSString *name;
/** 联系人电话数组,因为一个联系人可能存储多个号码*/
@property (nonatomic, strong) NSMutableArray *mobileArray;

@property (nonatomic, strong) NSString *nameMobile;
/** 联系人头像*/
@property (nonatomic, strong) UIImage *headerImage;

@property (nonatomic, strong) ATServerBookModel *serverModel;

@property (nonatomic, assign) NSInteger isFriend;


@end

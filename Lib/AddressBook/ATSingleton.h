//
//  ATSingleton.h
//  ATAddressBook
//
//  Created by 陈俏俊 on 2017/8/11.
//  Copyright © 2017年 陈俏俊. All rights reserved.
//

#ifndef ATSingleton_h
#define ATSingleton_h

// .h文件
#define ATSingletonH(name) + (instancetype)shared##name;

// .m文件
#define ATSingletonM(name) \
static id _instance; \
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}


#endif /* ATSingleton_h */

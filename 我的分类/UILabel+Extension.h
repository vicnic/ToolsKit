//
//  UILabel+Extension.h
//  Weibo11
//
//  Created by JYJ on 15/12/5.
//  Copyright © 2015年 itheima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

/**
* 创建 UILabel
*
*  @param title    标题
*  @param color    标题颜色
*  @param fontSize 字体大小
*
*  @return UILabel(文本水平居中)
*/
+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize;


/**
 * 创建 UILabel
 *
 *  @param title    标题
 *  @param color    标题颜色
 *  @param font     字体
 *
 *  @return UILabel(文本水平居中)
 */
+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font;


/**
 *  创建 UILabel
 *
 *  @param title     标题
 *  @param color     标题颜色
 *  @param fontSize  字体大小
 *  @param alignment 对齐方式
 *
 *  @return UILabel
 */
+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize alignment:(NSTextAlignment)alignment;


/**
 *  创建 UILabel
 *
 *  @param title     标题
 *  @param color     标题颜色
 *  @param font      字体
 *  @param alignment 对齐方式
 *
 *  @return UILabel
 */
+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font alignment:(NSTextAlignment)alignment;

/**
 *  修改行间距
 *
 *  @param lineSpace 间距
 */
- (void)xhq_lineSpace:(CGFloat)lineSpace;


/**
 *  修改字间距
 *
 *  @param wordSpace 间距
 */
- (void)xhq_wordSpace:(CGFloat)wordSpace;


/**
 *  修改段落间距（\n换行）
 *
 *  @param paragraphSpace 间距
 */
- (void)xhq_paragraphSpace:(CGFloat)paragraphSpace;


/**
 *  修改行间距与字间距
 *
 *  @param lineSpace 行间距
 *  @param wordSpace 字间距
 *  @param paragraphSpace 段间距
 */
- (void)xhq_lineSpace:(CGFloat)lineSpace wordSpace:(CGFloat)wordSpace paragraphSpace:(CGFloat)paragraphSpace;

@end

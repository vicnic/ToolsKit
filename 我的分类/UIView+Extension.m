//
//  UIView+Extension.m
//  01-黑酷
//
//  Created by apple on 14-6-27.
//  Copyright (c) 2014年 heima. All rights reserved.
//

#import "UIView+Extension.h"
#import <objc/runtime.h>
@implementation UIView (Extension)

- (void)setX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x {
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y {
    return self.frame.origin.y;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setSize:(CGSize)size {
//    self.width = size.width;
//    self.height = size.height;
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setCenterX:(CGFloat)centerX {
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY {
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY {
    return self.center.y;
}


- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

-(NSNumber *)leftTopCorner{
    return objc_getAssociatedObject(self, _cmd);
}
-(NSNumber*)rightTopCorner{
    return objc_getAssociatedObject(self, _cmd);
}
-(NSNumber*)leftBottomCorner{
    return objc_getAssociatedObject(self, _cmd);
}
-(NSNumber*)rightBottomCorner{
    return objc_getAssociatedObject(self, _cmd);
}
- (void)setLeftTopCorner:(NSNumber *)leftTopCorner{
    objc_setAssociatedObject(self, @selector(leftTopCorner), leftTopCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.leftTopCorner integerValue], [self.leftTopCorner integerValue]) corner:UIRectCornerTopLeft];
}
- (void)setRightTopCorner:(NSNumber *)rightTopCorner{
    objc_setAssociatedObject(self, @selector(rightTopCorner), rightTopCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.rightTopCorner integerValue], [self.rightTopCorner integerValue]) corner:UIRectCornerTopRight];
}
- (void)setLeftBottomCorner:(NSNumber *)leftBottomCorner{
    objc_setAssociatedObject(self, @selector(leftBottomCorner), leftBottomCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.leftBottomCorner integerValue], [self.leftBottomCorner integerValue]) corner:UIRectCornerBottomLeft];
}
- (void)setRightBottomCorner:(NSNumber *)rightBottomCorner{
    objc_setAssociatedObject(self, @selector(rightBottomCorner), rightBottomCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.rightBottomCorner integerValue], [self.rightBottomCorner integerValue]) corner:UIRectCornerBottomRight];
}
- (NSNumber *)left_right_topCorner{
    return objc_getAssociatedObject(self, _cmd);
}
- (NSNumber *)left_right_bottomCorner{
    return objc_getAssociatedObject(self, _cmd);
}
- (NSNumber *)left_top_bottomCorner{
    return objc_getAssociatedObject(self, _cmd);
}
- (NSNumber *)right_top_bottomCorner{
    return objc_getAssociatedObject(self, _cmd);
}
- (void)setLeft_right_topCorner:(NSNumber *)left_right_topCorner{
    objc_setAssociatedObject(self, @selector(left_right_topCorner), left_right_topCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.left_right_topCorner integerValue], [self.left_right_topCorner integerValue]) corner:UIRectCornerTopLeft|UIRectCornerTopRight];
}
- (void)setLeft_right_bottomCorner:(NSNumber *)left_right_bottomCorner{
    objc_setAssociatedObject(self, @selector(left_right_bottomCorner), left_right_bottomCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.left_right_bottomCorner integerValue], [self.left_right_bottomCorner integerValue]) corner:UIRectCornerBottomLeft|UIRectCornerBottomRight];
}
- (void)setLeft_top_bottomCorner:(NSNumber *)left_top_bottomCorner{
    objc_setAssociatedObject(self, @selector(left_top_bottomCorner), left_top_bottomCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.left_top_bottomCorner integerValue], [self.left_top_bottomCorner integerValue]) corner:UIRectCornerTopLeft|UIRectCornerBottomLeft];
}
- (void)setRight_top_bottomCorner:(NSNumber *)right_top_bottomCorner{
    objc_setAssociatedObject(self, @selector(right_top_bottomCorner), right_top_bottomCorner, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self drawLine:CGSizeMake([self.right_top_bottomCorner integerValue], [self.right_top_bottomCorner integerValue]) corner:UIRectCornerTopRight|UIRectCornerBottomRight];
}
-(void)drawLine:(CGSize)s corner:(UIRectCorner)c{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)) byRoundingCorners:c cornerRadii:s];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)addTarget:(id)target action:(SEL)action;
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:target
                                                                         action:action];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tap];
}
- (UIViewController*)getCurrentViewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UINavigationController class]] || [nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}
@end

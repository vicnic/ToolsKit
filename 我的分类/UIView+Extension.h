//
//  UIView+Extension.h
//  01-黑酷
//
//  Created by apple on 14-6-27.
//  Copyright (c) 2014年 heima. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (Extension)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;


/**
 *  9.上 < Shortcut for frame.origin.y
 */
@property (nonatomic) CGFloat top;

/**
 *  10.下 < Shortcut for frame.origin.y + frame.size.height
 */
@property (nonatomic) CGFloat bottom;

/**
 *  11.左 < Shortcut for frame.origin.x.
 */
@property (nonatomic) CGFloat left;

/**
 *  12.右 < Shortcut for frame.origin.x + frame.size.width
 */
@property (nonatomic) CGFloat right;

/// 左上
@property(nonatomic,strong)NSNumber* _Nonnull leftTopCorner;

/// 右上
@property(nonatomic,strong)NSNumber* _Nonnull rightTopCorner;

/// 左下
@property(nonatomic,strong)NSNumber* _Nonnull leftBottomCorner;

/// 右下
@property(nonatomic,strong)NSNumber* _Nonnull rightBottomCorner;

/// 左上右上
@property(nonatomic,strong)NSNumber* _Nonnull left_right_topCorner;

/// 左下右下
@property(nonatomic,strong)NSNumber* _Nonnull left_right_bottomCorner;

/// 左上左下
@property(nonatomic,strong)NSNumber* _Nonnull left_top_bottomCorner;

/// 右上右下
@property(nonatomic,strong)NSNumber* _Nonnull right_top_bottomCorner;
/**
添加点击事件
*/
- (void)addTarget:(id)target action:(SEL)action;
/**
获取当前的控制器
*/
- (UIViewController*)getCurrentViewController;
/**
左右抖动
*/
- (void)dl_errorAnimation;
@end

//
//  UIViewController+Ext.m
//  ShangDuHuiProject
//
//  Created by 帝云科技 on 2017/11/7.
//  Copyright © 2017年 APPLE. All rights reserved.
//

#import "UIViewController+Ext.h"

@implementation UIViewController (Ext)

#pragma mark - 返回控制器
- (void)xhq_popToViewControllerWithIndex:(NSInteger)aIndex {
    NSArray *viewControllers = [self.navigationController viewControllers];
    if (viewControllers.count >= aIndex) {
        [self.navigationController popToViewController:viewControllers[viewControllers.count - aIndex] animated:YES];
    }
}

#pragma mark - 判断当前控制器是否正在显示
- (BOOL)xhq_isVisible
{
    return (self.isViewLoaded && self.view.window);
}


@end

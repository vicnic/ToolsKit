//
//  NSString+BXExtension.h
//  BXInsurenceBroker
//
//  Created by JYJ on 16/2/23.
//  Copyright © 2016年 baobeikeji. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BXExtension)

/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

- (BOOL)isEmptyString;
/**身份证：仅显示前4后4*/
-(NSString*)getShelterIDCard;
/**银行卡：仅显示前4后3*/
-(NSString*)getShelterBankCard;
/**手机号：仅显示前3后4*/
-(NSString*)getShelterPhoneNum;
/**姓名 多个字只显示前1后1,2个字仅显示后1*/
-(NSString*)getShelterName;

/** 判断字符串是否为空 @return YES不为空 NO为空*/
+ (BOOL)notNULL:(NSString *)str;

/**
 密码格式判断 6~20 必须包含数字或者字母
 @return YES正确 NO错误
 */
+ (BOOL)pswFormatCheck:(NSString *)string;
/**
 身份证号格式检查并提示
 @return YES正确 NO错误
 */
+ (BOOL)idFormatCheck:(NSString *)string;
/**
 邮箱格式检查并提示
 @return YES正确 NO错误
 */
+ (BOOL)emailFormatCheck:(NSString *)string;
/**
 银行卡号格式检查并提示
 @return YES正确 NO错误
 */
+ (BOOL)bankCardFormatCheck:(NSString *)string;
/**
 中文姓名格式检查并提示
 @return YES正确 NO错误
 */
+ (BOOL)cnNameFormatCheck:(NSString *)string;

@end

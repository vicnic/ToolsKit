//
//  NSString+BXExtension.m
//  BXInsurenceBroker
//
//  Created by JYJ on 16/2/23.
//  Copyright © 2016年 baobeikeji. All rights reserved.
//

#import "NSString+BXExtension.h"

@implementation NSString (BXExtension)


- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize {
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (BOOL)isEmptyString {
    return self.length == 0 || [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0;
}
//仅显示前4后4
-(NSString*)getShelterIDCard{
    if (self.length<8) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(4, self.length-8) withString:@" **** **** "];
    return finalStr;
}
//仅显示前4后3
-(NSString*)getShelterBankCard{
    if (self.length<7) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(4, self.length-7) withString:@" **** **** "];
    return finalStr;
}
//仅显示前3后4
-(NSString*)getShelterPhoneNum{
    if (self.length<7) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(3, self.length-7) withString:@" **** "];
    return finalStr;
}
//姓名 多个字只显示前1后1,2个字仅显示后1
-(NSString*)getShelterName{
    if (self.length<=1) {
        return self;
    }
    if (self.length==2) {
        NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
        return finalStr;
    }else{
        NSString * rangeStr = [self substringWithRange:NSMakeRange(1, self.length-2)];
        NSString * starStr = @"";
        for (int i =0; i<self.length-2; i++) {
            starStr = [NSString stringWithFormat:@"%@*",starStr];
        }
        NSString * finalStr = [self stringByReplacingOccurrencesOfString:rangeStr withString:starStr];
        return finalStr;
    }
}
#pragma mark - 检测字符串是否为空
+ (BOOL)xhq_notNULL:(NSString *)str {
    if ([str isEqual:[NSNull null]] ||
        [str isKindOfClass:[NSNull class]] ||
        [str isEqualToString:@"(null)"] ||
        !str) {
        return NO;
    }
    return YES;
}

//6~20位密码 字母数字
+ (BOOL)pswFormatCheck:(NSString *)string {
    
    NSString *regex = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
    return [string predicateMatches:regex];
}

//身份证
+ (BOOL)idFormatCheck:(NSString *)string {
    
    NSString *regex = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    return [string predicateMatches:regex];
}

//邮箱检测
+ (BOOL)emailFormatCheck:(NSString *)string {
    
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    return [string predicateMatches:regex];
}

//银行卡检测
+ (BOOL)bankCardFormatCheck:(NSString *)string {
    
    if (!string || [string isKindOfClass:[NSNull class]] || string.length == 0) {
        return NO;
    }
    
    int oddsum = 0;     //奇数求和
    int evensum = 0;    //偶数求和
    int allsum = 0;
    int cardNoLength = (int)[string length];
    int lastNum = [[string substringFromIndex:cardNoLength-1] intValue];
    
    NSString *cardNo = [string substringToIndex:cardNoLength - 1];
    for (int i = cardNoLength -1 ; i>=1;i--) {
        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(i-1, 1)];
        int tmpVal = [tmpString intValue];
        if (cardNoLength % 2 ==1 ) {
            if((i % 2) == 0){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }else{
            if((i % 2) == 1){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }
    }
    
    allsum = oddsum + evensum;
    allsum += lastNum;
    if((allsum % 10) == 0)
        return YES;
    else
        return NO;
}

//中文检测
+ (BOOL)cnNameFormatCheck:(NSString *)string {
    NSString * regex = @"^[\u4E00-\u9FA5]*$";
    return [string predicateMatches:regex];
}

//正则检测
- (BOOL)predicateMatches:(NSString *)regex {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:self];
}

@end

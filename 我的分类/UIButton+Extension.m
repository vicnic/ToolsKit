//
//  UIButton+Extension.m
//  Weibo11
//
//  Created by JYJ on 15/12/5.
//  Copyright © 2015年 itheima. All rights reserved.
//

#import "UIButton+Extension.h"

@implementation UIButton (Extension)

#pragma mark --- 创建默认按钮--有字体、颜色--有图片---有背景
+ (instancetype)buttonWithTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font imageName:(NSString *)imageName target:(id)target action:(SEL)action backImageName:(NSString *)backImageName  {
    
    UIButton *b = [[UIButton alloc] init];
    // 设置标题
    [b setTitle:title forState:UIControlStateNormal];
    [b setTitleColor:titleColor forState:UIControlStateNormal];
    b.titleLabel.font = font;
    b.adjustsImageWhenHighlighted = NO;
    // 图片
    if (imageName != nil) {
        [b setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        NSString *highlighted = [NSString stringWithFormat:@"%@_highlighted", imageName];
        [b setImage:[UIImage imageNamed:highlighted] forState:UIControlStateHighlighted];
    }
    
    // 背景图片
    if (backImageName != nil) {
        [b setBackgroundImage:[UIImage imageNamed:backImageName] forState:UIControlStateNormal];
        
        NSString *backHighlighted = [NSString stringWithFormat:@"%@_highlighted", backImageName];
        [b setBackgroundImage:[UIImage imageNamed:backHighlighted] forState:UIControlStateHighlighted];
    }
    
    // 监听方法
    if (action != nil) {
        [b addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    return b;
}

#pragma mark  --- 有文字,有颜色，有字体，有图片，没有背景图片
+ (instancetype)buttonWithTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font imageName:(NSString *)imageName target:(id)target action:(SEL)action {
    return [self buttonWithTitle:title titleColor:titleColor font:font imageName:imageName  target:target action:action backImageName:nil];
}


#pragma mark  --- 有文字,有颜色，有字体，没有图片，没有背景
+ (instancetype)buttonWithTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font target:(id)target action:(SEL)action {
    return [self buttonWithTitle:title titleColor:titleColor font:font imageName:nil target:target action:action backImageName:nil];
}

#pragma mark  --- 有文字,有颜色,有字体,没图片，有背景
+ (instancetype)buttonWithTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font target:(id)target action:(SEL)action backImageName:(NSString *)backImageName {
    return [self buttonWithTitle:title titleColor:titleColor font:font imageName:nil target:target action:action backImageName:backImageName];
}
- (void)xhq_setImagePosition:(XHQImagePosition)position spacing:(CGFloat)spacing
{
    [self layoutIfNeeded];
    
    CGFloat imageViewWidth = self.imageView.image.size.width;
    CGFloat imageViewHeight = self.imageView.image.size.height;
    CGFloat titleLabelWidth = self.titleLabel.intrinsicContentSize.width;
    CGFloat titleLabelHeight = self.titleLabel.intrinsicContentSize.height;
    
    spacing = spacing * 0.5;

    CGFloat imageOffsetX = (imageViewWidth + titleLabelWidth) / 2 - imageViewWidth / 2;//image中心移动的x距离
    CGFloat imageOffsetY = imageViewHeight / 2 + spacing;//image中心移动的y距离
    CGFloat labelOffsetX = (imageViewWidth + titleLabelWidth / 2) - (imageViewWidth + titleLabelWidth) / 2;//label中心移动的x距离
    CGFloat labelOffsetY = titleLabelHeight / 2 + spacing;//label中心移动的y距离
    
    switch (position)
    {
        case XHQImagePositionLeft:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing, 0, spacing);
            self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, -spacing);
        }
            break;
        case XHQImagePositionRight:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(0, spacing + titleLabelWidth, 0, -(spacing + titleLabelWidth));
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(spacing + imageViewWidth), 0, spacing + imageViewWidth);
        }
            break;
        case XHQImagePositionTop:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(-imageOffsetY, imageOffsetX, imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(labelOffsetY, -labelOffsetX, -labelOffsetY, labelOffsetX);
        }
            break;
        case XHQImagePositionBottom:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(imageOffsetY, imageOffsetX, -imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(-labelOffsetY, -labelOffsetX, labelOffsetY, labelOffsetX);
        }
            break;
    }
}
@end

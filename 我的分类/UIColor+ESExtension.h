//
//  UIColor+ESExtension.h
//  ElectricSilverDelegate
//
//  Created by Jinniu on 2020/3/20.
//  Copyright © 2020 Jinniu. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (ESExtension)
//暗黑模式
+(UIColor*)normalColor:(UIColor*)nc darkColor:(UIColor*)dc;
//hex 颜色
+ (instancetype)dl_colorFromHexString:(NSString *)hexStr;
// rgb颜色
+(UIColor*)dl_rgbColorR:(NSInteger)r g:(NSInteger)g b:(NSInteger)b;
// rgba颜色
+(UIColor*)dl_rgbColorR:(NSInteger)r g:(NSInteger)g b:(NSInteger)b a:(CGFloat)a;
//随机色
+(UIColor *)dl_randomColor;

@end

NS_ASSUME_NONNULL_END

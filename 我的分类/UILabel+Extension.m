//
//  UILabel+Extension.m
//  Weibo11
//
//  Created by JYJ on 15/12/5.
//  Copyright © 2015年 itheima. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize {
    return [self labelWithTitle:title color:color fontSize:fontSize alignment:NSTextAlignmentCenter];
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize alignment:(NSTextAlignment)alignment {
    
    UILabel *lab = [[UILabel alloc] init];
    
    lab.text = title;
    lab.textColor = color;
    lab.font = [UIFont systemFontOfSize:fontSize];
    lab.numberOfLines = 0;
    lab.textAlignment = alignment;
    [lab sizeToFit];
    
    return lab;
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font {
    return [self labelWithTitle:title color:color font:font alignment:NSTextAlignmentCenter];
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font alignment:(NSTextAlignment)alignment {
    
    UILabel *lab = [[UILabel alloc] init];
    lab.text = title;
    lab.textColor = color;
    lab.font = font;
    lab.numberOfLines = 0;
    lab.textAlignment = alignment;
    [lab sizeToFit];
    return lab;
}


#pragma mark - 修改行间距
- (void)xhq_lineSpace:(CGFloat)lineSpace {
    
    [self xhq_lineSpace:lineSpace wordSpace:0 paragraphSpace:0];
}


#pragma mark - 修改字间距
- (void)xhq_wordSpace:(CGFloat)wordSpace {
    
    [self xhq_lineSpace:0 wordSpace:wordSpace paragraphSpace:0];
}


#pragma mark - 修改段间距
- (void)xhq_paragraphSpace:(CGFloat)paragraphSpace {
    
    [self xhq_lineSpace:0 wordSpace:0 paragraphSpace:paragraphSpace];
}


#pragma mark - 修改行间距，字体间距，段落间距
- (void)xhq_lineSpace:(CGFloat)lineSpace wordSpace:(CGFloat)wordSpace paragraphSpace:(CGFloat)paragraphSpace {
    
    NSString *string = self.text;
    
    NSMutableAttributedString *attributedString = nil;
    if (self.attributedText) {
        attributedString = [self.attributedText mutableCopy];
    }else {
        attributedString = [[NSMutableAttributedString alloc]initWithString:string];
    }
    
    if (wordSpace > 0) {
        [attributedString addAttributes:@{NSKernAttributeName:@(wordSpace)} range:NSMakeRange(0, [string length])];
    }
    
    if (lineSpace > 0) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:lineSpace];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    }
    
    if (paragraphSpace > 0) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setParagraphSpacing:paragraphSpace];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    }
    
    self.attributedText = attributedString;
    [self sizeToFit];
}
@end

//
//  UIViewController+Ext.h
//  ShangDuHuiProject
//
//  Created by 帝云科技 on 2017/11/7.
//  Copyright © 2017年 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Ext)

/**
 pop第几层
 */
- (void)dl_popToViewControllerWithIndex:(NSInteger)aIndex;

/**
 判断当前控制器是否正在显示
 */
- (BOOL)dl_isVisible;

@end

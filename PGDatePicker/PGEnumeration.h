//
//  PGEnumeration.h
//
//  Created by piggybear on 2018/1/7.
//  Copyright © 2018年 piggybear. All rights reserved.
//

#ifndef PGEnumeration_h
#define PGEnumeration_h

typedef NS_ENUM(NSUInteger, PGDatePickManagerStyle) {
    PGDatePickManagerStyle1,//仿系统
    PGDatePickManagerStyle2,//确定取消在上
    PGDatePickManagerStyle3//确定取消在下
};

#endif /* PGEnumeration_h */

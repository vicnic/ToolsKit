//
//  LocalVoiceFile.m
//  TwoOneEight
//
//  Created by Jinniu on 2018/11/9.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "LocalVoiceFile.h"
#import <AVFoundation/AVFoundation.h>
@implementation LocalVoiceFile
+(void)speakVoice:(NSString*)voice{
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    if ([[def objectForKey:@"WDMoneyVoiceSwitch"]isEqualToString:@"NO"]) {
        return;
    }
    AVSpeechSynthesisVoice *voiceLanguage = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];
    AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc] init];
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:voice];
    utterance.voice = voiceLanguage;
    // 语速 0.0f～1.0f
    utterance.rate = 0.5f;
    // 声音的音调 0.5f～2.0f
    utterance.pitchMultiplier = 0.8f;
    // 使播放下一句的时候有0.1秒的延迟
//    utterance.postUtteranceDelay = 0.1f;
    // 播放合成语音
    [synthesizer speakUtterance:utterance];
}
@end

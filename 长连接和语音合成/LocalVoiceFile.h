//
//  LocalVoiceFile.h
//  TwoOneEight
//
//  Created by Jinniu on 2018/11/9.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalVoiceFile : NSObject
+(void)speakVoice:(NSString*)voice;
@end

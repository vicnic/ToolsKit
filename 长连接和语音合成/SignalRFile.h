//
//  SignalRFile.h
//  O2O
//
//  Created by Jinniu on 2018/8/16.
//  Copyright © 2018年 金牛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignalRFile : NSObject
+(instancetype)createSignalR;
@end

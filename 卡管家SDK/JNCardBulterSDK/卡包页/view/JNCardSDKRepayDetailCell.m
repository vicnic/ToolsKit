//
//  JNCardSDKRepayDetailCell.m
//  IDCardManager
//
//  Created by 金牛 on 2017/8/1.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKRepayDetailCell.h"
@interface JNCardSDKRepayDetailCell()
{
    UILabel * _dateLab ,* _detailLab , *_typeStrLab ,*_statueStrLab ,*_feeLab;
}
@end
@implementation JNCardSDKRepayDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dateLab = [UILabel new];
        _dateLab.textAlignment = NSTextAlignmentCenter;
        _dateLab.font = Font(14);
        _dateLab.textColor = [UIColor colorWithHue:0.5 saturation:0.02 brightness:0.54 alpha:1];
        [self.contentView addSubview:_dateLab];
        
        _typeStrLab = [UILabel new];
        _typeStrLab.textAlignment = NSTextAlignmentLeft;
        _typeStrLab.font = Font(14);
        _typeStrLab.textColor = [UIColor colorWithHue:0.5 saturation:0.02 brightness:0.54 alpha:1];
        [self.contentView addSubview:_typeStrLab];
        
        _detailLab = [UILabel new];
        _detailLab.font = Font(16);
        _detailLab.textColor = [UIColor redColor];
        _detailLab.adjustsFontSizeToFitWidth = YES;
        _detailLab.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_detailLab];
        
        
        _statueStrLab = [UILabel new];
        _statueStrLab.font = Font(15);
        _statueStrLab.textColor = [UIColor colorWithHue:0 saturation:0 brightness:0.49 alpha:1];
        _statueStrLab.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_statueStrLab];
        
        _feeLab = [UILabel labelWithTitle:@"手续费" color:[UIColor colorWithHue:0 saturation:0 brightness:0.49 alpha:1] fontSize:AUTO(14)];
        [self.contentView addSubview:_feeLab];
        
        _detailLab.sd_layout.leftSpaceToView(self.contentView, AUTO(15)).topSpaceToView(self.contentView, AUTO(10)).heightIs(20);
        [_detailLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH/2];
        
        _statueStrLab.sd_layout.rightSpaceToView(self.contentView, AUTO(15)).centerYEqualToView(_detailLab).heightIs(20);
        [_statueStrLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH/2];
        _typeStrLab.sd_layout.leftEqualToView(_detailLab).topSpaceToView(_detailLab, AUTO(5)).heightIs(20);
        [_typeStrLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH/4];
        
        _dateLab.sd_layout.leftSpaceToView(_typeStrLab, 5).centerYEqualToView(_typeStrLab).heightIs(20);
        [_dateLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
        
        _feeLab.sd_layout.rightEqualToView(_statueStrLab).centerYEqualToView(_typeStrLab).heightIs(20);
        [_feeLab setSingleLineAutoResizeWithMaxWidth:200];
        
        [self setupAutoHeightWithBottomView:_dateLab bottomMargin:AUTO(10)];
    }
    return self;
}
-(void)setModel:(JNCardSDKRepayDetaiCellModel *)model{
    _dateLab.text = model.theDate.length>0?model.theDate: model.theTime;
    if ([model.theTypeStr isEqualToString:@"快速还款"]) {
        self.contentView.backgroundColor =  ColorByHexStr(@"#eeeeee");
    }
    _feeLab.text = [model.fee floatValue]==0?@"":[NSString stringWithFormat:@"手续费：%.2f",[model.fee floatValue]];
    _typeStrLab.text = [model.theType intValue]==2?@"[还款]":@"[消费]";//预生成2是还款，计划生成是1
    _statueStrLab.text = [model.theType intValue]==2?@"未还款":@"未消费";
//    if([model.TheType isEqualToString:@"1"]){//还款
//        if ([model.TheState isEqualToString:@"1"]) {
//            _statueStrLab.text = @"未还款";
//        }else if ([model.TheState isEqualToString:@"2"]){
//            _statueStrLab.text = @"已还款";
//        }else if ([model.TheState isEqualToString:@"3"]){
//            _statueStrLab.text = @"已暂停";
//        }else if ([model.TheState isEqualToString:@"4"]){
//            _statueStrLab.text = @"等待中";
//        }else{
//            _statueStrLab.text = model.TheDate.length>0?@"":@"已作废";
//        }
//    }else{
//        if ([model.TheState isEqualToString:@"1"]) {
//            _statueStrLab.text = @"未消费";
//        }else if ([model.TheState isEqualToString:@"2"]){
//            _statueStrLab.text = @"已消费";
//        }else if ([model.TheState isEqualToString:@"3"]){
//            _statueStrLab.text = @"已暂停";
//        }else if ([model.TheState isEqualToString:@"4"]){
//            _statueStrLab.text = @"等待中";
//        }else{
//            _statueStrLab.text = model.TheDate.length>0?@"":@"已作废";
//        }
//    }
    _detailLab.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",[model.theMoney floatValue]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end

//
//  JNCardSDKFVRepayDetailCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKFVRepayDetailModel.h" 
@interface JNCardSDKFVRepayDetailCell : UITableViewCell
@property(nonatomic,retain)UIView * bgView;
@property(nonatomic,retain)JNCardSDKFVRepayDetailModel * model;
@end

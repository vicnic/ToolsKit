//
//  JNCardSDKFVRepayCell.m
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKFVRepayCell.h"
#define ThisPageTitleFont AUTO(15)
@interface JNCardSDKFVRepayCell()
@property(nonatomic,retain)UIView * bgView,*noPayDayView/*h5添加的卡没有账单日还款日这时候这个view就要显示*/;
@property(nonatomic,retain)UIView * headView;
@property(nonatomic,retain)UIImageView * bankImgView;
@property(nonatomic,retain)UILabel * bankNameLab;
@property(nonatomic,retain)UILabel * planStateLab;//计划状态
@property(nonatomic,retain)UILabel * realNameLab,*billDayLab;
@property(nonatomic,retain)UILabel * bankFourNumLab;//银行卡尾号
@property(nonatomic,retain)UILabel * shouldPayLab;//应还金额
@property(nonatomic,retain)UILabel * payDayValue;//还款日期
@property(nonatomic,retain)UIImageView * stateImgView;//状态的图标

@end
@implementation JNCardSDKFVRepayCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self createCusUI];
    }
    return self;
}
- (void)addShadowToView:(UIView *)theView withColor:(UIColor *)theColor {
    // 阴影颜色
    theView.layer.shadowColor = theColor.CGColor;
    // 阴影偏移，默认(0, -3)
    theView.layer.shadowOffset = CGSizeMake(0,0);
    // 阴影透明度，默认0
    theView.layer.shadowOpacity = 0.5;
    // 阴影半径，默认3
    theView.layer.shadowRadius = 5;
    
}
-(void)createCusUI{
    _bgView = [UIView new];
    _bgView.backgroundColor = [UIColor whiteColor];
    _bgView.layer.cornerRadius = 6;
    [self.contentView addSubview:_bgView];
    _bgView.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.contentView,0).rightSpaceToView(self.contentView, 10);
    
    self.headView = [UIView new];
    self.headView.layer.cornerRadius = 6;
    [_bgView addSubview:self.headView];
    _headView.sd_layout.leftEqualToView(_bgView).topEqualToView(_bgView).rightEqualToView(_bgView).heightIs(50);
    
    UIImageView * imgBgView = [UIImageView new];
    imgBgView.backgroundColor = [UIColor whiteColor];
    imgBgView.layer.masksToBounds = YES;
    imgBgView.layer.cornerRadius = 15;
    [_headView addSubview:imgBgView];
    imgBgView.sd_layout.centerYEqualToView(_headView).leftSpaceToView(_headView, 10).heightIs(30).widthIs(30);
    
    self.bankImgView = [UIImageView new];
    _bankImgView.layer.masksToBounds = YES;
    _bankImgView.layer.cornerRadius = 15;
    [imgBgView addSubview:self.bankImgView];
    _bankImgView.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    
    self.bankNameLab = [UILabel labelWithTitle:@"银行名称" color:[UIColor whiteColor] fontSize:ThisPageTitleFont];
    [_headView addSubview:self.bankNameLab];
    _bankNameLab.sd_layout.leftSpaceToView(imgBgView, 10).centerYEqualToView(imgBgView).heightIs(20);
    [_bankNameLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vLineView = [UIView new];
    vLineView.backgroundColor = [UIColor whiteColor];
    [_headView addSubview:vLineView];
    vLineView.sd_layout.leftSpaceToView(_bankNameLab, 5).topEqualToView(_bankNameLab).bottomEqualToView(_bankNameLab).widthIs(1);
    
    self.bankFourNumLab = [UILabel labelWithTitle:@"尾号5118" color:[UIColor whiteColor] fontSize:ThisPageTitleFont];
    [_headView addSubview:self.bankFourNumLab];
    _bankFourNumLab.sd_layout.leftSpaceToView(vLineView, 5).centerYEqualToView(vLineView).heightIs(20);
    [_bankFourNumLab setSingleLineAutoResizeWithMaxWidth:200];
    
    self.realNameLab = [UILabel labelWithTitle:@"真名名" color:[UIColor whiteColor] fontSize:ThisPageTitleFont];
    [_headView addSubview:self.realNameLab];
    _realNameLab.sd_layout.leftSpaceToView(_bankFourNumLab, 5).centerYEqualToView(_bankFourNumLab).heightIs(20);
    [_realNameLab setSingleLineAutoResizeWithMaxWidth:200];
    
    self.planStateLab = [UILabel labelWithTitle:@"计划执行中" color:[UIColor whiteColor] fontSize:ThisPageTitleFont];
    [_headView addSubview:self.planStateLab];
    _planStateLab.sd_layout.rightSpaceToView(_headView, 10).centerYEqualToView(_realNameLab).heightIs(20);
    [_planStateLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UILabel * shouldTmpLab = [UILabel labelWithTitle:@"计划还款" color:[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1] fontSize:12];
    [_bgView addSubview:shouldTmpLab];
    shouldTmpLab.sd_layout.leftSpaceToView(_bgView, IPHONE_WIDTH==320?5:20).topSpaceToView(_headView, 10).heightIs(20);
    [shouldTmpLab setSingleLineAutoResizeWithMaxWidth:200];
    
    self.shouldPayLab = [UILabel labelWithTitle:@"3456.78" color:[UIColor blackColor] fontSize:14];
    [_bgView addSubview:self.shouldPayLab];
    _shouldPayLab.sd_layout.centerXEqualToView(shouldTmpLab).topSpaceToView(shouldTmpLab, 10).heightIs(20);
    [_shouldPayLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline2 = [UIView new];
    vline2.backgroundColor = LineColor;
    [_bgView addSubview:vline2];
    vline2.sd_layout.leftSpaceToView(@[shouldTmpLab,_shouldPayLab], AUTO(10)).topEqualToView(shouldTmpLab).bottomEqualToView(_shouldPayLab).widthIs(1);
    
    UILabel * payTmpLab = [UILabel labelWithTitle:@"账单日" color:[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1] fontSize:12];
    [_bgView addSubview:payTmpLab];
    payTmpLab.sd_layout.leftSpaceToView(vline2, AUTO(10)).centerYEqualToView(shouldTmpLab).heightIs(20);
    [payTmpLab setSingleLineAutoResizeWithMaxWidth:200];
    
    self.payDayValue = [UILabel labelWithTitle:@"03月25日" color:[UIColor blackColor] fontSize:14];
    [_bgView addSubview:self.payDayValue];
    _payDayValue.sd_layout.centerXEqualToView(payTmpLab).centerYEqualToView(_shouldPayLab).heightIs(20);
    [_payDayValue setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline3 = [UIView new];
    vline3.backgroundColor = LineColor;
    [_bgView addSubview:vline3];
    vline3.sd_layout.leftSpaceToView(@[_payDayValue,payTmpLab], AUTO(10)).topEqualToView(vline2).bottomEqualToView(vline2).widthIs(1);
    
    UILabel * tmpBillDay = [UILabel labelWithTitle:@"还款日" color:[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1] fontSize:12];
    [_bgView addSubview:tmpBillDay];
    tmpBillDay.sd_layout.leftSpaceToView(vline3, AUTO(10)).centerYEqualToView(payTmpLab).heightIs(20);
    [tmpBillDay setSingleLineAutoResizeWithMaxWidth:200];
    
    self.billDayLab = [UILabel labelWithTitle:@"03月25日" color:[UIColor blackColor] fontSize:14];
    [_bgView addSubview:self.billDayLab];
    _billDayLab.sd_layout.centerXEqualToView(tmpBillDay).centerYEqualToView(_payDayValue).heightIs(20);
    [_billDayLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.stateBtn = [UILabel labelWithTitle:@"制定还款计划" color:[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1] fontSize:13];
//    [_stateBtn setTitle:@"制定还款计划" forState:0];
//    [_stateBtn setTitleColor:[UIColor blackColor] forState:0];
//    _stateBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [_bgView addSubview:self.stateBtn];
    _stateBtn.sd_layout.rightSpaceToView(_bgView, 10).centerYEqualToView(vline3).heightIs(35);
    [_stateBtn setSingleLineAutoResizeWithMaxWidth:200];
//    [_stateBtn setupAutoSizeWithHorizontalPadding:0 buttonHeight:35];
    
    self.stateImgView = [UIImageView new];
    [_bgView addSubview:self.stateImgView];
    _stateImgView.sd_layout.rightSpaceToView(_stateBtn, 10).centerYEqualToView(_stateBtn).widthIs(30).heightEqualToWidth();
    
    self.noPayDayView = [UIView new];
    _noPayDayView.hidden = YES;
    _noPayDayView.backgroundColor = [UIColor whiteColor];
    [_bgView addSubview:self.noPayDayView];
    _noPayDayView.sd_layout.leftEqualToView(_bgView).topSpaceToView(_headView, 0).rightSpaceToView(_stateImgView, 5).bottomEqualToView(_shouldPayLab).offset(10);
    UILabel * fillCard = [UILabel labelWithTitle:@"卡信息待完善" color:[UIColor redColor] fontSize:15];
    [_noPayDayView addSubview:fillCard];
    fillCard.sd_layout.centerXEqualToView(_noPayDayView).centerYEqualToView(_noPayDayView).heightIs(17);
    [fillCard setSingleLineAutoResizeWithMaxWidth:200];
    
    [_bgView setupAutoHeightWithBottomView:_shouldPayLab bottomMargin:10];
    [self setupAutoHeightWithBottomView:_bgView bottomMargin:10];
    [self addShadowToView:_bgView withColor:[UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1]];
}
-(void)setModel:(JNCardSDKFVRepayCellModel *)model{
    [_bankImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,model.bankImg]]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self->_headView.backgroundColor=[JNCardSDKMytools mostColor:[UIImage imageWithData:UIImagePNGRepresentation(image)]];
    }];
    _bankNameLab.text = model.bankName;
    NSInteger nameCount = model.name.length;
    NSString * str = @"";
    for (int i =0; i<nameCount-2; i++) {
        str = [NSString stringWithFormat:@"%@*",str];
    }
    _realNameLab.text = [model.name stringByReplacingCharactersInRange:NSMakeRange(1, nameCount-2) withString:str];
    _planStateLab.text = model.theState==nil?@"暂无计划":[JNCardSDKReuserFile returnState:model.theState];
    _bankFourNumLab.text = [NSString stringWithFormat:@"尾号%@",[model.bankNum substringFromIndex:model.bankNum.length-4 ]];
    _shouldPayLab.text = model.repayMoney.length>0?model.repayMoney:@"暂无";
//    if ([model.TheState isEqualToString:@"0"]) {
//        _stateImgView.image = Image(@"icon_return_modify");
//    }else{
//        _stateImgView.image = Image(@"icon_return_detail");
//    }
    _stateImgView.image = Image(@"jn_modifyCard");
        _stateBtn.text = [self returnStateBtnTitle:model.theState];
    //判断是否显示“完善卡片信息"
    if (model.dueDate.length==0&&model.billDate.length==0) {
        self.noPayDayView.hidden = NO;
        _stateBtn.text = @"完善资料";
    }else{
        if (model.billDate.length<10) {
            [JNCardSDKMytools warnText:@"账单日格式错误" status:JNCardSDKError];
            _payDayValue.text =@"";
            return;
        }else if (model.dueDate.length<10){
            [JNCardSDKMytools warnText:@"还款日格式错误" status:JNCardSDKError];
            _payDayValue.text =@"";
            return;
        }
        NSMutableDictionary * correctBillPayDic = [JNCardSDKCalculaterBillPayDay getBillDateAndPayDate:model.billDate andPayDay:model.dueDate];
        //2018-05-31账单日还款日位置没对调，对调赋值
        _billDayLab.text = [correctBillPayDic[@"finalPayDay"] substringWithRange:NSMakeRange(5, 5)];
        _payDayValue.text = [correctBillPayDic[@"finalBillDay"] substringWithRange:NSMakeRange(5, 5)];
//    [_stateBtn setTitle:[self returnStateBtnTitle:model.TheState] forState:UIControlStateNormal];

    }
}

-(NSString*)returnStateBtnTitle:(NSString * )theState{

    if (theState == nil) {
        return @"创建计划";
    }
    switch ([theState integerValue]) {
        case 0:
            return @"查看计划";
            break;
        case 1:
            return @"查看明细";
            break;
        case 3:
            return @"查看计划";
            break;
        default:
            return @"创建计划";
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
        
}

@end

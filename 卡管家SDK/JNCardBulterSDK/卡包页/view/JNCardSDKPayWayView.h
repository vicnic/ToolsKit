//
//  PayWayView.h
//  IDCardManager
//
//  Created by 金牛 on 2017/9/13.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^JNCardSDKChosePayTypeBlock)(NSString * payTypeNum);
@interface JNCardSDKPayWayView : UIView
//type为1表示是卡片信息或个人中心的还款计划点的计划充值，2表示升级界面
- (instancetype)initWithFrame:(CGRect)frame andPayWayArr:(NSMutableArray*)payWayArr andType:(NSInteger)type andServeMoney:(NSString*)serveMoney andCardNum:(NSString*)cardNum andTotalMoney:(NSString*)totalMoney;
@property(nonatomic,copy)JNCardSDKChosePayTypeBlock payJNCardSDKTypeBlock;
@end

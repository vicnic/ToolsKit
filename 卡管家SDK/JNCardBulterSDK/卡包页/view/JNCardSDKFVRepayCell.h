//
//  JNCardSDKFVRepayCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKFVRepayCellModel.h"
typedef void (^FVRepayCellBtnBlock)(NSString*stateStr);
@interface JNCardSDKFVRepayCell : UITableViewCell
@property(nonatomic,retain)JNCardSDKFVRepayCellModel * model;
@property(nonatomic,copy)FVRepayCellBtnBlock btnBlock;
@property(nonatomic,retain)UILabel * stateBtn;
@end

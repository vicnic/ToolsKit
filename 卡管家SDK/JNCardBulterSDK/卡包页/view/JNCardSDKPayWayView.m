//
//  PayWayView.m
//  IDCardManager
//
//  Created by 金牛 on 2017/9/13.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKPayWayView.h"
@interface JNCardSDKPayWayView()
{
    NSMutableArray * _ppArr;
    NSInteger _isSelectedTag;
    UIButton * _seletedBtn;
}
@end
@implementation JNCardSDKPayWayView

- (instancetype)initWithFrame:(CGRect)frame andPayWayArr:(NSMutableArray*)payWayArr andType:(NSInteger)type andServeMoney:(NSString*)serveMoney andCardNum:(NSString*)cardNum andTotalMoney:(NSString*)totalMoney
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        _ppArr = payWayArr;
        self.backgroundColor = [UIColor whiteColor];
        CGFloat lineMaxY = 0.0;
        if (type==1) {
            NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
            paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
            paraStyle.alignment = NSTextAlignmentLeft;
            paraStyle.lineSpacing = 6; //设置行间距
    //        paraStyle.hyphenationFactor = 1.0;
    //        paraStyle.firstLineHeadIndent = 0.0;
    //        paraStyle.paragraphSpacingBefore = 0.0;
    //        paraStyle.headIndent = 0;
    //        paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
            NSInteger monepyy = ceil([totalMoney floatValue]);
            NSString * stri = [NSString stringWithFormat:@"你将执行该次计划，请确保卡片（尾号%@）有可用额度%@元来执行，若因卡片额度不足导致计划失败，概不负责",[cardNum substringWithRange:NSMakeRange(cardNum.length-4, 4)],[NSString stringWithFormat:@"%ld",monepyy]];
            
            NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0};
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:stri attributes:dic];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(14, 8)];
            NSString * moneyyyST = [NSString stringWithFormat:@"%ld",monepyy];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(27,moneyyyST.length)];
            
            UILabel * noticelab = [[UILabel alloc]initWithFrame:CGRectMake(AUTO(20), 5, IPHONE_WIDTH-AUTO(40), 70)];
            noticelab.backgroundColor = [UIColor whiteColor];
            noticelab.attributedText = attributeStr;
            noticelab.numberOfLines = 0;
            noticelab.font = [UIFont systemFontOfSize:15];
            [self addSubview:noticelab];
            
            UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(noticelab.frame)+10, IPHONE_WIDTH, 1)];
            lineView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.89 alpha:1];
            lineMaxY = CGRectGetMaxY(lineView.frame);
            [self addSubview:lineView];
        }
        
        UILabel * servelab = [[UILabel alloc]initWithFrame:CGRectMake(AUTO(20),type==1?lineMaxY:0, 80, 40)];
        servelab.text = type==1? @"手续费：":@"升级费：";
        servelab.backgroundColor = [UIColor whiteColor];
        servelab.font = [UIFont systemFontOfSize:15];
        [self addSubview:servelab];
        UILabel * moneyLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(servelab.frame), CGRectGetMinY(servelab.frame), 300, 40)];
        moneyLab.textColor = [UIColor colorWithHue:0 saturation:0 brightness:0.47 alpha:1];
        moneyLab.font = [UIFont systemFontOfSize:15];
        moneyLab.text = serveMoney;
        [self addSubview:moneyLab];
        UILabel * grayLab = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(moneyLab.frame), IPHONE_WIDTH, 40)];
        grayLab.backgroundColor =  [UIColor colorWithHue:0 saturation:0 brightness:0.93 alpha:1];
        grayLab.font = [UIFont systemFontOfSize:16];
        grayLab.text = @"    选择支付方式：";
        [self addSubview:grayLab];
        
        
        NSMutableArray * objArr = [NSMutableArray new];
        for (int i =0; i<payWayArr.count; i++) {
            UIButton * btn = [UIButton new];
            btn.tag = i+120;
            btn.selected = NO;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            [objArr addObject:btn];
        }
        UIButton * lastBtn = nil;
        for (int i =0; i<payWayArr.count; i++) {
            NSDictionary * dic = payWayArr[i];
            UIButton * btn = objArr[i];
            btn.sd_layout.leftEqualToView(self).rightEqualToView(self).topSpaceToView(lastBtn==nil?grayLab:lastBtn, AUTO(10)).heightIs(AUTO(50));
            UIImageView * imgView = [UIImageView new];
            imgView.image = ([dic[@"PayType"] intValue]==1)?Image(@"share_weixin"):([dic[@"PayType"] intValue]==2)?Image(@"支付宝"):([dic[@"PayType"] intValue]==7)?Image(@"百度钱包"):([dic[@"PayType"] intValue]==98)?Image(@"余额_payway"):([dic[@"PayType"] intValue]==99)?Image(@"佣金_payway"):Image(@"银联支付");
            [btn addSubview:imgView];
            imgView.sd_layout.leftSpaceToView(btn, AUTO(20)).centerYEqualToView(btn).widthIs(AUTO(30)).heightEqualToWidth();
            
            UILabel * titleLab = [UILabel new];
            titleLab.text = ([dic[@"PayType"] intValue]==1)?@"微信支付":([dic[@"PayType"] intValue]==2)?@"支付宝支付":([dic[@"PayType"] intValue]==7)?@"百度钱包":([dic[@"PayType"] intValue]==98)?@"余额支付":([dic[@"PayType"] intValue]==99)?@"佣金支付":@"银联支付";
            titleLab.font = Font(16);
            [btn addSubview:titleLab];
            titleLab.sd_layout.leftSpaceToView(imgView, AUTO(10)).centerYEqualToView(imgView).heightIs(AUTO(20));
            [titleLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
            UIImageView * circleView = [UIImageView new];
            circleView.layer.borderColor = [UIColor colorWithHue:0 saturation:0 brightness:0.8 alpha:1].CGColor;
            circleView.tag = i+130;
            circleView.layer.borderWidth = 1;
            circleView.layer.masksToBounds = YES;
            circleView.layer.cornerRadius = AUTO(10);
            [btn addSubview:circleView];
            circleView.sd_layout.rightSpaceToView(btn, AUTO(20)).centerYEqualToView(btn).widthIs(AUTO(20)).heightIs(AUTO(20));
            UIView * line = [UIView new];
            line.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.8 alpha:1];
            [btn addSubview:line];
            line.sd_layout.leftSpaceToView(btn, 0).rightSpaceToView(btn, 0).bottomEqualToView(btn).heightIs(1);
            lastBtn = btn;
        }
        UIButton * certainBtn = [UIButton new];
        certainBtn.backgroundColor = ThemeColor;
        [certainBtn setTitle:@"确定充值" forState:0];
        certainBtn.titleLabel.font = Font(16);
        certainBtn.layer.masksToBounds = YES;
        certainBtn.layer.cornerRadius = AUTO(5);
        [certainBtn addTarget:self action:@selector(certainBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:certainBtn];
        certainBtn.sd_layout.bottomSpaceToView(self , AUTO(20)).leftSpaceToView(self, AUTO(30)).rightSpaceToView(self, AUTO(30)).heightIs(AUTO(44));
    }
    return self;
}
-(void)btnClick:(UIButton*)sender{
    NSDictionary * dict = _ppArr[sender.tag-120];
    if ([dict[@"PayActive"] integerValue]!=1) {
        [JNCardSDKMytools warnText:@"通道正在维护中" status:JNCardSDKError];
        return;
    }
    if (sender != _seletedBtn) {
        UIImageView * imgView = [_seletedBtn viewWithTag:_seletedBtn.tag+10];
        imgView.image = Image(@"");
        _seletedBtn.selected = NO;
        
        _seletedBtn = sender;
        sender.selected = !sender.selected ;
        UIImageView * circle = [self viewWithTag:sender.tag+10];
        if (sender.selected==YES) {
            circle.image = Image(@"完成_new");
            
        }else{
            circle.image = Image(@"");
        }
    }
    
}

-(void)certainBtnClick{
    for (int i=120; i<120+_ppArr.count; i++) {
        NSDictionary * dic = _ppArr[i-120];
        UIButton * btn = [self viewWithTag:i];
        if (btn.selected == YES) {
            NSString * str = [NSString stringWithFormat:@"%d",[dic[@"PayType"] intValue]];
            self.payJNCardSDKTypeBlock(str);
        }
    }
}

@end

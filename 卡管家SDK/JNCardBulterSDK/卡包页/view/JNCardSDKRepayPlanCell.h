//
//  JNCardSDKRepayPlanCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/25.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKRepayPlanCellModel.h"
@interface JNCardSDKRepayPlanCell : UITableViewCell
@property(nonatomic,retain)UILabel * dateLab;
@property(nonatomic,retain)UIImageView * bankImgView;
@property(nonatomic,retain)UILabel * descLab;//执行中
@property(nonatomic,retain)UILabel * cardNumLab;
@property(nonatomic,retain)UILabel * planLab;//计划配置
@property(nonatomic,retain)UIButton * seePlanDetailBtn;
@property(nonatomic,retain)UILabel * chargeLab;
@property(nonatomic,retain)UILabel * realPayLab;//实际还款
@property(nonatomic,retain)UILabel * shouxuFeeLab;//手续费
@property(nonatomic,retain)UIButton * chargeBtn;
@property(nonatomic,retain)UIButton * modifyBtn;
@property(nonatomic,retain)UIButton * cancelBtn;
@property(nonatomic,retain)UIButton * cancelNewBtn;//计划撤销按钮
@property(nonatomic,retain)UIView * lineView2;

@property(nonatomic,retain)UIButton * changeCardBtn;//卡片修改
@property(nonatomic,retain)UIButton * restartBtn;//计划重启

@property(nonatomic,retain)JNCardSDKRepayPlanCellModel * model;

@end

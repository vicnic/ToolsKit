//
//  JNCardSDKPlanModelView.m
//  IDCardManager
//
//  Created by Jinniu on 2018/5/16.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKPlanModelView.h"
@interface JNCardSDKPlanModelView()
@property(nonatomic,retain)UILabel * settingLab,*planPayLab,*moneyLab,*realPayLab,*realReturnLab,*feeLab;
@end
@implementation JNCardSDKPlanModelView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}

-(void)createUI{
    
    UILabel * viewLab = [UILabel labelWithTitle:@"计划预生成结果" color:[UIColor blackColor] font:[UIFont boldSystemFontOfSize:16] alignment:NSTextAlignmentLeft];
    viewLab.frame = Frame(10,10, IPHONE_WIDTH-20, 16);
    [self addSubview:viewLab];
    UIView *whiteView = [UIView new];
    whiteView.layer.masksToBounds = YES;
    whiteView.layer.cornerRadius = 6;
    whiteView.layer.borderWidth = 0.8;
    whiteView.layer.borderColor = [UIColor colorWithHue:0 saturation:0 brightness:0.95 alpha:1].CGColor;
    whiteView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.97 alpha:1];
    [self addSubview:whiteView];
    whiteView.sd_layout.topSpaceToView(viewLab, 12).leftSpaceToView(self, 10).rightSpaceToView(self, 10).heightIs(180);
    
    UILabel * tmpOneLab = [UILabel labelWithTitle:@"计划配置    " color:[UIColor darkGrayColor] fontSize:14];
    tmpOneLab.frame = Frame(20, CGRectGetMaxY(viewLab.frame)+30, [@"计划配置    " sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)].width, 14);
    [self addSubview:tmpOneLab];
    
    self.settingLab = [UILabel labelWithTitle:@"消费5笔 | 还款7笔" color:[UIColor blackColor] fontSize:14];
    _settingLab.textAlignment = NSTextAlignmentLeft;
    _settingLab.frame = Frame(CGRectGetMaxX(tmpOneLab.frame), CGRectGetMinY(tmpOneLab.frame), IPHONE_WIDTH-CGRectGetMaxX(tmpOneLab.frame), 14);
    [self addSubview:self.settingLab];
    
    UILabel * tmpThrLab = [UILabel labelWithTitle:@"计划还款    " color:[UIColor darkGrayColor] fontSize:14];
    tmpThrLab.frame = Frame(20, CGRectGetMaxY(_settingLab.frame)+20, [@"计划还款    " sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)].width, 14);
    [self addSubview:tmpThrLab];
    self.planPayLab = [UILabel labelWithTitle:@"5800.00元" color:[UIColor blackColor] fontSize:14];
    _planPayLab.textAlignment = NSTextAlignmentLeft;
    _planPayLab.frame = Frame(CGRectGetMaxX(tmpThrLab.frame), CGRectGetMinY(tmpThrLab.frame), IPHONE_WIDTH-CGRectGetMaxX(tmpThrLab.frame), 14);
    [self addSubview:self.planPayLab];
    
    UILabel * tmpFourLab = [UILabel labelWithTitle:@"计划定金    " color:[UIColor darkGrayColor] fontSize:14];
    tmpFourLab.frame = Frame(20, CGRectGetMaxY(_planPayLab.frame)+20, [@"计划定金    " sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)].width, 14);
    [self addSubview:tmpFourLab];
    self.moneyLab = [UILabel labelWithTitle:@"1500.00元" color:[UIColor blackColor] fontSize:14];
    _moneyLab.textAlignment = NSTextAlignmentLeft;
    _moneyLab.frame = Frame(CGRectGetMaxX(tmpFourLab.frame), CGRectGetMinY(tmpFourLab.frame), IPHONE_WIDTH-CGRectGetMaxX(tmpFourLab.frame), 14);
    [self addSubview:self.moneyLab];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.95 alpha:1];
    [whiteView addSubview:lineView];
    lineView.sd_layout.topSpaceToView(tmpFourLab, 14).leftEqualToView(whiteView).rightEqualToView(whiteView).heightIs(0.6);
    
    UILabel * tmpPayLab = [UILabel labelWithTitle:@"实际消费" color:[UIColor lightGrayColor] fontSize:14];
    CGSize paySize = [@"实际消费" sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)];
    tmpPayLab.frame = Frame(IPHONE_WIDTH/6.f-paySize.width/2.f, CGRectGetMaxY(_moneyLab.frame)+20, paySize.width, 14);
    [self addSubview:tmpPayLab];
    self.realPayLab = [UILabel labelWithTitle:@"3950.60" color:[UIColor blackColor] fontSize:18];
    _realPayLab.frame = Frame(10, CGRectGetMaxY(tmpPayLab.frame)+10, (IPHONE_WIDTH-20)/3.f, 20);
    [self addSubview:self.realPayLab];
    
    UILabel * tmpReturn = [UILabel labelWithTitle:@"实际还款" color:[UIColor lightGrayColor] fontSize:14];
    CGSize returnSize = [@"实际还款" sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)];
    tmpReturn.frame = Frame(IPHONE_WIDTH/2.f-returnSize.width/2.f, CGRectGetMaxY(_moneyLab.frame)+20, returnSize.width, 14);
    [self addSubview:tmpReturn];
    self.realReturnLab = [UILabel labelWithTitle:@"3950" color:[UIColor blackColor] fontSize:20];
    _realReturnLab.frame = Frame(IPHONE_WIDTH/3.f, CGRectGetMaxY(tmpPayLab.frame)+10, (IPHONE_WIDTH-20)/3.f, 20);
    [self addSubview:self.realReturnLab];
    
    UILabel * tmpfee = [UILabel labelWithTitle:@"手续费" color:[UIColor lightGrayColor] fontSize:14];
    CGSize feeSize = [@"手续费" sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 14)];
    tmpfee.frame = Frame(IPHONE_WIDTH*5/6.f-feeSize.width/2.f, CGRectGetMaxY(_moneyLab.frame)+20, feeSize.width, 14);
    [self addSubview:tmpfee];
    self.feeLab = [UILabel labelWithTitle:@"36.25" color:[UIColor blackColor] fontSize:18];
    _feeLab.frame = Frame(IPHONE_WIDTH*2/3.f, CGRectGetMaxY(tmpPayLab.frame)+10, (IPHONE_WIDTH-20)/3.f, 20);
    [self addSubview:self.feeLab];
}
-(void)setModel:(JNCardSDKFivPreViewModel *)model{
    _settingLab.text = [NSString stringWithFormat:@"消费%ld笔 | 还款%ld笔",model.salenum,model.backnum];
    _planPayLab.text = [NSString stringWithFormat:@"%@元",model.vicPlanPay];
    _moneyLab.text = [NSString stringWithFormat:@"%@元",model.vicPlanMoney];
    _realPayLab.text = [NSString stringWithFormat:@"%@",[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%f",model.salemoney]]];
    _realReturnLab.text = [NSString stringWithFormat:@"%@",[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%f",model.backmoney]]];
    _feeLab.text = [NSString stringWithFormat:@"%@",[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%f",model.money]]];
}
@end

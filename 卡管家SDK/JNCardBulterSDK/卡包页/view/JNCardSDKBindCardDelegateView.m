//
//  BindCardDelegateView.m
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/14.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import "JNCardSDKBindCardDelegateView.h"

@implementation JNCardSDKBindCardDelegateView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.contentLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [self.contentScro addSubview:self.contentLab];
    self.contentLab.sd_layout.leftSpaceToView(self.contentScro,10).rightSpaceToView(self.contentScro,10).topEqualToView(self.contentScro).autoHeightRatio(0);
    [self.contentScro setupAutoContentSizeWithBottomView:self.contentLab bottomMargin:0];
    
}
- (IBAction)btnClick:(id)sender {
    if(self.btnBlock){
        self.btnBlock();
    }
}

@end

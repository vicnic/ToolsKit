//
//  BindCardDelegateView.h
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/14.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^CertainBtnClick)(void);
@interface JNCardSDKBindCardDelegateView : UIView
@property (weak, nonatomic) IBOutlet UILabel *viewTitleLab;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScro;
@property (retain, nonatomic) UILabel *contentLab;
@property(nonatomic,copy)CertainBtnClick btnBlock;
@end

NS_ASSUME_NONNULL_END

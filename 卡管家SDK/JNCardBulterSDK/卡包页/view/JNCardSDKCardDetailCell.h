//
//  JNCardSDKCardDetailCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BackVerifyCodeBlock)(UIButton*btn);
@interface JNCardSDKCardDetailCell : UITableViewCell
@property(nonatomic,retain)UILabel * titleLab;
@property(nonatomic,retain)UITextField * detailTF , * verifyTF;
@property(nonatomic,retain)UILabel * detailLab;
@property(nonatomic,retain)UIImageView * arrowImgView;
@property (nonatomic,strong) UIView *bottomLineView;
@property(nonatomic,copy)BackVerifyCodeBlock verifyJNBlock;
@end

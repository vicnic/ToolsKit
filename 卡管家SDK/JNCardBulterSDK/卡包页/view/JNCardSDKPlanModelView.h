//
//  JNCardSDKPlanModelView.h
//  IDCardManager
//
//  Created by Jinniu on 2018/5/16.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKFivPreViewModel.h" 
@interface JNCardSDKPlanModelView : UIView
@property(nonatomic,retain)JNCardSDKFivPreViewModel * model;
@end

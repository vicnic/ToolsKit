//
//  JNCardSDKNewRepayPlanCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface JNCardSDKNewRepayPlanCell : UITableViewCell
@property(nonatomic,retain)UILabel * titleLab;
@property(nonatomic,retain)UITextField * inputTF;
@property(nonatomic,retain)UIImageView * arrowImgView;
@property(nonatomic,copy)NSString * myTFText;
@end

//
//  JNCardSDKNewRepayPlanCell.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKNewRepayPlanCell.h"

@implementation JNCardSDKNewRepayPlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLab = [UILabel new];
        _titleLab.font = Font(15);
        [self.contentView addSubview:self.titleLab];
        _titleLab.sd_layout.leftSpaceToView(self.contentView, AUTO(10)).topSpaceToView(self.contentView, AUTO(5)).heightIs(30);
        [_titleLab setSingleLineAutoResizeWithMaxWidth:200];
        
        if ([reuseIdentifier isEqualToString:@"notextfield"]) {
            //            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.arrowImgView = [UIImageView new];
            _arrowImgView.image = Image(@"右箭头");
            [self.contentView addSubview:self.arrowImgView];
            _arrowImgView.sd_layout.rightSpaceToView(self.contentView, AUTO(20)).centerYEqualToView(_titleLab).widthIs(10).heightIs(20);
        }else if([reuseIdentifier isEqualToString:@"hesuan"]){
            UIButton*btn=[UIButton new];
            [btn setTitle: @"核算各项费用" forState: UIControlStateNormal];
            btn.layer.cornerRadius=3;
            btn.titleLabel.textColor=[UIColor whiteColor];
            btn.titleLabel.font=[UIFont systemFontOfSize:12];
            [btn addTarget:self action:@selector(hesuanBtnClick) forControlEvents:UIControlEventTouchUpInside];
            btn.backgroundColor=ThemeColor;
            [self.contentView addSubview:btn];
            btn.sd_layout.rightSpaceToView(self.contentView, 10).widthIs(80).heightIs(30).centerYEqualToView(self.contentView);
            self.inputTF = [UITextField new];
            _inputTF.font = Font(15);
            [self.contentView addSubview:self.inputTF];
            _inputTF.sd_layout.leftSpaceToView(_titleLab, AUTO(10)).rightSpaceToView(btn, 10).centerYEqualToView(_titleLab).heightIs(30);
        }
        else{
            self.inputTF = [UITextField new];
            _inputTF.font = Font(15);
            _inputTF.keyboardType = UIKeyboardTypeNumberPad;
            [self.contentView addSubview:self.inputTF];
            _inputTF.sd_layout.leftSpaceToView(_titleLab, AUTO(10)).rightEqualToView(self.contentView).centerYEqualToView(_titleLab).heightIs(30);
        }
    }
    return self;
}
-(void)setMyTFText:(NSString *)myTFText{
    _myTFText = myTFText;
    _inputTF.text = myTFText;
}
-(void)hesuanBtnClick{
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

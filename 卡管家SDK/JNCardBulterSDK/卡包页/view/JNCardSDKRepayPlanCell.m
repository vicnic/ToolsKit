//
//  JNCardSDKRepayPlanCell.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/25.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKRepayPlanCell.h"

@implementation JNCardSDKRepayPlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.dateLab = [UILabel new];
        _dateLab.font = Font(15);
        [self.contentView addSubview:self.dateLab];
        _dateLab.sd_layout.leftSpaceToView(self.contentView , AUTO(10)).topSpaceToView(self.contentView, AUTO(10)).heightIs(20);
        [_dateLab setSingleLineAutoResizeWithMaxWidth:300];
        
        self.descLab = [UILabel new];
        _descLab.textAlignment = NSTextAlignmentCenter;
        _descLab.font = Font(15);
        [self.contentView addSubview:self.descLab];
        _descLab.sd_layout.rightSpaceToView(self.contentView, AUTO(14)).centerYEqualToView(_dateLab).heightIs(20);
        [_descLab setSingleLineAutoResizeWithMaxWidth:200];
        
        UIView * lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.8 alpha:1];
        [self.contentView addSubview:lineView];
        lineView.sd_layout.leftEqualToView(_dateLab).rightEqualToView(_descLab).topSpaceToView(_dateLab, AUTO(5)).heightIs(1);
        
        self.bankImgView = [UIImageView new];
        [self.contentView addSubview:self.bankImgView];
        
        
        self.cardNumLab = [UILabel new];
        _cardNumLab.font = Font(16);
        [self.contentView addSubview:self.cardNumLab];
        _cardNumLab.sd_layout.leftSpaceToView(_bankImgView, AUTO(20)).topSpaceToView(_dateLab, AUTO(20)).heightIs(20);
        [_cardNumLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH-60];
        
        self.planLab = [UILabel new];
        _planLab.font = Font(14);
        [self.contentView addSubview:self.planLab];
        _planLab.sd_layout.leftEqualToView(_cardNumLab).topSpaceToView(_cardNumLab, AUTO(10)).heightIs(20);
        [_planLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH-60];
        
        self.lineView2 = [UIView new];
//        _lineView2.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.8 alpha:1];
        [self.contentView addSubview:self.lineView2];
        _lineView2.sd_layout.topSpaceToView(_planLab, AUTO(20)).leftEqualToView(lineView).rightEqualToView(lineView).heightIs(1);
        NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
        _planLab.sd_layout.leftEqualToView(_cardNumLab).topSpaceToView(_cardNumLab, 0).heightIs(20);
        [_planLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH-60];
        //计划额度
        self.chargeLab = [UILabel new];
        _chargeLab.font = Font(14);
        [self.contentView addSubview:self.chargeLab];
        _chargeLab.sd_layout.leftSpaceToView(_planLab, 10).centerYEqualToView(_planLab).heightIs(20);
        [_chargeLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH/2];
        self.realPayLab = [UILabel new];
        _realPayLab.font = Font(14);
        [self.contentView addSubview:self.realPayLab];
        _realPayLab.sd_layout.topSpaceToView(_chargeLab, 0).leftEqualToView(_planLab).heightIs(20);
        [_realPayLab setSingleLineAutoResizeWithMaxWidth:300];
        
        self.shouxuFeeLab = [UILabel new];
        _shouxuFeeLab.font = Font(14);
        [self.contentView addSubview:self.shouxuFeeLab];
        _shouxuFeeLab.sd_layout.leftSpaceToView(_realPayLab, AUTO(10)).centerYEqualToView(_realPayLab).heightIs(20);
        [_shouxuFeeLab setSingleLineAutoResizeWithMaxWidth:200];
        
        
        self.lineView2 = [UIView new];
        _lineView2.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.lineView2];
        _lineView2.sd_layout.topSpaceToView(_shouxuFeeLab, AUTO(10)).leftEqualToView(lineView).rightEqualToView(lineView).heightIs(1);
        
        UIView * bottomView = [UIView new];
        bottomView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.96 alpha:1];
        [self.contentView addSubview:bottomView];
        bottomView.sd_layout.leftEqualToView(self.contentView).rightEqualToView(self.contentView).bottomEqualToView(self.contentView).heightIs(AUTO(5));
        
    }
    return self;
}

-(void)setModel:(JNCardSDKRepayPlanCellModel *)model{
    if ([model.theState isEqualToString:@"0"]) {
        _bankImgView.sd_layout.leftEqualToView(_dateLab).centerYEqualToView(_cardNumLab).widthIs(AUTO(40)).heightIs(AUTO(40));
    }else if ([model.theState isEqualToString:@"3"]){//计划暂停
        _bankImgView.sd_layout.leftEqualToView(_dateLab).centerYEqualToView(_cardNumLab).offset(10).widthIs(AUTO(40)).heightIs(AUTO(40));
    }else{
        _bankImgView.sd_layout.leftEqualToView(_dateLab).centerYEqualToView(_planLab).offset(-10).widthIs(AUTO(40)).heightIs(AUTO(40));
    }
    _dateLab.text = model.createTime;
    _descLab.text = [JNCardSDKReuserFile returnState:model.theState];
    [_bankImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,model.bankImg]]];
    _cardNumLab.text = [NSString stringWithFormat:@"%@ %@",model.bankName,model.bankNum];
    _chargeLab.text = [NSString stringWithFormat:@"计划额度:%@",[NSString stringWithFormat:@"%.2f",[model.repayMoney floatValue]]];
    _realPayLab.text = [NSString stringWithFormat:@"实际还款:%@",model.factBackMoney.length>0?[NSString stringWithFormat:@"%.2f",[model.factBackMoney floatValue]]:@""];
    _shouxuFeeLab.text = [NSString stringWithFormat:@"手续费:%@",model.sumMoney.length>0?[NSString stringWithFormat:@"%.2f",[model.sumMoney floatValue]]:@""];
    _planLab.text = [NSString stringWithFormat:@"计划配置%@天%@笔",model.repayDays,model.repayTimes];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


//
//  JNCardSDKCardDetailCell.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKCardDetailCell.h"
@interface JNCardSDKCardDetailCell()<UITextFieldDelegate>
{
    UIButton * getVerifyCodeBtn;
}
@end
@implementation JNCardSDKCardDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLab = [[UILabel alloc]initWithFrame:Frame(5, 15, 82, 24)];
        _titleLab.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.titleLab];
        
        self.arrowImgView = [[UIImageView alloc]initWithFrame:CGRectMake(IPHONE_WIDTH-30, _titleLab.frame.origin.y, 20, 20)];
        
        [self.contentView addSubview:self.arrowImgView];
        
        CGFloat titleOrgX =_titleLab.frame.origin.x+_titleLab.frame.size.width;//标题标签的最右侧坐标数值
        self.detailTF = [[UITextField alloc]init];
        
        _detailTF.textAlignment = NSTextAlignmentLeft;
        _detailTF.font = [UIFont systemFontOfSize:14];
        _detailTF.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.detailTF];
        if ([reuseIdentifier isEqualToString:@"verifycell"]) {
            getVerifyCodeBtn = [UIButton new];
            getVerifyCodeBtn.backgroundColor = [UIColor whiteColor];
            [getVerifyCodeBtn setTitle:@"获取验证码" forState:0];
            [getVerifyCodeBtn addTarget:self action:@selector(getVerifyCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            getVerifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
            getVerifyCodeBtn.userInteractionEnabled = YES;
            [getVerifyCodeBtn setTitleColor:[UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1] forState:0];
            [self.contentView addSubview:getVerifyCodeBtn];
            getVerifyCodeBtn.sd_layout.rightSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 10).heightIs(30).widthIs(82.6);

            self.verifyTF = [UITextField new];
            _verifyTF.textAlignment = NSTextAlignmentLeft;
            _verifyTF.font = [UIFont systemFontOfSize:14];
            _verifyTF.keyboardType = UIKeyboardTypeNumberPad;
            [self.contentView addSubview:self.verifyTF];
            _verifyTF.sd_layout.leftSpaceToView(_titleLab, 5).centerYEqualToView(_titleLab).heightIs(30).rightSpaceToView(getVerifyCodeBtn, 5);
        }else{
            _detailTF.frame = Frame(titleOrgX+5, _titleLab.frame.origin.y-5, IPHONE_WIDTH- titleOrgX-30, 30);
        }
        self.bottomLineView = [UIView new];
        self.bottomLineView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
        [self.contentView addSubview:self.bottomLineView];
        self.bottomLineView.sd_layout.bottomEqualToView(self.contentView).leftEqualToView(self.contentView).rightEqualToView(self.contentView).heightIs(0.6);
    }
    return self;
}

-(void)getVerifyCodeBtnClick:(UIButton *)sender{
    if (self.verifyJNBlock) {
        self.verifyJNBlock(sender);
    }
}


//-(void)makeCodeBtn:(UIButton*)sender{
//    __block int time = 60;
//    __block UIButton *verifybutton = sender;
//    verifybutton.enabled = NO;
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
//    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
//    dispatch_source_set_event_handler(_timer, ^{
//        if(time<=0){ //倒计时结束，关闭
//            dispatch_source_cancel(_timer);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                verifybutton.backgroundColor = ThemeColor;
//                [verifybutton setTitle:@"获取验证码" forState:UIControlStateNormal];
//                verifybutton.enabled = YES;
//            });
//        }else{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //设置界面的按钮显示 根据自己需求设置
//                verifybutton.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.82 alpha:1];
//                NSString *strTime = [NSString stringWithFormat:@"%d s",time];
//                [verifybutton setTitle:strTime forState:UIControlStateNormal];
//                verifybutton.titleLabel.textColor = [UIColor whiteColor];
//            });
//            time--;
//        }
//    });
//    dispatch_resume(_timer);
//}


@end

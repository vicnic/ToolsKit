//
//  JNCardSDKRepayDetailCell.h
//  IDCardManager
//
//  Created by 金牛 on 2017/8/1.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKRepayDetaiCellModel.h"
@interface JNCardSDKRepayDetailCell : UITableViewCell
@property(nonatomic,retain)JNCardSDKRepayDetaiCellModel * model;
@end

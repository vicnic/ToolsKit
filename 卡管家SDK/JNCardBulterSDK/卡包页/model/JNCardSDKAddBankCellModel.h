//
//  JNCardSDKAddBankCellModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/25.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKAddBankCellModel : NSObject
@property(nonatomic,copy)NSString * bankName;//银行名称
@property(nonatomic,copy)NSString * bankImg;
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * billDate;
@property(nonatomic,copy)NSString * billDateStr;
@property(nonatomic,copy)NSString * dueDate;
@property(nonatomic,copy)NSString * dueDateStr;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * userCreditCardId;
@end

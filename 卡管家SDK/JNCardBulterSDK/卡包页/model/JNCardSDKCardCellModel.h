//
//  JNCardSDKCardCellModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/18.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKCardCellModel : NSObject
@property(nonatomic,copy)NSString * dueDate;
@property(nonatomic,copy)NSString * billDate;
@property(nonatomic,copy)NSString * userCreditCardId;
@property(nonatomic,copy)NSString * name;//用户名
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * bankName;
@property(nonatomic,copy)NSString * bankImg;
@property(nonatomic,copy)NSString * repayMoney;
@property(nonatomic,copy)NSString * theState;
@property(nonatomic,copy)NSString * userRepayPlayId;
@property(nonatomic,copy)NSString * firstMoney;
@property(nonatomic,copy)NSString * sumMoney;//手续费
@end

//
//  JNCardSDKFivMakePlanReqModel.m
//  IDCardManager
//
//  Created by Jinniu on 2018/5/17.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKFivMakePlanReqModel.h"
@implementation JNCardSDKFivMakePlanReqModel
+(void)previewReq:(NSMutableDictionary*)paraDic backBlock:(JNCardSDKFivBackBlock)block{
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p addEntriesFromDictionary:paraDic];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_FivMakePlanReqChannelReq];
    [SVProgressHUD show];
    [JNCardSDKMytools requestStringByPostUrl:url parameter:p success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        block(backDic);
    } fail:^(NSError *error) {
    }];
}
@end

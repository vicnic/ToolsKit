//
//  JNCardSDKFivNetFile.h
//  IDCardManager
//
//  Created by Jinniu on 2018/5/18.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^FivFileNetBackBlock)(NSDictionary*backDic);
@interface JNCardSDKFivNetFile : NSObject
+(void)savePlanIntoSQLWithKey:(NSString *)theKey couponID:(NSString*)couponID andBackBlock:(FivFileNetBackBlock)block;

+(void)makePlanToExcutingWithPlanID:(NSString*)planID andBackBlock:(FivFileNetBackBlock)block;
@end

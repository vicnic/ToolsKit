//
//  JNCardSDKFVRepayDetailModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKFVRepayDetailModel : NSObject
@property(nonatomic,copy)NSString * theStateStr;
@property(nonatomic,copy)NSString * theMoneyConsume;
@property(nonatomic,copy)NSString * Id;
@property(nonatomic,copy)NSString * payChannelId;
@property(nonatomic,copy)NSString * theType;
@property(nonatomic,copy)NSString * fee;
@property(nonatomic,copy)NSString * theState;
@property(nonatomic,copy)NSString * userRepayPlayId;
@property(nonatomic,copy)NSString * sumMoney;//手续费
@property(nonatomic,copy)NSString * theTime;
@property(nonatomic,copy)NSString * theMoneyRepay;
@property(nonatomic,copy)NSString * orderId;
@property(nonatomic,copy)NSString * proName;
@property(nonatomic,copy)NSString * theTypeStr;
@property(nonatomic,copy)NSString * theMoney;
@end

//
//  JNCardSDKRepayDetaiCellModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/8/1.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKRepayDetaiCellModel : NSObject
@property(nonatomic,copy)NSString * theDate;
@property(nonatomic,copy)NSString * sort;
@property(nonatomic,copy)NSString * flag;
@property(nonatomic,copy)NSString * payChannelId;

@property(nonatomic,copy)NSString * Id;
@property(nonatomic,copy)NSString * theMoney;//快速还款的金额
@property(nonatomic,copy)NSString * theMoneyConsume;//不知道
@property(nonatomic,copy)NSString * theMoneyRepay;//不知道
@property(nonatomic,copy)NSString * theState;//不知道
@property(nonatomic,copy)NSString * theStateStr;//“未付款”不知道干嘛的
@property(nonatomic,copy)NSString * theTime;//快速还款日期和消费日期
@property(nonatomic,copy)NSString * theType;//不知道
@property(nonatomic,copy)NSString * theTypeStr;//快速还款字符串“快速还款”或者“消费”
@property(nonatomic,copy)NSString * userRepayPlayId;//计划id
@property(nonatomic,copy)NSString * fee;//差额，手续费
@end

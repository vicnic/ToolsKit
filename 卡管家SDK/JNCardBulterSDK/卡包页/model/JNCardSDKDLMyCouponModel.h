//
//  JNCardSDKDLMyCouponModel.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/5/9.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*
 "CouponName":"平台通用类测试项（暂不删除）",
 "ValidEndDateStr":"2119-05-22",
 "BussTypeName":"平台通用",
 "UserId":26706,
 "Explain":"消费满1000.00可用",
 "UseRemark":"平台通用类测试项（暂不删除）平台通用类测试项（暂不删除）平台通用类测试项（暂不删除）平台通用类测试项（暂不删除）平台通用类测试项（暂不删除）",
 "ValidBeginDate":"2019-05-22 14:24:41",
 "CouponValue":12,
 "ValidBeginDateStr":"2019-05-22",
 "TheState":0,
 "ValidEndDate":"2119-05-22 14:24:41",
 "StateStr":null,
 "MeetMoney":1000,
 "UserCouponId":"1f755bf5-6473-42cf-95c2-ead70f0a4154"
 */
@interface JNCardSDKDLMyCouponModel : NSObject
@property (nonatomic, copy) NSString *CouponName;

@property (nonatomic, copy) NSString *ValidEndDateStr;

@property (nonatomic, copy) NSString *BussTypeName;
@property (nonatomic, copy) NSString *Explain;
@property (nonatomic, assign) NSInteger UserId;

@property (nonatomic, copy) NSString *UseRemark;

@property (nonatomic, copy) NSString *ValidBeginDate;

@property (nonatomic, assign) NSInteger TheState;
@property (nonatomic, copy) NSString * StateStr;
@property (nonatomic, assign) CGFloat CouponValue;

@property (nonatomic, copy) NSString *ValidBeginDateStr;

@property (nonatomic, copy) NSString *ValidEndDate;
@property (nonatomic, copy) NSString *CouponCode;
@property (nonatomic, copy) NSString *UserCouponId;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *MeetMoneyStr;
@property (nonatomic, assign) CGFloat MeetMoney;
@property (nonatomic, assign) NSInteger TheCount;
//isFold是自己添加的
@property (nonatomic, assign) BOOL isFold;
@property (nonatomic, assign) BOOL isSelected;
@end

NS_ASSUME_NONNULL_END

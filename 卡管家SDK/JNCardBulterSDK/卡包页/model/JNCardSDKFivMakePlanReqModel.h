//
//  JNCardSDKFivMakePlanReqModel.h
//  IDCardManager
//
//  Created by Jinniu on 2018/5/17.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^JNCardSDKFivBackBlock)(NSDictionary * backDic);
@interface JNCardSDKFivMakePlanReqModel : NSObject
+(void)previewReq:(NSMutableDictionary*)paraDic backBlock:(JNCardSDKFivBackBlock)block;
@end

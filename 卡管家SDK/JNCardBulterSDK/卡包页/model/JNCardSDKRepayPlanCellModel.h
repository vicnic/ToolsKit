//
//  JNCardSDKRepayPlanCellModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/25.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKRepayPlanCellModel : NSObject
@property(nonatomic,copy)NSString * bankName;
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * firstMoney;
@property(nonatomic,copy)NSString * billDate;//账单日
@property(nonatomic,copy)NSString * createTime;//创建改还款项目时间
@property(nonatomic,copy)NSString * dueDate;//还款日
@property(nonatomic,copy)NSString * repayDays;//不懂
@property(nonatomic,copy)NSString * repayMoney;//还款金额
@property(nonatomic,copy)NSString * factSaleMoney;//实际消费
@property(nonatomic,copy)NSString * factBackMoney;//实际还款
@property(nonatomic,copy)NSString * repayTimes;//分几次还
@property(nonatomic,copy)NSString * userRepayPlayId;//还款的planid，获取详情页会用到
@property(nonatomic,copy)NSString * theStateStr;//是否执行中
@property(nonatomic,copy)NSString * bankImg;
@property(nonatomic,copy)NSString * sumMoney;//第一笔要付的钱
@property(nonatomic,copy)NSString * theState;//
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString * userCreditCardId;
@end

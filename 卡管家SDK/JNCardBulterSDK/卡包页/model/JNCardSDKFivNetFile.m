//
//  JNCardSDKFivNetFile.m
//  IDCardManager
//
//  Created by Jinniu on 2018/5/18.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKFivNetFile.h"

@implementation JNCardSDKFivNetFile
+(void)savePlanIntoSQLWithKey:(NSString *)theKey couponID:(NSString*)couponID andBackBlock:(FivFileNetBackBlock)block{
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:theKey forKey:@"TheKey"];
    if (couponID.length>0) {
        [p setObject:couponID forKey:@"UserCouponId"];
    }    
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_FivSavePlanToSQL];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            block(responseObject);
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

+(void)makePlanToExcutingWithPlanID:(NSString*)planID andBackBlock:(FivFileNetBackBlock)block{
    [SVProgressHUD show];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_CancelCardState];
    [paraDic setObject:planID forKey:@"PlanId"];
    [paraDic setObject:@"1" forKey:@"TheState"];
    [paraDic setObject:@"2" forKey:@"NewType"];
    [PGNetworkHelper POST:url parameters:paraDic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            block(responseObject);
            [JNCardSDKMytools warnText:@"生成成功" status:JNCardSDKSuccess];
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
@end

//
//  JNCardSDKFivPreViewModel.m
//  IDCardManager
//
//  Created by Jinniu on 2018/5/18.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKFivPreViewModel.h"

@implementation JNCardSDKFivPreViewModel
+ (NSDictionary *)objectClassInArray{
    return @{@"data" : [JNCardSDKData class]};
}
@end

@implementation JNCardSDKData

+ (NSDictionary *)objectClassInArray{
    return @{@"PlanRecords" : [JNCardSDKPlanrecords class]};
}

@end


@implementation JNCardSDKPlanrecords

@end

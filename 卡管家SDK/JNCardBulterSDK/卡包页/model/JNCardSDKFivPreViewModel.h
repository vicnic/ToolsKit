//
//  JNCardSDKFivPreViewModel.h
//  IDCardManager
//
//  Created by Jinniu on 2018/5/18.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JNCardSDKData,JNCardSDKPlanrecords;
@interface JNCardSDKFivPreViewModel : NSObject

@property (nonatomic, copy) NSString *planid;

@property (nonatomic, assign) NSInteger myfeemoney;

@property (nonatomic, assign) CGFloat money;

@property (nonatomic, assign) CGFloat backmoney;

@property (nonatomic, assign) NSInteger mysalenum;

@property (nonatomic, assign) NSInteger backnum;

@property (nonatomic, assign) NSInteger mybackmoney;

@property (nonatomic, assign) CGFloat salemoney;

@property (nonatomic, assign) NSInteger mysalemoney;

@property (nonatomic, assign) CGFloat planrate;

@property (nonatomic, strong) JNCardSDKData *data;

@property (nonatomic, assign) NSInteger salenum;

@property (nonatomic, assign) NSInteger days;

@property (nonatomic, assign) NSInteger mybacknum;

@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *msg;
//以下两个是自己加的，后台返回没有这三项，但是这三项是必须的
@property (nonatomic, copy) NSString *vicPlanPay;

@property (nonatomic, copy) NSString *vicPlanMoney;

@end
@interface JNCardSDKData : NSObject

@property (nonatomic, assign) NSInteger minDays;

@property (nonatomic, assign) BOOL isSuccess;

@property (nonatomic, assign) NSInteger maxDays;

@property (nonatomic, copy) NSString *theKey;

@property (nonatomic, strong) NSArray<JNCardSDKPlanrecords *> *planRecords;

@property (nonatomic, copy) NSString *msg;

@end

@interface JNCardSDKPlanrecords : NSObject

@property (nonatomic, copy) NSString *TheDate;

@property (nonatomic, assign) CGFloat TheMoney;

@property (nonatomic, assign) CGFloat Fee;

@property (nonatomic, assign) NSInteger flag;

@property (nonatomic, assign) NSInteger PayChannelId;

@property (nonatomic, assign) NSInteger TheType;

@property (nonatomic, assign) NSInteger Sort;

@end


//
//  JNCardSDKPicClickVC.m
//  TwoOneEight
//
//  Created by Jinniu on 2018/10/30.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "JNCardSDKPicClickVC.h"
#import <WebKit/WebKit.h>
@interface JNCardSDKPicClickVC ()<WKNavigationDelegate> 

@end

@implementation JNCardSDKPicClickVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"详情";
    self.view.backgroundColor = [UIColor whiteColor];
    [self requestData];
}
-(void)requestData{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetArticleDetail];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:@{@"ArticleId":self.articleID} cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            NSString * pageurl = responseObject[@"Url"];
            if (![pageurl isKindOfClass:[NSNull class]] && pageurl.length>0) {
                [self showURLPage:responseObject];
            }else{
                [self createUIWithDic:responseObject];
            }
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)showURLPage:(NSDictionary*)dic{
    WKWebView * webView = [WKWebView new];
    webView.navigationDelegate = self;
    
    [webView loadHTMLString:dic[@"Url"] baseURL:nil];
    [self.view addSubview:webView];
    webView.sd_layout.leftSpaceToView(self.view, 10).rightSpaceToView(self.view, 10).topSpaceToView(self.view, kTopHeight).bottomSpaceToView(self.view, KTabbarSafeBottomMargin);
}
-(void)createUIWithDic:(NSDictionary*)dic{
    self.title = dic[@"ArticleTitle"];
    NSString *theContent = [NSString stringWithFormat:@"%@",dic[@"TheContent"]];
    if (theContent.length>0) {
        NSString *htmls = [NSString stringWithFormat:@"<html> \n"
                           "<head> \n"
                           "<style type=\"text/css\"> \n"
                           "body {font-size:15px;}\n"
                           "</style> \n"
                           "</head> \n"
                           "<body>"
                           "<script type='text/javascript'>"
                           "window.onload = function(){\n"
                           "var $img = document.getElementsByTagName('img');\n"
                           "for(var p in  $img){\n"
                           " $img[p].style.width = '100%%';\n"
                           "$img[p].style.height ='auto'\n"
                           "}\n"
                           "}"
                           "</script>%@"
                           "</body>"
                           "</html>",theContent];
        WKWebView * webView = [WKWebView new];
        webView.navigationDelegate = self;
        [webView loadHTMLString:htmls baseURL:nil];
        [self.view addSubview:webView];
        webView.sd_layout.leftSpaceToView(self.view, 10).rightSpaceToView(self.view, 10).topSpaceToView(self.view, kTopHeight).bottomSpaceToView(self.view, KTabbarSafeBottomMargin);
    }
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'" completionHandler:nil];
}
@end

//
//  JNCardSDKPreViewPlanVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/27.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
typedef void (^BackPlanIDBlock)(NSString * backPlanId);
@interface JNCardSDKPreViewPlanVC : JNCardSDKBaseViewCtr
@property(nonatomic,assign)BOOL isFromFivPre;
@property(nonatomic,retain)NSDictionary * backDic;

@property(nonatomic,copy)NSString * planID;
@property(nonatomic,copy)NSString * handMoney;
@property(nonatomic,copy)NSString * planMoney;//用户指定计划要还的钱
@property(nonatomic,copy)NSString * bankCardID;
@property(nonatomic,copy)NSString * saleMoney;//实际消费
@property(nonatomic,copy)NSString * salenum;//消费笔数
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * planDingjin;//计划本金
@property(nonatomic,copy)BackPlanIDBlock backPlanIDBlock;
@end

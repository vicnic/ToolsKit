//
//  JNCardSDKNewRepayPlanVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"

@interface JNCardSDKNewRepayPlanVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * billDay;
@property(nonatomic,copy)NSString * payDay;
@property(nonatomic,copy)NSString * bankCardID;//银行卡的id
@property(nonatomic,assign)NSInteger myPlanType;//1是制定计划，2是修改计划
@property(nonatomic,assign)CGFloat planTotalMoney;//修改计划传入的计划额度
@property(nonatomic,copy)NSString * bankImgUrl;
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * realName;
@property(nonatomic,copy)NSString * repayPlayId;
@property(nonatomic,copy)NSString * FirstMoney;

@property(nonatomic,retain)UILabel * bankName;
@property(nonatomic,retain)UIImageView * bankImgView;
@property(nonatomic,retain)UILabel * userNameLab;
@property(nonatomic,retain)UILabel * billDayLab;
@property(nonatomic,retain)UILabel * cardNumLab;
@property(nonatomic,retain)UILabel * payDayLab;

@property(nonatomic,retain)NSMutableDictionary * cardContentDic;
@property(nonatomic,retain)JNCardSDKCardCellModel * cardInfoModel;

@end

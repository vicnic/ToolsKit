//
//  JNCardSDKRepayPlanVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/25.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"
@interface JNCardSDKRepayPlanVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * userCreditCardId;//从计划详情点击往期计划进来需要传的信用卡卡片
@property(nonatomic,retain)JNCardSDKCardCellModel * cardModel;//从计划详情点击往期计划进来需要传的信用卡卡片
@end


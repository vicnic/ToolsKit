//
//  JNCardSDKAddNewCardVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKFVRepayCellModel.h"
#import "JNCardSDKAddBankCellModel.h"
@interface JNCardSDKAddNewCardVC : JNCardSDKBaseViewCtr
@property(nonatomic,retain)JNCardSDKFVRepayCellModel * model;
@property(nonatomic,retain)JNCardSDKAddBankCellModel * bankModel;
@end

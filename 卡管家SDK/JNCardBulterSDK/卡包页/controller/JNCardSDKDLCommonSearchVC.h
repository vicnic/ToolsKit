//
//  JNCardSDKDLCommonSearchVC.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/5/22.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchResultBlock)(NSString * resultStr);
@interface JNCardSDKDLCommonSearchVC : JNCardSDKBaseViewCtr
@property(nonatomic,retain)NSArray * dataArr;
@property(nonatomic,copy)SearchResultBlock resultBlock;
@end

NS_ASSUME_NONNULL_END

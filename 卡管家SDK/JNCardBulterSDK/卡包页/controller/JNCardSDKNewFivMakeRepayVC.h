//
//  JNCardSDKNewFivMakeRepayVC.h
//  TwoOneEight
//
//  Created by jinniu2 on 2019/4/23.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"

@interface JNCardSDKNewFivMakeRepayVC : JNCardSDKBaseViewCtr
@property(nonatomic,retain)JNCardSDKCardCellModel * cardInfoModel;
@property(nonatomic,retain)UIColor * receiveColor;
@end

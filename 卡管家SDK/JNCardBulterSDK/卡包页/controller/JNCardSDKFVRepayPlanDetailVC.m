//
//  JNCardSDKFVRepayPlanDetailVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKFVRepayPlanDetailVC.h"
#import "JNCardSDKFVRepayDetailCell.h"
#import "JNCardSDKFVRepayDetailModel.h"
#import <HDAlertView/HDAlertView.h>
#define ThisPageTextColor [UIColor colorWithHue:0 saturation:0 brightness:0.53 alpha:1]
@interface JNCardSDKFVRepayPlanDetailVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * _dataArr;
    NSInteger _page;
    NSDictionary * _backDic;
    NSString * _tmpDate;
}
@property(nonatomic,retain)UITableView * myTab;
@end

@implementation JNCardSDKFVRepayPlanDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _page = 1;
    _dataArr = [NSMutableArray new];
    _backDic = [NSDictionary new];
    [self.view addSubview:self.myTab];
    [self getRepayPlanDetail];
}
#pragma mark 请求
-(void)getRepayPlanDetail{
    [SVProgressHUD show];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:self.planID forKey:@"PlanId"];
    [p setObject:[NSString stringWithFormat:@"%ld",_page] forKey:@"currentpage"];
    [p setObject:@"30" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetRepayPlanDetail];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self->_myTab.mj_header endRefreshing];
        [self->_myTab.mj_footer endRefreshing];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            self->_backDic = responseObject;
            NSArray * dataArr = responseObject[@"data"];
            NSArray * newArr = [self dealWithDataIntoArray:dataArr].copy;
            NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKFVRepayDetailModel mj_objectArrayWithKeyValuesArray:newArr]];
            [self->_dataArr addObjectsFromArray:arrM];
            [self->_myTab reloadData];
            [self createBottomView];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}
#pragma mark 懒加载
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-60) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.backgroundColor = [UIColor colorWithHue:0.67 saturation:0.02 brightness:0.96 alpha:1];
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self->_dataArr removeAllObjects];
            [self getRepayPlanDetail];
        }];
        _myTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self->_page ++;
            [self getRepayPlanDetail];
        }];
    }
    return _myTab;
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2+_dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section<2) {
        return 0;
    }else{
        NSArray * arr = _dataArr[section-2];
        return arr.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JNCardSDKFVRepayDetailCell * cell = nil;
    if (!cell ) {
        cell = [[JNCardSDKFVRepayDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
        CGRect rect = CGRectMake(0, 0, IPHONE_WIDTH-20, 60);
        CGSize radio = CGSizeMake(10, 10);
        UIRectCorner corner = UIRectCornerAllCorners;
        NSArray * tmpArr = _dataArr[indexPath.section-2];
        if(indexPath.section == _dataArr.count+1&&indexPath.row==tmpArr.count-1){
            corner = UIRectCornerBottomLeft|UIRectCornerBottomRight;//这只圆角位置
            UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corner cornerRadii:radio];
            CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];//创建shapelayer
            masklayer.frame = cell.bgView.bounds;
            masklayer.path = path.CGPath;//设置路径
            cell.bgView.layer.mask = masklayer;
        }
        if (_dataArr.count!=0) {
            JNCardSDKFVRepayDetailModel * model = _dataArr[indexPath.section-2][indexPath.row];
            cell.model = model;
        }
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 120;
    }else if(section ==1){
        return 70;
    }else{
        return 40;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 50;
    }else if (section == _dataArr.count+1){
        return 0.01;
    }else{
        return 0.01;
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        UIView * zeroHeadView = [UIView new];
        [self createZeroSectionHeadView:zeroHeadView];
        return zeroHeadView;
    }else if(section==1){
        UIView * oneHeadView = [UIView new];
        [self createOneSectionHeadView:oneHeadView];
        return oneHeadView;
    }else{
        if (section-2>=_dataArr.count) {
            return nil;
        }
        JNCardSDKFVRepayDetailModel * model = _dataArr[section-2][0];
        UIView * headView= [UIView new];
        UILabel * grayView = [UILabel new];
        grayView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.6 alpha:1];
        grayView.textColor = [UIColor whiteColor];
        grayView.font = [UIFont systemFontOfSize:15];
        grayView.text = [NSString stringWithFormat:@"    %@",[model.theTime substringWithRange:NSMakeRange(0, 10)]];
        [headView addSubview:grayView];
        grayView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(0, 10, 0, 10));
        
        CGRect rect = CGRectMake(0, 0, IPHONE_WIDTH-20, 50);
        CGSize radio = CGSizeMake(5, 5);
        UIRectCorner corner = UIRectCornerAllCorners;
        if (section == 2) {
            corner = UIRectCornerTopLeft|UIRectCornerTopRight;//这只圆角位置
            UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corner cornerRadii:radio];
            CAShapeLayer *masklayer = [[CAShapeLayer alloc]init];//创建shapelayer
            masklayer.frame = grayView.bounds;
            masklayer.path = path.CGPath;//设置路径
            grayView.layer.mask = masklayer;
        }
        return headView;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView * zeroFootView = [UIView new];
        UILabel * lab = [UILabel labelWithTitle:@"温馨提示：在您确定计划提交后，系统即帮您执行本计划第一笔还款" color:[UIColor colorWithHue:0 saturation:0 brightness:0.43 alpha:1] fontSize:12];
        lab.textAlignment = NSTextAlignmentLeft;
        [zeroFootView addSubview:lab];
        lab.sd_layout.leftSpaceToView(zeroFootView, 10).centerYEqualToView(zeroFootView).rightSpaceToView(zeroFootView, 10).autoHeightRatio(0);
        return zeroFootView;
    }else{
        return nil;
    }
}
-(void)wenhaoBtnClick{
    NSString * newsrt = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_planInfoDic[@"sumMoney"] doubleValue]]];
    NSString * rate = [NSString stringWithFormat:@"%.2f",[_backDic[@"planrate"] doubleValue]];
    NSString * msg = [NSString stringWithFormat:@"手续费：%@\n单笔手续费=单笔消费额*%@%%+1\n手续费总额=全部单笔手续费相加之和",newsrt,rate];
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"手续费明细" andMessage:msg];
    alertView.messageTextAlignment = NSTextAlignmentLeft;
    [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
    }];
    [alertView show];
}
#pragma mark view创建
-(void)createBottomView{
    UIView * footer = [UIView new];
    footer.backgroundColor = ThemeColor;
    [self.view addSubview:footer];
    footer.sd_layout.leftEqualToView(self.view).rightEqualToView(self.view).bottomEqualToView(self.view).heightIs(60);
    if (_backDic.count !=0) {
        NSArray * valueArr = @[[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"backmoney"] doubleValue]]],[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"salemoney"] doubleValue]]],[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"money"] doubleValue]]]];
        NSArray * name = @[@"实际还款",@"实际消费",@"手续费"];
        NSMutableArray * labArr = [NSMutableArray new];
        for (int i =0 ; i<name.count; i++) {
            UILabel * lab = [UILabel labelWithTitle:name[i] color:[UIColor whiteColor] fontSize:14];
            [footer addSubview:lab];
            [labArr addObject:lab];
        }
        UILabel * lastLab = nil;
        for (int i =0 ; i<name.count; i++) {
            UILabel * lab = labArr[i];
            lab.sd_layout.leftSpaceToView(lastLab == nil?footer:lastLab, 0).topSpaceToView(footer, 10).heightIs(16).widthIs(IPHONE_WIDTH/3);
            UILabel * valuewLab = [UILabel labelWithTitle:valueArr[i] color:[UIColor whiteColor] fontSize:14];
            [footer addSubview:valuewLab];
            valuewLab.sd_layout.centerXEqualToView(lab).widthIs(IPHONE_WIDTH/3).topSpaceToView(lab, 10).heightIs(16);
            if (i<name.count-1) {
                UIView * line = [UIView new];
                line.backgroundColor = [UIColor whiteColor];
                [footer addSubview:line];
                line.sd_layout.leftSpaceToView(lab, 0).topSpaceToView(footer, 15).bottomSpaceToView(footer, 15).widthIs(1);
            }
            lastLab = lab;
        }
        UIButton * wenhaoBtn = [UIButton new];
        [wenhaoBtn addTarget:self action:@selector(wenhaoBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [wenhaoBtn setImage:Image(@"问号") forState:0];
        [footer addSubview:wenhaoBtn];
        wenhaoBtn.sd_layout.rightSpaceToView(footer, 5).topSpaceToView(footer, 5).widthIs(30).heightEqualToWidth();
    }
}
-(void)createZeroSectionHeadView:(UIView*)superView{
    UIView * bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    [superView addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(superView, 10).rightSpaceToView(superView, 10).topSpaceToView(superView, 5);
    UILabel * tmpPlanOrderLab = [UILabel labelWithTitle:@"计划单号：" color:[UIColor colorWithHue:0 saturation:0 brightness:0.53 alpha:1] fontSize:14];
    [bgView addSubview:tmpPlanOrderLab];
    tmpPlanOrderLab.sd_layout.leftSpaceToView(bgView, 5).topSpaceToView(bgView, 8).heightIs(14);
    [tmpPlanOrderLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planOrderValue = [UILabel labelWithTitle:self.planID color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planOrderValue];
    planOrderValue.sd_layout.leftSpaceToView(tmpPlanOrderLab, 0).centerYEqualToView(tmpPlanOrderLab).heightIs(14);
    [planOrderValue setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
    
    UILabel * planSettingLab = [UILabel labelWithTitle:@"计划配置：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planSettingLab];
    planSettingLab.sd_layout.leftEqualToView(tmpPlanOrderLab).topSpaceToView(tmpPlanOrderLab, 10).heightIs(14);
    [planSettingLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UILabel*label2_day=[UILabel new];
    label2_day.text=[NSString stringWithFormat:@"%@天%ld笔",self.planInfoDic[@"repayDays"],[self.planInfoDic[@"repayTimes"] integerValue]];
    label2_day.textColor=ColorByHexStr(@"#999999");
    label2_day.font=[UIFont systemFontOfSize:13];
    label2_day.textAlignment=NSTextAlignmentLeft;
    [bgView addSubview:label2_day];
    label2_day.sd_layout.leftSpaceToView(planSettingLab, 0).centerYEqualToView(planSettingLab).heightIs(15);
    [label2_day setSingleLineAutoResizeWithMaxWidth:200];
    
    UILabel * tmp1 = [UILabel new];
    tmp1.text = @"还款：";
    tmp1.font=[UIFont systemFontOfSize:13];
    tmp1.textColor = ColorByHexStr(@"#999999");
    [bgView addSubview:tmp1];
    tmp1.sd_layout.centerYEqualToView(label2_day).centerXEqualToView(bgView).heightIs(15);
    [tmp1 setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp1Value = [UILabel new];
    tmp1Value.textColor = [UIColor redColor];
    tmp1Value.font=[UIFont systemFontOfSize:14];
    tmp1Value.text = self.isFromRepayList==YES? [NSString stringWithFormat:@"%ld",[_backDic[@"mybacknum"] integerValue]]:[NSString stringWithFormat:@"%ld",[self.planInfoDic[@"useBackNum"] integerValue]];
    [bgView addSubview:tmp1Value];
    tmp1Value.sd_layout.centerYEqualToView(tmp1).leftSpaceToView(tmp1, 0).heightIs(15);
    [tmp1Value setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp1Value2 = [UILabel new];
    tmp1Value2.font=[UIFont systemFontOfSize:14];
    tmp1Value2.textColor = ColorByHexStr(@"#999999");
    tmp1Value2.text = self.isFromRepayList==YES? [NSString stringWithFormat:@"-%ld",[_backDic[@"backnum"] integerValue]]:[NSString stringWithFormat:@"-%ld",[self.planInfoDic[@"totalRepayCount"] integerValue]];
    [bgView addSubview:tmp1Value2];
    tmp1Value2.sd_layout.centerYEqualToView(tmp1Value).leftSpaceToView(tmp1Value, 0).heightIs(15);
    [tmp1Value2 setSingleLineAutoResizeWithMaxWidth:100];
    
    UILabel * tmp2 = [UILabel new];
    tmp2.text = @"消费：";
    tmp2.font=[UIFont systemFontOfSize:14];
    tmp2.textColor =ColorByHexStr(@"#999999");
    [bgView addSubview:tmp2];
    tmp2.sd_layout.leftSpaceToView(tmp1Value2, AUTO(30)).centerYEqualToView(tmp1Value2).heightIs(15);
    [tmp2 setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp2Value = [UILabel new];
    tmp2Value.textColor = [UIColor redColor];
    tmp2Value.font = [UIFont systemFontOfSize:14];
    tmp2Value.text =self.isFromRepayList==YES?[NSString stringWithFormat:@"%ld",[_backDic[@"mysalenum"] integerValue]] :[NSString stringWithFormat:@"%ld",[self.planInfoDic[@"useSaleNum"] integerValue]];
    [bgView addSubview:tmp2Value];
    tmp2Value.sd_layout.leftSpaceToView(tmp2, 0).centerYEqualToView(tmp2).heightIs(15);
    [tmp2Value setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * tmp2Value2 = [UILabel new];
    tmp2Value2.font = [UIFont systemFontOfSize:13];
    tmp2Value2.textColor =ColorByHexStr(@"#999999");
    tmp2Value2.text =self.isFromRepayList==YES?[NSString stringWithFormat:@"-%ld",[_backDic[@"salenum"] integerValue]]: [NSString stringWithFormat:@"-%ld",[self.planInfoDic[@"repayTimes"] integerValue]];
    [bgView addSubview:tmp2Value2];
    tmp2Value2.sd_layout.centerYEqualToView(tmp2Value).leftSpaceToView(tmp2Value, 0).heightIs(15);
    [tmp2Value2 setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * tmpPlanTotalLab = [UILabel labelWithTitle:@"计划总额：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpPlanTotalLab];
    tmpPlanTotalLab.sd_layout.leftEqualToView(planSettingLab).topSpaceToView(planSettingLab, 5).heightIs(18);
    [tmpPlanTotalLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planTotalValue = [UILabel labelWithTitle:[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_planInfoDic[@"repayMoney"] doubleValue]]] color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planTotalValue];
    planTotalValue.sd_layout.leftEqualToView(tmpPlanTotalLab).topSpaceToView(tmpPlanTotalLab, 5).heightIs(18);
    [planTotalValue setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline = [UIView new];
    vline.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline];
    vline.sd_layout.centerXIs((IPHONE_WIDTH-20)/3).topEqualToView(tmpPlanTotalLab).bottomEqualToView(planTotalValue).widthIs(1);
    
    UILabel * tmpPlanMoneyLab = [UILabel labelWithTitle:@"计划定金：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpPlanMoneyLab];
    tmpPlanMoneyLab.sd_layout.leftSpaceToView(vline, 5).centerYEqualToView(tmpPlanTotalLab).heightIs(18);
    [tmpPlanMoneyLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planMoneyValue = [UILabel labelWithTitle:[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_planInfoDic[@"firstMoney"] doubleValue]]] color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planMoneyValue];
    planMoneyValue.sd_layout.leftEqualToView(tmpPlanMoneyLab).centerYEqualToView(planTotalValue).heightIs(18);
    [planMoneyValue setSingleLineAutoResizeWithMaxWidth:200];
    UIView * vline2 = [UIView new];
    vline2.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline2];
    vline2.sd_layout.centerXIs((IPHONE_WIDTH-20)*2/3).topEqualToView(tmpPlanTotalLab).bottomEqualToView(planTotalValue).widthIs(1);
    
    UILabel * tmpFeeLab = [UILabel labelWithTitle:@"计划手续费：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpFeeLab];
    tmpFeeLab.sd_layout.leftSpaceToView(vline2, 5).centerYEqualToView(tmpPlanMoneyLab).heightIs(18);
    [tmpFeeLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * feeValue = [UILabel labelWithTitle:[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_planInfoDic[@"sumMoney"] doubleValue]]] color:ThisPageTextColor fontSize:14];
    [bgView addSubview:feeValue];
    feeValue.sd_layout.leftEqualToView(tmpFeeLab).centerYEqualToView(planMoneyValue).heightIs(18);
    [feeValue setSingleLineAutoResizeWithMaxWidth:200];
    [bgView setupAutoHeightWithBottomView:feeValue bottomMargin:10];
}
-(void)createOneSectionHeadView:(UIView*)superView{
    UIView * bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    [superView addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(superView, 10).rightSpaceToView(superView, 10).topEqualToView(superView);
    
    UILabel * tmpAlreadyPayLab = [UILabel labelWithTitle:@"已还款：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpAlreadyPayLab];
    tmpAlreadyPayLab.sd_layout.leftSpaceToView(bgView, 5).topSpaceToView(bgView, 10).heightIs(16);
    [tmpAlreadyPayLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * alreadyPayValue = [UILabel labelWithTitle:@"0.00" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:alreadyPayValue];
    alreadyPayValue.sd_layout.leftEqualToView(tmpAlreadyPayLab).topSpaceToView(tmpAlreadyPayLab, 5).heightIs(16);
    [alreadyPayValue setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline = [UIView new];
    vline.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline];
    vline.sd_layout.centerXIs((IPHONE_WIDTH-20)/3).topEqualToView(tmpAlreadyPayLab).bottomEqualToView(alreadyPayValue).widthIs(1);
    
    UILabel * hasBuyLab = [UILabel labelWithTitle:@"已消费：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:hasBuyLab];
    hasBuyLab.sd_layout.centerYEqualToView(tmpAlreadyPayLab).leftSpaceToView(vline, 5).heightIs(16);
    [hasBuyLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * hasBuyValue = [UILabel labelWithTitle:@"0.00" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:hasBuyValue];
    hasBuyValue.sd_layout.leftEqualToView(hasBuyLab).centerYEqualToView(alreadyPayValue).heightIs(16);
    [hasBuyValue setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline2 = [UIView new];
    vline2.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline2];
    vline2.sd_layout.centerXIs((IPHONE_WIDTH-20)*2/3).topEqualToView(tmpAlreadyPayLab).bottomEqualToView(alreadyPayValue).widthIs(1);
    
    UILabel * hasFeeLab = [UILabel labelWithTitle:@"已扣手续费：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:hasFeeLab];
    hasFeeLab.sd_layout.leftSpaceToView(vline2, 5).centerYEqualToView(hasBuyLab).heightIs(16);
    [hasFeeLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UILabel * hasFeeValue = [UILabel labelWithTitle:@"0.00" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:hasFeeValue];
    hasFeeValue.sd_layout.leftEqualToView(hasFeeLab).centerYEqualToView(hasBuyValue).heightIs(16);
    [hasFeeValue setSingleLineAutoResizeWithMaxWidth:200];
    [bgView setupAutoHeightWithBottomView:hasFeeValue bottomMargin:10];
    if (_backDic!=nil) {
        alreadyPayValue.text = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_backDic[@"mybackmoney"]doubleValue]]];
        hasBuyValue.text = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_backDic[@"mysalemoney"]doubleValue]]];
        hasFeeValue.text = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_backDic[@"myfeemoney"]doubleValue]]];
    }
}
-(NSMutableArray*)dealWithDataIntoArray:(NSArray*)array{
    NSMutableArray * arr = nil;
    NSMutableArray * dataArray = [NSMutableArray new];
    for (int i=0;i<array.count;i++) {
        NSDictionary * dic = array[i];
        NSString * dataDate = [dic[@"theTime"] substringWithRange:NSMakeRange(5, 5)];
        if (i ==0) {
            arr = [NSMutableArray new];
            _tmpDate = dataDate;
            [arr addObject:dic];
        }else{
            if ([_tmpDate isEqualToString:dataDate]) {
                [arr addObject:dic];
            }else{
                [dataArray addObject:arr];
                _tmpDate = dataDate;
                arr = [NSMutableArray new];
                [arr addObject:dic];
            }
        }
        if (i == array.count - 1) {
            [dataArray addObject:arr];
        }
    }
    return dataArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

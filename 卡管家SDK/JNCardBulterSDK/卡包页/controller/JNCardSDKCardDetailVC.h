//
//  JNCardSDKCardDetailVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"
typedef void (^JNCardSDKNotifyReloadTabView)(void);
@interface JNCardSDKCardDetailVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * userCreditCardId;
@property(nonatomic,retain)JNCardSDKCardCellModel * cardInfoModel;
@property(nonatomic,copy)JNCardSDKNotifyReloadTabView notifyJNCardSDKTabBlock;
@end

//
//  JNCardSDKResetPauseVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/12/5.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"
@interface JNCardSDKResetPauseVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * planID;
@property(nonatomic,retain)JNCardSDKCardCellModel * model;
@property(nonatomic,copy)NSString * pausePlanStr;
@end
/*
 UILabel * beginLab = [UILabel labelWithTitle:@"开始时间：" color:[UIColor blackColor] fontSize:14];
 [bgView addSubview:beginLab];
 beginLab.sd_layout.leftEqualToView(setTitleLab).topSpaceToView(setTitleLab, 20).heightIs(16);
 [beginLab setSingleLineAutoResizeWithMaxWidth:200];
 
 NSDate * beijingTime = [JNCardSDKMytools getCurrentDate];
 beijingTime = [beijingTime dateByAddingTimeInterval:20*60];
 _startLabValue = [UILabel labelWithTitle:[JNCardSDKMytools dateToDateStr:beijingTime] color:[UIColor lightGrayColor] fontSize:14];
 _startLabValue.userInteractionEnabled = YES;
 [bgView addSubview:_startLabValue];
 _startLabValue.sd_layout.centerYEqualToView(beginLab).leftSpaceToView(beginLab, 10).heightIs(20).widthIs(250);
 UITapGestureRecognizer * startTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(startTapClick)];
 [_startLabValue addGestureRecognizer:startTap];
 
 
 UILabel * endLab = [UILabel labelWithTitle:@"结束时间：" color:[UIColor blackColor] fontSize:14];
 [bgView addSubview:endLab];
 endLab.sd_layout.leftEqualToView(beginLab).topSpaceToView(beginLab, 30).heightIs(16);
 [endLab setSingleLineAutoResizeWithMaxWidth:200];
 NSString * endTmpStr = self.model.DueDate;
 if (![self.model.DueDate containsString:@":"]) {
 endTmpStr = [NSString stringWithFormat:@"%@ 00:00:00",self.model.DueDate];
 }
 NSDate * datep = [JNCardSDKMytools dateStrToDate:endTmpStr];
 datep = [datep dateByAddingHours:-6];
 _endLabValue = [UILabel labelWithTitle:[JNCardSDKMytools dateToDateStr:datep] color:[UIColor lightGrayColor] fontSize:14];
 _endLabValue.userInteractionEnabled = YES;
 [bgView addSubview:_endLabValue];
 _endLabValue.sd_layout.leftEqualToView(_startLabValue).centerYEqualToView(endLab).heightIs(20).widthIs(250);
 UITapGestureRecognizer * endTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endTapClick)];
 [_endLabValue addGestureRecognizer:endTap];
 
 [bgView setupAutoHeightWithBottomView:endLab bottomMargin:30];
 
 
 */

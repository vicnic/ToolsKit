//
//  MyWebView.h
//  7UGame
//
//  Created by 111 on 2017/2/21.
//  Copyright © 2017年 111. All rights reserved.
//


#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"
typedef void (^JNCardSDKWebNotifyPaySuccessBlock)(void);

@interface JNCardSDKMyWebView : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * currentWebUrl;
@property(nonatomic,copy)NSString * htmlStr;
@property(nonatomic,assign)NSInteger webType;
//进度条 标志
@property NSInteger progressFlag;
@property BOOL speedFlag;
@property (nonatomic, strong)UIProgressView *progressBar;

@property(nonatomic,copy)JNCardSDKWebNotifyPaySuccessBlock successBlock;
@property(nonatomic,assign)BOOL isHidden;
@property(nonatomic,copy)NSString * webTitle;

@property(nonatomic,assign)BOOL isGoToSMSVerify;
@property(nonatomic,retain)JNCardSDKCardCellModel * cardModel;

@property(nonatomic,assign)BOOL isFromFivMakePlan;

@end


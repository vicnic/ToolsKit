//
//  JNCardSDKCardDetailVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKCardDetailVC.h"
#import "JNCardSDKCardDetailCell.h"
#import "JNCardSDKNewRepayPlanVC.h"
#import "JNCardSDKRepayPlanVC.h" 
#import "JNCardSDKRepayPlanCellModel.h"
#import "JNCardSDKAddBankCellModel.h"
#import "SWAlertController.h"
#import "JXButton.h"
#import "JNCardSDKResetPauseVC.h"
#import "HDAlertView.h"
#import "JNCardSDKFVRepayPlanDetailVC.h"
#import "JNCardSDKChannelModel.h"
#import "JNCardSDKAddNewCardVC.h"
#import "JNCardSDKNewFivMakeRepayVC.h"
#import "JNCardSDKTradeChannelVC.h"
#define kRequestTime 3.0f
#define kDelay 1.0f

@interface JNCardSDKCardDetailVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * _sectionArr;
    NSMutableArray * _cardDataArr, * _payWayArr,*_repayPlanArr;
    NSMutableDictionary * _cardInfoDetailDic;
    NSInteger _page;
    NSString * _shouldPayMoney/*计划重启的弹窗里的本金*/,*_planTime,*_billDayStr,*_payDayStr;
    BOOL _isHasDataInPausePlan/*点击计划撤销和计划重启要判断暂停的那个点是否是还款类型，表达在代码上就是后台是否返回内容，如果返回没有数据就要弹窗，有数据就要在点击事件里比对时间*/,_isEmpty/*判断是否是有计划*/;
    UIView*topView;
}
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)UIView * myFooterView;
@property (nonatomic,strong) UIView *shelterView; 
@end

@implementation JNCardSDKCardDetailVC
- (void)viewDidLoad {
    [super viewDidLoad];
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(middleNotify) name:@"reloadcardplandetail" object:nil];
    self.view.backgroundColor = [UIColor colorWithHue:0.67 saturation:0.02 brightness:0.96 alpha:1];
    _page = 1;
    _sectionArr = [NSMutableArray new];
    _payWayArr = [NSMutableArray new];
    _repayPlanArr = [NSMutableArray new];
    
//    [self requestPayWayAll];
    [JNCardSDKReuserFile getPlanBeforeDay];
    [self.view addSubview:self.myTab];
    [self.view addSubview:self.myFooterView];
}
-(void)viewWillAppear:(BOOL)animated{
    [self middleNotify];
}
#pragma mark 请求区
-(void)middleNotify{
    [_repayPlanArr removeAllObjects];
    [JNCardSDKReuserFile reloadCardInfo:^(NSDictionary *backDic) {
        NSArray * dataArr = backDic[@"data"];
        NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKCardCellModel mj_objectArrayWithKeyValuesArray:dataArr]];
        for (int i =0 ;i<dataArr.count;i++) {
            NSDictionary * dicp = dataArr[i];
            if ([self.userCreditCardId isEqualToString:dicp[@"userCreditCardId"]]) {
                self.cardInfoModel = arrM[i];
            }
        }
        [self getUserCardPlanListRequest];
    }];
}
//获取信用卡计划列表
-(void)getUserCardPlanListRequest{
    [SVProgressHUD show];
    //请求数据
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:self.cardInfoModel.userCreditCardId forKey:@"UserCreditCardId"];
    [p setObject:[NSString stringWithFormat:@"%ld",_page] forKey:@"currentpage"];
    [p setObject:@"10" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetCardPlanStateList];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self->_myTab.mj_header endRefreshing];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSArray * dataArr = responseObject[@"data"];
            if (dataArr.count!=0) {
                self->_isEmpty = NO;//表示有数据
                self->_cardInfoDetailDic = responseObject[@"data"][0];
                NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKCardCellModel mj_objectArrayWithKeyValuesArray:(NSArray*)responseObject[@"data"]]];
                [self->_repayPlanArr addObjectsFromArray:arrM];
                [self getPausePlanInfo];
            }else{
                self->_isEmpty = YES;
            }
            [self->_myTab reloadData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
}
//取消计划
-(void)cancelPlanRequest:(NSString *)planId{
    [SVProgressHUD show];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_CancelCardState];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    [paraDic setObject:planId forKey:@"PlanId"];
    [paraDic setObject:@"5" forKey:@"TheState"];
    [PGNetworkHelper POST:url parameters:paraDic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [JNCardSDKMytools warnText:@"取消计划成功" status:JNCardSDKSuccess];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)requestPayWayAll{//这个去掉，Java不要
    [JNCardSDKReuserFile getAllPayWay:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            self->_payWayArr = backDic[@"data"];
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    }];
}
//撤销计划
-(void)applyCancelPausePlan{
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:self.cardInfoModel.userRepayPlayId forKey:@"PlanId"];
    [p setObject:@"1" forKey:@"Cancel"];
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_ApplyCancelPlan];
    [SVProgressHUD show];
    [PGNetworkHelper POST:urlStr parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            [JNCardSDKMytools warnText:@"你的计划已成功撤销" status:JNCardSDKSuccess];
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark 懒加载
-(UITableView *)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-AUTO(50)-KTabbarSafeBottomMargin) style:UITableViewStylePlain];
        _myTab.backgroundColor=[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        _myTab.delegate = self;
        _myTab.dataSource =self;
        _myTab.showsVerticalScrollIndicator = NO;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self->_cardDataArr removeAllObjects];
            [self middleNotify];
        }];
    }
    return _myTab;
}

#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    //    return _sectionArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = nil;
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //    return AUTO(150);
    if ([_cardInfoDetailDic[@"theState"] integerValue] ==1) {
        return 250;
    }else{
        return 270;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGFloat heightKop = 0;
    if ([_cardInfoDetailDic[@"theState"] integerValue] ==1) {
        heightKop = 230;
    }else{
        heightKop = 250;
    }
    UIView * headView = [UIView new];
    headView.backgroundColor = [UIColor whiteColor];
    UIView *bgView = [[UIView alloc]initWithFrame:Frame(15, 10, IPHONE_WIDTH-30, heightKop)];
    //        bgView.backgroundColor = [UIColor redColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    bgView.layer.borderColor=[[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1]CGColor];
    bgView.layer.borderWidth = 1;
    [headView addSubview:bgView];
    
    topView=[UIView new];
    [[SDWebImageManager sharedManager]loadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,self.cardInfoModel.bankImg]] options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        UIColor * c = [JNCardSDKMytools mostColor:image];
        self->topView.backgroundColor=c;
        self->topView.layer.cornerRadius = 1;
    }];
    [bgView addSubview:topView];
    topView.sd_layout.topEqualToView(bgView).leftEqualToView(bgView).rightEqualToView(bgView).heightIs(50);
    
    UIView * bankImg=[UIView new];
    bankImg.backgroundColor=[UIColor whiteColor];
    bankImg.layer.cornerRadius = 15;
    bankImg.layer.masksToBounds = YES;
    [topView addSubview:bankImg];
    bankImg.sd_layout.leftSpaceToView(topView, AUTO(10)).centerYEqualToView(topView).widthIs(30).heightEqualToWidth();
    
    UIImageView * bankImgView= [UIImageView new];
    [bankImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,self.cardInfoModel.bankImg]]];
    [bankImg addSubview:bankImgView];
    bankImgView.sd_layout.topSpaceToView(bankImg, 2).leftSpaceToView(bankImg, 2).bottomSpaceToView(bankImg, 2).rightSpaceToView(bankImg, 2);
    
    UILabel*bankName = [UILabel new];
    bankName.font = [UIFont systemFontOfSize:13];
    bankName.text = self.cardInfoModel.bankName;
    bankName.textColor=[UIColor whiteColor];
    bankName.textAlignment=NSTextAlignmentLeft;
    [topView addSubview:bankName];
    bankName.sd_layout.leftSpaceToView(bankImg, AUTO(15)).centerYEqualToView(topView).widthIs(110).heightIs(20);
    //    [_bankName setSingleLineAutoResizeWithMaxWidth:200];
    
    
    UIView*line=[UIView new];
    line.backgroundColor=[UIColor whiteColor];
    [topView addSubview:line];
    line.sd_layout.topSpaceToView(topView, 12).bottomSpaceToView(topView, 12).widthIs(1).leftSpaceToView(bankName,0).centerYEqualToView(topView);
    
    UILabel*cardNameLabel=[UILabel new];
    cardNameLabel.text=@"尾号:";
    [topView addSubview:cardNameLabel];
    cardNameLabel.textColor=[UIColor whiteColor];
    cardNameLabel.font=[UIFont systemFontOfSize:13];
    cardNameLabel.textAlignment=NSTextAlignmentLeft;
    cardNameLabel.sd_layout.widthIs(50).centerYEqualToView(topView).leftSpaceToView(line, 10).autoHeightRatio(0);
    [cardNameLabel setSingleLineAutoResizeWithMaxWidth:50];
    
    UILabel*cardNumLab = [UILabel new];
    cardNumLab.font = [UIFont systemFontOfSize:13];
    cardNumLab.textAlignment=NSTextAlignmentLeft;
    cardNumLab.textColor=[UIColor whiteColor];
    [topView addSubview:cardNumLab];
    cardNumLab.sd_layout.leftSpaceToView(cardNameLabel, AUTO(0)).widthIs(40).centerYEqualToView(topView).heightIs(20);
    
    UILabel*userNameLab = [UILabel new];
    userNameLab.font = [UIFont systemFontOfSize:13];
    userNameLab.textColor=[UIColor whiteColor];
    [topView addSubview:userNameLab];
    userNameLab.sd_layout.rightSpaceToView(topView,2).centerYEqualToView(topView).widthIs(40).autoHeightRatio(0);
    [userNameLab setSingleLineAutoResizeWithMaxWidth:AUTO(IPHONE_WIDTH/2)];
    //    [_userNameLab setSingleLineAutoResizeWithMaxWidth:AUTO(IPHONE_WIDTH/2)];
    
    userNameLab.text = [self.cardInfoModel.name stringByReplacingCharactersInRange:NSMakeRange(1, self.cardInfoModel.name.length>3?self.cardInfoModel.name.length-2:1) withString:@"*"];
    cardNumLab.text=[self.cardInfoModel.bankNum substringFromIndex:self.cardInfoModel.bankNum.length-4 ];
    
    UIView*MiddleView=[UIView new];
    //        MiddleView.backgroundColor=[UIColor redColor];
    [bgView addSubview:MiddleView];
    MiddleView.sd_layout.topSpaceToView(topView,1).leftEqualToView(bgView).rightEqualToView(bgView).heightIs(70);
    
    UIView*leftView=[UIView new];
    //        leftView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:leftView];
    leftView.sd_layout.topEqualToView(MiddleView).bottomEqualToView(MiddleView);
    
    UILabel*label_plan_money=[UILabel new];
    label_plan_money.text=@"计划还款";
    label_plan_money.font=[UIFont systemFontOfSize:13];
    label_plan_money.textColor=ColorByHexStr(@"#333333");
    label_plan_money.textAlignment=NSTextAlignmentCenter;
    [leftView addSubview:label_plan_money];
    label_plan_money.sd_layout.topSpaceToView(leftView, 10).centerXEqualToView(leftView).widthIs(70).autoHeightRatio(0);
    UILabel*label_plan_num=[UILabel new];
    if (_repayPlanArr.count ==0) {
        label_plan_num.text=@"0.00";
    }else{
        label_plan_num.text=[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_cardInfoDetailDic[@"repayMoney"] doubleValue]]];
    }
    label_plan_num.textColor=ColorByHexStr(@"#999999");
    label_plan_num.font=[UIFont systemFontOfSize:13];
    label_plan_num.textAlignment=NSTextAlignmentCenter;
    [leftView addSubview:label_plan_num];
    label_plan_num.sd_layout.topSpaceToView(label_plan_money, 10).widthIs(80).autoHeightRatio(0).centerXEqualToView(leftView);
    
    UIView*firstLine=[UIView new];
    firstLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    [leftView addSubview:firstLine];
    
    
    UIView*centerView=[UIView new];
    //        centerView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:centerView];
    centerView.sd_layout.topEqualToView(MiddleView).bottomEqualToView(MiddleView).leftSpaceToView(leftView, 0);
    //获取正确的账单日还款日
    NSMutableDictionary * correctBillPayDic = [JNCardSDKCalculaterBillPayDay getBillDateAndPayDate:self.cardInfoModel.billDate.length==0?@"1970-00-00 00:00:00":self.cardInfoModel.billDate andPayDay:self.cardInfoModel.dueDate.length==0?@"1970-00-00 00:00:00":self.cardInfoModel.dueDate];
    
    UILabel*label_ZDday=[UILabel new];
    label_ZDday.text=@"账单日";
    label_ZDday.font=[UIFont systemFontOfSize:13];
    label_ZDday.textAlignment=NSTextAlignmentCenter;
    label_ZDday.textColor=ColorByHexStr(@"#333333");
    [centerView addSubview:label_ZDday];
    label_ZDday.sd_layout.centerXEqualToView(centerView).centerYEqualToView(label_plan_money).widthIs(70).autoHeightRatio(0);
    UILabel*label_ZDday_num=[UILabel new];
    label_ZDday_num.textAlignment=NSTextAlignmentCenter;
    
    _billDayStr = correctBillPayDic[@"finalBillDay"];
    _payDayStr = correctBillPayDic[@"finalPayDay"];
    label_ZDday_num.text=[NSString stringWithFormat:@"%@月%@号",[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[correctBillPayDic[@"finalBillDay"] substringWithRange:NSMakeRange(5, 2)]],[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[correctBillPayDic[@"finalBillDay"] substringWithRange:NSMakeRange(8, 2)]]];
    label_ZDday_num.font=[UIFont systemFontOfSize:13];
    label_ZDday_num.textColor=ColorByHexStr(@"#999999");
    [centerView addSubview:label_ZDday_num];
    label_ZDday_num.sd_layout.centerYEqualToView(label_plan_num).widthIs(80).autoHeightRatio(0).centerXEqualToView(label_ZDday);
    
    UIView*secondLine=[UIView new];
    secondLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    [centerView addSubview:secondLine];
    secondLine.sd_layout.rightEqualToView(centerView).widthIs(1).topSpaceToView(centerView,15).bottomSpaceToView(centerView,15);
    firstLine.sd_layout.rightEqualToView(leftView).widthIs(1).topSpaceToView(leftView, 15).bottomSpaceToView(leftView, 15);
    
    
    UIView*rightView=[UIView new];
    //        rightView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:rightView];
    rightView.sd_layout.topEqualToView(MiddleView).leftSpaceToView(centerView, 0).bottomEqualToView(MiddleView);
    
    UILabel*label_repayDay=[UILabel new];
    label_repayDay.text=@"还款日";
    label_repayDay.textColor=ColorByHexStr(@"#333333");
    label_repayDay.font=[UIFont systemFontOfSize:13];
    label_repayDay.textAlignment=NSTextAlignmentCenter;
    [rightView addSubview:label_repayDay];
    label_repayDay.sd_layout.centerXEqualToView(rightView).centerYEqualToView(label_plan_money).widthIs(70).autoHeightRatio(0);
    UILabel*label_repayDay_num=[UILabel new];
    label_repayDay_num.textAlignment=NSTextAlignmentCenter;
    label_repayDay_num.text=[NSString stringWithFormat:@"%@月%@号",[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[correctBillPayDic[@"finalPayDay"] substringWithRange:NSMakeRange(5, 2)]],[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[correctBillPayDic[@"finalPayDay"] substringWithRange:NSMakeRange(8, 2)]]];
    label_repayDay_num.font=[UIFont systemFontOfSize:13];
    label_repayDay_num.textColor=ColorByHexStr(@"#999999");
    [rightView addSubview:label_repayDay_num];
    label_repayDay_num.sd_layout.centerYEqualToView(label_plan_num).widthIs(80).autoHeightRatio(0).centerXEqualToView(label_repayDay);
    
    MiddleView.sd_equalWidthSubviews=@[leftView,centerView,rightView];
    
    UIView*MiddlebottomLine=[UIView new];
    MiddlebottomLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    [MiddleView addSubview:MiddlebottomLine];
    MiddlebottomLine.sd_layout.heightIs(1).leftEqualToView(MiddleView).rightEqualToView(MiddleView).bottomEqualToView(MiddleView);
    
    UIView*footerView=[UIView new];
    //        footerView.backgroundColor=[UIColor redColor];
    [bgView addSubview:footerView];
    footerView.sd_layout.topSpaceToView(MiddleView,0).bottomEqualToView(bgView).leftEqualToView(bgView).rightEqualToView(bgView);
    if(_cardInfoDetailDic.count == 0||_repayPlanArr.count==0){
        UIImageView*nodataImg=[UIImageView new];
        nodataImg.image=[UIImage imageNamed:@"暂无数据_shanghuruwang"];
        [footerView addSubview:nodataImg];
        nodataImg.sd_layout.leftSpaceToView(footerView, 28).centerYEqualToView(footerView).widthIs(80).heightIs(80);
        
        
        UILabel*tips_header=[UILabel new];
        tips_header.text=@"暂无数据";
        tips_header.textColor=ColorByHexStr(@"#333333");
        tips_header.font=[UIFont systemFontOfSize:13];
        [footerView addSubview:tips_header];
        tips_header.sd_layout.centerYEqualToView(nodataImg).offset(-13).widthIs(80).autoHeightRatio(0).leftSpaceToView(nodataImg, 5);
        
        UILabel*tips_footer=[UILabel new];
        tips_footer.text=@"你还没有创建还款计划";
        tips_footer.font=[UIFont systemFontOfSize:13];
        tips_footer.textColor=ColorByHexStr(@"#999999");
        [footerView addSubview:tips_footer];
        tips_footer.sd_layout.centerYEqualToView(nodataImg).offset(13).widthIs(150).autoHeightRatio(0).leftEqualToView(tips_header);
    }else {
        UILabel*label1=[UILabel new];
        label1.text=_cardInfoDetailDic[@"createTime"];
        label1.textColor=ColorByHexStr(@"#333333");
        label1.font=[UIFont systemFontOfSize:13];
        [footerView addSubview:label1];
        label1.sd_layout.leftEqualToView(label_plan_money).autoHeightRatio(0).topSpaceToView(footerView, 10);
        [label1 setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
        
        UILabel*status=[UILabel new];
        NSString * stateStrp = [NSString stringWithFormat:@"%ld",[_cardInfoDetailDic[@"theState"] integerValue]];
        status.text=[JNCardSDKReuserFile returnState:stateStrp];
        status.font=[UIFont systemFontOfSize:13];
        status.textColor=ThemeColor;
        status.textAlignment=NSTextAlignmentRight;
        [footerView addSubview:status];
        status.sd_layout.centerYEqualToView(label1).rightSpaceToView(footerView, 10).widthIs(100).autoHeightRatio(0);
        
        UILabel*label2=[UILabel new];
        label2.textColor=[UIColor blackColor];
        label2.text=@"计划配置:";
        label2.textAlignment=NSTextAlignmentLeft;
        label2.textColor=ColorByHexStr(@"#999999");
        label2.font=[UIFont systemFontOfSize:13];
        [footerView addSubview:label2];
        label2.sd_layout.leftEqualToView(label1).topSpaceToView(label1, 10).widthIs(80).autoHeightRatio(0);
        [label2 setSingleLineAutoResizeWithMaxWidth:80];
        
        UILabel*label2_day=[UILabel new];
        label2_day.text=[NSString stringWithFormat:@"%ld天%ld笔",[_cardInfoDetailDic[@"repayDays"] integerValue],[_cardInfoDetailDic[@"repayTimes"] integerValue]];
        label2_day.textColor=ColorByHexStr(@"#999999");
        label2_day.font=[UIFont systemFontOfSize:13];
        label2_day.textAlignment=NSTextAlignmentLeft;
        [footerView addSubview:label2_day];
        label2_day.sd_layout.leftSpaceToView(label2, 1).centerYEqualToView(label2).widthIs(120).autoHeightRatio(0);
        [label2_day setSingleLineAutoResizeWithMaxWidth:150];
        if ([_cardInfoDetailDic[@"theState"] integerValue]!=0) {
            UILabel * tmp1 = [UILabel new];
            tmp1.text = @"还款：";
            tmp1.font=[UIFont systemFontOfSize:13];
            tmp1.textColor = ColorByHexStr(@"#999999");
            [footerView addSubview:tmp1];
            tmp1.sd_layout.centerYEqualToView(label2).centerXEqualToView(footerView).heightIs(AUTO(20));
            [tmp1 setSingleLineAutoResizeWithMaxWidth:100];
            UILabel * tmp1Value = [UILabel new];
            tmp1Value.textColor = [UIColor redColor];
            tmp1Value.font=[UIFont systemFontOfSize:13];
            tmp1Value.text = [NSString stringWithFormat:@"%ld",[_cardInfoDetailDic[@"useBackNum"] integerValue]];
            [footerView addSubview:tmp1Value];
            tmp1Value.sd_layout.centerYEqualToView(tmp1).leftSpaceToView(tmp1, 0).heightIs(AUTO(20));
            [tmp1Value setSingleLineAutoResizeWithMaxWidth:100];
            UILabel * tmp1Value2 = [UILabel new];
            tmp1Value2.font=[UIFont systemFontOfSize:13];
            tmp1Value2.textColor = ColorByHexStr(@"#999999");
            tmp1Value2.text = [NSString stringWithFormat:@"-%ld",[_cardInfoDetailDic[@"totalRepayCount"] integerValue]];
            [footerView addSubview:tmp1Value2];
            tmp1Value2.sd_layout.centerYEqualToView(tmp1Value).leftSpaceToView(tmp1Value, 0).heightIs(AUTO(20));
            [tmp1Value2 setSingleLineAutoResizeWithMaxWidth:100];
            
            UILabel * tmp2 = [UILabel new];
            tmp2.text = @"消费：";
            tmp2.font=[UIFont systemFontOfSize:13];
            tmp2.textColor =ColorByHexStr(@"#999999");
            [footerView addSubview:tmp2];
            tmp2.sd_layout.leftSpaceToView(tmp1Value2, AUTO(30)).centerYEqualToView(tmp1Value2).heightIs(AUTO(20));
            [tmp2 setSingleLineAutoResizeWithMaxWidth:100];
            UILabel * tmp2Value = [UILabel new];
            tmp2Value.textColor = [UIColor redColor];
            tmp2Value.font = [UIFont systemFontOfSize:13];
            tmp2Value.text = [NSString stringWithFormat:@"%ld",[_cardInfoDetailDic[@"useSaleNum"] integerValue]];
            [footerView addSubview:tmp2Value];
            tmp2Value.sd_layout.leftSpaceToView(tmp2, 0).centerYEqualToView(tmp2).heightIs(AUTO(20));
            [tmp2Value setSingleLineAutoResizeWithMaxWidth:200];
            UILabel * tmp2Value2 = [UILabel new];
            tmp2Value2.font = [UIFont systemFontOfSize:13];
            tmp2Value2.textColor =ColorByHexStr(@"#999999");
            tmp2Value2.text = [NSString stringWithFormat:@"-%ld",[_cardInfoDetailDic[@"repayTimes"] integerValue]];
            [footerView addSubview:tmp2Value2];
            tmp2Value2.sd_layout.centerYEqualToView(tmp2Value).leftSpaceToView(tmp2Value, 0).heightIs(AUTO(20));
            [tmp2Value2 setSingleLineAutoResizeWithMaxWidth:200];
        }
        
        UILabel * noticeLab = [UILabel new];
        noticeLab.textColor =ColorByHexStr(@"#999999");
        noticeLab.text = [NSString stringWithFormat:@"手续费：%.2f",[_cardInfoDetailDic[@"sumMoney"] floatValue]];
        noticeLab.font = [UIFont systemFontOfSize:13];
        [footerView addSubview:noticeLab];
        noticeLab.sd_layout.leftEqualToView(label2).topSpaceToView(label2, 10).heightIs(20);
        [noticeLab setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
        
        UIButton * viewDetailBtn = [UIButton new];
        [viewDetailBtn addTarget:self action:@selector(viewDetailBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:viewDetailBtn];
        viewDetailBtn.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
        if ([_cardInfoDetailDic[@"theState"] integerValue] ==1||[_cardInfoDetailDic[@"theState"] integerValue] ==3||[_cardInfoDetailDic[@"theState"] integerValue] ==4) {
            UIProgressView*oneProgressView = [[UIProgressView alloc]init];
            oneProgressView.frame=CGRectMake(30,90,IPHONE_WIDTH-100,40);
            oneProgressView.transform = CGAffineTransformMakeScale(1.0f, 6.0f);
            oneProgressView.backgroundColor= [UIColor clearColor];
            oneProgressView.progressViewStyle = UIProgressViewStyleDefault;
            oneProgressView.alpha=1.0;
            oneProgressView.layer.masksToBounds = YES;
            //                oneProgressView.layer.cornerRadius = 10;
            oneProgressView.progressTintColor= ThemeColor;
            oneProgressView.trackTintColor= [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
            oneProgressView.progress=0.0;
            CGFloat value;
            if([_cardInfoDetailDic[@"useBackNum"] isEqual:@"0"]||[_cardInfoDetailDic[@"repayTimes"] isEqual:@"0"]){
                value=0;
            }else{
                if(([_cardInfoDetailDic[@"repayTimes"] floatValue]+[_cardInfoDetailDic[@"repayDays"] floatValue])==0){
                    value=0;
                }else{
                    value=([_cardInfoDetailDic[@"useBackNum"] floatValue]+[_cardInfoDetailDic[@"useSaleNum"] floatValue])/([_cardInfoDetailDic[@"repayTimes"] floatValue]+[_cardInfoDetailDic[@"totalRepayCount"] floatValue]);
                }
            }
            
            [oneProgressView setProgress:value animated:NO];
            [footerView addSubview:oneProgressView];
            UILabel*processLabel=[UILabel new];
            processLabel.textAlignment=NSTextAlignmentRight;
            if(value!=0){
                processLabel.text=[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%0.lf%%",value]];
            }else{
                processLabel.text=@"0%";
            }
            
            processLabel.font=[UIFont systemFontOfSize:12];
            [footerView addSubview:processLabel];
            processLabel.sd_layout.rightSpaceToView(footerView, 12).autoHeightRatio(0).centerYEqualToView(oneProgressView);
            [processLabel setSingleLineAutoResizeWithMaxWidth:80];
        }else{
            UIView*buttonLine=[UIView new];
            buttonLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
            [footerView addSubview:buttonLine];
            buttonLine.sd_layout.topSpaceToView(noticeLab, 5).leftEqualToView(label1).rightEqualToView(status).heightIs(1);
            
            UIView*cancelBtn=[UIView new];
            //            cancelBtn.backgroundColor=[UIColor redColor];
            [footerView addSubview:cancelBtn];
            cancelBtn.sd_layout.rightSpaceToView(footerView, 0).topEqualToView(buttonLine).bottomEqualToView(footerView).widthIs((IPHONE_WIDTH-30)/3);
            
            UIImageView*cancelImg=[UIImageView new];
            cancelImg.image=[UIImage imageNamed:@"店员删除"];
            [cancelBtn addSubview:cancelImg];
            cancelImg.sd_layout.centerYEqualToView(cancelBtn).widthIs(20).heightEqualToWidth().leftSpaceToView(cancelBtn,IPHONE_WIDTH==320?20:AUTO(30));
            UILabel*cancelLabel=[UILabel new];
            cancelLabel.text=@"取消计划";
            cancelLabel.textColor=ColorByHexStr(@"#999999");
            cancelLabel.textAlignment=NSTextAlignmentLeft;
            cancelLabel.font=[UIFont systemFontOfSize:12];
            [cancelBtn addSubview:cancelLabel];
            cancelLabel.sd_layout.leftSpaceToView(cancelImg, 5).centerYEqualToView(cancelImg).widthIs(100).autoHeightRatio(0);
            [cancelLabel setSingleLineAutoResizeWithMaxWidth:150];
            UIButton * cancelRealBtn = [UIButton new];
            [cancelRealBtn addTarget:self action:@selector(cancelRealBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [cancelBtn addSubview:cancelRealBtn];
            cancelRealBtn.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
            
            
            UIView*jihuaBtn=[UIView new];
            //            jihuaBtn.backgroundColor=[UIColor redColor];
            [footerView addSubview:jihuaBtn];
            jihuaBtn.sd_layout.rightSpaceToView(cancelBtn, 0).topEqualToView(buttonLine).bottomEqualToView(footerView).widthIs((IPHONE_WIDTH-30)/3);
            UIImageView*jihuaImg=[UIImageView new];
            jihuaImg.image=[UIImage imageNamed:@"店员启用"];
            [jihuaBtn addSubview:jihuaImg];
            jihuaImg.sd_layout.centerYEqualToView(jihuaBtn).widthIs(20).heightEqualToWidth().leftSpaceToView(jihuaBtn,IPHONE_WIDTH==320?20:AUTO(30));
            UILabel*jihuaLabel=[UILabel new];
            jihuaLabel.text=@"启动计划";
            jihuaLabel.textColor=ColorByHexStr(@"#999999");
            jihuaLabel.textAlignment=NSTextAlignmentLeft;
            jihuaLabel.font=[UIFont systemFontOfSize:12];
            [jihuaBtn addSubview:jihuaLabel];
            jihuaLabel.sd_layout.leftSpaceToView(jihuaImg, 5).centerYEqualToView(jihuaImg).widthIs(100).autoHeightRatio(0);
            [jihuaLabel setSingleLineAutoResizeWithMaxWidth:150];
            UIButton * jihuaRealBtn = [UIButton new];
            [jihuaRealBtn addTarget:self action:@selector(jihuaRealBtnClick) forControlEvents:UIControlEventTouchUpInside];
            [jihuaBtn addSubview:jihuaRealBtn];
            jihuaRealBtn.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
            
            
            //            UIView*editBtn=[UIView new];
            //            //            editBtn.backgroundColor=[UIColor redColor];
            //            [footerView addSubview:editBtn];
            //            editBtn.sd_layout.rightSpaceToView(jihuaBtn, 0).topEqualToView(buttonLine).bottomEqualToView(footerView).widthIs((IPHONE_WIDTH-30)/3);
            //            UIImageView*editImg=[UIImageView new];
            //            editImg.image=[UIImage imageNamed:@"修改计划"];
            //            [editBtn addSubview:editImg];
            //            editImg.sd_layout.centerYEqualToView(editBtn).widthIs(20).heightEqualToWidth().leftSpaceToView(editBtn, IPHONE_WIDTH==320?20:AUTO(30));
            //            UILabel*editLabel=[UILabel new];
            //            editLabel.text=@"修改计划";
            //            editLabel.textColor=ColorByHexStr(@"#999999");
            //            editLabel.textAlignment=NSTextAlignmentLeft;
            //            editLabel.font=[UIFont systemFontOfSize:12];
            //            [editBtn addSubview:editLabel];
            //            editLabel.sd_layout.leftSpaceToView(editImg, 5).centerYEqualToView(editImg).widthIs(100).autoHeightRatio(0);
            //            [editLabel setSingleLineAutoResizeWithMaxWidth:150];
            //            UIButton * editRealBtn = [UIButton new];
            //            [editRealBtn addTarget:self action:@selector(editRealBtnClick) forControlEvents:UIControlEventTouchUpInside];
            //            [editBtn addSubview:editRealBtn];
            //            editRealBtn.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
        }
        
    }
    return headView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //    return AUTO(50);
    return IPHONE_WIDTH/2+20+160;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 0.01;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView*footer=[[UIView alloc]init];
        footer.backgroundColor=[UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1];
    
    UIView*footerView=[[UIView alloc]init];
    footerView.backgroundColor=[UIColor whiteColor];
    [footer addSubview:footerView];
    footerView.sd_layout.topSpaceToView(footer, 10).bottomSpaceToView(footer, 150).leftEqualToView(footer).rightEqualToView(footer);
    
    NSArray * titleArr = @[@"查看明细",@"重启计划",@"计划撤销"];
    NSMutableArray * topThrArr = [NSMutableArray new];
    for (int i =0; i<titleArr.count; i++) {
        JXButton * btn = [JXButton new];
        btn.tag = 190+i;
        btn.imgSize = CGSizeMake(40, 40);
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn addTarget:self action:@selector(sixBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:Image(titleArr[i]) forState:UIControlStateNormal];
        [footerView addSubview:btn];
        [topThrArr addObject:btn];
    }
    JXButton * lastBtn = nil;
    CGFloat btnWidth = (IPHONE_WIDTH-2)/3;
    CGFloat btnHeight = IPHONE_WIDTH/4;
    for (int i =0 ; i<titleArr.count; i++) {
        JXButton * btn = topThrArr[i];
        btn.sd_layout.leftSpaceToView(lastBtn==nil?footerView:lastBtn, lastBtn==nil?0:1).widthIs(btnWidth).heightIs(btnHeight).topEqualToView(footerView);
        lastBtn = btn;
    }
    NSArray * botThrTitleArr = @[@"卡片修改",@"卡片解绑",@"往期计划"];
    NSMutableArray * botThrAr = [NSMutableArray new];
    for (int i =0 ; i<botThrTitleArr.count; i++) {
        JXButton * btn = [JXButton new];
        btn.tag = 193+i;
        [btn addTarget:self action:@selector(sixBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.imgSize = CGSizeMake(40, 40);
        [btn setTitle:botThrTitleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setImage:Image(botThrTitleArr[i]) forState:UIControlStateNormal];
        [footerView addSubview:btn];
        [botThrAr addObject:btn];
    }
    JXButton * botLastBtn = nil;
    for (int i =0 ; i<botThrTitleArr.count; i++) {
        JXButton * btn = botThrAr[i];
        btn.sd_layout.leftSpaceToView(botLastBtn==nil?footerView:botLastBtn, botLastBtn==nil?0:1).widthIs(btnWidth).heightIs(btnHeight).topSpaceToView(lastBtn, 1);
        botLastBtn = btn;
    }
    return footer;
}
#pragma mark 事件触发
-(void)viewDetailBtnClick{
    JNCardSDKFVRepayPlanDetailVC * vc = [JNCardSDKFVRepayPlanDetailVC new];
    vc.myTitle = @"计划详情";
    vc.planInfoDic = _cardInfoDetailDic;
    vc.planID = self.cardInfoModel.userRepayPlayId;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)cancelRealBtnClick:(UIButton*)sender{
    [self verifyPayPasswordWithTitle:@"你将取消当前计划，请输入支付密码" typeString:@"cancel"];
}
-(void)jihuaRealBtnClick{//计划充值
    [JNCardSDKMytools warnText:@"启动计划" status:normal];
}
-(void)editRealBtnClick{
    if(_repayPlanArr.count==0) return;
    JNCardSDKCardCellModel * model = _repayPlanArr[0];
    JNCardSDKNewRepayPlanVC * vc = [JNCardSDKNewRepayPlanVC new];
    vc.cardInfoModel = model;
    vc.repayPlayId = _cardInfoDetailDic[@"userRepayPlayId"];
    vc.myTitle = @"修改还款计划";
    vc.cardContentDic = _cardInfoDetailDic;
    vc.myPlanType = 2;//原本的ModifyPlanVC弃用，使用type在新增信用卡判断是修改还是新增
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)deleteBindCardBtnClick{
    if (_cardInfoDetailDic.count==0) {//无计划才能解绑
        [self verifyPayPasswordWithTitle:@"你将对信用卡进行解绑，请完成验证\n支付密码" typeString:@"delete"];
    }else{
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"此卡有计划正在执行，不允许进行卡片解绑" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void)verifyPayPasswordWithTitle:(NSString*)title typeString:(NSString*)typeStr{
    __weak JNCardSDKCardDetailVC *weakSelf = self;
    [weakSelf checktPayPasswordAndTpye:typeStr];
}
-(void)planResetBtnClick{
    NSInteger statueNum = [_cardInfoDetailDic[@"theState"] integerValue];
    if (_cardInfoDetailDic == nil) {
        [self showAlertWithTitle:@"重启提醒" message:@"你暂无计划执行，无需进行重启操作" appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
            alertMaker.addActionDefaultTitle(@"确定",[UIColor blackColor]);
        } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
            
        }];
    }else{
        if (statueNum == 3) {
            if (_isHasDataInPausePlan==YES) {
                NSDate * planTimeDate = [JNCardSDKMytools dateStrToDate:_planTime];
                NSDate * currentDate  = [JNCardSDKMytools getCurrentDate];
                if ([JNCardSDKMytools compareOneDay:planTimeDate withAnotherDay:currentDate]==1) {
                    [[UIApplication sharedApplication].delegate.window addSubview:self.shelterView];
                    UIView * view = [UIView new];
                    view.backgroundColor = [UIColor whiteColor];
                    view.layer.masksToBounds = YES;
                    view.layer.cornerRadius = 5;
                    [_shelterView addSubview:view];
                    view.sd_layout.leftSpaceToView(_shelterView, AUTO(15)).rightSpaceToView(_shelterView, AUTO(15)).centerYEqualToView(_shelterView);
                    UILabel * titleLab = [UILabel new];
                    titleLab.text = @"重启提醒";
                    titleLab.font = [UIFont systemFontOfSize:17];
                    [view addSubview:titleLab];
                    titleLab.sd_layout.centerXEqualToView(view).topSpaceToView(view, 5).heightIs(20);
                    [titleLab setSingleLineAutoResizeWithMaxWidth:200];
                    
                    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
                    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
                    paraStyle.alignment = NSTextAlignmentLeft;
                    paraStyle.lineSpacing = 6; //设置行间距
                    //        paraStyle.hyphenationFactor = 1.0;
                    //        paraStyle.firstLineHeadIndent = 0.0;
                    //        paraStyle.paragraphSpacingBefore = 0.0;
                    //        paraStyle.headIndent = 0;
                    //        paraStyle.tailIndent = 0;
                    //设置字间距 NSKernAttributeName:@1.5f
                    NSInteger monepyy = ceil([_shouldPayMoney floatValue]);
                    NSString * stri = [NSString stringWithFormat:@"你将重启该计划，并继续执行还款，请确保你的卡片（尾号%@）有足够额度（消费金额：%@）以完成消费及还款操作，避免造成计划终止及影响还款信用。",[self.cardInfoModel.bankNum substringWithRange:NSMakeRange(self.cardInfoModel.bankNum.length-4, 4)],[NSString stringWithFormat:@"%ld",monepyy]];
                    
                    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0};
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:stri attributes:dic];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(23, 8)];
                    NSString * moneyyyST = [NSString stringWithFormat:@"%ld",monepyy];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(36,moneyyyST.length+7)];
                    
                    UILabel * contentLab = [UILabel new];
                    contentLab.font = [UIFont systemFontOfSize:15];
                    contentLab.attributedText = attributeStr;
                    contentLab.isAttributedContent = YES;
                    [view addSubview:contentLab];
                    contentLab.sd_layout.leftSpaceToView(view, 10).rightSpaceToView(view, 10).topSpaceToView(titleLab, 20).autoHeightRatio(0);
                    
                    UIView * line = [UIView new];
                    line.backgroundColor = [UIColor lightGrayColor];
                    [view addSubview:line];
                    line.sd_layout.leftEqualToView(view).rightEqualToView(view).topSpaceToView(contentLab, 10).heightIs(1);
                    
                    UIView * hhLine = [UIView new];
                    hhLine.backgroundColor = [UIColor lightGrayColor];
                    [view addSubview:hhLine];
                    hhLine.sd_layout.centerXEqualToView(view).topSpaceToView(line, 0).widthIs(1).heightIs(40);
                    
                    UIButton * btn = [UIButton new];
                    [btn setTitle:@"确定" forState:0];
                    btn.tag = 9090;
                    [btn addTarget:self action:@selector(sendResetRequest:) forControlEvents:UIControlEventTouchUpInside];
                    [btn setTitleColor:[UIColor blackColor] forState:0];
                    [view addSubview:btn];
                    btn.sd_layout.leftEqualToView(view).rightSpaceToView(hhLine, 0).topSpaceToView(line, 0).heightIs(40);
                    
                    UIButton * cbtn = [UIButton new];
                    [cbtn setTitle:@"取消" forState:0];
                    cbtn.tag = 9091;
                    [cbtn addTarget:self action:@selector(sendResetRequest:) forControlEvents:UIControlEventTouchUpInside];
                    [cbtn setTitleColor:[UIColor grayColor] forState:0];
                    [view addSubview:cbtn];
                    cbtn.sd_layout.leftSpaceToView(hhLine, 0).rightEqualToView(view).topSpaceToView(line, 0).heightIs(40);
                    [view setupAutoHeightWithBottomView:cbtn bottomMargin:10];
                }else{
                    JNCardSDKResetPauseVC * vc = [JNCardSDKResetPauseVC new];
                    vc.planID = self.cardInfoModel.userRepayPlayId;
                    vc.model = self.cardInfoModel;
                    vc.pausePlanStr = _planTime;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{
                [self showAlertWithTitle:@"重启提醒" message:@"当前暂停状态不允许进行此操作，请联系客服专员处理！" appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
                    alertMaker.addActionDefaultTitle(@"确定",[UIColor blackColor]);
                } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
                }];
            }
        }else if (statueNum == 1||statueNum == 0){//执行中
            [self showAlertWithTitle:@"重启提醒" message:@"你的计划未处于暂停状态，无需重启" appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
                alertMaker.addActionDefaultTitle(@"确定",[UIColor blackColor]);
            } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
            }];
        }else{//无计划
            [self showAlertWithTitle:@"重启提醒" message:@"你暂无计划执行，无需进行重启操作" appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
                alertMaker.addActionDefaultTitle(@"确定",[UIColor blackColor]);
            } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
                
            }];
        }
    }
}
-(void)getPausePlanInfo{
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_GetPauseInfo];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    [paraDic setObject:_cardInfoDetailDic[@"userCreditCardId"] forKey:@"UserCreditCardId"];
    [paraDic setObject:_cardInfoDetailDic[@"userRepayPlayId"] forKey:@"PlanId"];
    [PGNetworkHelper POST:urlStr parameters:paraDic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            self->_isHasDataInPausePlan = YES;
            self->_shouldPayMoney = responseObject[@"data"][@"money"];
            self->_planTime = responseObject[@"data"][@"PlanTime"];
        }else{
            self->_isHasDataInPausePlan = NO;
            if (![responseObject[@"msg"]isEqualToString:@"没有数据"]) {
                [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)sendResetRequest:(UIButton*)sender{
    [self tapClick];
    if (sender.tag==9091) {
        return;
    }
    [SVProgressHUD show];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_CancelCardState];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    [paraDic setObject:_cardInfoDetailDic[@"userRepayPlayId"] forKey:@"PlanId"];
    [paraDic setObject:@"1" forKey:@"TheState"];//重启计划
    [JNCardSDKMytools requestStringByPostUrl:url parameter:paraDic success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            [JNCardSDKMytools warnText:@"重启成功" status:JNCardSDKSuccess];
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        
    }];
}
-(void)checktPayPasswordAndTpye:(NSString*)type{
    if ([type isEqualToString:@"edit"]) {//修改
        JNCardSDKAddNewCardVC * vc = [JNCardSDKAddNewCardVC new];
        vc.bankModel = (JNCardSDKAddBankCellModel*)self.cardInfoModel;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([type isEqualToString:@"delete"]){//解绑
        [self sendDeleteCardRequest];
    }else if ([type isEqualToString:@"applycancel"]){
        [self showAlertWithTitle:@"温馨提示" message:@"将对计划进行撤销，撤销后可重新创建或自行线下还款，以免造成逾期而影响信用！" appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
            alertMaker.addActionCancelTitle(@"暂不操作",[UIColor blackColor]);
            alertMaker.addActionDefaultTitle(@"确定撤销",[UIColor redColor]);
        } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
            if(buttonIndex==1){
                [self applyCancelPausePlan];
            }
        }];
    }else{//取消计划
        [self cancelPlanRequest:self->_cardInfoDetailDic[@"userRepayPlayId"]];
    }
}
-(void)sendDeleteCardRequest{
    [SVProgressHUD show];
    //请求数据
    NSMutableDictionary * paraDic =[JNCardSDKReuserFile returnHYKBaseDic];
    [paraDic setObject:self.cardInfoModel.userCreditCardId forKey:@"UserCreditCardId"];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_DeleteUserCard];
    [PGNetworkHelper POST:url parameters:paraDic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [JNCardSDKMytools warnText:@"解绑成功" status:JNCardSDKSuccess];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)newPlanBtnClick{
    JNCardSDKVicSingleObject *single = [JNCardSDKVicSingleObject getInstance];
    NSInteger birthYear = [[single.hykIdCard substringWithRange:NSMakeRange(6, 4)] integerValue];
    NSInteger currentYear = [[[JNCardSDKMytools getBeiJingTime] substringWithRange:NSMakeRange(0, 4)] integerValue];
    if (currentYear - birthYear >=60){
        HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"温馨提醒" andMessage:@"因计划监管及支付渠道调整需要，仅针对60岁以下用户开放还款计划功能，你的年龄已超过该范围，无法提交计划！"];
        [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
        }];
        [alertView show];
    }else{
        NSDictionary * dic = [JNCardSDKReuserFile newPlanBtnClick:_billDayStr andPayStr:_payDayStr andCardModel:self.cardInfoModel];
        if ([dic[@"state"]isEqualToString:@"0"]) {
            [JNCardSDKMytools warnText:dic[@"msg"] status:JNCardSDKError];
            return;
        }else{//2018.8.6制定计划要请求渠道列表，有就进入渠道页面，没有就进入计划页面
            //2018-8-30,八月六号的那条作废，现在一律进入计划页面
            JNCardSDKNewFivMakeRepayVC *fivVC = [JNCardSDKNewFivMakeRepayVC new];
            fivVC.cardInfoModel = self.cardInfoModel;
            fivVC.receiveColor = topView.backgroundColor;
            [self.navigationController pushViewController:fivVC animated:YES];
        }
    }
}

-(void)sixBtnClick:(UIButton*)sender{
    if (sender.tag == 190) {
        if (self.cardInfoModel.userRepayPlayId == nil) {
            [JNCardSDKMytools warnText:@"当前无计划" status:JNCardSDKError];
            return;
        }
        JNCardSDKFVRepayPlanDetailVC * vc = [JNCardSDKFVRepayPlanDetailVC new];
        vc.myTitle = @"计划详情";
        vc.planInfoDic = _cardInfoDetailDic;
        vc.planID = self.cardInfoModel.userRepayPlayId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (sender.tag == 191){
        [self planResetBtnClick];
    }else if (sender.tag == 192){
        if (self.cardInfoModel.userRepayPlayId == nil) {
            [JNCardSDKMytools warnText:@"当前无计划" status:JNCardSDKError];
            return;
        }
        [self verifyPayPasswordWithTitle:@"你将撤销当前计划，请输入支付密码" typeString:@"applycancel"];
    }else if (sender.tag == 193){
        NSString * msg = @"";
        int flag = 0;
        if ([_cardInfoDetailDic[@"theState"] integerValue] ==3) {
            msg = @"为不影响计划执行，当前状态不允许对卡片进行修改！";
            flag = 1;
        }else{
            msg = @"将对卡片资料进行修改，请确认";
            flag = 2;
        }
        [self showAlertWithTitle:@"温馨提示" message:msg appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
            if (flag==2) {
                alertMaker.addActionCancelTitle(@"取消",[UIColor blackColor]);
                alertMaker.addActionDefaultTitle(@"去修改",[UIColor redColor]);
            }else{
                alertMaker.addActionDefaultTitle(@"我已知晓",[UIColor blueColor]);
            }
        } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
            if (flag==2) {
                if(buttonIndex==1){
                    [self editCard:sender];
                }
            }
        }];
    }else if (sender.tag == 194){//卡片解绑
        NSString * msg = @"";
        NSInteger butCount = 0;
        if (self.cardInfoModel.userRepayPlayId == nil) {
            msg = @"将对卡片进行解绑，请确认";
            butCount = 2;
        }else{
            butCount = 1;
            msg = @"为不影响计划执行，当前状态不允许对卡片进行解绑！";
        }
        [self showAlertWithTitle:@"温馨提示" message:msg appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
            if (butCount==2) {
                alertMaker.addActionCancelTitle(@"取消",[UIColor blackColor]);
                alertMaker.addActionDefaultTitle(@"确定解绑",[UIColor redColor]);
            }else{
                alertMaker.addActionDefaultTitle(@"我已知晓",[UIColor blackColor]);
            }
        } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
            if (butCount==2) {
                if(buttonIndex==1){
                    [self deleteBindCardBtnClick];
                }
            }
        }];
    }else if (sender.tag == 195){//往期计划
        JNCardSDKRepayPlanVC * vc = [JNCardSDKRepayPlanVC new];
        vc.userCreditCardId = self.userCreditCardId;
        vc.cardModel = self.cardInfoModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark 其他功能
-(CGFloat)calculateStrLength:(NSString *)str{
    UIFont * font = [UIFont systemFontOfSize:AUTO(16)];
    CGRect rect = [str boundingRectWithSize:CGSizeMake(IPHONE_WIDTH, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName] context:nil];
    return rect.size.width;
}

-(UIView*)myFooterView{
    UIView*footerView=[[UIView alloc]initWithFrame:CGRectMake(0, IPHONE_HEIGHT-59-KTabbarSafeBottomMargin, IPHONE_WIDTH, 49)];
    footerView.backgroundColor=[UIColor clearColor];
    UIButton*btn=[UIButton new];
    //    [btn.layer setBorderWidth:1.0];
    [btn.layer setCornerRadius:3.0];
    btn.backgroundColor=[UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1];
    //    btn.layer.borderColor=[UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1].CGColor;
    [btn.layer setMasksToBounds:YES];
    [btn setTitle:@"创建还款计划" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [footerView addSubview:btn];
    btn.sd_layout.leftSpaceToView(footerView, 15).rightSpaceToView(footerView, 15).centerYEqualToView(footerView).topSpaceToView(footerView, 0).bottomSpaceToView(footerView, 0);
    [btn addTarget: self action:@selector(newPlanBtnClick) forControlEvents:UIControlEventTouchUpInside];
    return footerView;
}


-(void)editCard:(UIButton*)sender{
    [self verifyPayPasswordWithTitle:@"你将对信用卡进行修改，请完成验证\n支付密码" typeString:@"edit"];
//    if (_cardInfoDetailDic.count==0||[_cardInfoDetailDic[@"theState"] integerValue] ==3) {
//        [self verifyPayPasswordWithTitle:@"你将对信用卡进行修改，请完成验证\n支付密码" typeString:@"edit"];
//    }else{
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"此卡有计划正在执行，不允许进行卡片修改" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        }]];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

-(UIView*)shelterView{
    if (_shelterView == nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
        [_shelterView addGestureRecognizer:tap];
    }
    return _shelterView;
}
-(void)tapClick{
    [_shelterView removeFromSuperview];
    _shelterView = nil;
}


@end


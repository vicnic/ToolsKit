//
//  JNCardSDKAddNewCardVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKAddNewCardVC.h"
#import "JNCardSDKCardDetailCell.h"
#import "FSCalendar.h"
#import "JNCardSDKPicClickVC.h"
#import "JNCardSDKDLCommonSearchVC.h"
#import "JNCardSDKBindCardDelegateView.h"
@interface JNCardSDKAddNewCardVC ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,FSCalendarDataSource,FSCalendarDelegate,UITextFieldDelegate>
{
    NSMutableArray * _dataArr, *_placeHolderArr, *_bankArray/*选择银行数组*/;
    NSInteger _choseTableViewIdx,_choseRow/*账单日和还款日的点击行数*/;
    NSString * _choseBank/*选择的银行*/;
    NSDate  *_calBillDate,*_calDueDate;
    NSArray *_keyArr;
    NSString *_userInputVerfy/*输入的验证码//2018-05-25注：现在是orderID不是验证码*/,*_newVerifyCode/*2018-05-25注:验证码*/;
    NSIndexPath  * _bankIndexPath;
    NSMutableDictionary * _paraDic;
    BOOL _isChoseBox,_isHasBillDueDate/*判断是否一开始就带有账单日还款日*/;
}
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)UIView * shadowView;
@property(nonatomic,retain)UIView * shelterView;
@end

@implementation JNCardSDKAddNewCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.86 alpha:1];
    
    _newVerifyCode = @"";
    _paraDic = [NSMutableDictionary new];
    _dataArr = [NSMutableArray arrayWithObjects:@[@"持卡人：",@"身份证号："],@[@"信用卡号：",@"发卡银行："/*,@"卡等级：",@"卡种类型："*/,@"账单日：",@"还款日："],@[@"预留手机：",@"验证码："], nil];
    _placeHolderArr = [NSMutableArray arrayWithObjects:@[@"请输入真实姓名",@"请输入身份证号"],@[@"请输入信用卡号",@"选择发卡银行"/*,@"选择信用卡等级",@"选择信用卡卡种类型"*/,@"选择信用卡账单日",@"选择信用卡还款日"],@[@"信用卡在银行开卡时预留的手机号",@"输入验证码"], nil];
    _keyArr = @[@[@"name",@"idcard"],@[@"banknum",@"bankname"/*,@"LevelCode",@"CardtBankTypeCode"*/,@"billdate",@"duedate"],@[@"mobile",@"TelCode"]];
    if (self.bankModel==nil) {//表示是新增
        self.myTitle = @"添加卡片";
        for (NSArray * keyarr in _keyArr) {
            for (NSString * key  in keyarr) {
                [_paraDic setObject:@"" forKey:key];
            }
        }
        JNCardSDKVicSingleObject *single = [JNCardSDKVicSingleObject getInstance];
        [_paraDic setObject:single.hykRealName forKey:@"name"];
        [_paraDic setObject:single.hykIdCard forKey:@"idcard"];
        _isHasBillDueDate = NO;
    }else{//修改卡片
        self.myTitle = @"修改卡片";
        [self getCurrentCardInfo];//获取卡片信息
    }
    _bankArray = [NSMutableArray new];
    [self getBankNameList];
    [self.view addSubview:self.myTab];
}
-(void)getBankNameList{
    [SVProgressHUD show];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetAllBankList];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSArray * dataArr = responseObject[@"data"];
            for (NSDictionary * dic  in dataArr) {
                [self->_bankArray addObject:dic[@"bankName"]];
            }
            //            [self setDLPickView:self->_bankArray.copy];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getUserChoseBankName{
    JNCardSDKDLCommonSearchVC *vc=[[JNCardSDKDLCommonSearchVC alloc]initWithNibName:@"JNCardSDKDLCommonSearchVC" bundle:SDKBundle];
//    JNCardSDKDLCommonSearchVC * vc = [JNCardSDKDLCommonSearchVC new];
    vc.dataArr = self->_bankArray;
    vc.resultBlock = ^(NSString * _Nonnull resultStr) {
        NSIndexPath *  indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
        JNCardSDKCardDetailCell * cell = [self.myTab cellForRowAtIndexPath:indexPath];
        cell.detailTF.text = resultStr;
        [self->_paraDic setObject:resultStr forKey:@"bankname"];
    };
    vc.myTitle = @"选择银行";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark 懒加载
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight) style:UITableViewStyleGrouped];
        _myTab.showsVerticalScrollIndicator = NO;
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    }
    return _myTab;
}

-(UIView*)shadowView{
    if (_shadowView == nil) {
        _shadowView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowViewTapClick)];
        tap.delegate = self;
        [_shadowView addGestureRecognizer:tap];
    }
    return _shadowView;
}
-(UIView*)shelterView{
    if (_shelterView==nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    }
    return _shelterView;
}
-(void)setDLPickView:(NSArray*)array{
    DLPickerView * picker = [[DLPickerView alloc]initWithDataSource:array withSelectedItem:nil withSelectedBlock:^(id  _Nonnull item) {
        [self->_paraDic setObject:item forKey:@"bankname"];
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:1 inSection:1];
        [self->_myTab reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    }];
    [picker show];
}

#pragma mark tableView代理方法
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * arr = _dataArr[section];
    return arr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JNCardSDKCardDetailCell * cell= nil ;
    if (!cell) {
        if (indexPath.section == 2 && indexPath.row == 0) {
            //创建验证码cell
            cell = [[JNCardSDKCardDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"verifycell"];
            [cell.verifyTF addTarget:self action:@selector(textFieldDidChangeValue:)
                    forControlEvents:UIControlEventEditingChanged];
            cell.verifyTF.keyboardType = UIKeyboardTypePhonePad;
            cell.verifyTF.placeholder = _placeHolderArr[indexPath.section][indexPath.row];
        }else{
            cell = [[JNCardSDKCardDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            if (indexPath.section==0) {
                cell.userInteractionEnabled = NO;
            }else{
                [cell.detailTF addTarget:self action:@selector(textFieldDidChangeValue:)
                        forControlEvents:UIControlEventEditingChanged];
                cell.detailTF.tag = 460 + indexPath.section*10 + indexPath.row;
                cell.detailTF.delegate = self;
                cell.detailTF.keyboardType = (indexPath.section==1&&indexPath.row==0)||(indexPath.section==1&&indexPath.row>=0)||(indexPath.section==2&&indexPath.row==1)?UIKeyboardTypeNumberPad:UIKeyboardTypeDefault;
                cell.detailTF.placeholder = _placeHolderArr[indexPath.section][indexPath.row];
                if ((indexPath.section==0&&indexPath.row>0)||(indexPath.section==1&&indexPath.row>0)) {
                    cell.arrowImgView.image = Image(@"右箭头");
                    cell.detailTF.enabled = NO;
                }
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleLab.text = _dataArr[indexPath.section][indexPath.row];
        cell.titleLab.textAlignment = NSTextAlignmentRight;
        if (indexPath.section==1&&indexPath.row>1) {
            id sr = _paraDic[_keyArr[indexPath.section][indexPath.row]];
            if([sr isKindOfClass:[NSNull class]]){
                cell.detailTF.text = @"";
            }else{
                NSString * objStr = (NSString *)sr;
                if (objStr.length>10) {
                    cell.detailTF.text = [objStr substringWithRange:NSMakeRange(0, 10)];
                }
            }
        }else{
            id  dateStr = _paraDic[_keyArr[indexPath.section][indexPath.row]];
            if ([dateStr isKindOfClass:[NSNull class]]) {
                cell.detailTF.text = @"";
            }else{
                cell.detailTF.text = dateStr;
            }
        }
        cell.verifyTF.text = _paraDic[_keyArr[indexPath.section][indexPath.row]];
        cell.verifyJNBlock = ^(UIButton *btn) {
            [self applySMSWithBtn:btn];
        };
        cell.bottomLineView.hidden = ((indexPath.section==0&&indexPath.row>0)||(indexPath.section==1&&indexPath.row>2)||(indexPath.section==2&&indexPath.row>0))?YES:NO;
    }
    return cell;
}
-(void)makeCodeBtn:(UIButton*)sender{
    __block int time = 60;
    __block UIButton *verifybutton = sender;
    verifybutton.enabled = NO;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [verifybutton setTitle:@"获取验证码" forState:UIControlStateNormal];
                [verifybutton setTitleColor:[UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1] forState:UIControlStateNormal];
                verifybutton.enabled = YES;
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSString *strTime = [NSString stringWithFormat:@"%d后重获",time];
                [verifybutton setTitle:strTime forState:UIControlStateNormal];
                [verifybutton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?44:10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        UIView * headViewe = [UIView new];
        UILabel * lab = [UILabel new];
        lab.text = @"  正确填写信用卡信息，提高卡鉴权通过效率";
        lab.font = Font(16);
        lab.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.94 alpha:1];
        [headViewe addSubview:lab];
        lab.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
        return headViewe;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==2?70+49:0.01;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==2) {
        UIView * footView = [UIView new];
        UIButton * boxBtN = [UIButton buttonWithType:UIButtonTypeCustom];
        boxBtN.selected = _isChoseBox;
        [boxBtN addTarget:self action:@selector(boxBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [boxBtN setImage: Image(@"box_unsel") forState:UIControlStateNormal];
        [boxBtN setImage:Image(@"box_sel") forState:UIControlStateSelected];
        [boxBtN setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [footView addSubview:boxBtN];
        boxBtN.sd_layout.leftSpaceToView(footView, 10).topSpaceToView(footView, 10).heightIs(30).widthEqualToHeight();
        
        NSMutableAttributedString * boxStr = [[NSMutableAttributedString alloc]initWithString:@"我已知晓《关于信用卡绑定须知内容》"];
        [boxStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(0, 5)];
        //设置下划线和颜色
        [boxStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                NSForegroundColorAttributeName: [UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1],
                                NSUnderlineColorAttributeName:[UIColor whiteColor],
                                NSUnderlineStyleAttributeName:@(1)} range:NSMakeRange(5, 11)];
        
        [boxStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(16, 1)];
        UILabel * boxLab = [UILabel new];
        boxLab.attributedText = boxStr;
        boxLab.isAttributedContent = YES;
        [boxLab addTarget:self action:@selector(boxLabClick)];
        [footView addSubview:boxLab];
        boxLab.sd_layout.leftSpaceToView(boxBtN, 10).centerYEqualToView(boxBtN).heightIs(16);
        [boxLab setSingleLineAutoResizeWithMaxWidth:300];
        
        UIButton * btn = [[UIButton alloc]init];
        btn.backgroundColor = [UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 2;
        [btn setTitle:@"确定，并绑卡" forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        [footView addSubview:btn];
        [btn addTarget:self action:@selector(addIDCardBtnClick) forControlEvents:UIControlEventTouchUpInside];
        btn.sd_layout.leftSpaceToView(footView, 10).rightSpaceToView(footView, 10).topSpaceToView(boxBtN, 10).heightIs(49);
        return footView;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _choseRow = indexPath.row;
    if (indexPath.section==1 && indexPath.row == 1) {
        _bankIndexPath = indexPath;
        [self getUserChoseBankName];
        
    }else if (indexPath.section == 1 && indexPath.row>1){
        JNCardSDKCardDetailCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.detailTF.text.length>0) {
            if (_isHasBillDueDate) {
                return;
            }else{
                //表示是完善卡信息，一进来没有账单日还款日，这里的有数据是在这个页面选择的
            }
        }
        //        if (cell.detailTF.text.length>0&&self.bankModel) {
        //            return;
        //        }
        [self.view addSubview:self.shadowView];
        FSCalendar * calendar = [[FSCalendar alloc]initWithFrame:Frame(0, IPHONE_HEIGHT/2-150, IPHONE_WIDTH, 300)];
        calendar.backgroundColor = [UIColor whiteColor];
        calendar.delegate = self;
        calendar.dataSource =self;
        [_shadowView addSubview:calendar];
    }
}
-(void)getBankNameWithBankNum:(NSString*)bankNum{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetBankNameFromBin];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnBaseParaDicWithoutToken];
    [p setObject:bankNum forKey:@"BankNum"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            NSString * bankName = responseObject[@"BankName"];
            if (bankName.length) {
                //                获取的名字未必在后台有，所以要先跟后台给的银行名称列表进行比对，有才存储赋值
                for (NSString * name in self->_bankArray) {
                    if ([name isEqualToString:bankName]) {
                        NSIndexPath *  indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
                        JNCardSDKCardDetailCell * cell = [self.myTab cellForRowAtIndexPath:indexPath];
                        cell.detailTF.text = bankName;
                        [self->_paraDic setObject:bankName forKey:@"bankname"];
                        break;
                    }
                }
                
            }
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark textField代理
- (void)textFieldDidChangeValue:(UITextField*)textField{
    JNCardSDKCardDetailCell * cell = (JNCardSDKCardDetailCell*)[[textField superview]superview];
    NSIndexPath * indexPath = [self.myTab indexPathForCell:cell];
    switch (indexPath.section) {
        case 0:{
            if (indexPath.row==0) {
                [_paraDic setObject:textField.text forKey:@"name"];
            }else{
                [_paraDic setObject:textField.text forKey:@"idcard"];
            }
        }break;
        case 1:{
            if (indexPath.row==0) {
                [_paraDic setObject:textField.text forKey:@"banknum"];
                if (textField.text.length == 10) {
                    [self getBankNameWithBankNum:textField.text];
                }
            }
        }break;
        case 2:{
            if (indexPath.row ==0) {
                [_paraDic setObject:textField.text forKey:@"mobile"];
            }else{
                _newVerifyCode = textField.text;
                [_paraDic setObject:textField.text forKey:@"TelCode"];
            }
        }break;
        default:
            break;
    }
}
#pragma mark 点击事件触发区
-(void)addIDCardBtnClick{
    if (_isChoseBox==NO) {
        [JNCardSDKMytools warnText:@"请同意《关于信用卡绑定须知内容》" status:JNCardSDKError];
        return;
    }
    if([_calDueDate isEarlierThanDate:_calBillDate]){
        //还款日比账单日还早
        [JNCardSDKMytools warnText:@"还款日必须大于账单日" status:JNCardSDKError];
        return;
    }
    NSInteger days = [_calBillDate distanceInDaysToDate:_calDueDate];
    if (!(days>=15&&days<=25)) {
        [JNCardSDKMytools warnText:@"信用卡账单周期请设置在15-25天范围内，请重设！" status:JNCardSDKError];
        return;
    }
    /*2019-10-11 21:08:26去掉了cvn2和有效期
     NSString * inputStr = _paraDic[@"validdata"];
     if (inputStr.length==0) {
     [JNCardSDKMytools warnText:@"有效期不能为空" status:JNCardSDKError];
     return;
     }
     NSString * month = [inputStr substringWithRange:NSMakeRange(0, 2)];
     NSString * year = [inputStr substringWithRange:NSMakeRange(2, 2)];
     NSString * currentYear = [[JNCardSDKMytools getBeiJingTime] substringWithRange:NSMakeRange(2, 2)];
     //    currentYear = [currentYear substringWithRange:NSMakeRange(0, 4)];
     NSString * currentMonth =[[JNCardSDKMytools getBeiJingTime] substringWithRange:NSMakeRange(5, 2)];
     if([year integerValue]<[currentYear integerValue]){
     [JNCardSDKMytools warnText:@"年份必须大于今年" status:JNCardSDKError];
     return;
     }else{
     if ([year integerValue] == [currentYear integerValue]) {
     if ([month integerValue]>12) {
     [JNCardSDKMytools warnText:@"月份输入错误" status:JNCardSDKError];
     return;
     }else{
     if ([month integerValue]<[currentMonth integerValue]) {
     [JNCardSDKMytools warnText:@"有效期错误" status:JNCardSDKError];
     return;
     }
     }
     }else{
     if ([month integerValue]>12) {
     [JNCardSDKMytools warnText:@"月份输入错误" status:JNCardSDKError];
     return;
     }
     }
     }
     */
    [self.view endEditing:YES];
    [self verifyTheVerifyCode];
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)shadowViewTapClick{
    [_shadowView removeFromSuperview];
    _shadowView = nil;
}
-(void)applySMSWithBtn:(UIButton*)btn{//2018-05-25增加信用卡的验证码获取走这个接口，跟其他的验证码接口不一样
    if ([self alertMethod]) {
        return;
    }
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_OpenChannelSendSms];
    NSMutableDictionary * prDic = [JNCardSDKReuserFile returnHYKBaseDic];
    [prDic setObject:@"" forKey:@"UserCreditCardId"];
    [prDic setObject:@"1" forKey:@"OpenUnion"];
    [prDic addEntriesFromDictionary:_paraDic];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:prDic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            [JNCardSDKMytools warnText:@"发送成功" status:JNCardSDKSuccess];
            self->_userInputVerfy = responseObject[@"data"];
            //这里是模拟的，没有收到验证码的，正式后要改掉
            NSIndexPath *  indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
            JNCardSDKCardDetailCell * cell = [self.myTab cellForRowAtIndexPath:indexPath];
            cell.detailTF.text = [[[NSString stringWithFormat:@"%@",responseObject[@"msg"]] componentsSeparatedByString:@":"] lastObject];
            self->_newVerifyCode = cell.detailTF.text;
            [self->_paraDic setObject:cell.detailTF.text forKey:@"TelCode"];
            [self makeCodeBtn:btn];
        }else{
            [JNCardSDKMytools warnText:[[[NSString stringWithFormat:@"%@",responseObject[@"msg"]] componentsSeparatedByString:@":"] firstObject] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)verifyTheVerifyCode{//验证验证码
    if(_newVerifyCode.length==0){
        [JNCardSDKMytools warnText:@"验证码不能为空" status:JNCardSDKError];
        return;
    }
    if (_userInputVerfy.length==0) {
        [JNCardSDKMytools warnText:@"验证码验证不通过" status:JNCardSDKError];
        return;
    }
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    NSMutableDictionary * praDic = [NSMutableDictionary new];
    [praDic addEntriesFromDictionary:p];
    [praDic setObject:_userInputVerfy forKey:@"OrderId"];
    [praDic setObject:_newVerifyCode forKey:@"TelCode"];
    [praDic addEntriesFromDictionary:_paraDic];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_ChannelSendSmsConfirm];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:praDic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"] integerValue]==1) {
            [self sendUserCardInfoToNet];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(BOOL)alertMethod{
    BOOL isStop = NO;
    NSString * keyStr = @"";
    NSArray * keyArr = @[@"bankname",@"banknum",/*@"cvn2",@"validdata",*/@"mobile",@"billdate",@"duedate"];
    NSArray * warnStrArr = @[@"您未选择银行名称",@"您未填写银行卡号",/*@"您未填写cvn2号",@"您未填写有效期",*/@"您未填写手机号",@"您未填写账单日",@"您未填写还款日"];
    for (int i =0; i<keyArr.count; i++) {
        keyStr = [_paraDic objectForKey:keyArr[i]];
        if (keyStr.length==0) {
            [JNCardSDKMytools warnText:warnStrArr[i] status:JNCardSDKError];
            isStop = YES;
            break;
        }
    }
    return isStop;
}
-(void)sendUserCardInfoToNet{//发送信用卡信息到后台
    if ([self alertMethod] == YES) {
        [SVProgressHUD dismiss];
        return;
    }
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [_paraDic addEntriesFromDictionary:p];
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_BindUserCreditCard];
    [PGNetworkHelper POST:urlStr parameters:_paraDic cache:NO responseCache:nil success:^(id responseObject) {
        if([responseObject[@"status"]isEqualToString:@"1"]){
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:@"操作成功" status:JNCardSDKSuccess];
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)boxBtnClick:(UIButton*)sender{
    _isChoseBox = !_isChoseBox;
    sender.selected = _isChoseBox;
}
-(void)boxLabClick{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetArticle];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:@"6" forKey:@"classCode"];
    [p setObject:@"2" forKey:@"type"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            [self.view addSubview:self.shelterView];
            NSArray * obj = [SDKBundle loadNibNamed:@"JNCardSDKBindCardDelegateView" owner:self options:nil];
            JNCardSDKBindCardDelegateView * v = obj.firstObject;
            v.viewTitleLab.text =responseObject[@"data"][@"title"];
            v.contentLab.text = responseObject[@"data"][@"content"];
            v.btnBlock = ^{
                [self.shelterView removeFromSuperview];
                self.shelterView = nil;
            };
            v.frame = Frame(25, IPHONE_HEIGHT/2-150, IPHONE_WIDTH-50, 300);
            [self.shelterView addSubview:v];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark 修改卡片的请求
-(void)getCurrentCardInfo{
    [SVProgressHUD show];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_GetUserCardInfo];
    [paraDic setObject:self.bankModel.userCreditCardId forKey:@"UserCreditCardId"];
    [PGNetworkHelper POST:urlStr parameters:paraDic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSDictionary * infodic = responseObject[@"data"][@"cardinfo"];
            if (infodic==nil) {
                infodic = responseObject[@"data"];
            }else{
                infodic = responseObject[@"data"][@"cardinfo"];
            }
            NSString * billDateStr = infodic[@"billDate"];
            NSString * dueDateStr = infodic[@"dueDate"];
            if (![billDateStr isKindOfClass:[NSNull class]] && ![dueDateStr isKindOfClass:[NSNumber class]]) {
                if (billDateStr.length>0 && dueDateStr.length>0) {
                    NSMutableDictionary * correctBillPayDic = [JNCardSDKCalculaterBillPayDay getBillDateAndPayDate:billDateStr andPayDay:dueDateStr];
                    self->_calBillDate = [JNCardSDKMytools dateStrToDate:correctBillPayDic[@"finalBillDay"]];
                    self->_calDueDate = [JNCardSDKMytools dateStrToDate:correctBillPayDic[@"finalPayDay"]];
                }
            }else{
                if ([billDateStr isKindOfClass:[NSNull class]]) {
                    billDateStr = @"";
                }
                if ([dueDateStr isKindOfClass:[NSNull class]]) {
                    dueDateStr = @"";
                }
            }
            if (billDateStr.length>10 && dueDateStr.length>10) {
                self->_isHasBillDueDate = NO;
            }else{
                self->_isHasBillDueDate = NO;
            }
            [self->_paraDic setObject:infodic[@"name"] forKey:@"name"];
            [self->_paraDic setObject:infodic[@"idCard"] forKey:@"idcard"];
            [self->_paraDic setObject:infodic[@"bankNum"] forKey:@"banknum"];
            [self->_paraDic setObject:infodic[@"cvn2"] forKey:@"cvn2"];
            [self->_paraDic setObject:infodic[@"validData"] forKey:@"validdata"];
            [self->_paraDic setObject:infodic[@"bankName"] forKey:@"bankname"];
            [self->_paraDic setObject:billDateStr forKey:@"billdate"];
            [self->_paraDic setObject:dueDateStr forKey:@"duedate"];
            [self->_paraDic setObject:infodic[@"mobile"] forKey:@"mobile"];
            [self->_paraDic setObject:@"" forKey:@"TelCode"];
            [self->_myTab reloadData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark 日历回调
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    date = [date dateByAddingTimeInterval:8 * 60 * 60];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T00:00:00.0000000+08:00'";
    NSString *dateStr = [formatter stringFromDate:date];
    if (_choseRow == 2) {//点击第七行，账单日
        _calBillDate = date;
        [_paraDic setObject:dateStr forKey:@"billdate"];
    }else if (_choseRow == 3){
        //还款日
        _calDueDate = date;
        [_paraDic setObject:dateStr forKey:@"duedate"];
    }
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:_choseRow inSection:1];
    [_myTab reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    [_shadowView removeFromSuperview];
    _shadowView = nil;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSString * clsNameStr = NSStringFromClass([touch.view class]);
    if ([clsNameStr containsString:@"UITableViewCell"]&&[clsNameStr hasSuffix:@"ContentView"]) {
        //返回为NO则屏蔽手势事件
        return NO;
    }else{
        if ([touch.view isEqual:_shadowView]) {
            return YES;
        }else {
            return NO;
        }
    }
}

@end

//
//  JNCardSDKDLCommonSearchVC.m
//  TwoOneEight
//
//  Created by Jinniu on 2019/5/22.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKDLCommonSearchVC.h"
#import "HXSearchBar.h"
@interface JNCardSDKDLCommonSearchVC ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)NSMutableArray * sourceArr , * filterArr;
@property (weak, nonatomic) IBOutlet HXSearchBar *mySearch;
@property (weak, nonatomic) IBOutlet UITableView *myTab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;

@end

@implementation JNCardSDKDLCommonSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sourceArr = [NSMutableArray new];
    self.filterArr = [NSMutableArray new];
    [self.sourceArr addObjectsFromArray: [self.dataArr arrayWithPinYinFirstLetterFormat]];
    [self config];
}

-(void)config{
    self.topHeight.constant = kTopHeight;
    self.botHeight.constant = KTabbarSafeBottomMargin;
    self.mySearch.backgroundColor = [UIColor whiteColor];
    self.mySearch.delegate = self;
    self.mySearch.placeholder = @"请输入关键词搜索";
    //光标颜色
    self.mySearch.cursorColor = [UIColor lightGrayColor];
    self.mySearch.clearButtonImage = [UIImage imageNamed:@"search_delete"];
    //去掉取消按钮灰色背景
    self.mySearch.hideSearchBarBackgroundImage = YES;
    self.myTab.delegate = self;
    self.myTab.dataSource = self;
    self.myTab.rowHeight = 45;
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sourceArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSDictionary * dic = self.sourceArr[section];
    NSArray * arr = dic[@"content"];
    return arr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary * dic = self.sourceArr[indexPath.section];
    NSLog(@"---====>>>%@",dic);
    NSArray * arr = dic[@"content"];
    cell.textLabel.text = arr[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * dic = self.sourceArr[indexPath.section];
    NSArray * arr = dic[@"content"];
    if (self.resultBlock) {
        self.resultBlock(arr[indexPath.row]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    NSMutableArray *resultArray =[NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (NSDictionary *dict in self.sourceArr) {
        NSString *title = dict[@"firstLetter"];
        [resultArray addObject:title];
    }
    return resultArray;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSDictionary * dic = self.sourceArr[section];
    return dic[@"firstLetter"];
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return index;
}
#pragma mark - UISearchBar Delegate
//已经开始编辑时的回调
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    HXSearchBar *sear = (HXSearchBar *)searchBar;
    searchBar.showsCancelButton = YES;
    //取消按钮
    sear.cancleButton.backgroundColor = [UIColor whiteColor];
    [sear.cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [sear.cancleButton setTitleColor:ThemeColor forState:UIControlStateNormal];
    sear.cancleButton.titleLabel.font = [UIFont systemFontOfSize:14];
}
//编辑文字改变的回调
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.sourceArr removeAllObjects];
    [self.filterArr removeAllObjects];
    if (searchBar.text.length==0) {
        [self.sourceArr addObjectsFromArray:[self.dataArr arrayWithPinYinFirstLetterFormat]];
    }else{
        for (NSString * bankName in self.dataArr) {
            if ([bankName containsString:searchBar.text]) {
                [self.filterArr addObject:bankName];
            }
        }
        [self.sourceArr addObjectsFromArray:[self.filterArr arrayWithPinYinFirstLetterFormat]];
    }
    [self.myTab reloadData];
}
//取消按钮点击的回调
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    [searchBar endEditing:YES];
    searchBar.text= @"";
    [self.filterArr removeAllObjects];
    [self.sourceArr removeAllObjects];
    [self.sourceArr addObjectsFromArray:[self.dataArr arrayWithPinYinFirstLetterFormat]];
    [self.myTab reloadData];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.mySearch endEditing:YES];
}
@end

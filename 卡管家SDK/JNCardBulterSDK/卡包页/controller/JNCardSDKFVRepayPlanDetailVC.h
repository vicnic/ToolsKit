//
//  JNCardSDKFVRepayPlanDetailVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/11/16.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"

@interface JNCardSDKFVRepayPlanDetailVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * planID;//就是UserRepayPlayId;
@property(nonatomic,retain)NSMutableDictionary * planInfoDic;//就是上一页的_cardInfoDetailDic
@property(nonatomic,assign)BOOL isFromRepayList;//是否是从个人的还款计划进来的
@property(nonatomic,copy)NSString * planDinJin;//计划定金
@end

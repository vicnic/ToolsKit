//
//  JNCardSDKNewRepayPlanVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKNewRepayPlanVC.h"
#import "JNCardSDKNewRepayPlanCell.h"
#import "FSCalendar.h"
#import "JNCardSDKPreViewPlanVC.h"
#import "SWAlertController.h"
#import <HDAlertView/HDAlertView.h>
#import "JNCardSDKMyWebView.h"
@interface JNCardSDKNewRepayPlanVC ()<UITableViewDelegate,UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance ,UITextFieldDelegate>
{
    UIButton * _seletedBtn,*_twoBtnSelect;
    NSString * _backPlanid,*_TheKey,*_newsLabStr;
    UILabel * _canchoseLab,*_newLab;
    NSArray * _titleArr ,* _placeHoldArr;
    BOOL _isFoldCell;
    BOOL _todayCanMake/*今天是否能执行计划的按钮的可选状态*/,_isChoseToday/*是选了今日执行还是次日执行*/;
    NSMutableDictionary * _paraDic;//参数字典
    NSMutableArray * _choseDayList/*选择的还款日期的列表*/,*_tmpCellTFArr;
    NSString * _billDateStr, * _payDateStr;
    NSInteger _choseMinCount,_choseMaxCount/*根据金额计算的允许选择的范围*/,_userChoseCount/*用户选择的日期数*/,_calendarDays/*日历可选的天数*/,_selectBtnTag/*三个消费按钮选中的记录*/,_minPlanMoney/*确定了计划总额后的最小还款金额*/;
    NSInteger  _planMoney/*计划本金输入框内的数值*/;
    UITextField * rightTF;
    UIView * _secHeadTwo,*_calendarBgView;
}
@property(nonatomic,retain)UITableView * myTab;
@end

@implementation JNCardSDKNewRepayPlanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFoldCell = YES;
    _calendarDays = 0;
    _userChoseCount = 0;
    _paraDic = [NSMutableDictionary new];
    _choseDayList = [NSMutableArray new];
    _tmpCellTFArr = [NSMutableArray new];
    [self isTodayCanMakePlan];
    _titleArr = @[@[@"计划总额",@"计划定金"],@[@"消费模式"],@[],@[@"选择还款日期"]];
    _placeHoldArr = @[@[@"账单还款金额",@""],@[]];
    [self.view addSubview:self.myTab];
    [JNCardSDKReuserFile getPlanBeforeDay];//获取信用卡计划的提前时间及最大笔数
    NSMutableDictionary * dic = [JNCardSDKCalculaterBillPayDay getBillDateAndPayDate:self.cardInfoModel.billDate andPayDay:self.cardInfoModel.dueDate];
    _billDateStr = dic[@"finalBillDay"];
    _payDateStr = dic[@"finalPayDay"];
}
-(void)viewWillDisappear:(BOOL)animated{
    _userChoseCount = 0;//进入下一页的时候置空以免返回修改造成可选时间不足
}
-(void)isTodayCanMakePlan{
    //当前时间对比13：30，看当天执行按钮是否可用
    NSDate * currentDate = [NSDate date];
    NSString * currentDayStr = [JNCardSDKMytools dateToDateStr:currentDate];
    currentDayStr = [currentDayStr stringByReplacingCharactersInRange:NSMakeRange(11, 8) withString:@"13:30:00"];
    NSDate * standDate = [JNCardSDKMytools dateStrToDate:currentDayStr];
    NSDate * nowDate = [currentDate dateByAddingHours:8];
    if ([JNCardSDKMytools compareOneDay:nowDate withAnotherDay:standDate]==1) {
        //当前时间超过13：30
        _todayCanMake = NO;
    }else{
        //后来又加上了判断当天是否是节假日，如果是即便是没超过13：30也不可选
        if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:nowDate isFromSelect:NO]==YES) {
            _todayCanMake = YES;
        }else{
            _todayCanMake = NO;
        }
    }
}
#pragma mark 懒加载
-(UITableView *)myTab {
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource =self;
        _myTab.backgroundColor=ColorByHexStr(@"#EEEEEE");
    }
    return _myTab;
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        NSArray * arr = _titleArr[section];
        if (section==2) {
            return 0;
        }else{
            return arr.count;
        }
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JNCardSDKNewRepayPlanCell * cell = nil;
    if (!cell) {
        if (indexPath.section == 0) {
            cell = [[JNCardSDKNewRepayPlanCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            UIView * lefBgView = [UIView new];
            lefBgView.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:lefBgView];
            lefBgView.sd_layout.centerXIs(IPHONE_WIDTH/4).topEqualToView(cell.contentView).heightIs(30);
            UIImageView * leftView = [UIImageView new];
            leftView.image = Image(@"累计收益");
            [lefBgView addSubview:leftView];
            leftView.sd_layout.leftEqualToView(lefBgView).topSpaceToView(lefBgView, 10).widthIs(25).heightIs(25);
            UILabel * leftTitle = [UILabel labelWithTitle:@"计划总额(元)" color:[UIColor blackColor] fontSize:14];
            [lefBgView addSubview:leftTitle];
            leftTitle.sd_layout.leftSpaceToView(leftView, 0).centerYEqualToView(leftView).heightIs(16);
            [leftTitle setSingleLineAutoResizeWithMaxWidth:200];
            [lefBgView setupAutoWidthWithRightView:leftTitle rightMargin:0];
            UITextField * leftTF = [UITextField new];
            leftTF.delegate = self;
            [leftTF addTarget:self action:@selector(textField1TextChange:) forControlEvents:UIControlEventEditingChanged];
            leftTF.textAlignment = NSTextAlignmentCenter;
            if (self.myPlanType==2) {
                NSInteger moneytp = ceil([self.cardInfoModel.repayMoney floatValue]);
                _planTotalMoney = moneytp;
                leftTF.text = [NSString stringWithFormat:@"%ld",moneytp];
                if (_tmpCellTFArr.count<2) {
                    [_tmpCellTFArr addObject:self.cardInfoModel.repayMoney];
                }else{
//                    [_tmpCellTFArr replaceObjectAtIndex:0 withObject:self.cardInfoModel.RepayMoney];
                }
                
                [_paraDic setObject:[NSString stringWithFormat:@"%ld",moneytp] forKey:@"TotalMoney"];//计划总额参数
            }
            NSMutableParagraphStyle *stylePsw = [[NSMutableParagraphStyle alloc] init];
            stylePsw.alignment = NSTextAlignmentCenter;
            NSAttributedString *attriPsw = [[NSAttributedString alloc] initWithString:@"请输入计划金额" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHue:0.67 saturation:0.01 brightness:0.44 alpha:1],NSFontAttributeName:[UIFont systemFontOfSize:15], NSParagraphStyleAttributeName:stylePsw}];
            leftTF.attributedPlaceholder = attriPsw;
            leftTF.tag = 80;
            leftTF.keyboardType = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:leftTF];
            leftTF.sd_layout.leftSpaceToView(cell.contentView, 0).topSpaceToView(lefBgView, 10).heightIs(30).widthIs(IPHONE_WIDTH/2);
            
            UIView * vline1 = [UIView new];
            vline1.backgroundColor = [UIColor colorWithHue:0.21 saturation:0.02 brightness:0.73 alpha:1];
            [cell.contentView addSubview:vline1];
            vline1.sd_layout.centerXEqualToView(cell.contentView).widthIs(1).topSpaceToView(cell.contentView, 10).bottomEqualToView(leftTF);
            
            UIView * rightBgView = [UIView new];
            rightBgView.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:rightBgView];
            rightBgView.sd_layout.centerXIs(IPHONE_WIDTH*3/4).centerYEqualToView(lefBgView).heightIs(30);
            UIImageView * rightView = [UIImageView new];
            rightView.image = Image(@"本月收益");
            [rightBgView addSubview:rightView];
            rightView.sd_layout.leftEqualToView(rightBgView).topSpaceToView(rightBgView, 10).widthIs(25).heightIs(25);
            UILabel * rightTitle = [UILabel labelWithTitle:@"计划定金(元)" color:[UIColor blackColor] fontSize:14];
            [rightBgView addSubview:rightTitle];
            rightTitle.sd_layout.leftSpaceToView(rightView, 0).centerYEqualToView(rightView).heightIs(16);
            [rightTitle setSingleLineAutoResizeWithMaxWidth:200];
            [rightBgView setupAutoWidthWithRightView:rightTitle rightMargin:0];
            rightTF = [UITextField new];
            rightTF.delegate = self;
            [rightTF addTarget:self
                          action:@selector(textFieldDidChangeValue:)
                forControlEvents:UIControlEventEditingChanged];

            rightTF.tag = 81;
            rightTF.textAlignment = NSTextAlignmentCenter;
            if (self.myPlanType==2) {
                //                NSInteger moneyTk = [self.cardInfoModel.FirstMoney integerValue];
                if (_tmpCellTFArr.count<2) {
                    [_tmpCellTFArr addObject:self.cardInfoModel.firstMoney];
                }else{
//                    [_tmpCellTFArr replaceObjectAtIndex:1 withObject:self.cardInfoModel.RepayMoney];
                }
                
                _planMoney = [self.cardInfoModel.firstMoney integerValue];
                //                rightTF.text = [NSString stringWithFormat:@"%.ld",(NSInteger)_planMoney];
                [_paraDic setObject:[NSString stringWithFormat:@"%.ld",(NSInteger)_planMoney] forKey:@"BuysCount"];//计划本金参数
            }
            rightTF.keyboardType = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:rightTF];
            rightTF.sd_layout.leftSpaceToView(vline1, 10).centerYEqualToView(leftTF).heightIs(30).rightEqualToView(cell.contentView);
            if(_tmpCellTFArr.count ==2){
                leftTF.text = _tmpCellTFArr[indexPath.row];
                rightTF.text = _tmpCellTFArr[indexPath.row+1];
            }
            UIView * hline = [UIView new];
            hline.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.94 alpha:1];
            [cell.contentView addSubview:hline];
            hline.sd_layout.leftEqualToView(cell.contentView).rightEqualToView(cell.contentView).topSpaceToView(leftTF, 10).heightIs(1);
            _newLab = [UILabel new];
            _newLab.text = _newsLabStr.length>0?_newsLabStr: @"计划定金不能低于计划总额的%5";
            _newLab.textColor = [UIColor colorWithHue:0.67 saturation:0.03 brightness:0.8 alpha:1];
            _newLab.font = Font(15);
            [cell.contentView addSubview:_newLab];
            _newLab.sd_layout.leftSpaceToView(cell.contentView, AUTO(10)).rightSpaceToView(cell.contentView, AUTO(10)).topSpaceToView(hline, 10).autoHeightRatio(0);
            UIView * botView = [UIView new];
            botView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.94 alpha:1];
            [cell.contentView addSubview:botView];
            botView.sd_layout.leftEqualToView(cell.contentView).bottomEqualToView(cell.contentView).heightIs(10).rightEqualToView(cell.contentView);
        }else if(indexPath.section ==3){
            cell = [[JNCardSDKNewRepayPlanCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"notextfield"];
            if (indexPath.row == 0) {
                cell.arrowImgView.hidden = YES;
            }
            cell.inputTF.tag = 82;
            cell.titleLab.text = _titleArr[indexPath.section][indexPath.row];
            if (_calendarBgView==nil) {
                _calendarBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 45, IPHONE_WIDTH, 300)];
                [cell.contentView addSubview:_calendarBgView];
                [self createCalendar];
            }else{
                [cell.contentView addSubview:_calendarBgView];
            }
        }else{
            cell = [[JNCardSDKNewRepayPlanCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?165:section==1?kTopHeight+AUTO(20):section==2?60:44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?0.01:section==1?10:section==2?0.01:AUTO(100);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==3) {
        return 344;
    }else if(indexPath.section == 0){
        return 136;
    }else{
        return 0.01;
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==3) {
        UIView * footView = [UIView new];
        UIButton * preViewBtn = [UIButton new];
        preViewBtn.backgroundColor = ThemeColor;
        preViewBtn.layer.masksToBounds = YES;
        preViewBtn.layer.cornerRadius = AUTO(5); 
        preViewBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        //修改了需求，该按钮先显示为“下一步”然后等到后台返回planid才显示“预览计划列表”
        [preViewBtn setTitle:@"下一步,预览计划" forState:0];
        [preViewBtn addTarget:self action:@selector(preViewBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [footView addSubview:preViewBtn];
        preViewBtn.sd_layout.leftSpaceToView(footView, AUTO(20)).rightSpaceToView(footView, AUTO(20)).topSpaceToView(footView, AUTO(27)).bottomSpaceToView(footView, AUTO(25));
        return footView;
    }else{
        return nil;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}
#pragma mark 事件触发
-(void)preViewBtnClick{
    NSString * typeStr = _paraDic[@"BackType"];
    if (_seletedBtn == nil||typeStr.length==0) {
        [JNCardSDKMytools warnText:@"请选择消费模式" status:JNCardSDKError];
        return;
    }
    NSString * tmpCount = @"";
    if (_tmpCellTFArr.count==2) {
        tmpCount = _tmpCellTFArr[1];
    }else{
        tmpCount = _paraDic[@"BuysCount"];
    }
    if (tmpCount.length==0) {
        [JNCardSDKMytools warnText:@"计划本金不能为空" status:JNCardSDKError];
        return;
    }
    if (_userChoseCount<_choseMinCount) {
        [JNCardSDKMytools warnText:@"选择天数过少" status:JNCardSDKError];
        return;
    }
    if (_backPlanid.length>0||self.myPlanType==2) {
        //发送修改计划请求
        [self changePlanReSendRequest];
    }else{
        //发送生成计划请求
        [SVProgressHUD show];
        NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_XINCREATE_REPAYPLAN];
        [_paraDic setObject:_choseDayList forKey:@"DateList"];
        [_paraDic setObject:self.myPlanType==2?self.cardInfoModel.userCreditCardId :self.bankCardID forKey:@"UserCreditCardId"];
        [_paraDic setObject:[JNCardSDKMytools dateToDateStr:[JNCardSDKMytools dateStrToDate:_billDateStr]] forKey:@"BillDate"];
        [_paraDic setObject:[JNCardSDKMytools dateToDateStr:[JNCardSDKMytools dateStrToDate:_payDateStr]] forKey:@"DueDate"];
        [_paraDic setObject:_TheKey forKey:@"TheKey"];
        [_paraDic setObject:@"1" forKey:@"OpenUnion"];//新增参数，表示要开启银联
        NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
        [_paraDic addEntriesFromDictionary:p];
        [JNCardSDKMytools requestStringByPostUrl:urlStr parameter:_paraDic success:^(NSDictionary *backDic) {
            [SVProgressHUD dismiss];
            if ([backDic[@"status"]isEqualToString:@"1"]) {
                [JNCardSDKMytools warnText:@"生成计划成功" status:JNCardSDKSuccess];
                JNCardSDKPreViewPlanVC * vc = [JNCardSDKPreViewPlanVC new];
                vc.planID = backDic[@"planid"];
                vc.myTitle = @"计划预览";
                vc.handMoney =  [NSString stringWithFormat:@"%.2f",[backDic[@"money"] floatValue]];
                vc.saleMoney = [NSString stringWithFormat:@"%ld",[backDic[@"salemoney"] integerValue]];
                vc.salenum = [NSString stringWithFormat:@"%ld",[backDic[@"salenum"] integerValue]];
                vc.planMoney = self->_paraDic[@"TotalMoney"];
                vc.planDingjin =self->_paraDic[@"BuysCount"];
                vc.bankNum = self.bankNum;
                vc.bankCardID = self.cardInfoModel.userCreditCardId;
                vc.backPlanIDBlock = ^(NSString *backPlanId) {
                    self->_backPlanid = backPlanId;
                };
                [self.navigationController pushViewController:vc animated:YES];
            }else if ([backDic[@"status"]isEqualToString:@"2"]){
                JNCardSDKMyWebView * w = [JNCardSDKMyWebView new];
                NSString * url = backDic[@"msg"];
                if([JNCardSDKMytools IsChinese:url]){
                    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                }
                [w setSuccessBlock:^{
                    [[JNCardSDKMytools currentViewController].navigationController popViewControllerAnimated:YES];
                }];
                w.currentWebUrl = url;
                [self.navigationController pushViewController:w animated:YES];
            }else{
                self->_planMoney = [JNCardSDKReuserFile calculateMinPayMoneyWithTotalMoney:self->_paraDic[@"TotalMoney"]];
                [SVProgressHUD dismiss];
                [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
            }
        } fail:^(NSError *error) {
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
        }];
    }
    
}
//修改计划再发送
-(void)changePlanReSendRequest{
    [SVProgressHUD show];
    NSString * urlStr = [NSString stringWithFormat:@"%@?UserRepayPlayId=%@",[JNCardSDKWorkUrl returnURL:Interface_For_XINCREATE_REPAYPLAN],_backPlanid.length==0?self.repayPlayId:_backPlanid];
    [_paraDic setObject:_choseDayList forKey:@"DateList"];
    [_paraDic setObject:self.cardInfoModel.userCreditCardId forKey:@"UserCreditCardId"];
    [_paraDic setObject:[JNCardSDKMytools dateToDateStr:[JNCardSDKMytools dateStrToDate:_billDateStr]] forKey:@"BillDate"];
    [_paraDic setObject:[JNCardSDKMytools dateToDateStr:[JNCardSDKMytools dateStrToDate:_payDateStr]] forKey:@"DueDate"];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [_paraDic addEntriesFromDictionary:p];
    [JNCardSDKMytools requestStringByPostUrl:urlStr parameter:_paraDic success:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:@"修改计划成功" status:JNCardSDKSuccess];
            JNCardSDKPreViewPlanVC * vc = [JNCardSDKPreViewPlanVC new];
            vc.planID = backDic[@"planid"];
            vc.myTitle = @"计划预览";
            vc.bankCardID = self.cardInfoModel.userCreditCardId ;
            vc.handMoney =  [NSString stringWithFormat:@"%.2f",[backDic[@"money"] floatValue]];
            vc.saleMoney = [NSString stringWithFormat:@"%ld",[backDic[@"salemoney"] integerValue]];
            vc.bankNum = self.bankNum;
            vc.planMoney = self->_paraDic[@"TotalMoney"];
            vc.planDingjin =self->_paraDic[@"BuysCount"];
            vc.salenum = [NSString stringWithFormat:@"%ld",[backDic[@"salenum"] integerValue]];
            vc.backPlanIDBlock = ^(NSString *backPlanId) {
                self->_backPlanid = backPlanId;
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}
-(void)modelBtnClick:(UIButton*)sender{
    if (_planTotalMoney==0) {
        [JNCardSDKMytools warnText:@"计划总额不能为空" status:JNCardSDKError];
        return;
    }
    NSString * busyCus = _paraDic[@"BuysCount"];
    if (busyCus.length==0) {
        [JNCardSDKMytools warnText:@"计划本金不能为空" status:JNCardSDKError];
        return;
    }
    [sender becomeFirstResponder];
    if (sender != _seletedBtn) {
        UIImageView * imgView = [_seletedBtn viewWithTag:_seletedBtn.tag+10];
        imgView.image = Image(@"");
        _seletedBtn.selected = NO;
        _selectBtnTag = sender.tag;
        _seletedBtn = sender;
        sender.selected = !sender.selected ;
        UIImageView * circle = [sender viewWithTag:sender.tag+10];
        if (sender.selected==YES) {
            circle.image = Image(@"完成_new");
            
        }else{
            circle.image = Image(@"");
        }
    }
    NSString * backType = sender.tag==320?@"1":sender.tag == 321?@"2":@"3";
    [_paraDic setObject:backType forKey:@"BackType"];
    _twoBtnSelect.selected = NO;
    if (_twoBtnSelect!=nil) {
        _twoBtnSelect = nil;
    }
    [self createCalendar];
}
-(void)getCanChoiceDayNumber:(NSString*)btnNumber{
    [SVProgressHUD show];
    NSString * urlStr =[NSString stringWithFormat:@"%@",[JNCardSDKWorkUrl returnURL:Interface_For_REPAYPLANTRYCREATE]];
    //    [_paraDic setObject:_choseDayList forKey:@"DateList"];
    
    //    [_paraDic setObject:_billDateStr forKey:@"BillDate"];
    //    [_paraDic setObject:_payDateStr forKey:@"DueDate"];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [_paraDic addEntriesFromDictionary:p];
    [_paraDic setObject:_paraDic[@"TotalMoney"] forKey:@"TotalMoney"];
    [_paraDic setObject:_paraDic[@"BuysCount"] forKey:@"BuysCount"];
    [_paraDic setObject:btnNumber forKey:@"BackType"];
    [_paraDic setObject:_billDateStr forKey:@"BillDate"];
    [_paraDic setObject:_payDateStr forKey:@"DueDate"];
    [_paraDic setObject:@[@"2018-02-08 15:55:25"] forKey:@"DateList"];
    [_paraDic setObject:self.cardInfoModel.userCreditCardId forKey:@"UserCreditCardId"];
    [JNCardSDKMytools requestStringByPostUrl:urlStr parameter:_paraDic success:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            //            NSMutableDictionary*dataList;
            //            backDic[@"data"];
            self->_choseMinCount=[backDic[@"data"][@"MinDays"] integerValue];
            self->_choseMaxCount=[backDic[@"data"][@"MaxDays"] integerValue];
            self->_TheKey=[NSString stringWithFormat:@"%@",backDic[@"data"][@"TheKey"]];
            self->_canchoseLab.text = [NSString stringWithFormat:@"建议还款计划天数(%ld-%ld)",self->_choseMinCount,self->_choseMaxCount];
            [SVProgressHUD dismiss];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}

-(void)wenhaoBtnClick{
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"温馨提示" andMessage:@"消费1：还款1次，固定消费1次\n消费1-2：还款1次，随机消费1-2次\n消费1-3：还款1次，随机消费1-3次\n\n注：还款1次配置消费次数。"];
    [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
    }];
    [alertView show];
}
-(void)twoBtnClick:(UIButton *)sender{
    NSString * total = _paraDic[@"TotalMoney"];
    if (total.length==0) {
        [JNCardSDKMytools warnText:@"计划总额不能为空" status:JNCardSDKError];
        return;
    }
    NSString * busyCus = _paraDic[@"BuysCount"];
    if (busyCus.length==0) {
        [JNCardSDKMytools warnText:@"计划本金不能为空" status:JNCardSDKError];
        return;
    }
    if(_seletedBtn == nil){
        [JNCardSDKMytools warnText:@"请选择消费模式" status:JNCardSDKError];
        return;
    }
    sender.selected = !sender.selected;
    if (_twoBtnSelect!=nil) {
        _twoBtnSelect.selected = !_twoBtnSelect.selected;
    }
    _twoBtnSelect = sender;
    if ([sender.titleLabel.text isEqualToString:@"当日执行"]) {
        _isChoseToday = YES;
        [_paraDic setObject:@"1" forKey:@"today"];
    }else{
        _isChoseToday = NO;
        [_paraDic setObject:@"0" forKey:@"today"];
    }
    [_choseDayList removeAllObjects];//清空日期列表
    [_paraDic setObject:@"1" forKey:@"CalcType"];
    NSString * backType = _paraDic[@"BackType"];//原本是点了消费模式就发请求，现在他妈的变成点开始时间发请求
    [self createCalendar];
    [self getCanChoiceDayNumber:backType];
}
-(void)createCalendar{
    _userChoseCount=0;
    if (_calendarBgView.subviews.count!=0) {
        for (UIView * v  in _calendarBgView.subviews) {
            [v removeFromSuperview];
        }
    }
    
    FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, 300)];
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.appearance.todayColor = [UIColor clearColor];
    //                calendar.
    calendar.allowsMultipleSelection = YES;
    [_calendarBgView addSubview:calendar];
}
#pragma mark textfield代理
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    for (int i =0 ; i<3; i++){
        UIButton * btn = [self.view viewWithTag:320+i];
        btn.selected=NO;
        UIImageView * imgView = [_seletedBtn viewWithTag:_seletedBtn.tag+10];
        imgView.image = Image(@"");
    }
    _seletedBtn=nil;
    
    NSString * TotalMoney = _paraDic[@"TotalMoney"];
    if (textField.tag == 80&&[TotalMoney integerValue]<1000&&textField.text.length>0) {
        [JNCardSDKMytools warnText:@"计划总额不能低于1000元" status:JNCardSDKError];
        return;
    }
    if (textField.tag==81&&TotalMoney.length==0) {
        [JNCardSDKMytools warnText:@"请先输入计划还款" status:JNCardSDKError];
        textField.text = @"";
        [textField resignFirstResponder];
    }
}
-(void)textField1TextChange:(UITextField *)textField{//这是计划总额的监听
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    NSInteger tmpMoney = [JNCardSDKReuserFile calculateMinPayMoneyWithTotalMoney:textField.text];
    if (tmpMoney>single.MaxPayAmount) {
        _newLab.text = [NSString stringWithFormat:@"计划定金不能大于%.2f，请重新输入！",single.MaxPayAmount];
        rightTF.userInteractionEnabled = NO;
    }else{
        _newLab.text = [NSString stringWithFormat:@"计划定金不能小于%ld",tmpMoney];
        _minPlanMoney = tmpMoney;
        rightTF.userInteractionEnabled = YES;
    }
}
- (void)textFieldDidChangeValue:(UITextField*)textField{
    //计划本金有更改，就要把三个按钮清空
    UIImageView * imgView = [_seletedBtn viewWithTag:_seletedBtn.tag+10];
    imgView.image = Image(@"");
    [_paraDic setObject:@"" forKey:@"BackType"];
    _twoBtnSelect.selected = NO;
    if (_twoBtnSelect!=nil) {
        _twoBtnSelect = nil;
    }
    _seletedBtn.selected = NO;
    if (_seletedBtn != nil) {
        _seletedBtn = nil;
    }
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    if ([textField.text floatValue]>single.MaxPayAmount){
        _newLab.text = [NSString stringWithFormat:@"计划定金不能大于%.2f，请重新输入！",single.MaxPayAmount];
        textField.text= @"";
        return;
    }else{
        if ([textField.text floatValue]<single.MinPlanMoney) {
            _newsLabStr = [NSString stringWithFormat:@"计划定金不能小于%.0f，请重新输入！",single.MinPlanMoney];
        }else{
            if (_minPlanMoney<single.MinPlanMoney) {
                _newsLabStr = [NSString stringWithFormat:@"计划定金不能低于：%.0f",single.MinPlanMoney];
            }else{
                _newsLabStr = [NSString stringWithFormat:@"计划定金不能低于：%ld",_minPlanMoney];
            }
            
            rightTF.userInteractionEnabled = YES;
        }
        _newLab.text = _newsLabStr;
    }
    [self dealWithPlanMoney:textField isMonitor:YES];
}
-(void)dealWithPlanMoney:(UITextField *)textField isMonitor:(BOOL)m{
    if (_planTotalMoney==0) {
        textField.text = @"";
        [JNCardSDKMytools warnText:@"计划总额不能为空" status:JNCardSDKError];
        return;
    }
    if (m==NO) {
        NSString * alertStr= [NSString stringWithFormat:@"计划定金不能低于最小定金%ld元",_planMoney];
        if ([textField.text integerValue]<_planMoney) {
            [self showAlertWithTitle:@"温馨提示" message:alertStr appearanceProcess:^(SWAlertController * _Nullable alertMaker) {
                alertMaker.addActionDefaultTitle(@"确定",[UIColor blackColor]);
            } actionsBlock:^(NSInteger buttonIndex, UIAlertAction * _Nullable action, SWAlertController * _Nullable alertSelf) {
                textField.text = @"";
            }];
        }else{
            _planMoney = [textField.text integerValue];
            [_paraDic setObject:textField.text forKey:@"BuysCount"];//计划本金参数
            if (_tmpCellTFArr.count==1) {
                [_tmpCellTFArr addObject:textField.text];
            }else{
                [_tmpCellTFArr replaceObjectAtIndex:1 withObject:textField.text];
            }
        }
    }else{
        _planMoney = [textField.text integerValue];
        [_paraDic setObject:textField.text forKey:@"BuysCount"];//计划本金参数
        if (_tmpCellTFArr.count==1) {
            [_tmpCellTFArr addObject:textField.text];
        }else{
            [_tmpCellTFArr replaceObjectAtIndex:1 withObject:textField.text];
        }
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text containsString:@"."]) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"." withString:@""];
    }
    if (textField.tag == 80) {
        _planTotalMoney = [textField.text floatValue];
        [_paraDic setObject:textField.text forKey:@"TotalMoney"];//计划总额参数
        
        _planMoney = [JNCardSDKReuserFile calculateMinPayMoneyWithTotalMoney:textField.text];
        if (_tmpCellTFArr.count==0) {
            [_tmpCellTFArr addObject:textField.text];
            [_tmpCellTFArr addObject:@""];
        }else{
            [_tmpCellTFArr replaceObjectAtIndex:0 withObject:textField.text];
        }
    }else if (textField.tag == 81){
        if (textField.text.length==0) {
            return;
        }else{
            if ([textField.text integerValue]<_minPlanMoney) {
                NSString * sr = [NSString stringWithFormat:@"计划定金不能少于%ld，请重新输入",_minPlanMoney];
                textField.text = @"";
                [JNCardSDKMytools warnText:sr status:JNCardSDKError];
                return;
            }
            [self dealWithPlanMoney:textField isMonitor:NO];
        }
    }
}
#pragma mark 日历代理

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
//121版本的在这有对_TheKey的长度做判断，如果是空就返回@"请选择消费模式" ，以下代码是在121以后修改的
    if (_seletedBtn==nil) {
        [JNCardSDKMytools warnText:@"请选择消费模式" status:JNCardSDKError];
        return NO;
    }else if (_twoBtnSelect==nil){
        [JNCardSDKMytools warnText:@"请选择开始时间" status:JNCardSDKError];
        return NO;
    }
    if (_TheKey.length==0||_TheKey==nil) {
        [JNCardSDKMytools warnText:@"出现异常，请重新选择开始时间" status:JNCardSDKError];
        _twoBtnSelect.selected = NO;
        _twoBtnSelect = nil;
        return NO;
    }
    UITextField * rightTF = [self.view viewWithTag:81];
    if (rightTF.text.length == 0) {
        [JNCardSDKMytools warnText:@"计划定金不能为空" status:JNCardSDKError];
        return NO;
    }

    date = [date dateByAddingTimeInterval:8 * 60 * 60];
    BOOL reg = [JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:YES];
    if (reg == NO) {
        return NO;
    }
    
    NSDictionary * backDict = [self returnLegalDate];
    NSDate *twoDayDate = backDict[@"twoDay"];
    NSDate *twoDayBeforePayDate = backDict[@"before"];
    NSDate *twoDayAfterBillDate = backDict[@"after"];
    NSTimeInterval time = [backDict[@"time"] doubleValue];
    NSDate *todayDate = backDict[@"todayDate"];
    if ([self.billDay intValue]<[self.payDay intValue]) {
        //当月
        if([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1){
            //日历的日期大于当天日期且小于还款日期，可选
            _userChoseCount ++;
            BOOL bl = [self verifyChouseCountIsInRange];
            if (bl == YES) {
                [_choseDayList addObject:[JNCardSDKMytools dateToDateStr:date]];
                return YES;
            }else{
                [JNCardSDKMytools warnText:@"选择天数超出范围" status:JNCardSDKError];
                return NO;
            }
        }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
            //因为要根据开始时间默认选择那个日子，所以要让点击事件不可点
            return NO;
        }else{
            if(_todayCanMake==YES){
                if ([date isToday]) {
                    if (_isChoseToday) {
                        _userChoseCount ++;
                        [_choseDayList addObject:[JNCardSDKMytools dateToDateStr:date]];
                        return YES;
                    }else{
                        return NO;
                    }
                }else{
                    return NO;
                }
            }else{
                return NO;
            }
        }
    }else{
        //跨月
        if ([JNCardSDKMytools compareOneDay:twoDayDate withAnotherDay:twoDayAfterBillDate]==1) {
            //表示在未跨的那个月，比如账单日7月25，当天7月28，还款8月9
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1) {
                _userChoseCount ++;
                BOOL bl = [self verifyChouseCountIsInRange];
                if (bl == YES) {
                    [_choseDayList addObject:[JNCardSDKMytools dateToDateStr:date]];
                    return YES;
                }else{
                    [JNCardSDKMytools warnText:@"选择天数超出范围" status:JNCardSDKError];
                    return NO;
                }
            }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
                //因为要根据开始时间默认选择那个日子，所以要让点击事件不可点
                return NO;
            }else{
                if(_todayCanMake==YES){
                    if ([date isToday]) {
                        if (_isChoseToday) {
                            _userChoseCount ++;
                            [_choseDayList addObject:[JNCardSDKMytools dateToDateStr:date]];
                            return YES;
                        }else{
                            return NO;
                        }
                    }else{
                        return NO;
                    }
                }else{
                    return NO;
                }
            }
        }else{
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayBeforePayDate]==1) {
                return NO;
            }else{
                //因为账单在这个页面一定存在，所以除去上面的那个情况，当天日期一定在8月9之前7月31之后，要做的就是限制8月9之后显示为黑色的bug
                NSInteger  tmpMonth = [[self getTheCorrectNum:[_payDateStr substringWithRange:NSMakeRange(5, 2)]] intValue];
                if (tmpMonth == 1) {
                    tmpMonth = 12;
                }else{
                    tmpMonth = tmpMonth - 1;
                }
                NSString * tmpMonthStr = [NSString stringWithFormat:@"%ld",tmpMonth];
                NSString * newPayDateStr = [_payDateStr stringByReplacingCharactersInRange:NSMakeRange(5, 2) withString:tmpMonthStr];
                NSDate * newPayDate  = [self dateStrBecomeNewDate:newPayDateStr andFormat:@"MM-dd"];
                NSDate * twoDayBeforNewPayDate = [NSDate dateWithTimeInterval:-time sinceDate:newPayDate];
                
                NSString * todayDateStr = [JNCardSDKMytools dateToDateStr:todayDate];
                NSString * todayDay = [self getTheCorrectNum:[todayDateStr substringWithRange:NSMakeRange(8, 2)]];
                if ([todayDay intValue]<[self.payDay intValue]) {
                    if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforNewPayDate withAnotherDay:date]==1) {
                        _userChoseCount ++;
                        BOOL bl = [self verifyChouseCountIsInRange];
                        if (bl == YES) {
                            [_choseDayList addObject:[JNCardSDKMytools dateToDateStr:date]];
                            return YES;
                        }else{
                            [JNCardSDKMytools warnText:@"选择天数超出范围" status:JNCardSDKError];
                            return NO;
                        }
                    }else{
                        return NO;
                    }
                }else{
                    return NO;
                }
            }
        }
    }
}
//验证用户输入的天数是否在可选天数范围内
-(BOOL)verifyChouseCountIsInRange{
    if (_userChoseCount>_choseMaxCount) {//_choseMaxCount 改动+1
        _userChoseCount--;
        return NO;
    }else{
        return YES;
    }
}
//返回可选范围和不可选范围的颜色
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    date = [date dateByAddingTimeInterval:8 * 60 * 60];
    BOOL reg = [JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO];
    if (reg == NO) {
        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
    }
    NSDictionary * backDict = [self returnLegalDate];
    NSDate *twoDayDate = backDict[@"twoDay"];
    NSDate *twoDayBeforePayDate = backDict[@"before"];
    NSDate *twoDayAfterBillDate = backDict[@"after"];
    NSTimeInterval time = [backDict[@"time"] doubleValue];
    NSDate *todayDate = backDict[@"todayDate"];
    if ([self.billDay intValue]<[self.payDay intValue]) {
        //当月
        if([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1){
            //日历的日期大于当天日期且小于还款日期，可选
            _calendarDays ++;
            return [UIColor blackColor];
        }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
            //date是twoDayDate，如果twoDayDate不是今天，那么一定是明天，就要执行else
            NSString * selDate = [JNCardSDKMytools dateToDateStr:date];
            BOOL isIn = NO;
            for (NSString * datpstr in _choseDayList) {
                if([selDate isEqualToString:datpstr]){
                    isIn = YES;
                    break;
                }
            }
            if (isIn==YES) {
                return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
            }
            if ([date isToday]) {
                if (_isChoseToday) {
                    if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }else{
                        _userChoseCount ++;
                        [_choseDayList addObject:selDate];
                        return [UIColor whiteColor];
                    }
                }else{
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }
            }else{
                if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }else{
                    _userChoseCount ++;
                    [_choseDayList addObject:selDate];
                    return [UIColor whiteColor];
                }
            }
        }else{
            if(_todayCanMake==YES){
                if ([date isToday]) {
                    if (_isChoseToday) {
                        return [UIColor blackColor];
                    }else{
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }
                }else{
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }
            }else{
                return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
            }
        }
    }else{
        //跨月
        if ([JNCardSDKMytools compareOneDay:twoDayDate withAnotherDay:twoDayAfterBillDate]==1) {
            //表示在未跨的那个月，比如账单日7月25，当天7月28，还款8月9
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1) {
                _calendarDays ++;
                return [UIColor blackColor];
            }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
                //date是twoDayDate，如果twoDayDate不是今天，那么一定是明天，就要执行else
                NSString * selDate = [JNCardSDKMytools dateToDateStr:date];
                BOOL isIn = NO;
                for (NSString * datpstr in _choseDayList) {
                    if([selDate isEqualToString:datpstr]){
                        isIn = YES;
                        break;
                    }
                }
                if (isIn==YES) {
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }
                if ([date isToday]) {
                    if (_isChoseToday) {
                        if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                            return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                        }else{
                            _userChoseCount ++;
                            [_choseDayList addObject:selDate];
                            return [UIColor whiteColor];
                        }
                    }else{
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }
                }else{
                    if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }else{
                        _userChoseCount ++;
                        [_choseDayList addObject:selDate];
                        return [UIColor whiteColor];
                    }
                }
            }else{
                if(_todayCanMake==YES){
                    if ([date isToday]) {
                        if (_isChoseToday) {
                            return [UIColor blackColor];
                        }else{
                            return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                        }
                    }else{
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }
                }else{
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }
            }
        }else{
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayBeforePayDate]==1) {
                return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
            }else{
                //因为账单在这个页面一定存在，所以除去上面的那个情况，当天日期一定在8月9之前7月31之后，要做的就是限制8月9之后显示为黑色的bug
                NSInteger  tmpMonth = [[self getTheCorrectNum:[_payDateStr substringWithRange:NSMakeRange(0, 2)]] intValue];
                if (tmpMonth == 1) {
                    tmpMonth = 12;
                }else{
                    tmpMonth = tmpMonth - 1;
                }
                NSString * tmpMonthStr = [NSString stringWithFormat:@"%ld",tmpMonth];
                NSString * newPayDateStr = [_payDateStr stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:tmpMonthStr];
                NSDate * newPayDate  = [self dateStrBecomeNewDate:newPayDateStr andFormat:@"MM-dd"];
                NSDate * twoDayBeforNewPayDate = [NSDate dateWithTimeInterval:-time sinceDate:newPayDate];
                
                NSString * todayDateStr = [JNCardSDKMytools dateToDateStr:todayDate];
                NSString * todayDay = [self getTheCorrectNum:[todayDateStr substringWithRange:NSMakeRange(8, 2)]];
                if ([todayDay intValue]<[self.payDay intValue]) {
                    if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforNewPayDate withAnotherDay:date]==1) {
                        _calendarDays ++;
                        return [UIColor blackColor];
                    }else{
                        return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                    }
                }else{
                    return [UIColor colorWithHue:0 saturation:0 brightness:0.67 alpha:1];
                }
            }
        }
    }
}
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date{
    date = [date dateByAddingTimeInterval:8 * 60 * 60];
    NSDictionary * backDict = [self returnLegalDate];
    NSDate *twoDayDate = backDict[@"twoDay"];
    NSDate *twoDayBeforePayDate = backDict[@"before"];
    NSDate *twoDayAfterBillDate = backDict[@"after"];
    NSTimeInterval time = [backDict[@"time"] doubleValue];
    NSDate *todayDate = backDict[@"todayDate"];
    if ([self.billDay intValue]<[self.payDay intValue]) {
        //当月
        if([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1){
            //日历的日期大于当天日期且小于还款日期，可选
            _calendarDays ++;
            return [UIColor whiteColor];
        }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
            //date是twoDayDate，如果twoDayDate不是今天，那么一定是明天，就要执行else
            if ([date isToday]) {
                if (_isChoseToday) {
                    if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                        return [UIColor whiteColor];
                    }else{
                        return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
                    }
                }else{
                    return [UIColor whiteColor];
                }
            }else{
                return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
            }
        }else{
            if(_todayCanMake==YES){
                if ([date isToday]) {
                    if (_isChoseToday) {
                        if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                            return [UIColor whiteColor];
                        }else{
                            return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
                        }
                    }else{
                        return [UIColor whiteColor];
                    }
                }else{
                    return [UIColor whiteColor];
                }
            }else{
                return [UIColor whiteColor];
            }
        }
    }else{
        //跨月
        if ([JNCardSDKMytools compareOneDay:twoDayDate withAnotherDay:twoDayAfterBillDate]==1) {
            //表示在未跨的那个月，比如账单日7月25，当天7月28，还款8月9
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforePayDate withAnotherDay:date]==1) {
                return [UIColor whiteColor];
            }else if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==0){
                //date是twoDayDate，如果twoDayDate不是今天，那么一定是明天，就要执行else
                if ([date isToday]) {
                    if (_isChoseToday) {
                        if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                            return [UIColor whiteColor];
                        }else{
                            return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
                        }
                    }else{
                        return [UIColor whiteColor];
                    }
                }else{
                    if ([JNCardSDKCalculaterBillPayDay holidayListCanChose:date isFromSelect:NO]==NO) {
                        return [UIColor whiteColor];
                    }else{
                        return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
                    }
                }
            }else{
                if(_todayCanMake==YES){
                    if ([date isToday]) {
                        if (_isChoseToday) {
                            return [UIColor colorWithHue:0.58 saturation:0.84 brightness:0.88 alpha:1];
                        }else{
                            return [UIColor whiteColor];
                        }
                    }else{
                        return [UIColor whiteColor];
                    }
                }else{
                    return [UIColor whiteColor];
                }
            }
        }else{
            if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayBeforePayDate]==1) {
                return [UIColor whiteColor];
            }else{
                //因为账单在这个页面一定存在，所以除去上面的那个情况，当天日期一定在8月9之前7月31之后，要做的就是限制8月9之后显示为黑色的bug
                NSInteger  tmpMonth = [[self getTheCorrectNum:[_payDateStr substringWithRange:NSMakeRange(0, 2)]] intValue];
                if (tmpMonth == 1) {
                    tmpMonth = 12;
                }else{
                    tmpMonth = tmpMonth - 1;
                }
                NSString * tmpMonthStr = [NSString stringWithFormat:@"%ld",tmpMonth];
                NSString * newPayDateStr = [_payDateStr stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:tmpMonthStr];
                NSDate * newPayDate  = [self dateStrBecomeNewDate:newPayDateStr andFormat:@"MM-dd"];
                NSDate * twoDayBeforNewPayDate = [NSDate dateWithTimeInterval:-time sinceDate:newPayDate];
                
                NSString * todayDateStr = [JNCardSDKMytools dateToDateStr:todayDate];
                NSString * todayDay = [self getTheCorrectNum:[todayDateStr substringWithRange:NSMakeRange(8, 2)]];
                if ([todayDay intValue]<[self.payDay intValue]) {
                    if ([JNCardSDKMytools compareOneDay:date withAnotherDay:twoDayDate]==1&&[JNCardSDKMytools compareOneDay:twoDayBeforNewPayDate withAnotherDay:date]==1) {
                        return [UIColor whiteColor];
                    }else{
                        return [UIColor whiteColor];
                    }
                }else{
                    return [UIColor whiteColor];
                }
            }
        }
    }
}
//将日期改变不同的格式返回
-(NSString*)dateBecomeNewStr:(NSDate*)date andFormat:(NSString*)format{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:format];
    NSString * dateStr = [formatter stringFromDate:date];
    return dateStr;
}
- (void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    //重复选择一个日期就删掉
    date = [date dateByAddingTimeInterval:8 * 60 * 60];
    NSString * datestr = [JNCardSDKMytools dateToDateStr:date];
    for (NSString * dapin in _choseDayList) {
        if ([datestr isEqualToString:dapin]) {
            _userChoseCount --;
            [_choseDayList removeObject:dapin];
            break;
        }else{
            
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section ==0) {
        UIView*allView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, 165)];
        allView.backgroundColor=[UIColor clearColor];
        [self createHeaderSectionZeroWithView:allView];
        return allView;
    }else if(section == 2){
        if (_twoBtnSelect==nil) {//防止滑出屏幕又回来选中的按钮消失
            _secHeadTwo = [UIView new];
            _secHeadTwo.backgroundColor = [UIColor whiteColor];
            [self createHeadViewSectionTwoWithView:_secHeadTwo];
            return _secHeadTwo;
        }else{
            return _secHeadTwo;
        }
    }else if(section == 3){
        UIView * headView = [UIView new];
        [self createHeaderSectionThrWithView:headView];
        return headView;
    }else{
        UIView * headView = [UIView new];
        headView.backgroundColor = [UIColor whiteColor];
        [self createHeaderSectionOneWithView:headView];
        return headView;
    }
}
-(void)createHeaderSectionOneWithView:(UIView*)headView{
    UILabel * titleLab = [UILabel new];
    titleLab.text = @"消费模式";
    titleLab.font = Font(17);
    [headView addSubview:titleLab];
    titleLab.sd_layout.leftSpaceToView(headView, AUTO(10)).topSpaceToView(headView, AUTO(10)).heightIs(20);
    [titleLab setSingleLineAutoResizeWithMaxWidth:200];
    UIButton * wenhaoBtn = [UIButton new];
    [wenhaoBtn addTarget:self action:@selector(wenhaoBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [wenhaoBtn setImage:Image(@"问_newplan") forState:0];
    [headView addSubview:wenhaoBtn];
    wenhaoBtn.sd_layout.leftSpaceToView(titleLab, 10).centerYEqualToView(titleLab).heightIs(AUTO(17)).widthEqualToHeight();
    
    NSArray * textArr = @[@"消费1",@"消费1-2",@"消费1-3"];
    NSMutableArray * btnArr = [NSMutableArray new];
    for (int i =0 ; i<textArr.count; i++) {
        UIButton * btn = [UIButton new];
        btn.selected = NO;
        btn.tag = 320+i;
        [btn addTarget:self action:@selector(modelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:btn];
        UIImageView * imgView = [UIImageView new];
        imgView.layer.masksToBounds = YES;
        imgView.layer.cornerRadius = 10;
        imgView.tag = 330+i;
        imgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        if (i+320 == _selectBtnTag) {
            imgView.image = Image(@"完成_new");
            _seletedBtn = btn;
        }
        imgView.layer.borderWidth = 1;
        [btn addSubview:imgView];
        imgView.sd_layout.leftSpaceToView(btn, 10).centerYEqualToView(btn).widthIs(20).heightIs(20);
        UILabel * lab = [UILabel new];
        lab.text = textArr[i];
        lab.font = Font(17);
        [btn addSubview:lab];
        lab.sd_layout.leftSpaceToView(imgView, 10).centerYEqualToView(imgView).heightIs(20);
        [lab setSingleLineAutoResizeWithMaxWidth:200];
        [btnArr addObject:btn];
    }
    UIButton * lastBtn = nil;
    for (int i =0 ; i<textArr.count; i++) {
        UIButton * newBtn = btnArr[i];
        newBtn.sd_layout.leftSpaceToView(lastBtn==nil?headView:lastBtn, 0).topSpaceToView(titleLab, AUTO(10)).heightIs(44).widthIs(IPHONE_WIDTH/3);
        lastBtn = newBtn;
    }
}
-(void)createHeaderSectionThrWithView:(UIView*)headView{
    _canchoseLab = [UILabel new];
    _canchoseLab.textColor = [UIColor redColor];
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    NSInteger mincount = 1;
    if (_tmpCellTFArr.count==2){
        NSString * tmpcOUNT = _tmpCellTFArr[1];
        if (tmpcOUNT.length==0) {
            tmpcOUNT = @"1";
        }
        NSInteger tmpppp = [tmpcOUNT integerValue];
        mincount = ceil(_planTotalMoney/tmpppp);
    }
    if (single.DailyMaxPays ==1) {
        _choseMinCount = mincount;
        _choseMaxCount = 30;
        _canchoseLab.text = [NSString stringWithFormat:@"建议还款计划天数:%ld天",_choseMinCount];
    }else{
        if (mincount == (NSInteger)_planTotalMoney) {//说明mincount=1
            _canchoseLab.text = @"建议还款计划天数：";
        }else{
            _choseMaxCount = mincount;
            _choseMinCount = ceil(((float)mincount)/single.DailyMaxPays);
            if (_choseMaxCount>0&&_choseMaxCount!=1) {
                _canchoseLab.text = [NSString stringWithFormat:@"建议还款计划天数(%ld-%ld)",_choseMinCount,_choseMaxCount];
            }else{
                _canchoseLab.text = @"建议还款计划天数：";
            }
        }
        
    }
    [headView addSubview:_canchoseLab];
    _canchoseLab.sd_layout.spaceToSuperView(UIEdgeInsetsMake(0, AUTO(10), 0, 0));
}
-(void)createHeadViewSectionTwoWithView:(UIView*)headView{
    UILabel * makeLab = [UILabel labelWithTitle:@"开始时间" color:[UIColor blackColor] fontSize:AUTO(17)];
    [headView addSubview:makeLab];
    makeLab.sd_layout.leftSpaceToView(headView, 10).topSpaceToView(headView, 10).heightIs(16);
    [makeLab setSingleLineAutoResizeWithMaxWidth:200];
    
    UIButton * todayBtn = [UIButton new];
    todayBtn.selected = NO;
    [todayBtn setTitleColor:[UIColor blackColor] forState:0];
    [todayBtn setImage:Image(@"cicleunselect") forState:UIControlStateNormal];
    [todayBtn setImage:Image(@"完成_new") forState:UIControlStateSelected];
    [todayBtn setTitle:@"当日执行" forState:0];
    [todayBtn addTarget:self action:@selector(twoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    if (_todayCanMake==NO) {
        todayBtn.userInteractionEnabled = NO;
        [todayBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    [todayBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    [todayBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -5)];
    todayBtn.titleLabel.font = Font(17);
    [headView addSubview:todayBtn];
    todayBtn.sd_layout.leftSpaceToView(headView, 10).topSpaceToView(makeLab, -5).heightIs(40).widthIs(100);
    
    UIButton * tomorrowBtn = [UIButton new];
    tomorrowBtn.selected = NO;
    [tomorrowBtn addTarget:self action:@selector(twoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [tomorrowBtn setTitleColor:[UIColor blackColor] forState:0];
    [tomorrowBtn setImage:Image(@"cicleunselect") forState:UIControlStateNormal];
    [tomorrowBtn setImage:Image(@"完成_new") forState:UIControlStateSelected];
    [tomorrowBtn setTitle:@"次日执行" forState:0];
    [tomorrowBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    [tomorrowBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -5)];
    tomorrowBtn.titleLabel.font = Font(17);
    [headView addSubview:tomorrowBtn];
    tomorrowBtn.sd_layout.leftSpaceToView(todayBtn, 20).centerYEqualToView(todayBtn).heightIs(45).widthIs(100);
}
-(void)createHeaderSectionZeroWithView:(UIView*)allView{
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, 155)];
    headView.backgroundColor = [UIColor whiteColor];
    UIView *bgView = [[UIView alloc]initWithFrame:Frame(15, 15, IPHONE_WIDTH-30, 120)];
    //        bgView.backgroundColor = [UIColor redColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    bgView.layer.borderColor=[[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1]CGColor];
    bgView.layer.borderWidth = 1;
    [headView addSubview:bgView];
    
    UIView*topView=[UIView new];
    //    topView.backgroundColor=[UIColor colorWithRed:0/255.0 green:142/255.0 blue:255/255.0 alpha:1];
    topView.backgroundColor=[JNCardSDKMytools mostColor:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,self.cardInfoModel.bankImg]]]]];
    topView.layer.cornerRadius = 1;
    [bgView addSubview:topView];
    topView.sd_layout.topEqualToView(bgView).leftEqualToView(bgView).rightEqualToView(bgView).heightIs(50);
    
    UIView * bankImg=[UIView new];
    bankImg.backgroundColor=[UIColor whiteColor];
    bankImg.layer.cornerRadius = 15;
    bankImg.layer.masksToBounds = YES;
    [topView addSubview:bankImg];
    bankImg.sd_layout.leftSpaceToView(topView, AUTO(10)).centerYEqualToView(topView).widthIs(30).heightEqualToWidth();
    
    
    UIImageView * bankImgView= [UIImageView new];
    [bankImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JNCardSDKImageDomain,self.cardInfoModel.bankImg]]];
    [bankImg addSubview:bankImgView];
    bankImgView.sd_layout.topSpaceToView(bankImg, 2).leftSpaceToView(bankImg, 2).bottomSpaceToView(bankImg, 2).rightSpaceToView(bankImg, 2);
    
    UILabel*bankName = [UILabel new];
    bankName.font = [UIFont systemFontOfSize:13];
    bankName.text = self.cardInfoModel.bankName;
    bankName.textColor=[UIColor whiteColor];
    bankName.textAlignment=NSTextAlignmentLeft;
    [topView addSubview:bankName];
    bankName.sd_layout.leftSpaceToView(bankImg, AUTO(15)).centerYEqualToView(topView).widthIs(110).heightIs(20);
    //    [_bankName setSingleLineAutoResizeWithMaxWidth:200];
    
    
    UIView*line=[UIView new];
    line.backgroundColor=[UIColor whiteColor];
    [topView addSubview:line];
    line.sd_layout.topSpaceToView(topView, 12).bottomSpaceToView(topView, 12).widthIs(1).leftSpaceToView(bankName,0).centerYEqualToView(topView);
    
    UILabel*cardNameLabel=[UILabel new];
    cardNameLabel.text=@"尾号:";
    [topView addSubview:cardNameLabel];
    cardNameLabel.textColor=[UIColor whiteColor];
    cardNameLabel.font=[UIFont systemFontOfSize:13];
    cardNameLabel.textAlignment=NSTextAlignmentLeft;
    cardNameLabel.sd_layout.widthIs(50).centerYEqualToView(topView).leftSpaceToView(line, 10).autoHeightRatio(0);
    [cardNameLabel setSingleLineAutoResizeWithMaxWidth:50];
    
    UILabel*cardNumLab = [UILabel new];
    cardNumLab.font = [UIFont systemFontOfSize:13];
    cardNumLab.textAlignment=NSTextAlignmentLeft;
    cardNumLab.textColor=[UIColor whiteColor];
    [topView addSubview:cardNumLab];
    cardNumLab.sd_layout.leftSpaceToView(cardNameLabel, AUTO(0)).widthIs(40).centerYEqualToView(topView).heightIs(20);
    
    UILabel*userNameLab = [UILabel new];
    userNameLab.font = [UIFont systemFontOfSize:13];
    userNameLab.textColor=[UIColor whiteColor];
    [topView addSubview:userNameLab];
    userNameLab.sd_layout.rightSpaceToView(topView,2).centerYEqualToView(topView).widthIs(40).autoHeightRatio(0);
    [userNameLab setSingleLineAutoResizeWithMaxWidth:AUTO(IPHONE_WIDTH/2)];
    [_userNameLab setSingleLineAutoResizeWithMaxWidth:AUTO(IPHONE_WIDTH/2)];
    
    userNameLab.text = [self.cardInfoModel.name stringByReplacingCharactersInRange:NSMakeRange(1, self.cardInfoModel.name.length>3?self.cardInfoModel.name.length-2:1) withString:@"*"];
    cardNumLab.text=[self.cardInfoModel.bankNum substringFromIndex:self.cardInfoModel.bankNum.length-4 ];
    
    UIView*MiddleView=[UIView new];
    //        MiddleView.backgroundColor=[UIColor redColor];
    [bgView addSubview:MiddleView];
    MiddleView.sd_layout.topSpaceToView(topView,1).leftEqualToView(bgView).rightEqualToView(bgView).heightIs(70);
    
    UIView*leftView=[UIView new];
    //        leftView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:leftView];
    leftView.sd_layout.topEqualToView(MiddleView).bottomEqualToView(MiddleView);
    
    UILabel*label_plan_money=[UILabel new];
    label_plan_money.text=@"计划额度";
    label_plan_money.font=[UIFont systemFontOfSize:13];
    label_plan_money.textColor=ColorByHexStr(@"#333333");
    label_plan_money.textAlignment=NSTextAlignmentCenter;
    [leftView addSubview:label_plan_money];
    label_plan_money.sd_layout.topSpaceToView(leftView, 10).centerXEqualToView(leftView).widthIs(70).autoHeightRatio(0);
    UILabel*label_plan_num=[UILabel new];
    label_plan_num.text=self.myPlanType==1?@"0.00":[NSString stringWithFormat:@"%@",self.cardInfoModel.repayMoney];
    label_plan_num.textColor=ColorByHexStr(@"#999999");
    label_plan_num.font=[UIFont systemFontOfSize:13];
    label_plan_num.textAlignment=NSTextAlignmentCenter;
    [leftView addSubview:label_plan_num];
    label_plan_num.sd_layout.topSpaceToView(label_plan_money, 10).widthIs(80).autoHeightRatio(0).centerXEqualToView(leftView);
    
    UIView*firstLine=[UIView new];
    firstLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    [leftView addSubview:firstLine];
    
    
    UIView*centerView=[UIView new];
    //        centerView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:centerView];
    centerView.sd_layout.topEqualToView(MiddleView).bottomEqualToView(MiddleView).leftSpaceToView(leftView, 0);
    
    UILabel*label_ZDday=[UILabel new];
    label_ZDday.text=@"账单日";
    label_ZDday.font=[UIFont systemFontOfSize:13];
    label_ZDday.textAlignment=NSTextAlignmentCenter;
    label_ZDday.textColor=ColorByHexStr(@"#333333");
    [centerView addSubview:label_ZDday];
    label_ZDday.sd_layout.centerXEqualToView(centerView).centerYEqualToView(label_plan_money).widthIs(70).autoHeightRatio(0);
    UILabel*label_ZDday_num=[UILabel new];
    label_ZDday_num.textAlignment=NSTextAlignmentCenter;
    label_ZDday_num.text=[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[NSString stringWithFormat:@"%@月%@号",[_billDateStr substringWithRange:NSMakeRange(5, 2)],[_billDateStr substringWithRange:NSMakeRange(8, 2)]]];
    label_ZDday_num.font=[UIFont systemFontOfSize:13];
    label_ZDday_num.textColor=ColorByHexStr(@"#999999");
    [centerView addSubview:label_ZDday_num];
    label_ZDday_num.sd_layout.centerYEqualToView(label_plan_num).widthIs(80).autoHeightRatio(0).centerXEqualToView(label_ZDday);
    
    UIView*secondLine=[UIView new];
    secondLine.backgroundColor=[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1];
    [centerView addSubview:secondLine];
    secondLine.sd_layout.rightEqualToView(centerView).widthIs(1).topSpaceToView(centerView,15).bottomSpaceToView(centerView,15);
    firstLine.sd_layout.rightEqualToView(leftView).widthIs(1).topSpaceToView(leftView, 15).bottomSpaceToView(leftView, 15);
    
    
    UIView*rightView=[UIView new];
    //        rightView.backgroundColor=[UIColor redColor];
    [MiddleView addSubview:rightView];
    rightView.sd_layout.topEqualToView(MiddleView).leftSpaceToView(centerView, 0).bottomEqualToView(MiddleView);
    
    UILabel*label_repayDay=[UILabel new];
    label_repayDay.text=@"还款日";
    label_repayDay.textColor=ColorByHexStr(@"#333333");
    label_repayDay.font=[UIFont systemFontOfSize:13];
    label_repayDay.textAlignment=NSTextAlignmentCenter;
    [rightView addSubview:label_repayDay];
    label_repayDay.sd_layout.centerXEqualToView(rightView).centerYEqualToView(label_plan_money).widthIs(70).autoHeightRatio(0);
    UILabel*label_repayDay_num=[UILabel new];
    label_repayDay_num.textAlignment=NSTextAlignmentCenter;
    label_repayDay_num.text=[JNCardSDKCalculaterBillPayDay getTheCorrectNum:[NSString stringWithFormat:@"%@月%@号",[ _payDateStr substringWithRange:NSMakeRange(5, 2)],[_payDateStr substringWithRange:NSMakeRange(8, 2)]]];
    label_repayDay_num.font=[UIFont systemFontOfSize:13];
    label_repayDay_num.textColor=ColorByHexStr(@"#999999");
    [rightView addSubview:label_repayDay_num];
    label_repayDay_num.sd_layout.centerYEqualToView(label_plan_num).widthIs(80).autoHeightRatio(0).centerXEqualToView(label_repayDay);
    
    MiddleView.sd_equalWidthSubviews=@[leftView,centerView,rightView];
    
    [allView addSubview:headView];
}
-(NSDictionary*)returnLegalDate{
    NSDate * billDate = [JNCardSDKMytools dateStrToDate:_billDateStr];
    NSDate * payDate = [JNCardSDKMytools dateStrToDate:_payDateStr];
    NSDate * todayDate = [JNCardSDKMytools getCurrentDate];//当天日期
    NSString * todayStr = [JNCardSDKMytools dateToDateStr:todayDate];
    todayStr = [todayStr stringByReplacingCharactersInRange:NSMakeRange(11, 8) withString:@"00:00:00"];
    todayDate = [JNCardSDKMytools dateStrToDate:todayStr];
    
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    NSInteger pos1 = single.dayStatus==1?1:single.beforeDay;
    NSInteger pos2 = single.dayStatus==1?1:single.beforeHour;
    if (single.beforeDay==0) {
        pos1 = 1;
        pos2 = single.beforeHour==0?1:single.beforeHour;
    }else if (single.beforeHour == 0){
        pos1 = single.beforeDay==0?1:single.beforeDay;
        pos2 = 24;
    }
    NSTimeInterval time = pos1 * pos2 * 60 * 59;//计算给定后的日期
    /*2018-03-23新增判断*/
    NSDate * twoDayDate = nil;
    if (_todayCanMake==YES&&_isChoseToday==YES) {
        twoDayDate = todayDate;//今日可选
    }else{
        twoDayDate = [todayDate dateByAddingDays:1];//今日不可选
    }
    NSDate * twoDayBeforePayDate = [NSDate dateWithTimeInterval:-(time+24*60*59) sinceDate:payDate];//还款日的前single.beforeDay+1天
    NSDate * twoDayAfterBillDate = [NSDate dateWithTimeInterval:time sinceDate:billDate];//账单日的后single.beforeDay天
    NSDictionary * backDic = @{@"twoDay":twoDayDate,@"before":twoDayBeforePayDate,@"after":twoDayAfterBillDate,@"time":[NSNumber numberWithDouble:time],@"todayDate":todayDate};
    return backDic;
}
-(NSString*) getTheCorrectNum:(NSString*)tempString{
    while ([tempString hasPrefix:@"0"]){
        tempString = [tempString substringFromIndex:1];
        //        NSLog(@"压缩之后的tempString:%@",tempString);
    }
    return tempString;
}
-(NSDate*)dateStrBecomeNewDate:(NSString*)dateStr andFormat:(NSString*)format{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    NSDate *date =[dateFormat dateFromString:dateStr];
    NSDate *date2 = [date dateByAddingTimeInterval:8 * 60 * 60];
    return date2;
}
@end

//
//  JNCardSDKCommonWebView.h
//  TwoOneEight
//
//  Created by Jinniu on 2018/10/12.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKCardCellModel.h"
typedef void (^WebNotifySuccessBlock)(void);
@interface JNCardSDKCommonWebView : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * htmlStr,*productDetailsStr;//html代码
@property(nonatomic,copy)NSString * url;
@property(nonatomic,copy)WebNotifySuccessBlock successBlock;
@property (nonatomic,assign) BOOL isCreditCardVC;//信用卡申请界面过来的
@property(nonatomic,assign)BOOL isHideStatueBar,isGoToSMSVerify;
//因为h5要在webview里开webview，所以需要知道当pop的时候是哪一个webview，所以需要记录webview的索引，规定从其他页面来的webview为第0个
@property(nonatomic,assign)int openCount;
@property(nonatomic,retain)JNCardSDKCardCellModel * cardModel;
@end

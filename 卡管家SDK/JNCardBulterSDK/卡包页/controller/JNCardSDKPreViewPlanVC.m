//
//  JNCardSDKPreViewPlanVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/27.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKPreViewPlanVC.h"
#import "JNCardSDKRepayDetaiCellModel.h"
#import "JNCardSDKRepayDetailCell.h"
#import <HDAlertView/HDAlertView.h>
#import "JNCardSDKPayWayView.h"
#import "JNCardSDKMoneyWebViewVC.h"
#import "JNCardSDKVerifyVC.h"
#define ThisPageTextColor [UIColor colorWithHue:0 saturation:0 brightness:0.53 alpha:1]
static NSInteger countp;
@interface JNCardSDKPreViewPlanVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * _dataArr;
    NSInteger _page;
    NSString * _tmpDate;
    UILabel * _handLab/*手续费*/,*_serveLab;/*服务费*/
    NSString * _userPlanid,*_planrate;
    UILabel * tmpLab;
}
@property(nonatomic,retain)UIView * shelterView;
@property(nonatomic,retain)UITableView * myTab;
@end

@implementation JNCardSDKPreViewPlanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    countp = 0;
    self.myTitle = @"计划预览";
    self.view.backgroundColor = ThemeColor;
    _page = 1;
    _dataArr = [NSMutableArray new];
    if(self.isFromFivPre==NO){
        [self requestData];
    }else{
        _userPlanid = self.planID;
        _planrate = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%f",[self.backDic[@"planrate"] doubleValue]]];
        NSArray * pR = self.backDic[@"data"][@"planRecords"];
        NSArray * newArr = [self dealWithDataIntoArray:pR].copy;
        NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKRepayDetaiCellModel mj_objectArrayWithKeyValuesArray:newArr]];
        [_dataArr addObjectsFromArray:arrM];
        UIView * v = [[UIView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-65, IPHONE_WIDTH, 65)];
        v.backgroundColor = ThemeColor;
        [self.view addSubview:v];
        [self createPlanDetail:v];
    }
    [self.view addSubview:self.myTab];
}
-(void)createHeadLabAndBottomButton{
    UIButton * certainBtn = [UIButton new];
    certainBtn.layer.masksToBounds = YES;
    [certainBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [certainBtn setTitle:@"确定提交计划" forState:0];
    certainBtn.layer.cornerRadius = AUTO(5);
    certainBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    certainBtn.backgroundColor = [UIColor whiteColor];
    [certainBtn addTarget:self action:@selector(certainBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:certainBtn];
    certainBtn.sd_layout.rightSpaceToView(self.view, AUTO(20)).bottomSpaceToView(self.view, AUTO(10)).heightIs(AUTO(40)).widthIs(IPHONE_WIDTH/2-AUTO(40));
    
    UIButton * cancelBtn = [UIButton new];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn setTitle:@"返回修改计划" forState:0];
    [cancelBtn addTarget:self action:@selector(cancelResetPlanBtnClick) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.backgroundColor = [UIColor whiteColor];
    cancelBtn.layer.masksToBounds = YES;
    cancelBtn.layer.cornerRadius = AUTO(5);
    [self.view addSubview:cancelBtn];
    cancelBtn.sd_layout.centerYEqualToView(certainBtn).leftSpaceToView(self.view, AUTO(20)).widthIs(IPHONE_WIDTH/2-AUTO(40)).heightIs(AUTO(40));
    UIView * bgView = [UIView new];
    [self.view addSubview:bgView];
    bgView.sd_layout.leftEqualToView(self.view).rightEqualToView(self.view).bottomSpaceToView(cancelBtn, 0).heightIs(65);
    if (_backDic.count!=0) {
        [self createPlanDetail:bgView];//创建三个模块
    }
}
-(void)createPlanDetail:(UIView*)bgView{
    NSArray * valueArr = @[[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"backmoney"] doubleValue]]],[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"salemoney"] doubleValue]]],[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[_backDic[@"money"] doubleValue]]]];
    NSArray * name = @[@"实际还款",@"实际消费",@"手续费"];
    NSMutableArray * labArr = [NSMutableArray new];
    for (int i =0 ; i<name.count; i++) {
        UILabel * lab = [UILabel labelWithTitle:name[i] color:[UIColor whiteColor] fontSize:14];
        [bgView addSubview:lab];
        [labArr addObject:lab];
    }
    UILabel * lastLab = nil;
    for (int i =0 ; i<name.count; i++) {
        UILabel * lab = labArr[i];
        lab.sd_layout.leftSpaceToView(lastLab == nil?bgView:lastLab, 0).topSpaceToView(bgView, 10).heightIs(16).widthIs(IPHONE_WIDTH/3);
        UILabel * valuewLab = [UILabel labelWithTitle:valueArr[i] color:[UIColor whiteColor] fontSize:14];
        [bgView addSubview:valuewLab];
        valuewLab.sd_layout.centerXEqualToView(lab).widthIs(IPHONE_WIDTH/3).topSpaceToView(lab, 10).heightIs(16);
        if (i<name.count-1) {
            UIView * line = [UIView new];
            line.backgroundColor = [UIColor whiteColor];
            [bgView addSubview:line];
            line.sd_layout.leftSpaceToView(lab, 0).topSpaceToView(bgView, 15).bottomSpaceToView(bgView, 15).widthIs(1);
        }
        lastLab = lab;
    }
    UIButton * wenhaoBtn = [UIButton new];
    [wenhaoBtn addTarget:self action:@selector(wenhaoBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [wenhaoBtn setImage:Image(@"问号") forState:0];
    [bgView addSubview:wenhaoBtn];
    wenhaoBtn.sd_layout.rightSpaceToView(bgView, 5).topSpaceToView(bgView, 5).widthIs(30).heightEqualToWidth();
}
-(void)wenhaoBtnClick{
    NSString * newsrt = [JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_backDic[@"money"] doubleValue]]];
    NSString * rate = [NSString stringWithFormat:@"%.2f",[_backDic[@"planrate"] doubleValue]];
    NSString * msg = [NSString stringWithFormat:@"手续费：%@\n单笔手续费=单笔消费额*%@%%+1\n手续费总额=全部单笔手续费相加之和",newsrt,rate];
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"手续费明细" andMessage:msg];
    alertView.messageTextAlignment = NSTextAlignmentLeft;
    [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
    }];
    [alertView show];
}
-(void)requestData{
    [SVProgressHUD show];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:self.planID forKey:@"PlanId"];
    [p setObject:[NSString stringWithFormat:@"%ld",_page] forKey:@"currentpage"];
    [p setObject:@"15" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetRepayPlanDetail];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [self->_myTab.mj_header endRefreshing];
        [self->_myTab.mj_footer endRefreshing];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            self->_backDic = responseObject;
            NSArray * dataArr = responseObject[@"data"];
            if (dataArr.count!=0) {
                self->_userPlanid = dataArr[0][@"UserRepayPlayId"];
            }
            self->_planrate = responseObject[@"planrate"];
            NSArray * newArr = [self dealWithDataIntoArray:dataArr].copy;
            NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKRepayDetaiCellModel mj_objectArrayWithKeyValuesArray:newArr]];
            [self->_dataArr addObjectsFromArray:arrM];
            [self->_myTab reloadData];
            [SVProgressHUD dismiss];
            [self createHeadLabAndBottomButton];
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}
#pragma mark 懒加载
-(UITableView *)myTab {
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH,self.isFromFivPre==YES?IPHONE_HEIGHT-kTopHeight-65:  IPHONE_HEIGHT-kTopHeight-AUTO(60)-kTopHeight) style:UITableViewStylePlain];
        _myTab.delegate = self;
        _myTab.dataSource =self;
        _myTab.rowHeight = 65.f;
        if (self.isFromFivPre == NO) {
            _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                self->_page = 1;
                [self->_dataArr removeAllObjects];
                [self requestData];
            }];
            _myTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                self->_page ++;
                [self requestData];
            }];
        }
    }
    return _myTab;
}

#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count+1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 0;
    }else{
        NSArray * arr = _dataArr[section-1];
        return arr.count;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JNCardSDKRepayDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[JNCardSDKRepayDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.model = _dataArr[indexPath.section-1][indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 120;
    }
    return 44;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        UIView * zeroHeadView = [UIView new];
        [self createZeroSectionHeadView:zeroHeadView];
        return zeroHeadView;
    }
    JNCardSDKRepayDetaiCellModel * model = _dataArr[section-1][0];
    UIView * headView = [UIView new];
    UILabel * lab = [[UILabel alloc]initWithFrame:Frame(AUTO(10), 0, IPHONE_WIDTH-AUTO(20), 44)];
    lab.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.49 alpha:1];
    lab.text = model.theDate.length>0?[NSString stringWithFormat:@"    %@",[model.theDate substringWithRange:NSMakeRange(0, 10)]]:[NSString stringWithFormat:@"    %@",[model.theTime substringWithRange:NSMakeRange(0, 10)]];
    lab.font = Font(16);
    lab.textColor = [UIColor whiteColor];
    [headView addSubview:lab];
    CGRect rect = CGRectMake(0, 0, IPHONE_WIDTH-20, 50);
    CGSize radio = CGSizeMake(5, 5);
    if (section==1) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:radio];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = lab.bounds;
        maskLayer.path = maskPath.CGPath;
        lab.layer.mask = maskLayer;
    }
    return headView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)cellContentViewWith{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    return width;
}
-(void)createZeroSectionHeadView:(UIView*)superView{
    UIView * bgbgViwe = [UIView new];
    bgbgViwe.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.96 alpha:1];
    [superView addSubview:bgbgViwe];
    bgbgViwe.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    UIView * bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    [superView addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(superView, 10).rightSpaceToView(superView, 10).topSpaceToView(superView, 5);
    UILabel * tmpPlanOrderLab = [UILabel labelWithTitle:@"计划单号：" color:[UIColor colorWithHue:0 saturation:0 brightness:0.53 alpha:1] fontSize:14];
    [bgView addSubview:tmpPlanOrderLab];
    tmpPlanOrderLab.sd_layout.leftSpaceToView(bgView, 5).topSpaceToView(bgView, 8).heightIs(14);
    [tmpPlanOrderLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planOrderValue = [UILabel labelWithTitle:self.planID color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planOrderValue];
    planOrderValue.sd_layout.leftSpaceToView(tmpPlanOrderLab, 0).centerYEqualToView(tmpPlanOrderLab).heightIs(14);
    [planOrderValue setSingleLineAutoResizeWithMaxWidth:IPHONE_WIDTH];
    
//    UILabel * planSettingLab = [UILabel labelWithTitle:@"计划配置：" color:ThisPageTextColor fontSize:12];
//    [bgView addSubview:planSettingLab];
//    planSettingLab.sd_layout.leftEqualToView(tmpPlanOrderLab).topSpaceToView(tmpPlanOrderLab, 10).heightIs(14);
//    [planSettingLab setSingleLineAutoResizeWithMaxWidth:200];
    
//    UILabel*label2_day=[UILabel new];
//    label2_day.text=[NSString stringWithFormat:@"%@天%ld笔",_backDic[@"days"],[_backDic[@"backnum"] integerValue]];
//    label2_day.textColor=ColorByHexStr(@"#999999");
//    label2_day.font=[UIFont systemFontOfSize:13];
//    label2_day.textAlignment=NSTextAlignmentLeft;
//    [bgView addSubview:label2_day];
//    label2_day.sd_layout.leftSpaceToView(planSettingLab, 0).centerYEqualToView(planSettingLab).heightIs(15);
//    [label2_day setSingleLineAutoResizeWithMaxWidth:200];
    
    UILabel * tmp1 = [UILabel new];
    tmp1.text = @"还款：";
    tmp1.font=[UIFont systemFontOfSize:13];
    tmp1.textColor = ColorByHexStr(@"#999999");
    [bgView addSubview:tmp1];
    tmp1.sd_layout.leftEqualToView(tmpPlanOrderLab).topSpaceToView(tmpPlanOrderLab, 10).heightIs(15);
    [tmp1 setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp1Value = [UILabel new];
    tmp1Value.textColor = [UIColor redColor];
    tmp1Value.font=[UIFont systemFontOfSize:14];
    tmp1Value.text = [NSString stringWithFormat:@"%ld",[_backDic[@"mybacknum"] integerValue]];
    [bgView addSubview:tmp1Value];
    tmp1Value.sd_layout.centerYEqualToView(tmp1).leftSpaceToView(tmp1, 0).heightIs(15);
    [tmp1Value setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp1Value2 = [UILabel new];
    tmp1Value2.font=[UIFont systemFontOfSize:14];
    tmp1Value2.textColor = ColorByHexStr(@"#999999");
    tmp1Value2.text = [NSString stringWithFormat:@"-%ld",[_backDic[@"backnum"] integerValue]];
    [bgView addSubview:tmp1Value2];
    tmp1Value2.sd_layout.centerYEqualToView(tmp1Value).leftSpaceToView(tmp1Value, 0).heightIs(15);
    [tmp1Value2 setSingleLineAutoResizeWithMaxWidth:100];
    
    UILabel * tmp2 = [UILabel new];
    tmp2.text = @"消费：";
    tmp2.font=[UIFont systemFontOfSize:14];
    tmp2.textColor =ColorByHexStr(@"#999999");
    [bgView addSubview:tmp2];
    tmp2.sd_layout.leftSpaceToView(tmp1Value2, AUTO(30)).centerYEqualToView(tmp1Value2).heightIs(15);
    [tmp2 setSingleLineAutoResizeWithMaxWidth:100];
    UILabel * tmp2Value = [UILabel new];
    tmp2Value.textColor = [UIColor redColor];
    tmp2Value.font = [UIFont systemFontOfSize:14];
    tmp2Value.text = [NSString stringWithFormat:@"%ld",[_backDic[@"mysalenum"] integerValue]];
    [bgView addSubview:tmp2Value];
    tmp2Value.sd_layout.leftSpaceToView(tmp2, 0).centerYEqualToView(tmp2).heightIs(15);
    [tmp2Value setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * tmp2Value2 = [UILabel new];
    tmp2Value2.font = [UIFont systemFontOfSize:14];
    tmp2Value2.textColor =ColorByHexStr(@"#999999");
    tmp2Value2.text = [NSString stringWithFormat:@"-%ld",[_backDic[@"salenum"] integerValue]];
    [bgView addSubview:tmp2Value2];
    tmp2Value2.sd_layout.centerYEqualToView(tmp2Value).leftSpaceToView(tmp2Value, 0).heightIs(15);
    [tmp2Value2 setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * tmpPlanTotalLab = [UILabel labelWithTitle:@"计划总额：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpPlanTotalLab];
    tmpPlanTotalLab.sd_layout.leftEqualToView(tmpPlanOrderLab).topSpaceToView(tmp2, 5).heightIs(18);
    [tmpPlanTotalLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planTotalValue = [UILabel labelWithTitle:self.planMoney color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planTotalValue];
    planTotalValue.sd_layout.leftEqualToView(tmpPlanTotalLab).topSpaceToView(tmpPlanTotalLab, 5).heightIs(18);
    [planTotalValue setSingleLineAutoResizeWithMaxWidth:200];
    
    UIView * vline = [UIView new];
    vline.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline];
    vline.sd_layout.centerXIs((IPHONE_WIDTH-20)/3).topEqualToView(tmpPlanTotalLab).bottomEqualToView(planTotalValue).widthIs(1);
    
    UILabel * tmpPlanMoneyLab = [UILabel labelWithTitle:@"计划定金：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpPlanMoneyLab];
    tmpPlanMoneyLab.sd_layout.leftSpaceToView(vline, 5).centerYEqualToView(tmpPlanTotalLab).heightIs(18);
    [tmpPlanMoneyLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * planMoneyValue = [UILabel labelWithTitle:self.planDingjin color:ThisPageTextColor fontSize:14];
    [bgView addSubview:planMoneyValue];
    planMoneyValue.sd_layout.leftEqualToView(tmpPlanMoneyLab).centerYEqualToView(planTotalValue).heightIs(18);
    [planMoneyValue setSingleLineAutoResizeWithMaxWidth:200];
    UIView * vline2 = [UIView new];
    vline2.backgroundColor = ThisPageTextColor;
    [bgView addSubview:vline2];
    vline2.sd_layout.centerXIs((IPHONE_WIDTH-20)*2/3).topEqualToView(tmpPlanTotalLab).bottomEqualToView(planTotalValue).widthIs(1);
    
    UILabel * tmpFeeLab = [UILabel labelWithTitle:@"计划手续费：" color:ThisPageTextColor fontSize:14];
    [bgView addSubview:tmpFeeLab];
    tmpFeeLab.sd_layout.leftSpaceToView(vline2, 5).centerYEqualToView(tmpPlanMoneyLab).heightIs(18);
    [tmpFeeLab setSingleLineAutoResizeWithMaxWidth:200];
    UILabel * feeValue = [UILabel labelWithTitle:[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%.2f",[_backDic[@"money"] doubleValue]]] color:ThisPageTextColor fontSize:14];
    [bgView addSubview:feeValue];
    feeValue.sd_layout.leftEqualToView(tmpFeeLab).centerYEqualToView(planMoneyValue).heightIs(18);
    [feeValue setSingleLineAutoResizeWithMaxWidth:200];
    [bgView setupAutoHeightWithBottomView:feeValue bottomMargin:10];
}
#pragma mark 事件触发
-(void)certainBtnClick:(UIButton*)sender{
//    sender.backgroundColor = [UIColor lightGrayColor];
//    [sender setTitleColor:[UIColor whiteColor] forState:0];
//    sender.userInteractionEnabled = NO;
    if (_userPlanid.length==0) {
        [JNCardSDKMytools warnText:@"等待数据中" status:JNCardSDKError];
        return;
    }
    [self verifyPayPasswordWithTitle:@"请输入支付密码完成提交"];
}
-(void)makePlantoOne:(NSString *)planId{//将计划状态改为执行中
    countp ++;
    [SVProgressHUD show];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_CancelCardState];
    [paraDic setObject:@"1" forKey:@"TheState"];
    [paraDic setObject:@"2" forKey:@"NewType"];
    [JNCardSDKMytools requestStringByPostUrl:url parameter:paraDic success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            [JNCardSDKMytools warnText:@"生成成功" status:JNCardSDKSuccess];
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        
    }];
}
-(void)requestPayWayAll{
    [JNCardSDKReuserFile getAllPayWay:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            NSMutableArray * payWayArr = backDic[@"data"];
            [self jihuaRealBtnClick:payWayArr];
            
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    }];
}
-(void)jihuaRealBtnClick:(NSMutableArray * )pyaWayArr{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication].keyWindow addSubview:self.shelterView];
        CGFloat height = pyaWayArr.count*AUTO(50)+AUTO(94)+180;
        JNCardSDKPayWayView * view = [[JNCardSDKPayWayView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-height, IPHONE_WIDTH,height) andPayWayArr:pyaWayArr andType:1 andServeMoney:[NSString stringWithFormat:@"%.2f",[self.handMoney floatValue]] andCardNum:self.bankNum andTotalMoney:self.planDingjin];
        view.payJNCardSDKTypeBlock = ^(NSString *payTypeNum) {
            [self tapClick];
            if ([payTypeNum isEqualToString:@"98"]||[payTypeNum isEqualToString:@"99"]) {
//                [self verifyPayPasswordWithTitle:@"请输入支付密码完成支付" typeString:payTypeNum];
            }else{
                [self getWXPayUrl:self.handMoney andPlanID:self.planID andPayWay:payTypeNum];
            }            
        };
        [self->_shelterView addSubview:view];
    });
}
-(void)verifyPayPasswordWithTitle:(NSString*)title{
    __weak JNCardSDKPreViewPlanVC *weakSelf = self;
//    NewPayMoneyPopView *view = [[NewPayMoneyPopView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT) withCenterStr:title withIsHYKVC:YES];
//    [view setReturnRefreshPayBlock:^(BOOL isForgetPassword,NSString * payPwd){
//        if (isForgetPassword) {
//            PhoneSetPayPswVC *vc = [PhoneSetPayPswVC new];
//            vc.isValidationVC = YES;
//            vc.isFromHYKVC = YES;
//            [weakSelf.navigationController pushViewController:vc animated:YES];
//        }else{
//            [weakSelf makePlantoOne:self->_userPlanid];
//        }
//    }];
//    [[UIApplication sharedApplication].keyWindow addSubview:view];
    [weakSelf makePlantoOne:self->_userPlanid];
}
-(void)getWXPayUrl:(NSString *)money andPlanID:(NSString*)planID andPayWay:(NSString*)payWay{
    [SVProgressHUD show];
    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_WeiXinPay];
    [paraDic setObject:payWay forKey:@"payway"];
    [paraDic setObject:planID forKey:@"UserRepayPlayId"];
    [paraDic setObject:money forKey:@"money"];
    [paraDic setObject:@"0" forKey:@"paytype"];//0充值，1升级
    [JNCardSDKMytools requestStringByPostUrl:url parameter:paraDic success:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            [SVProgressHUD dismiss];
            if ([payWay isEqualToString:@"1"]) {
                JNCardSDKVerifyVC * vc = [JNCardSDKVerifyVC new];
                vc.wxUrl = backDic[@"data"];
                vc.orderID = backDic[@"orderid"];
                [self.navigationController pushViewController:vc animated:YES];
            }else if([payWay isEqualToString:@"2"]){
                NSString* encodedString = [backDic[@"data"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:encodedString]];
            }else if([payWay isEqualToString:@"98"]||[payWay isEqualToString:@"99"]){//余额支付佣金支付
                if (IsEmptyStr(backDic[@"data"])) {
                    [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKSuccess];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    if ([backDic[@"data"]containsString:@"http:"]) {
                        JNCardSDKMoneyWebViewVC * web = [JNCardSDKMoneyWebViewVC new];
                        web.currentWebUrl = backDic[@"data"];
                        [self.navigationController pushViewController:web animated:YES];
                    }else{
                        [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKSuccess];
                    }
                }
            }else{
                JNCardSDKMoneyWebViewVC * web = [JNCardSDKMoneyWebViewVC new];
                web.currentWebUrl = backDic[@"data"];
                [self.navigationController pushViewController:web animated:YES];
            }
//            dispatch_async(dispatch_get_main_queue(), ^{
//                int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
//                if (index>2) {
//                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index-2)] animated:YES];
//                }else{
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//            });
        }else{
            [SVProgressHUD dismiss];
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(UIView*)shelterView{
    if (_shelterView == nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
        [_shelterView addGestureRecognizer:tap];
    }
    return _shelterView;
}
-(void)tapClick{
    [_shelterView removeFromSuperview];
    _shelterView = nil;
}

-(void)cancelResetPlanBtnClick{
    
    [self backClick];
}

-(void)backClick{//删除计划后返回上上页
    if (self.isFromFivPre) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_DelPlan];

    NSMutableDictionary * paraDic = [JNCardSDKReuserFile returnHYKBaseDic];
    [paraDic setObject:self.bankCardID forKey:@"UserCreditCardId"];
    [paraDic setObject:_userPlanid forKey:@"PlanId"];
    [JNCardSDKMytools requestStringByPostUrl:urlStr parameter:paraDic success:^(NSDictionary *backDic) {
        if ([backDic[@"status"]isEqualToString:@"1"]) {
            NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
            NSNotification * message = [[NSNotification alloc]initWithName:@"reloadcardplandetail" object:self userInfo:nil];
            [center postNotification:message];
            [self.navigationController popViewControllerAnimated:YES];
            //                int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
            //                if (index>2) {
            //                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index-2)] animated:YES];
            //                }else{
            //                    [self.navigationController popToRootViewControllerAnimated:YES];
            //                }
        }
    } fail:^(NSError *error) {

    }];

}

-(NSMutableArray*)dealWithDataIntoArray:(NSArray*)array{
    NSMutableArray * arr = nil;
    NSMutableArray * dataArray = [NSMutableArray new];
    for (int i=0;i<array.count;i++) {
        NSDictionary * dic = array[i];
        NSString * dataDate =@"";
        if(self.isFromFivPre==NO){
            dataDate = [dic[@"theTime"] substringWithRange:NSMakeRange(5, 5)];
        }else{
            dataDate = [dic[@"theDate"] substringWithRange:NSMakeRange(5, 5)];
        }
        if (i ==0) {
            arr = [NSMutableArray new];
            _tmpDate = dataDate;
            [arr addObject:dic];
        }else{
            if ([_tmpDate isEqualToString:dataDate]) {
                [arr addObject:dic];
            }else{
                [dataArray addObject:arr];
                _tmpDate = dataDate;
                arr = [NSMutableArray new];
                [arr addObject:dic];
            }
        }
        if (i == array.count - 1) {
            [dataArray addObject:arr];
        }
    }
    return dataArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

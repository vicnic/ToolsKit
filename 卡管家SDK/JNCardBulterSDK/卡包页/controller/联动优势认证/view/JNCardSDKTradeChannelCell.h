//
//  JNCardSDKTradeChannelCell.h
//  IDCardManager
//
//  Created by Jinniu on 2018/8/2.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKChannelModel.h"
@interface JNCardSDKTradeChannelCell : UITableViewCell

@property(nonatomic,retain)UISwitch * openSwitch;
@property(nonatomic,retain)JNCardSDKChannelModel * model;
@end

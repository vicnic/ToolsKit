//
//  JNCardSDKTradeChannelCell.m
//  IDCardManager
//
//  Created by Jinniu on 2018/8/2.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKTradeChannelCell.h"
@interface JNCardSDKTradeChannelCell()
@property(nonatomic,retain)UILabel * titleLab, * moneyLab, * timeLab;
@property(nonatomic,retain)UIImageView * openView;
@property(nonatomic,retain)UIButton * openBtn;
@property (nonatomic,retain) UILabel * openLab;
@property (nonatomic,retain) UIView * bgView;
@end
@implementation JNCardSDKTradeChannelCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithHue:0.69 saturation:0.02 brightness:0.95 alpha:1];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.bgView = [UIView new];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.cornerRadius = 3;
    [self.contentView addSubview:self.bgView];
    self.bgView.sd_layout.spaceToSuperView(UIEdgeInsetsMake(10, 10, 0, 10));
    
    self.openView = [UIImageView new];
    _openView.image = Image(@"hasopen");
    [self.bgView addSubview:self.openView];
    _openView.sd_layout.leftEqualToView(self.bgView).topEqualToView(self.bgView).widthIs(50).heightIs(50);
    
    self.openSwitch = [UISwitch new];
    
    
    self.titleLab = [UILabel labelWithTitle:@"23 - 合利宝" color:[UIColor blackColor] fontSize:16];
    [self.bgView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.bgView, 30).topSpaceToView(self.bgView, 10).heightIs(18);
    [_titleLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.moneyLab = [UILabel labelWithTitle:@"金额 10 ~ 12000" color:[UIColor lightGrayColor] fontSize:13];
    [self.bgView addSubview:self.moneyLab];
    _moneyLab.sd_layout.leftEqualToView(_titleLab).topSpaceToView(_titleLab, 8).heightIs(15);
    [_moneyLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.timeLab = [UILabel labelWithTitle:@"时间：08:00 ~ 22:00" color:[UIColor lightGrayColor] fontSize:13];
    [self.bgView addSubview:self.timeLab];
    _timeLab.sd_layout.leftEqualToView(_moneyLab).topSpaceToView(_moneyLab, 8).heightIs(15);
    [_timeLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.openLab = [UILabel labelWithTitle:@"去开通 >" color:[UIColor blackColor] fontSize:14];
    [self.bgView addSubview:self.openLab];
    self.openLab.sd_layout.rightSpaceToView(self.bgView, 10).centerYEqualToView(_timeLab).heightIs(16);
    [self.openLab setSingleLineAutoResizeWithMaxWidth:300];
    
}
-(void)setModel:(JNCardSDKChannelModel *)model{
    _model = model;
    [self.bgView addSubview:self.openSwitch];
    self.openSwitch.hidden = NO;
    if (model.TheState == 1) {
        self.openLab.hidden = YES;
        self.openView.hidden = NO;
        self.openSwitch.sd_layout.rightSpaceToView(self.bgView, 10).centerYEqualToView(self.bgView).widthIs(65).heightIs(30);
    }else{
        self.openLab.hidden = NO;
        self.openView.hidden = YES;
        self.openSwitch.sd_layout.rightSpaceToView(self.bgView, 10).topSpaceToView(self.bgView, 15).widthIs(65).heightIs(30);
    }
    [_openSwitch setOn:!model.IsOpen];
    _titleLab.text = [NSString stringWithFormat:@"%ld - %@",model.PayChannelId,model.Remark];
    _moneyLab.text = [NSString stringWithFormat:@"单笔限额：%ld-%ld",model.PayLowMoney,model.PayHightMoney];
    _timeLab.text = [NSString stringWithFormat:@"交易时间：%@",model.PayStartTime];
    
}

@end

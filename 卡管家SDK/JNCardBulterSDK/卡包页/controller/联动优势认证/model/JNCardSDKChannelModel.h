//
//  JNCardSDKChannelModel.h
//  IDCardManager
//
//  Created by Jinniu on 2018/8/6.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKChannelModel : NSObject
@property(nonatomic,copy)NSString * BankNum;
@property(nonatomic,assign)NSInteger IsOpen;
@property(nonatomic,assign)NSInteger OpenUnionPay;
@property(nonatomic,assign)NSInteger PayActive;
@property(nonatomic,assign)NSInteger PayChannelId;
@property(nonatomic,assign)NSInteger PayCode;
@property(nonatomic,copy)NSString *PayEndTime;
@property(nonatomic,assign)NSInteger PayHightMoney;
@property(nonatomic,assign)NSInteger PayLowMoney;
@property(nonatomic,copy)NSString *PayName;
@property(nonatomic,copy)NSString *PayStartTime;
@property(nonatomic,copy)NSString *Remark;
@property(nonatomic,assign)NSInteger TheState;
@end

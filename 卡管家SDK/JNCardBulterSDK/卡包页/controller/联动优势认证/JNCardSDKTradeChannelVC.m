//
//  JNCardSDKTradeChannelVC.m
//  IDCardManager
//
//  Created by Jinniu on 2018/8/2.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKTradeChannelVC.h"
#import "JNCardSDKTradeChannelCell.h"
#import "JNCardSDKMyWebView.h"
@interface JNCardSDKTradeChannelVC ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray * _dataArr;
}
@property(nonatomic,retain)UITableView * myTab;
@end

@implementation JNCardSDKTradeChannelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"交易渠道";
    _dataArr = [NSMutableArray new];
    [_dataArr addObjectsFromArray:self.modelArr];
    [self createUI];
}
-(void)createUI{
    NSString *string = @"温馨提示:当你已开通通道并且开关按钮为关闭状态时，计划制定将不再进行该渠道的显示验证。";
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14.0] maxSize:CGSizeMake(IPHONE_WIDTH-20, 0)];
    UIView *bottomView = [UIView new];
    bottomView.backgroundColor = [UIColor colorWithHue:0.69 saturation:0.02 brightness:0.95 alpha:1];
    [self.view addSubview:bottomView];
    bottomView.sd_layout.bottomEqualToView(self.view).leftEqualToView(self.view).rightEqualToView(self.view).heightIs(size.height+20);
    UILabel *label = [UILabel labelWithTitle:string color:[UIColor darkGrayColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [bottomView addSubview:label];
    label.sd_layout.topSpaceToView(bottomView, 10).leftSpaceToView(bottomView, 10).rightSpaceToView(bottomView, 10).heightIs(size.height);
    [self.view addSubview:self.myTab];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JNCardSDKTradeChannelCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[JNCardSDKTradeChannelCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.openSwitch.tag = indexPath.row;
    [cell.openSwitch addTarget:self action:@selector(openSwitchClick:) forControlEvents:UIControlEventValueChanged];
    cell.model = _dataArr[indexPath.row];
    return cell;
}
-(void)openSwitchClick:(UISwitch*)sender{
    
    JNCardSDKChannelModel *model = _dataArr[sender.tag];
    model.IsOpen = !sender.isOn;
    NSString * url = [NSString stringWithFormat:@"%@?UserCreditCardId=%@&ChannelId=%ld",[JNCardSDKWorkUrl returnURL:Interface_For_UpdateMyChannel],self.cardInfoModel.userCreditCardId,model.PayChannelId];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [SVProgressHUD show];
    [JNCardSDKMytools requestStringByPostUrl:url parameter:p success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        if ([backDic[@"status"] integerValue]!=1) {
            model.IsOpen = !sender.isOn;
            [sender setOn:!sender.isOn];
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        model.IsOpen = !sender.isOn;
        [sender setOn:!sender.isOn];
        [SVProgressHUD dismiss];
    }];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel * lab = [UILabel labelWithTitle:@"开通多条交易渠道，有助于计划制定及完美账单" color:[UIColor blackColor] fontSize:14];
    return lab;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JNCardSDKChannelModel * model = _dataArr[indexPath.row];
    [self openChannelWithCardID:self.cardInfoModel.userCreditCardId andChannelID:model.PayChannelId];
}
-(void)openChannelWithCardID:(NSString*)cardID andChannelID:(NSInteger)ChannelID{
    NSString * url = [NSString stringWithFormat:@"%@?UserCreditCardId=%@&ChannelId=%ld",[JNCardSDKWorkUrl returnURL:Interface_For_OpenChannel],cardID,ChannelID];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [SVProgressHUD show];
    [JNCardSDKMytools requestStringByPostUrl:url parameter:p success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        if ([backDic[@"status"] integerValue]==1) {
            NSDictionary * data = backDic[@"data"];
            if ([data[@"open"] integerValue]==1) {
                JNCardSDKMyWebView * w = [JNCardSDKMyWebView new];
                NSString * webUrl = data[@"url"];
                if([JNCardSDKMytools IsChinese:webUrl]){
                    webUrl = [webUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                }
                [w setSuccessBlock:^{
                    [[JNCardSDKMytools currentViewController].navigationController popViewControllerAnimated:YES];
                }];
                w.currentWebUrl = webUrl;
                [self.navigationController pushViewController:w animated:YES];
            }else{
                [JNCardSDKMytools warnText:@"已开通过了" status:JNCardSDKCommon];
            }
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        NSString *string = @"温馨提示:当你已开通通道并且开关按钮为关闭状态时，计划制定将不再进行该渠道的显示验证。";
        CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14.0] maxSize:CGSizeMake(IPHONE_WIDTH-20, 0)];
        CGRect frme = Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-(size.height+20));
        _myTab = [[UITableView alloc]initWithFrame:frme style:UITableViewStyleGrouped];
        _myTab.backgroundColor = [UIColor colorWithHue:0.69 saturation:0.02 brightness:0.95 alpha:1];
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.rowHeight = 100;
    }
    return _myTab;
}

@end

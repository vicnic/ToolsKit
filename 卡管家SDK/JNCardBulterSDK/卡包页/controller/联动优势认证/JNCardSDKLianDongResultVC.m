//
//  JNCardSDKLianDongResultVC.m
//  IDCardManager
//
//  Created by Jinniu on 2018/5/23.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKLianDongResultVC.h"
#import "JNCardSDKTradeChannelVC.h"
#import "JNCardSDKChannelModel.h"
#import "JNCardSDKCardDetailVC.h"
@interface JNCardSDKLianDongResultVC ()

@end

@implementation JNCardSDKLianDongResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"渠道开通结果";
    [self createNewUI];
}
-(void)backClick{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[JNCardSDKCardDetailVC class]]) {
            JNCardSDKCardDetailVC *A =(JNCardSDKCardDetailVC *)controller;
            [self.navigationController popToViewController:A animated:YES];
        }
    }

}

-(void)noVerify{
    NSString * url = [NSString stringWithFormat:@"%@?UserCreditCardId=%@",[JNCardSDKWorkUrl returnURL:Interface_For_GetChannelList],self.cardModel.userCreditCardId];
    NSMutableDictionary * para = [JNCardSDKReuserFile returnHYKBaseDic];
    [SVProgressHUD show];
    [JNCardSDKMytools requestStringByPostUrl:url parameter:para success:^(NSDictionary *backDic) {
        [SVProgressHUD dismiss];
        if ([backDic[@"status"] integerValue]==1) {
            NSArray * arrm = [JNCardSDKChannelModel mj_objectArrayWithKeyValuesArray:backDic[@"data"]];
            if (arrm.count>0) {
                JNCardSDKTradeChannelVC * vc = [JNCardSDKTradeChannelVC new];
                vc.modelArr = arrm;
                vc.isFromPersonVC = NO;
                vc.isFromLianDongResult = YES;
                vc.cardInfoModel = self.cardModel;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                [JNCardSDKMytools warnText:@"暂无数据" status:JNCardSDKError];
            }
        }else{
            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
    
//    for (UIViewController *controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[JNCardSDKCardDetailVC class]]) {
//            JNCardSDKCardDetailVC *A =(JNCardSDKCardDetailVC *)controller;
//            [self.navigationController popToViewController:A animated:YES];
//        }
//    }
}
-(void)createNewUI{//新版的错误结果页面2018-08-27
    UIImageView * picView = [UIImageView new];
    picView.image = Image(@"短信发送失败");
    [self.view addSubview:picView];
    picView.sd_layout.leftSpaceToView(self.view, 20).centerYIs(0.25*IPHONE_HEIGHT).widthIs(80).heightIs(80);
    
    UILabel * titleLab = [UILabel labelWithTitle:@"很抱歉，该渠道未成功开通" color:[UIColor blackColor] fontSize:20 alignment:NSTextAlignmentCenter];
    [self.view addSubview:titleLab];
    titleLab.sd_layout.leftSpaceToView(picView, 10).topEqualToView(picView).offset(10).rightSpaceToView(self.view, 0).heightIs(22);
    
    UILabel * descLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:15 alignment:NSTextAlignmentLeft];
    [self.view addSubview:descLab];
    descLab.sd_layout.leftEqualToView(titleLab).bottomEqualToView(picView).offset(-10).heightIs(17).rightSpaceToView(self.view, 10);
    
    UIView * bgView = [UIView new];
    bgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    bgView.layer.borderWidth = 0.5;
    bgView.cornerRadius = 5;
    [self.view addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(self.view, 10).rightSpaceToView(self.view, 10).topSpaceToView(picView, 30);
    
    UILabel * tmpAlertLab = [UILabel labelWithTitle:@"温馨提示" color:[UIColor blackColor] fontSize:17];
    [bgView addSubview:tmpAlertLab];
    tmpAlertLab.sd_layout.leftSpaceToView(bgView, 10).topSpaceToView(bgView, 15).heightIs(19);
    [tmpAlertLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UILabel * contentLab = [UILabel labelWithTitle:@"以下情况都有可能造成您未能成功开通交易渠道：\n1.渠道暂不支持实名银行卡开通商户；\n2.所填写的信用卡信息与实名卡数据不一致；\n3.信用卡信息不一致，请核对持卡人、信用卡号、预留手机号、CVN2及有效期等信息是否一致\n4. 渠道暂不支持该信用卡交易，无法验证；" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [bgView addSubview:contentLab];
    contentLab.sd_layout.leftEqualToView(tmpAlertLab).topSpaceToView(tmpAlertLab, 15).rightSpaceToView(bgView, 10).autoHeightRatio(0);
    [bgView setupAutoHeightWithBottomView:contentLab bottomMargin:25];
    
    UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.backgroundColor = ThemeColor;
    backBtn.cornerRadius = 5;
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setTitle:@"返回重新制定计划" forState:UIControlStateNormal];
    backBtn.titleLabel.font =[UIFont systemFontOfSize:15];
    [self.view addSubview:backBtn];
    backBtn.sd_layout.leftSpaceToView(self.view, 10).rightSpaceToView(self.view, 10).bottomSpaceToView(self.view, 15).heightIs(50);
    
    UIButton * openBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    openBnt.backgroundColor = [UIColor colorWithHue:0.67 saturation:0.28 brightness:0.77 alpha:1];
    openBnt.cornerRadius = 5;
    [openBnt addTarget:self action:@selector(noVerify) forControlEvents:UIControlEventTouchUpInside];
    [openBnt setTitle:@"继续开通其他渠道" forState:UIControlStateNormal];
    openBnt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:openBnt];
    openBnt.sd_layout.leftEqualToView(backBtn).rightEqualToView(backBtn).bottomSpaceToView(backBtn, 15).heightIs(50);
    
}

@end

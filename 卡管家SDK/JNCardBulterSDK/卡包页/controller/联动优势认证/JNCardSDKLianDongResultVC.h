//
//  JNCardSDKLianDongResultVC.h
//  IDCardManager
//
//  Created by Jinniu on 2018/5/23.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKCardCellModel.h"
@interface JNCardSDKLianDongResultVC : UIViewController
@property(nonatomic,retain)JNCardSDKCardCellModel * cardModel;
@end

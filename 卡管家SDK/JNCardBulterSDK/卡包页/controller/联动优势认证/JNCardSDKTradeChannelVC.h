//
//  JNCardSDKTradeChannelVC.h
//  IDCardManager
//
//  Created by Jinniu on 2018/8/2.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKCardCellModel.h"
@interface JNCardSDKTradeChannelVC : UIViewController
@property(nonatomic,retain)JNCardSDKCardCellModel *cardInfoModel;
@property(nonatomic,retain)UIColor * receiveColor;
@property(nonatomic,assign)BOOL isFromPersonVC, isFromLianDongResult;
@property(nonatomic,retain)NSArray * modelArr;//JNCardSDKChannelModel的数组
@end

//
//  JNCardSDKNewConsumptionPatternsCtr.m
//  CardButlerSDK
//
//  Created by jinniu2 on 2019/10/10.
//  Copyright © 2019 jinniu2. All rights reserved.
//

#import "JNCardSDKNewConsumptionPatternsCtr.h"

@interface JNCardSDKNewConsumptionPatternsCtr (){
    UIScrollView *_bgScrol;
    NSMutableArray *_viewArr;
}
@property(nonatomic,retain)JNCardSDKNewConsumTypeModel * userTypeModel;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation JNCardSDKNewConsumptionPatternsCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.dataArr = [NSMutableArray new];
    _viewArr = [NSMutableArray new];
    self.myTitle = @"消费模式";
    [self getUserConsumeType];
}
-(void)getUserConsumeType{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetConsumeSetInfo];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            self.userTypeModel = [JNCardSDKNewConsumTypeModel mj_objectWithKeyValues:responseObject[@"data"]];
            [self getListData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getListData{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_GetConsumeSetList];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            NSArray * arrm = [JNCardSDKNewConsumTypeModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self createUI];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)updateUserAction{
    NSString * url = [JNCardSDKWorkUrl returnURL:Interface_For_PostUpdateConsumeSet];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:@(self.userTypeModel.tid) forKey:@"id"];
    [p setObject:@"1" forKey:@"updateType"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"] integerValue]==1) {
            
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)createUI{
    
    _bgScrol = [UIScrollView new];
    _bgScrol.alwaysBounceVertical = YES;
    _bgScrol.showsVerticalScrollIndicator = NO;
    _bgScrol.backgroundColor = [UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1];
    [self.view addSubview:_bgScrol];
    _bgScrol.sd_layout.spaceToSuperView(UIEdgeInsetsMake(kTopHeight, 0, 0, 0));
    
    UIView *lastView = nil;
    for (int i=0; i<self.dataArr.count; i++) {
        UIView *view = [UIView new];
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 6;
        view.backgroundColor = [UIColor whiteColor];
        [_bgScrol addSubview:view];
        view.sd_layout.topSpaceToView(lastView==nil?_bgScrol:lastView, lastView==nil?15:10).leftSpaceToView(_bgScrol, 13).rightSpaceToView(_bgScrol, 13).heightIs(80);
        lastView = view;
        [self createThreeViewSubViewsWithSuperView:view withTag:10+i];
    }
    
    UILabel *bottomLab = [UILabel labelWithTitle:@"温馨提示\n1.默认开启3消1还模式（即随机1-3消费搭配1笔还款）\n2.多消模式助于小额本金还大额账单，利于计划创建与执行\n3.多消模式有助于账单多样化，完美账单数据\n4.多消模式在一定程度上会增加手续费，可依账单及本金进行合理设置" color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] fontSize:12 alignment:NSTextAlignmentLeft];
    [_bgScrol addSubview:bottomLab];
    bottomLab.sd_layout.topSpaceToView(lastView, 29).leftSpaceToView(_bgScrol, 13).rightSpaceToView(_bgScrol, 13).autoHeightRatio(0);
    [_bgScrol setupAutoHeightWithBottomView:bottomLab bottomMargin:29];
}
-(void)createThreeViewSubViewsWithSuperView:(UIView*)superView withTag:(NSInteger)tag{
    
    UIView *view = [UIView new];
    view.layer.borderColor = [UIColor colorWithRed:0.49 green:0.49 blue:0.49 alpha:1].CGColor;
    view.layer.borderWidth = 1;
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 10;
    [superView addSubview:view];
    view.sd_layout.centerYEqualToView(superView).rightSpaceToView(superView, 20).heightIs(20).widthEqualToHeight();
    
    JNCardSDKNewConsumTypeModel * mo = self.dataArr[tag -10];
    UILabel *topLab = [UILabel labelWithTitle:mo.name color:[UIColor blackColor] fontSize:16 alignment:NSTextAlignmentLeft];
    [superView addSubview:topLab];
    topLab.sd_layout.topSpaceToView(superView, 20).leftSpaceToView(superView, 16).rightSpaceToView(view, 16).heightIs(15);
    
    UILabel *bottomLab = [UILabel labelWithTitle:mo.remark color:[UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1] fontSize:12 alignment:NSTextAlignmentLeft];
    [superView addSubview:bottomLab];
    bottomLab.sd_layout.topSpaceToView(topLab, 13).leftSpaceToView(superView, 16).rightSpaceToView(view, 16).heightIs(12);
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = tag;
    [btn addTarget:self action:@selector(selectedBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    btn.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    
    UIView *roundView = [UIView new];
    roundView.layer.masksToBounds = YES;
    roundView.layer.cornerRadius = 10;
    roundView.userInteractionEnabled = NO;
    if (self.userTypeModel.tid == mo.tid) {
        roundView.hidden = NO;
    }else{
        roundView.hidden = YES;
    }    
    roundView.backgroundColor = [UIColor colorWithRed:0.13 green:0.62 blue:0.85 alpha:1];
    [superView addSubview:roundView];
    roundView.sd_layout.centerYEqualToView(superView).rightSpaceToView(superView, 20).heightIs(20).widthEqualToHeight();
    [_viewArr addObject:roundView];
    
    UIView *roundView1 = [UIView new];
    roundView1.layer.masksToBounds = YES;
    roundView1.layer.cornerRadius = 5;
    roundView1.userInteractionEnabled = NO;
    roundView1.backgroundColor = [UIColor whiteColor];
    [roundView addSubview:roundView1];
    roundView1.sd_layout.centerXEqualToView(roundView).centerYEqualToView(roundView).heightIs(10).widthEqualToHeight();
}
-(void)selectedBtnClick:(UIButton*)sender{
    JNCardSDKNewConsumTypeModel * mo = self.dataArr[sender.tag-10];
    self.userTypeModel = mo;
    if (self.returnPageIndexBlock) {
        self.returnPageIndexBlock(mo);
    }
    for (UIView *view in _viewArr) {
        view.hidden = YES;
    }
    UIView *selView = _viewArr[sender.tag-10];
    selView.hidden = NO;
    [self updateUserAction];
}
@end

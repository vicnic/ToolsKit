//
//  JNCardSDKNewConsumTypeModel.h
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/12.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JNCardSDKNewConsumTypeModel : NSObject
@property(nonatomic,copy)NSString * consumeTime,*name,*remark;
@property(nonatomic,assign)NSInteger defaultType,isOpen,tid;
@end

NS_ASSUME_NONNULL_END

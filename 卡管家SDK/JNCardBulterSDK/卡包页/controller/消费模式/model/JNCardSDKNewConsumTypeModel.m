//
//  JNCardSDKNewConsumTypeModel.m
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/12.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import "JNCardSDKNewConsumTypeModel.h"

@implementation JNCardSDKNewConsumTypeModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"tid":@"id"};
}
@end

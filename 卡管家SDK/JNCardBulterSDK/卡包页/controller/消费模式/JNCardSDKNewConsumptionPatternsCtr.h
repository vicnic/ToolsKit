//
//  JNCardSDKNewConsumptionPatternsCtr.h
//  CardButlerSDK
//
//  Created by jinniu2 on 2019/10/10.
//  Copyright © 2019 jinniu2. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKNewConsumTypeModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ReturnPageIndexBlock)(JNCardSDKNewConsumTypeModel * selModel);
@interface JNCardSDKNewConsumptionPatternsCtr : JNCardSDKBaseViewCtr
@property(nonatomic,assign)BOOL isFromeCreatePlan;//是否是创建计划
@property (nonatomic,assign) NSInteger pageIndex;
@property (nonatomic,copy) ReturnPageIndexBlock returnPageIndexBlock;
@end

NS_ASSUME_NONNULL_END

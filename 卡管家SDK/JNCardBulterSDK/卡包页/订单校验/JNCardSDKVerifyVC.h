//
//  JNCardSDKVerifyVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/8/2.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JNCardSDKVerifyVC : UIViewController
@property(nonatomic,copy)NSString * orderID;
@property(nonatomic,copy)NSString * wxUrl;
@end

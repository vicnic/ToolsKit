//
//  JNCardSDKVerifyVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/8/2.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKVerifyVC.h"
#import "JNCardSDKQRCodeTool.h" 
@interface JNCardSDKVerifyVC ()
{
    NSTimer * _timer;
}
@end

@implementation JNCardSDKVerifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    _timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checkoutDealSuccess) userInfo:nil repeats:YES];
    [_timer fire];
}
-(void)createUI{
    UIImageView * localView = [UIImageView new];
    localView.image = Image(@"logoooo");
    [self.view addSubview:localView];
    localView.sd_layout.centerXEqualToView(self.view).topSpaceToView(self.view, 84).widthIs(AUTO(100)).heightIs(AUTO(100));
    
    UILabel * nameLab = [UILabel new];
    nameLab.textAlignment = NSTextAlignmentCenter;
    nameLab.font = Font(AUTO(18));
    nameLab.text = @"信用卡管家";
    [self.view addSubview:nameLab];
    nameLab.sd_layout.topSpaceToView(localView, AUTO(20)).centerXEqualToView(self.view).heightIs(20).widthIs(200);
    
    UILabel * descLab = [UILabel new];
    descLab.text = @"扫一扫付款";
    descLab.font = Font(AUTO(16));
    descLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:descLab];
    descLab.sd_layout.topSpaceToView(nameLab, AUTO(20)).centerXEqualToView(self.view).widthIs(150).heightIs(20);
    
    UIImageView * verfiyView = [UIImageView new];
    verfiyView.image = [JNCardSDKQRCodeTool SG_generateWithDefaultQRCodeData:self.wxUrl imageViewWidth:AUTO(200)];
    [self.view addSubview:verfiyView];
    verfiyView.sd_layout.centerXEqualToView(self.view).topSpaceToView(descLab, AUTO(20)).widthIs(AUTO(200)).heightIs(AUTO(200));
    
}

-(void)checkoutDealSuccess{
    //检验订单是否交易成功
    NSString * url = [NSString stringWithFormat:@"%@orderid=%@",[JNCardSDKWorkUrl returnURL:Interface_For_CheckoutWXPayStatus],self.orderID];
    NSString *utf = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [PGNetworkHelper GET:utf parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            [JNCardSDKMytools warnText:@"充值成功" status:JNCardSDKSuccess];
            [self->_timer invalidate];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
//    [JNCardSDKMytools requestWithGetUrl:utf parameter:nil success:^(NSDictionary *backDic) {
//        VLog(@"%@",backDic);
//        VLog(@"----->%@",backDic[@"msg"]);
//        if ([backDic[@"status"]isEqualToString:@"1"]) {
//            [JNCardSDKMytools warnText:@"充值成功" status:JNCardSDKSuccess];
//            [_timer invalidate];
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }else{
//            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
//        }
//    } fail:^(NSError *error) {
//        
//    }];

}
-(void)viewWillDisappear:(BOOL)animated{
    [_timer invalidate];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  PGNetworkHelper.m
//
//  Created by piggybear on 16/8/22.
//  Copyright © 2016年 piggybear. All rights reserved.
//

#import "PGNetworkHelper.h"
#import <objc/runtime.h>
static int t_count;
@implementation PGNetworkHelper
+ (NSURLSessionTask *)GET:(NSString *)URL
               parameters:(id)parameters
                    cache:(BOOL)cache
            responseCache:(HttpRequestCache)responseCache
                  success:(HttpRequestSuccess)success
                  failure:(HttpRequestFailed)failure {
    NSString *cacheKey = URL;
    if (parameters) {
        cacheKey = [URL stringByAppendingString:[self convertJsonStringFromDictionaryOrArray:parameters]];
    }
    if (responseCache) {
        responseCache([PGNetworkCache getResponseCacheForKey:cacheKey]);
    }
    AFHTTPSessionManager *manager = [self manager];
    return [manager GET:URL parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (cache) {
            [PGNetworkCache saveResponseCache:responseObject forKey:cacheKey];
        }
        VLog(@"url=%@\nparam=%@\ndata=%@",URL,parameters,responseObject);
        NSString * msg = responseObject[@"msg"];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString * flag =[msg isKindOfClass:[NSNull class]]?@"":msg;
            if ([flag containsString:@"无效的请求:"] && flag.length > 6) {
                flag = [flag stringByReplacingOccurrencesOfString:@"无效的请求:" withString:@""];
                [self turnToLoginView:flag];
            }else if([flag containsString:@"无效的请求"] && (flag.length == 6||flag.length==5)){
                [self turnToLoginView:@"登录超时"];
            }else if ([flag containsString:@"F104:"]) {
                flag = [flag stringByReplacingOccurrencesOfString:@"F104:" withString:@""];
                [self turnToLoginView:flag];
            }else{
                if (success) {
                    success(responseObject);
                }
            }
        });
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            NSDictionary * dic = error.userInfo;
            VLog(@"error=%@",dic);
            NSString * errorStr = dic[@"NSDebugDescription"];
            if ([errorStr containsString:@"JSON text did not start with array or object and option to allow fragments not set"]) {
                [self turnToLoginView:@"登录超时"];
            }else{
                failure ? failure(error) : nil;
            }
        });
    }];
}
+(void)turnToLoginView:(NSString*)warnStr{
    if(t_count>0){
        return;
    }
    t_count ++ ;
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"" andMessage:warnStr];
    [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
        t_count = 0;
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        NSNotification * message = [[NSNotification alloc]initWithName:LoginOutByOtherLogin object:self userInfo:nil];
        [center postNotification:message];
    }];
    alertView.buttonFont = [UIFont systemFontOfSize:[UIFont buttonFontSize]];
    [alertView show];
}
+ (NSURLSessionTask *)POST:(NSString *)URL
                parameters:(id)parameters
                     cache:(BOOL)cache
             responseCache:(HttpRequestCache)responseCache
                   success:(HttpRequestSuccess)success
                   failure:(HttpRequestFailed)failure {
    NSString *cacheKey = URL;
    if (parameters) {
        cacheKey = [URL stringByAppendingString:[self convertJsonStringFromDictionaryOrArray:parameters]];
    }
    if (responseCache) {
        responseCache([PGNetworkCache getResponseCacheForKey:cacheKey]);
    }
    AFHTTPSessionManager *manager = [self manager];
    return [manager POST:URL parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (cache) {
            [PGNetworkCache saveResponseCache:responseObject forKey:cacheKey];
        }
        VLog(@"url=%@\nparam=%@\ndata=%@",URL,parameters,responseObject);
        NSString * msg = responseObject[@"msg"];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString * flag =[msg isKindOfClass:[NSNull class]]?@"":msg;
            if ([flag containsString:@"无效的请求:"] && flag.length > 6) {
                flag = [flag stringByReplacingOccurrencesOfString:@"无效的请求:" withString:@""];
                [self turnToLoginView:flag];
            }else if([flag containsString:@"无效的请求"] && (flag.length == 6||flag.length==5)){
                [self turnToLoginView:@"登录超时"];
            }else if ([flag containsString:@"F104:"]) {
                flag = [flag stringByReplacingOccurrencesOfString:@"F104:" withString:@""];
                [self turnToLoginView:flag];
            }else{
                if (success) {
                    success(responseObject);
                }
            }
        });
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            NSDictionary * dic = error.userInfo;
            VLog(@"error=%@",dic);
            NSString * errorStr = dic[@"NSDebugDescription"];
            if ([errorStr containsString:@"JSON text did not start with array or object and option to allow fragments not set"]) {
                [self turnToLoginView:@"登录超时"];
            }else{
                failure ? failure(error) : nil;
            }
        });
    }];
}

+ (NSURLSessionTask *)uploadWithURL:(NSString *)URL
                         parameters:(NSDictionary *)parameters
                             images:(NSArray<UIImage *> *)images
                               name:(NSString *)name
                           mimeType:(NSString *)mimeType
                           progress:(HttpProgress)progress
                            success:(HttpRequestSuccess)success
                            failure:(HttpRequestFailed)failure {
    AFHTTPSessionManager *manager = [self manager];
    return [manager POST:URL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull image, NSUInteger idx, BOOL * _Nonnull stop) {
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            long index = idx;
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
            long long totalMilliseconds = interval * 1000 ;
            NSString *fileName = [NSString stringWithFormat:@"%lld.png", totalMilliseconds];
            NSString *name1 = [NSString stringWithFormat:@"%@%ld", name, index];
            [formData appendPartWithFileData:imageData name:name1 fileName:fileName mimeType:[NSString stringWithFormat:@"image/%@",mimeType?mimeType:@"jpeg"]];
        }];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress ? progress(uploadProgress) : nil;
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure ? failure(error) : nil;
    }];
}

+ (NSURLSessionTask *)downloadWithURL:(NSString *)URL
                              fileDir:(NSString *)fileDir
                             progress:(HttpProgress)progress
                              success:(void(^)(NSString *))success
                              failure:(HttpRequestFailed)failure {
    AFHTTPSessionManager *manager = [self manager];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL]];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        progress ? progress(downloadProgress) : nil;
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        NSString *downloadDir = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:fileDir ? fileDir : @"PGNetworkHelper"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager createDirectoryAtPath:downloadDir withIntermediateDirectories:YES attributes:nil error:nil];
        NSString *filePath = [downloadDir stringByAppendingPathComponent:response.suggestedFilename];
        return [NSURL fileURLWithPath:filePath];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        success ? success(filePath.absoluteString /** NSURL->NSString*/) : nil;
        failure && error ? failure(error) : nil;
    }];
    [downloadTask resume];
    return downloadTask;
}

+ (PGNetAPIClient *)manager {
    static PGNetAPIClient *manager = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [PGNetAPIClient sharedClient];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = 20.0f;
        manager.requestSerializer.cachePolicy = NSURLCacheStorageNotAllowed;
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    });
    return manager;
}

+ (void)timeoutInterval:(NSTimeInterval)timeInterval {
    AFHTTPSessionManager *manager = [self manager];
    manager.requestSerializer.timeoutInterval = timeInterval;
}

+ (void)cancelAllOperations {
    [[self manager].operationQueue cancelAllOperations];
}

+ (NSString *)convertJsonStringFromDictionaryOrArray:(id)parameter {
    NSData *data = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return jsonStr;
}

@end


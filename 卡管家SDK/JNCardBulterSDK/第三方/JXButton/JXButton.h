//
//  JXButton.h
//  JXButtonDemo
//
//  Created by pconline on 2016/11/28.
//  Copyright © 2016年 pconline. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXButton : UIButton
@property(nonatomic,assign)CGSize imgSize;
@property(nonatomic,assign)CGFloat title_Y;
@property(nonatomic,assign)CGFloat image_Y;
@property (nonatomic,assign) NSInteger section;
@property (nonatomic,assign) NSInteger row;
@property(nonatomic,assign)NSInteger cornerTag;
@end

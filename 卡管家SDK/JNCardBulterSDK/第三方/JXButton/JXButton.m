//
//  JXButton.m
//  JXButtonDemo
//
//  Created by pconline on 2016/11/28.
//  Copyright © 2016年 pconline. All rights reserved.
//

#import "JXButton.h"
@interface JXButton()
@property(nonatomic,retain)UILabel * tagLab;
@end
@implementation JXButton

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self=[super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}
- (void)setCornerTag:(NSInteger)cornerTag{
    NSString * url = @"";
    if(cornerTag<=0){
        _tagLab.hidden = YES;
        return;
    }else if(cornerTag>99){
        url = @"99+";
    }else{
        url = [NSString stringWithFormat:@"%ld",cornerTag];
    }
    _tagLab.hidden = NO;
    CGFloat width = [url sizeWithFont:[UIFont systemFontOfSize:12] maxSize:CGSizeMake(MAXFLOAT, 16)].width;
    if(width<16){
        width = 16;
    }else{
        width = width + 10;
    }
    _tagLab.sd_layout.rightEqualToView(self).widthIs(width);
    _tagLab.text = url;
}
-(void)commonInit{
    self.tagLab = [UILabel labelWithTitle:@"" color:[UIColor whiteColor] fontSize:12];
    _tagLab.hidden = YES;
    _tagLab.cornerRadius = 8;
    _tagLab.backgroundColor = [UIColor redColor];
    [self addSubview:self.tagLab];
    _tagLab.sd_layout.rightEqualToView(self).topEqualToView(self).offset(-8).widthIs(16).heightIs(16);
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.titleLabel.font = [UIFont systemFontOfSize:14];
}
-(void)setImgSize:(CGSize)imgSize{
    if (imgSize.width!=0) {
        _imgSize = imgSize;
    }else{
        _imgSize = CGSizeMake(0, 0);
    }
}
-(void)setTitle_Y:(CGFloat)title_Y{
    if (title_Y !=0) {
        _title_Y = title_Y;
    }else{
        _title_Y = 0;
    }
}
-(void)setImage_Y:(CGFloat)image_Y{
    if (image_Y!=0) {
        _image_Y = image_Y;
    }else{
        _image_Y = 0;
    }
}
-(CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGFloat titleX = -0.25*contentRect.size.width;
    CGFloat titleY = 0;
    if (self.title_Y !=0) {
        titleY = self.title_Y;
    }else{
        titleY = contentRect.size.height *0.75;
    }
    CGFloat titleW = 1.5*contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(titleX, titleY, titleW, titleH);
}

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    CGFloat imageW = 0;
    CGFloat imageH = 0;
    CGFloat imageY = 0;
    if (self.imgSize.width !=0) {
        imageW = self.imgSize.width;
        imageH = self.imgSize.height;
        imageY = self.image_Y!=0?self.image_Y:(CGRectGetHeight(self.frame)- imageH)/2-5;
        return CGRectMake((CGRectGetWidth(self.frame)-imageW)/2,imageY ,imageW,imageH);
    }else{
        imageW = CGRectGetWidth(contentRect);
        imageH = contentRect.size.height * 0.7;
        imageY = self.image_Y!=0?self.image_Y:0;
        return CGRectMake(0, 0, imageW, imageH);
    }
}
@end

//
//  JNCardSDKCalculaterBillPayDay.m
//  IDCardManager
//
//  Created by 金牛 on 2017/9/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKCalculaterBillPayDay.h"

@implementation JNCardSDKCalculaterBillPayDay
+(NSMutableDictionary*)getBillDateAndPayDate:(NSString *)billDay andPayDay:(NSString*)payDay{
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    NSDate * todayDate = [JNCardSDKMytools getCurrentDate];//当天日期
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * todayDateStr = [formatter stringFromDate:todayDate];
    //先要判断当天日子是否在账单日内，若在就显示当次账单日还款日，否则显示下一个
    NSString * todayDay = [todayDateStr substringWithRange:NSMakeRange(8, 2)];
    //借助当天的日期将日子替换成账单日和还款日
    NSString * billDateStr = nil;
    NSString * payDateStr = nil;
    NSString * dayBillStr = [billDay substringWithRange:NSMakeRange(8, 2)];
    NSString * dayPayStr = [payDay substringWithRange:NSMakeRange(8, 2)];
    
    if ([dayBillStr intValue]<[dayPayStr intValue]) {
        //当月
        //借助当天的日期将日子替换成账单日和还款日
        //这个月有几天，主要是为了防止大小月
        NSString * monthStr = [todayDateStr substringWithRange:NSMakeRange(5, 2)];
        NSString * yearStr = [todayDateStr substringWithRange:NSMakeRange(0, 4)];
        NSInteger thisMonthDayCount = [JNCardSDKMytools howManyDaysInThisYear:[yearStr integerValue] withMonth:[monthStr integerValue]];
        NSInteger nextMonthDayCount = 0;
        if ([monthStr isEqualToString:@"12"]) {
            nextMonthDayCount = 31;
        }else{
            nextMonthDayCount = [JNCardSDKMytools howManyDaysInThisYear:[yearStr integerValue] withMonth:[monthStr integerValue]+1];
        }
        if ([dayPayStr integerValue]>thisMonthDayCount) {
            if ([todayDay integerValue]<[dayBillStr integerValue]) {
                NSString * befroeMonthStr = @"";
                NSInteger beforeMonthDayCount = 0;
                NSString * beforeYearStr = @"";
                if ([monthStr isEqualToString:@"01"]) {
                    befroeMonthStr = @"12";
                    beforeMonthDayCount = 31;
                    beforeYearStr = [NSString stringWithFormat:@"%ld",[yearStr integerValue]-1];
                }else{
                    befroeMonthStr = [NSString stringWithFormat:@"%02ld",[monthStr integerValue]-1];
                    beforeMonthDayCount = [JNCardSDKMytools howManyDaysInThisYear:[yearStr integerValue] withMonth:[monthStr integerValue]-1];
                    beforeYearStr = yearStr;
                }
                NSInteger dayCountNum = 0;
                if (thisMonthDayCount > beforeMonthDayCount) {
                    befroeMonthStr = monthStr;
                    dayCountNum = labs(thisMonthDayCount - [dayPayStr integerValue]);
                }else{
                    dayCountNum = [dayPayStr integerValue];
                }
                if ([befroeMonthStr integerValue]<10) {
                    befroeMonthStr = [NSString stringWithFormat:@"%@",befroeMonthStr];
                }
                NSString * dayCountStr = @"";
                if (dayCountNum<10) {
                    dayCountStr = [NSString stringWithFormat:@"0%ld",dayCountNum];
                }else{
                    dayCountStr = [NSString stringWithFormat:@"%ld",dayCountNum];
                }
                billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",beforeYearStr,befroeMonthStr,dayBillStr];
                payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",beforeYearStr,befroeMonthStr,dayCountStr];
            }else if([todayDay integerValue]>=[dayBillStr integerValue]&&[todayDay integerValue]<=[dayPayStr integerValue]){
                billDateStr = [todayDateStr stringByReplacingCharactersInRange:NSMakeRange(8, 2) withString:dayBillStr];
                NSString * payMonthTmpStr = @"";
                if ([monthStr integerValue]<11&&[monthStr integerValue]!=10) {
                    payMonthTmpStr = [NSString stringWithFormat:@"0%ld",[monthStr integerValue] +1];
                }else if([monthStr integerValue]==11){
                    payMonthTmpStr = @"01";
                }else if ([monthStr integerValue]==12){
                    payMonthTmpStr = @"02";
                }else{
                    payMonthTmpStr = @"10";
                }
                NSInteger dayNums = labs(thisMonthDayCount - [dayPayStr integerValue]);
                NSString * payDayTmpStr = [NSString stringWithFormat:@"0%ld",dayNums];
                payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearStr,payMonthTmpStr,payDayTmpStr];
            }
        }else{
            if ([todayDay integerValue]<[dayBillStr integerValue]) {
                NSString * befroeMonthStr = @"";
                NSInteger beforeMonthDayCount = 0;
                NSString * beforeYearStr = @"";
                if ([monthStr isEqualToString:@"01"]) {
                    befroeMonthStr = @"12";
                    beforeMonthDayCount = 31;
                    beforeYearStr = [NSString stringWithFormat:@"%ld",[yearStr integerValue]-1];
                }else{
                    befroeMonthStr = [NSString stringWithFormat:@"%02ld",[monthStr integerValue]-1];
                    beforeMonthDayCount = [JNCardSDKMytools howManyDaysInThisYear:[yearStr integerValue] withMonth:[monthStr integerValue]-1];
                    beforeYearStr = yearStr;
                }
                NSInteger dayCountNum = 0;
                if (thisMonthDayCount > beforeMonthDayCount) {
                    befroeMonthStr = monthStr;
                    dayCountNum = labs(thisMonthDayCount - [dayPayStr integerValue]);
                }else{
                    dayCountNum = [dayPayStr integerValue];
                }
                if ([befroeMonthStr integerValue]<10) {
                    befroeMonthStr = [NSString stringWithFormat:@"%@",befroeMonthStr];
                }
                NSString * dayCountStr = @"";
                if (dayCountNum<10) {
                    dayCountStr = [NSString stringWithFormat:@"0%ld",dayCountNum];
                }else{
                    dayCountStr = [NSString stringWithFormat:@"%ld",dayCountNum];
                }
                billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",beforeYearStr,befroeMonthStr,dayBillStr];
                payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",beforeYearStr,befroeMonthStr,dayCountStr];
            }else if([todayDay integerValue]>=[dayBillStr integerValue]&&[todayDay integerValue]<=[dayPayStr integerValue]){
                billDateStr = [todayDateStr stringByReplacingCharactersInRange:NSMakeRange(8, 2) withString:dayBillStr];
                payDateStr = [todayDateStr stringByReplacingCharactersInRange:NSMakeRange(8, 2) withString:dayPayStr];
            }else if ([todayDay integerValue]>=[dayPayStr integerValue]){
                NSString * nextMonthStr = @"";
                NSString * nextYearSWT = @"";
                if ([monthStr integerValue]!=12) {
                    nextMonthStr = [NSString stringWithFormat:@"%02ld",[monthStr integerValue]+1];
                    nextYearSWT = yearStr;
                }else{
                    nextMonthStr = @"01";
                    nextYearSWT = [NSString stringWithFormat:@"%ld",[yearStr integerValue]+1];
                }
                billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",nextYearSWT,nextMonthStr,dayBillStr];
                payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",nextYearSWT,nextMonthStr,dayPayStr];
            }
        }
    }else{
        //跨月
        //情况1：当天日子<账单日&&>还款日->就是上次账单已完成，下次账单未出
        NSString * yearBillStr = @"";
        NSString * yearPayStr = @"";
        NSString * monthPayStr = @"";
        NSString * monthBillStr = @"";
        if ([todayDay integerValue]<=[dayBillStr integerValue]&&[todayDay integerValue]>[dayPayStr integerValue]) {
            monthBillStr = [todayDateStr substringWithRange:NSMakeRange(5, 2)];
            yearBillStr = [todayDateStr substringWithRange:NSMakeRange(0, 4)];
            if ([monthBillStr integerValue]!=12) {
                yearPayStr = yearBillStr;
                monthPayStr = [NSString stringWithFormat:@"%02ld",[monthBillStr integerValue]+1];
            }else{
                yearPayStr = [NSString stringWithFormat:@"%02ld",[yearBillStr integerValue]+1];
                monthPayStr = @"01";
            }
            billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearBillStr,monthBillStr,dayBillStr];
            payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearPayStr,monthPayStr,dayPayStr];
        }else if ([todayDay integerValue]<[dayBillStr integerValue]&&[todayDay integerValue]<=[dayPayStr integerValue]){
            //情况2：当天日子<账单日&&<还款日->就是当天日期处于跨月的月份但未到还款日
            yearPayStr = [todayDateStr substringWithRange:NSMakeRange(0, 4)];
            monthPayStr = [todayDateStr substringWithRange:NSMakeRange(5, 2)];
            if ([monthPayStr integerValue]==1) {
                monthBillStr = @"12";
                yearBillStr = [NSString stringWithFormat:@"%ld",[yearPayStr integerValue]-1];
            }else{
                if ([monthPayStr integerValue]<11) {
                    monthBillStr = [NSString stringWithFormat:@"0%ld",[monthPayStr integerValue]-1];
                }else{
                    monthBillStr = [NSString stringWithFormat:@"%ld",[monthPayStr integerValue]-1];
                }
                
                yearBillStr = yearPayStr;
            }
            billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearBillStr,monthBillStr,dayBillStr];
            payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearPayStr,monthPayStr,dayPayStr];
        }else if ([todayDay integerValue]>[dayBillStr integerValue]&&[todayDay integerValue]>=[dayPayStr integerValue]){
            //情况3：当天日子>账单日&&>还款日->就是当天日期处于未跨月的月份
            monthBillStr = [todayDateStr substringWithRange:NSMakeRange(5, 2)];
            yearBillStr = [todayDateStr substringWithRange:NSMakeRange(0, 4)];
            if ([monthBillStr integerValue]!=12) {
                yearPayStr = yearBillStr;
                monthPayStr = [NSString stringWithFormat:@"%02ld",[monthBillStr integerValue]+1];
            }else{
                yearPayStr = [NSString stringWithFormat:@"%ld",[yearBillStr integerValue]+1];
                monthPayStr = @"01";
            }
            billDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearBillStr,monthBillStr,dayBillStr];
            payDateStr = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00",yearPayStr,monthPayStr,dayPayStr];
        }else{
            //情况4：当天日子>账单日&&<还款日->这种情况不存在跨月里
            [JNCardSDKMytools warnText:@"该情况不存在跨月模型中" status:JNCardSDKError];
            billDateStr = @"1970-01-01 00:00:00";
            payDateStr = @"1970-01-01 00:00:00";
        }
    }
    //将日子合法处理避免11-31这种
    NSString * billMonthDayString = [billDateStr substringWithRange:NSMakeRange(5, 2)];
    NSString * payMonthDayString = [payDateStr substringWithRange:NSMakeRange(5, 2)];
    NSString * yearString = [todayDateStr substringWithRange:NSMakeRange(0, 4)];
    NSInteger billMonthDay = [JNCardSDKMytools howManyDaysInThisYear:[yearString integerValue] withMonth:[billMonthDayString integerValue]];
    NSInteger payMonthDay = [JNCardSDKMytools howManyDaysInThisYear:[yearString integerValue] withMonth:[payMonthDayString integerValue]];
    if ([dayBillStr integerValue]>billMonthDay) {
        dayBillStr = [NSString stringWithFormat:@"%ld",billMonthDay];
    }
    if ([dayPayStr integerValue]>payMonthDay) {
        dayPayStr = [NSString stringWithFormat:@"%ld",payMonthDay];
    }
    if ([payMonthDayString integerValue]>[billMonthDayString integerValue]&&[dayPayStr integerValue]>[dayBillStr integerValue]) {//修复1-10到1-29到2月只有28的bug
        dayPayStr = [payDateStr substringWithRange:NSMakeRange(8, 2)];
    }
    billDateStr = [billDateStr stringByReplacingCharactersInRange:NSMakeRange(8, 2) withString:dayBillStr];
    payDateStr = [payDateStr stringByReplacingCharactersInRange:NSMakeRange(8, 2) withString:dayPayStr];
    [dic setObject:billDateStr forKey:@"finalBillDay"];
    [dic setObject:payDateStr forKey:@"finalPayDay"];
    return dic;
}
+(NSString*) getTheCorrectNum:(NSString*)tempString{
    while ([tempString hasPrefix:@"0"]){
        tempString = [tempString substringFromIndex:1];
        //        NSLog(@"压缩之后的tempString:%@",tempString);
    }
    return tempString;
}
//将日期改变不同的格式返回
+(NSString*)dateBecomeNewStr:(NSDate*)date andFormat:(NSString*)format{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:format];
    NSString * dateStr = [formatter stringFromDate:date];
    return dateStr;
}
+(BOOL)holidayListCanChose:(NSDate*)date isFromSelect:(BOOL)isFromSel{
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    NSDictionary * holidayDic = single.holidayDic;
    NSInteger typeH = [holidayDic[@"holidaytype"] integerValue];
    NSArray * arr = holidayDic[@"holiday"];
    switch (typeH) {
        case 0:{//不做拦截
            return YES;
        }break;
        case 1:{//星期天不能选
            if ([date isSunday]==YES) {
                if (isFromSel==YES) {
                    [JNCardSDKMytools warnText:@"周日不允许制定计划" status:JNCardSDKError];
                }
                return NO;
            }else{
                return YES;
            }
        }break;
        case 2:{//周末不能选
            if ([date isTypicallyWeekend]==YES) {
                if (isFromSel==YES) {
                    [JNCardSDKMytools warnText:@"周末不允许制定计划" status:JNCardSDKError];
                }
                return NO;
            }else{
                return YES;
            }
        }break;
        case 3:{
            return [self isHoliday:arr andDate:date];
        }break;
        case 4:{//周末和假日都不能选
            if ([date isTypicallyWeekend]==YES) {
                if (isFromSel==YES) {
                    [JNCardSDKMytools warnText:@"周末不允许制定计划" status:JNCardSDKError];
                }
                return NO;
            }else if([self isHoliday:arr andDate:date]==YES){
                return NO;
            }else{
                return YES;
            }
        }break;
        case 5:{//星期天和假日不可选
            if ([date isSunday]==YES) {
                if (isFromSel==YES) {
                    [JNCardSDKMytools warnText:@"周日不允许制定计划" status:JNCardSDKError];
                }
                return NO;
            }else if([self isHoliday:arr andDate:date]==YES){
                return NO;
            }else{
                return YES;
            }
        }break;
        default:
            return YES;
            break;
    }
}
+(BOOL)isHoliday:(NSArray*)arr andDate:(NSDate*)date{
    NSString * datep = [JNCardSDKMytools dateToDateStr:date];
    datep = [datep substringWithRange:NSMakeRange(0, 10)];
    int count = 0;
    for (NSDictionary * dic  in arr) {
        NSString * dateStr = dic[@"holiday"];
        if ([dateStr isEqualToString:datep]) {
            count ++;
        }
    }
    if (count==0) {
        return NO;
    }else{
        return YES;
    }
}
+(NSString*)returnNoticeMsgWithPlanMoney:(NSString*)planMoney andPayMoney:(NSString*)payMoney{
    NSString * returnStr = @"";
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    NSInteger tmpMoney = [JNCardSDKReuserFile calculateMinPayMoneyWithTotalMoney:payMoney];
    
    if ([planMoney floatValue]>single.MaxPayAmount){
        returnStr = [NSString stringWithFormat:@"计划定金不能大于%.2f，请重新输入！",single.MaxPayAmount];
        return returnStr;
    }else{
        if ([planMoney floatValue]<single.MinPlanMoney) {
            returnStr = [NSString stringWithFormat:@"计划定金不能低于：%ld",tmpMoney];
            return returnStr;
        }else{
            if (tmpMoney<=single.MaxPayAmount) {
                if (tmpMoney<single.MinPlanMoney) {
                    returnStr = [NSString stringWithFormat:@"计划定金不能低于：%.0f",single.MinPlanMoney];
                    return returnStr;
                }else{
                    returnStr = [NSString stringWithFormat:@"计划定金不能低于：%ld",tmpMoney];
                    return returnStr;
                }
            }else{
                return @"计划还款金额出错";
            }
        }
    }
}
@end

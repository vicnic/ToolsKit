//
//  JNCardSDKMoneyWebViewVC.h
//  IDCardManager
//
//  Created by 金牛 on 2017/10/21.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"

@interface JNCardSDKMoneyWebViewVC : JNCardSDKBaseViewCtr
@property(nonatomic,copy)NSString * currentWebUrl;
@property(nonatomic,copy)NSString * htmlStr;
@property(nonatomic,assign)NSInteger webType;
//进度条 标志
@property NSInteger progressFlag;
@property BOOL speedFlag;
@property (nonatomic, strong)UIProgressView *progressBar;

@property(nonatomic,copy)NSString * isHidden;
@property(nonatomic,copy)NSString * webTitle;
@end

//
//  JNCardSDKHYKFadeTabbar.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/3/27.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
NS_ASSUME_NONNULL_BEGIN

@interface JNCardSDKHYKFadeTabbar : JNCardSDKBaseViewCtr

@property(nonatomic,copy)NSString * name,*token,*userid,*idCard,*jnDomain;
@property(nonatomic,retain)UIImage * jnAvatar;//非必传
//默认要打开的页面索引默认0
@property(nonatomic,assign)NSInteger selectedIndex;
//第一个卡片列表页的标题
@property(nonatomic,copy)NSString * jnTitleName;
@end

NS_ASSUME_NONNULL_END

//
//  JNCardSDKHYKFadeTabbar.m
//  TwoOneEight
//
//  Created by Jinniu on 2019/3/27.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKHYKFadeTabbar.h"
#import <QMUIKit/QMUIKit.h>
#import "JNCardSDKAddNewCardVC.h"
#import "JNCardSDKFVRepayCtr.h"
#import "JNCardSDKBillCtr.h"
#import "JNCardSDKJNCardPersonVC.h"
#import <objc/runtime.h>
@interface JNCardSDKHYKFadeTabbar ()
{
    QMUIButton * _selBtn;
    UIScrollView * _bgScro;
    UIView * _bgView;
}
@property(nonatomic,retain)NSArray <NSDictionary*>* btnArr;
@end

@implementation JNCardSDKHYKFadeTabbar
- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar = YES;
    self.isHideBottomLine = YES;
    
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    single.hykToken = self.token;
    single.hykUserid = self.userid;
    single.hykRealName = self.name;
    single.hykIdCard = self.idCard;
    if (self.jnAvatar) {
        single.jnAvatar = self.jnAvatar;
    }
    NSUserDefaults * def =[NSUserDefaults standardUserDefaults];
    [def setObject:self.jnDomain forKey:@"JNCardSDKDomain"];
    [self createUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex{
    _selectedIndex = selectedIndex;
    _bgScro.contentOffset = CGPointMake(selectedIndex * IPHONE_WIDTH, 0);
    QMUIButton * btn = [_bgView viewWithTag:10+selectedIndex];
    _selBtn.selected = !_selBtn.selected;
    btn.selected = !btn.selected;
    _selBtn = btn;
}
- (void)setJnTitleName:(NSString *)jnTitleName{
    _jnTitleName = jnTitleName;
}

-(void)btnClick:(QMUIButton*)sender{
    if ([sender isEqual:_selBtn]) {
        return;
    }
    sender.selected = !sender.selected;
    _selBtn.selected = !_selBtn.selected;
    _selBtn = sender;
    _bgScro.contentOffset = CGPointMake((sender.tag-10) * IPHONE_WIDTH, 0);
}

-(void)setupBtnArr{
    UIButton *lastBtn = nil;
    for (int i =0; i<self.btnArr.count; i++) {
        QMUIButton * btn = [QMUIButton buttonWithType:UIButtonTypeCustom];
        NSDictionary * dic = _btnArr[i];
        [btn setTitle:dic[@"name"] forState:UIControlStateNormal];
        [btn setImage:Image(dic[@"img_nor"]) forState:UIControlStateNormal];
        [btn setImage:Image(dic[@"img_sel"]) forState:UIControlStateSelected];
        [btn setTitleColor:dic[@"title_nor"] forState:UIControlStateNormal];
        [btn setTitleColor:dic[@"title_sel"] forState:UIControlStateSelected];
        btn.imagePosition = QMUIButtonImagePositionTop;
        btn.spacingBetweenImageAndTitle=5;
        btn.tag = 10 + i;
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        //        btn.frame = Frame(IPHONE_WIDTH*i/btnArr.count+IPHONE_WIDTH/(btnArr.count*2)-49/2.f, 2.5, 49, 44);
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:btn];
        btn.sd_layout.topSpaceToView(_bgView, 2.5).leftSpaceToView(lastBtn==nil?_bgView:lastBtn, 0).heightIs(44).widthIs(IPHONE_WIDTH/_btnArr.count);
        if (i==self.selectedIndex) {
            btn.selected = YES;
            _selBtn = btn;
        }
        lastBtn = btn;
    }
}
-(void)dealloc{
    self.navigationController.navigationBarHidden = NO;
}
-(void)createUI{
    _bgView = [[UIView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-KTabbarSafeBottomMargin-49, IPHONE_WIDTH, 49)];
    _bgView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1];
    [self.view addSubview:_bgView];
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithRed:0.71 green:0.71 blue:0.71 alpha:1];
    [_bgView addSubview:lineView];
    lineView.sd_layout.topEqualToView(_bgView).leftEqualToView(_bgView).rightEqualToView(_bgView).heightIs(0.6);
    UIColor * sel_color = [UIColor colorWithRed:0.3 green:0.68 blue:0.87 alpha:1];
    _btnArr = @[@{@"name":@"卡包",@"img_nor":@"卡包-黑",@"img_sel":@"卡包-蓝",@"title_nor":[UIColor blackColor],@"title_sel":sel_color,@"isCommonWebView":@"",@"url":@"",@"isHideNavBar":@""},
                @{@"name":@"账单",@"img_nor":@"账单-黑",@"img_sel":@"账单-蓝",@"title_nor":[UIColor blackColor],@"title_sel":sel_color,@"isCommonWebView":@"",@"url":@"",@"isHideNavBar":@""},
                @{@"name":@"我的",@"img_nor":@"我的-黑",@"img_sel":@"我的-蓝",@"title_nor":[UIColor blackColor],@"title_sel":sel_color,@"isCommonWebView":@"",@"url":@"",@"isHideNavBar":@""}];
    [self setupBtnArr];
    CGFloat bgscro_y = IPHONE_HEIGHT-KTabbarSafeBottomMargin-49;
    _bgScro = [[UIScrollView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, bgscro_y)];
    _bgScro.contentSize=CGSizeMake(IPHONE_WIDTH*3, CGRectGetMinY(_bgView.frame));
    _bgScro.pagingEnabled=NO;
    _bgScro.scrollEnabled = NO;
    _bgScro.showsHorizontalScrollIndicator=NO;
    _bgScro.showsVerticalScrollIndicator=NO;
    [self.view addSubview:_bgScro];
    //    [self createChildVC:self.selectedIndex];//默认创建第一个控制器
    JNCardSDKFVRepayCtr * vc = [JNCardSDKFVRepayCtr new];
    JNCardSDKBillCtr * vc2 = [JNCardSDKBillCtr new];
    JNCardSDKJNCardPersonVC * vc3 = [JNCardSDKJNCardPersonVC new];
    [self addChildViewController:vc];
    [self addChildViewController:vc2];
    [self addChildViewController:vc3];
    if (_jnTitleName.length>0) {
        objc_setAssociatedObject(vc, (__bridge const void * _Nonnull)(vc), _jnTitleName, OBJC_ASSOCIATION_ASSIGN);
    }else{
        objc_setAssociatedObject(vc, (__bridge const void * _Nonnull)(vc), @"卡包", OBJC_ASSOCIATION_ASSIGN);
    }
    
    CGRect rect = CGRectMake(0, 0, IPHONE_WIDTH, _bgScro.height);
    CGRect rect2 = CGRectMake(IPHONE_WIDTH, 0, IPHONE_WIDTH, _bgScro.height);
    CGRect rect3 = CGRectMake(IPHONE_WIDTH*2, 0, IPHONE_WIDTH, _bgScro.height);
    vc.view.frame = rect;
    vc2.view.frame = rect2;
    vc3.view.frame = rect3;
    
    [_bgScro addSubview:vc.view];
    [_bgScro addSubview:vc2.view];
    [_bgScro addSubview:vc3.view];
    
    _bgScro.contentOffset = CGPointMake(self.selectedIndex*IPHONE_WIDTH, 0);
}

@end

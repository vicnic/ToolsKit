//
//  JNCardSDKCalculaterBillPayDay.h
//  IDCardManager
//
//  Created by 金牛 on 2017/9/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKCalculaterBillPayDay : NSObject
+(NSMutableDictionary*)getBillDateAndPayDate:(NSString *)billDay andPayDay:(NSString*)payDay;
+(NSString*)dateBecomeNewStr:(NSDate*)date andFormat:(NSString*)format;
+(NSString*) getTheCorrectNum:(NSString*)tempString;
//假日可选列表
+(BOOL)holidayListCanChose:(NSDate*)date isFromSelect:(BOOL)isFromSel;
//planMoney计划定金
+(NSString*)returnNoticeMsgWithPlanMoney:(NSString*)planMoney andPayMoney:(NSString*)payMoney;
@end

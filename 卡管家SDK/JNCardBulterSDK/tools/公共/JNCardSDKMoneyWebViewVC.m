//
//  JNCardSDKMoneyWebViewVC.m
//  IDCardManager
//
//  Created by 金牛 on 2017/10/21.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKMoneyWebViewVC.h"
#import <WebKit/WebKit.h>
@interface JNCardSDKMoneyWebViewVC ()<UIWebViewDelegate>
@property(nonatomic,retain)UIWebView * MyWebView;
@end

@implementation JNCardSDKMoneyWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createNavigationBar];
}
-(void)createNavigationBar{
    
    UIView * navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, kTopHeight)];
    navView.backgroundColor = ThemeColor;
    [self.view addSubview:navView];
    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight)];
    [backBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"左箭头"] forState:UIControlStateNormal];
    [navView addSubview:backBtn];
    UILabel * titleLab = [[UILabel alloc]initWithFrame:Frame(IPHONE_WIDTH/2-120, 30, 240, 30)];
    titleLab.textColor = [UIColor whiteColor];
    if (IPHONE_WIDTH==320) {
        titleLab.font = [UIFont systemFontOfSize:17.f];
    }else{
        titleLab.font = Font(18);
    }
    titleLab.text = self.webTitle;
    titleLab.textAlignment = NSTextAlignmentCenter;
    [navView addSubview:titleLab];
    [self.view addSubview:self.MyWebView];
    [self setProgressView];
    [self webViewRequest];
}
-(void)webViewRequest
{
    if (self.webType==2) {
        [_MyWebView loadHTMLString:self.htmlStr baseURL:nil];
    }else{
        //缓存机制
        NSString *utf = [_currentWebUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest * req = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:utf]];
        [_MyWebView loadRequest:req];
    }
    
}
- (BOOL)isChinese:(NSString *)str
{
    NSString *match = @"(^.*?[\u4e00-\u9fa5]+$)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:str];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
-(UIWebView*)MyWebView{
    if (_MyWebView == nil) {
        _MyWebView = [[UIWebView alloc]initWithFrame:CGRectMake(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight)];
        _MyWebView.autoresizesSubviews = YES;//自动调整大小
        _MyWebView.scrollView.scrollsToTop = YES;
        _MyWebView.delegate = self;
        _MyWebView.backgroundColor = [UIColor whiteColor];
        _MyWebView.scrollView.bounces = NO;
        _MyWebView.scalesPageToFit =YES;//自动对页面进行缩放以适应屏幕
        _MyWebView.mediaPlaybackRequiresUserAction = YES;
    }
    return _MyWebView;
}
-(void)setProgressView
{
    //进度条
    _progressFlag = 0;
    _speedFlag = NO;
    
    _progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    _progressBar.frame = CGRectMake(0, kTopHeight, IPHONE_WIDTH, 1);
    _progressBar.backgroundColor = [UIColor clearColor];
    _progressBar.hidden = NO;
    
    _progressBar.tintColor = [UIColor colorWithRed:59/255.0 green:158/255.0 blue:250/255.0 alpha:1];
    _progressBar.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:_progressBar];
}
#pragma mark - Timer
- (void)progressLoading
{
    if (_progressBar.progress >= 1){
        _progressBar.progress = 0.0;
        _progressFlag = 0;
        _speedFlag = NO;
    }
    if (_progressFlag == 1){
        float progress = _progressBar.progress;
        if (_progressBar.progress >0.8 && _speedFlag == NO) {
            progress += 0;
        }else{
            progress +=  0.0005;
        }
        [_progressBar setProgress:progress animated:progress];
        [NSTimer scheduledTimerWithTimeInterval: _speedFlag ? 0.000001 : 0.005
                                         target:self
                                       selector:@selector(progressLoading)
                                       userInfo:nil
                                        repeats:NO];
    }
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    
    
    //进度条开始加载
    _progressFlag = 1;
    _speedFlag = NO;
    [self progressLoading];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    //进度条加载完成
    _speedFlag = YES;
    self.myTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)leftBtnClick{
    if (self.navigationController.topViewController == self) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    //    [self.navigationController popViewControllerAnimated:YES];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

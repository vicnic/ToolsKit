//
//  JNCardSDKMytools.h
//  7UGame
//
//  Created by 111 on 2017/2/14.
//  Copyright © 2017年 111. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef  enum{
    JNCardSDKCommon,//普通
    JNCardSDKSuccess,//成功
    JNCardSDKToast,//吐司模式
    JNCardSDKError,//错误
}JNCardSDKNoticeType;
typedef void (^SuccessArrBlock)(NSString* unionid);
typedef void (^SuccessBlock)(NSDictionary*backDic);
typedef void (^FailBlock)(NSError * error);
@interface JNCardSDKMytools : NSObject
//post方法发json字符串给后台
+ (void)requestStringByPostUrl:(NSString *)url parameter:(NSDictionary *)parameter success:(SuccessBlock)aSuccess fail:(FailBlock)aFail;

//用于MD5加密的封装方法
+(NSString *)getMD5code:(NSString *)pswStr;

//获取当前时间戳
+(NSString*)getCurrentTimestamp;

//字符串转日期
+(NSDate*)dateStrToDate:(NSString *)dateStr;

//日期转字符串
+(NSString *)dateToDateStr:(NSDate*)date;

//返回当天的日期
+(NSDate *)getCurrentDate;

//日期大小比较yyyy-MM-dd
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;

//时间戳转时间
+ (NSString *)timeStampTotime:(NSString *)timeString;

//转化为金钱格式
+(NSString *)numberToMoneyFormatter:(long)num;

//sv消息弹窗
+(void)warnText:(NSString *)str status:(JNCardSDKNoticeType)typeP;

+(BOOL)IsChinese:(NSString *)str ;

//返回一个月多少天
+(NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month;

//获取北京时间
+(NSString *)getBeiJingTime;
//取主色调
+(UIColor*)mostColor:(UIImage*)image;
+(NSDate*)dateStrToDate:(NSString *)dateStr withFormat:(NSString*)format;
+(NSString*)returnDecimalNumStr:(NSString*)numStr;//解决json解析精度丢失的问题
//获取当前视图控制器
+(UIViewController*)currentViewController;
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
@end

//
//  JNCardSDKWorkUrl.h
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>
//请求枚举
typedef  enum
{
    Interface_For_GetBankName,//获取银行名称
    Interface_For_GetArticleDetail,//获取文章内容
    Interface_For_GetBankNameFromBin,//根据银行卡号获取银行名称
#pragma mark 汇用卡
    Interface_For_GetUserBankList,////还款界面获取卡片列表
    Interface_For_OpenChannel,//开通通道
    Interface_For_UpdateMyChannel,//修改通道的状态值是否开启或者关闭
    Interface_For_CheckoutWXPayStatus,//查询支付是否成功
    Interface_For_GetRepayPlanDetail,//获取还款计划详情，非列表
    Interface_For_CancelCardState,//还款计划取消计划,但是通过改状态可以改为执行中等其他状态
    Interface_For_DelPlan,//预览界面返回箭头删除计划
    Interface_For_AllPayWay,//所有支付通道
    Interface_For_WeiXinPay,//微信支付
    Interface_For_ResetPlanTime,//重启计划时间
    Interface_For_GetResetPlanDay,//重启计划获取后台返回的可选时间范围
    Interface_For_GetCardPlanStateList,//还款界面卡片详情获取信用卡计划状态列表
    Interface_For_GetCardPlanlist,//个人页面获取还款计划的列表，用于筛选
    Interface_For_GetRepayPlanList,//计划列表，非计划详情
    Interface_For_REPAYPLANTRYCREATE,//三个消费模式按钮的接口，返回可选天数//SDK里变成了预生成后调用的接口了（2019-10-12）
    Interface_For_XINCREATE_REPAYPLAN,//生成计划接口
    Interface_For_GetAllBankList,//获取所有银行名称
    Interface_For_GetUserCardInfo,//个人中心修改卡信息调用的获取卡信息的接口
    Interface_For_ChannelSendSmsConfirm,//确认开通通道
    Interface_For_OpenChannelSendSms,//开通通道发送验证码
    Interface_For_GetChannelList,//需要开通通道列表
    Interface_For_FivSavePlanToSQL,//Fiv版预生成计划进入数据库
    Interface_For_FivMakePlanReqChannelReq,//2018-9-3新的生成计划请求接口
    Interface_For_GetPlanBeforeTime,//获取计划提前时间
    Interface_For_DeleteUserCard,//还款界面卡片解绑
    Interface_For_GetPauseInfo,//获取暂停计划数据
    Interface_For_ApplyCancelPlan,//计划撤销
    Interface_For_BindUserCreditCard,//还款界面绑定信用卡
    Interface_For_GetBill,//账单信息接口
    Interface_For_NewCharge,//账单界面的充值新接口
    Interface_For_GetCollectRecord,//刷卡
    Interface_For_PostDelegateAgreement,//获取协议内容
    Interface_For_GetConsumeSetList,//获取会员设置消费模式-列表
    Interface_For_GetConsumeSetInfo,//获取会员设置消费模式-信息
    Interface_For_PostUpdateConsumeSet,//会员设置消费模式
    Interface_For_GetArticle,//查询绑卡协议接口
}Interface_Type;
@interface JNCardSDKWorkUrl : NSObject
+ (NSString *)returnURL:(Interface_Type)type;
@end

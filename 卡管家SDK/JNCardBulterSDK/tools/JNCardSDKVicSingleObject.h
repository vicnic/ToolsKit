//
//  JNCardSDKVicSingleObject.h
//  TwoOneEight
//
//  Created by Jinniu on 2018/6/8.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface JNCardSDKVicSingleObject : NSObject
+(id)getInstance;
+(void)attempDealloc;
#pragma mark 汇用卡
@property(nonatomic,copy)NSString * hykToken;
@property(nonatomic,copy)NSString * hykUserid;
@property(nonatomic,copy)NSString * hykRealName;
@property(nonatomic,copy)NSString * hykIdCard;
@property(nonatomic,assign)CGFloat MinPlanMoney;//最小计划本金
@property(nonatomic,assign)CGFloat MaxPayAmount;//最大计划本金
@property(nonatomic,assign)NSInteger DailyMaxPays;//最多每天还几笔
@property(nonatomic,assign)NSInteger dayStatus;//0为天数1为小时
@property(nonatomic,assign)NSInteger beforeDay;
@property(nonatomic,assign)NSInteger beforeHour;
@property(nonatomic,assign)NSInteger kaOpen;//0开启1关闭
@property(nonatomic,assign)CGFloat MinBuyAmount;//最低消费金额
@property(nonatomic,assign)CGFloat ServerMoney;//单笔手续费
@property (nonatomic,assign) CGFloat HighRate;//高汇率通道费率
@property (nonatomic,assign) CGFloat HighSer;//高汇率单笔
@property(nonatomic,retain)NSDictionary * holidayDic;//节假日列表包含type的字典

@property(nonatomic,copy)NSString * domain1,*domain2,*username;
@property(nonatomic,retain)UIImage * jnAvatar;
@end

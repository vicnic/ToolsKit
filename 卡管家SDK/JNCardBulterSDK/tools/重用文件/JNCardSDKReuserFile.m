//
//  JNCardSDKReuserFile.m
//  TwoOneEight
//
//  Created by Jinniu on 2018/6/8.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "JNCardSDKReuserFile.h"
@implementation JNCardSDKReuserFile
+(NSMutableDictionary*)returnBaseParaDic{
    NSMutableDictionary * p = [NSMutableDictionary new];
    NSUserDefaults * def = [NSUserDefaults standardUserDefaults];
    NSString * token = [def objectForKey:@"token"];
    NSString * userid = [def objectForKey:@"userid"];
    NSString * timestr = [JNCardSDKMytools getBeiJingTime];
    NSString * sign = [[JNCardSDKMytools getMD5code:[NSString stringWithFormat:@"%@%@%@%@",MySecret,timestr,token,userid]] uppercaseString];
    //    [p setObject:token forKey:@"token"];
    [p setObject:userid forKey:@"userid"];
    [p setObject:timestr forKey:@"timestr"];
    [p setObject:JNUnitFlag forKey:@"deviceid"];
    [p setObject:sign forKey:@"sign"];
    [p setObject:VersionNum forKey:@"ver"];
    [p setObject:@"1" forKey:@"appsystem"];
    return p;
}
#pragma mark 汇用卡
+(NSMutableDictionary * )returnHYKBaseDic{
    NSMutableDictionary * p = [NSMutableDictionary new];
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    [p setValue:single.hykToken forKey:@"token"];
    
    NSString * token = single.hykToken;
    NSString * timestr = [JNCardSDKMytools getBeiJingTime];
    NSString * sign = [[JNCardSDKMytools getMD5code:[NSString stringWithFormat:@"%@%@%@%@",MySecret,timestr,token,single.hykUserid]] uppercaseString];
    [p setObject:single.hykUserid forKey:@"userid"];
    [p setObject:timestr forKey:@"timestr"];
    [p setObject:JNUnitFlag forKey:@"deviceid"];
    [p setObject:sign forKey:@"sign"];
    [p setObject:VersionNum forKey:@"ver"];
    [p setObject:@"1" forKey:@"appsystem"];
    return p;
}

+(void)getPlanBeforeDay{
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_GetPlanBeforeTime];
    NSString *utf = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [PGNetworkHelper GET:utf parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
            single.MinBuyAmount = [responseObject[@"MinBuyAmount"] floatValue];
            single.MinPlanMoney = [responseObject[@"MinPlanMoney"] floatValue];
            single.dayStatus = [responseObject[@"DayState"] integerValue];
            single.DailyMaxPays = [responseObject[@"DailyMaxPays"] integerValue];
            single.ServerMoney =[responseObject[@"ServerMoney"] floatValue];
            single.MaxPayAmount =[responseObject[@"MaxPayAmount"] integerValue];
            single.kaOpen = [responseObject[@"KaOpen"] integerValue];
            single.HighRate = [responseObject[@"HighRate"] floatValue];
            single.HighSer = [responseObject[@"HighSer"] floatValue]; 
            if ([responseObject[@"DayState"] integerValue]==1) {
                //小时
                single.beforeHour = [responseObject[@"Hour"] integerValue];
            }else{
                //天数
                single.beforeDay = [responseObject[@"Day"] integerValue];
            }
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
}
+(NSDictionary*)newPlanBtnClick:(NSString*)billDateStr andPayStr:(NSString*)payDateStr andCardModel:(JNCardSDKCardCellModel*)model{
    NSDictionary * dic = [NSDictionary new];
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    if (single.hykRealName.length==0) {
        dic = @{@"msg":@"真实姓名为空",@"state":@"0",@"billday":@"",@"payday":@""};
        return dic;
    }
    NSString * realName = single.hykRealName;
    if (![realName isEqualToString:model.name]) {
        dic = @{@"msg":@"卡片信息与实名信息不一致",@"state":@"0",@"billday":@"",@"payday":@""};
        return dic;
    }
    //先判断是否有计划，有计划就不让再制定
#warning 这里被修改了，mark
    if (model.repayMoney.length>0 && ![model.repayMoney isEqualToString:@"0"]) {
        dic = @{@"msg":@"计划已存在无法重新创建",@"state":@"0",@"billday":@"",@"payday":@""};
        return dic;
    }
    //先去判断是否有账单，判断依据：先判断是否跨月，跨月：账单日>=当天&&当天>=还款日 不跨月，账单日<=当日<还款日
    NSString * billDayStr = [JNCardSDKCalculaterBillPayDay getTheCorrectNum:[billDateStr substringWithRange:NSMakeRange(8, 2)]];
    NSString * payDayStr = [JNCardSDKCalculaterBillPayDay getTheCorrectNum:[payDateStr substringWithRange:NSMakeRange(8, 2)]];
    NSDate * todayDate = [JNCardSDKMytools getCurrentDate];
    NSString * todayStr = [JNCardSDKMytools dateToDateStr:todayDate];
    NSString * thisMonth = [todayStr substringWithRange:NSMakeRange(5, 2)];
    //当天的日子
    NSString * todayDay = [JNCardSDKCalculaterBillPayDay getTheCorrectNum:[todayStr substringWithRange:NSMakeRange(8, 2)]];
    if ([billDayStr intValue]>=[payDayStr intValue]) {
        //跨月
        if ([billDayStr intValue]>[todayDay intValue]&&[todayDay intValue]>[payDayStr intValue]) {
            dic = @{@"msg":@"尚未生成账单",@"state":@"0",@"billday":@"",@"payday":@""};
        }else if ([billDayStr integerValue]==[todayDay integerValue]){
            dic = @{@"msg":@"账单日当天不允许制定计划",@"state":@"0",@"billday":@"",@"payday":@""};
        }else if ([payDayStr integerValue]==[todayDay integerValue]){
            NSString * payDayMonth = [payDateStr substringWithRange:NSMakeRange(5, 2)];
            if ([thisMonth integerValue] == [payDayMonth integerValue]) {
                dic = @{@"msg":@"还款日当天不允许制定计划",@"state":@"0",@"billday":@"",@"payday":@""};
            }else{
                dic = @{@"msg":@"尚未生成账单",@"state":@"0",@"billday":@"",@"payday":@""};
            }
        }else{
            dic = @{@"msg":@"可以生成账单",@"state":@"1",@"billday":billDateStr,@"payday":payDateStr};
        }
    }else{
        //当月
        if ([billDayStr intValue]<=[todayDay intValue]&&[todayDay intValue]<=[payDayStr intValue]) {
            if ([billDayStr integerValue]==[todayDay integerValue]) {
                dic = @{@"msg":@"账单日当天不允许制定计划",@"state":@"0",@"billday":@"",@"payday":@""};
            }else if ([payDayStr integerValue]==[todayDay integerValue]){
                dic = @{@"msg":@"还款日当天不允许制定计划",@"state":@"0",@"billday":@"",@"payday":@""};
            }else{
                dic = @{@"msg":@"可以生成账单",@"state":@"1",@"billday":billDateStr,@"payday":payDateStr};
            }
        }else{
            dic = @{@"msg":@"尚未生成账单",@"state":@"0",@"billday":@"",@"payday":@""};
        }
    }
    return dic;
}
+(void)getUserCardPlanListRequest:(JNCardSDKRepayPlanCellModel*)planModel andBackBlock:(JNCardSDKRepayPlanVCRequestBlock)repayBlock{
    //请求数据
    NSMutableDictionary * p = [self returnHYKBaseDic];
    [p setObject:planModel.userCreditCardId forKey:@"UserCreditCardId"];
    [p setObject:@"1" forKey:@"currentpage"];
    [p setObject:@"10" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetCardPlanStateList];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            repayBlock(responseObject);
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
}
+(void)getUserAllPlanWithPage:(NSInteger)page andBackBlock:(JNCardSDKRepayPlanVCRequestBlock)block{
    [SVProgressHUD show];
    NSMutableDictionary * p = [self returnHYKBaseDic];
    [p setObject:[NSString stringWithFormat:@"%ld",page] forKey:@"currentpage"];
    [p setObject:@"10" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetRepayPlanList];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            block(responseObject);
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}
+(NSString*)returnState:(NSString *)numStr{
    switch ([numStr integerValue]) {
        case 0:
            return @"提交未执行";
            break;
        case 1:
            return @"执行中";
            break;
        case 2:
            return @"已完成";
            break;
        case 3:
            return @"已暂停";
            break;
        case 4:
            return @"等待中";
            break;
        case 5:
            return @"已作废";
            break;
        case 6:
            return @"已终止";
            break;
        default:
            break;
    }
    return @"";
}
+(void)reloadCardInfo:(JNCardSDKRepayPlanVCRequestBlock)repayBlock{
    //请求数据
    NSMutableDictionary * p = [self returnHYKBaseDic];
    [p setObject:@"1" forKey:@"currentpage"];
    [p setObject:@"10" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetUserBankList];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            repayBlock(responseObject);
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
    }];
    //    [JNCardSDKMytools requestWithGetUrl:utf parameter:p success:^(NSDictionary *backDic) {
    //        if ([backDic[@"status"]isEqualToString:@"1"]) {
    //            repayBlock(backDic);
    //        }else{
    //            [JNCardSDKMytools warnText:backDic[@"msg"] status:JNCardSDKError];
    //        }
    //    } fail:^(NSError *error) {
    //        [SVProgressHUD dismiss];
    //    }];
    //    //请求完了刷新表
}
+(void)getAllPayWay:(JNCardSDKRepayPlanVCRequestBlock)block{
    //请求数据
    NSMutableDictionary * p = [self returnHYKBaseDic];
    NSString * urlStr = [JNCardSDKWorkUrl returnURL:Interface_For_AllPayWay];
    [PGNetworkHelper POST:urlStr parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        block(responseObject);
    } failure:^(NSError *error) {
        
    }];
}
+(NSMutableDictionary*)returnBaseParaDicWithoutToken{
    NSMutableDictionary * p = [NSMutableDictionary new];
    NSString * timestr = [JNCardSDKMytools getBeiJingTime];
    NSString * sign = [[JNCardSDKMytools getMD5code:[NSString stringWithFormat:@"%@%@%@",MySecret,timestr,@"0"]] uppercaseString];
    //    [p setObject:token forKey:@"token"];
    [p setObject:@"0" forKey:@"userid"];
    [p setObject:timestr forKey:@"timestr"];
    [p setObject:sign forKey:@"sign"];
    [p setObject:JNUnitFlag forKey:@"deviceid"];
    [p setObject:VersionNum forKey:@"ver"];
    [p setObject:@"1" forKey:@"appsystem"];
    return p;
}
+(NSInteger)calculateMinPayMoneyWithTotalMoney:(NSString*)totalMoney{
    double dTotalMoney = [totalMoney doubleValue];
    double fivePer = dTotalMoney* 0.05;
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance]; 
    double minestMoney = 0.f;//最低消费金额
    if (single.MinPlanMoney <fivePer) {
        fivePer = ceil(fivePer);
        minestMoney = fivePer;
    }else{
        minestMoney = single.MinPlanMoney;
    }
    return minestMoney;
}
@end

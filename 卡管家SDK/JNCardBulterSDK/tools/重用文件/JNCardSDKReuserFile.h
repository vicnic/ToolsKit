//
//  JNCardSDKReuserFile.h
//  TwoOneEight
//
//  Created by Jinniu on 2018/6/8.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JNCardSDKRepayPlanCellModel.h"
#import "JNCardSDKCardCellModel.h"
typedef void (^JNCardSDKRepayPlanVCRequestBlock)(NSDictionary * backDic);
@interface JNCardSDKReuserFile : NSObject
//封装公共入参
+(NSMutableDictionary*)returnBaseParaDic;
#pragma mark 汇用卡
+(NSMutableDictionary * )returnHYKBaseDic;
+(void)getAllPayWay:(JNCardSDKRepayPlanVCRequestBlock)block;
+(void)reloadCardInfo:(JNCardSDKRepayPlanVCRequestBlock)repayBlock;
//还款计划的列表
+(void)getUserAllPlanWithPage:(NSInteger)page andBackBlock:(JNCardSDKRepayPlanVCRequestBlock)block;
+(void)getPlanBeforeDay;
+(NSDictionary*)newPlanBtnClick:(NSString*)billDateStr andPayStr:(NSString*)payDateStr andCardModel:(JNCardSDKCardCellModel*)model; 
+(void)getUserCardPlanListRequest:(JNCardSDKRepayPlanCellModel*)planModel andBackBlock:(JNCardSDKRepayPlanVCRequestBlock)repayBlock;
+(NSString*)returnState:(NSString *)numStr;
+(NSMutableDictionary*)returnBaseParaDicWithoutToken;
+(NSInteger)calculateMinPayMoneyWithTotalMoney:(NSString*)totalMoney;
@end

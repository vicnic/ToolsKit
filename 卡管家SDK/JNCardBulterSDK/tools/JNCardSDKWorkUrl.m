//
//  JNCardSDKWorkUrl.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/26.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKWorkUrl.h"

@implementation JNCardSDKWorkUrl
+ (NSString *)returnURL:(Interface_Type)type{
    NSString *url;
    switch (type) {
        case Interface_For_GetBankName:{
            url = [NSString stringWithFormat:@"%@/api/Common/GetBankName",MainDomain];
        }break;
        case Interface_For_GetArticleDetail:{
            url = [NSString stringWithFormat:@"%@/agent/Article/GetArticle",MainDomain];
        }break;
        case Interface_For_GetBankNameFromBin:{
            url = [NSString stringWithFormat:@"%@/api/Common/GetBankNameFromBin",MainDomain];
        }break;
#pragma mark 汇用卡
        case Interface_For_GetUserBankList:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard/list",JNCardSDKDomain];
        }break;
        case Interface_For_OpenChannel:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/OpenChannel",JNCardSDKDomain];
        }break;
        case Interface_For_UpdateMyChannel:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/UpdateMyChannel",JNCardSDKDomain];
        }break;
        case Interface_For_CheckoutWXPayStatus:{
            url = [NSString stringWithFormat:@"%@/api/Pay/SeachPay?",JNCardSDKDomain];
        }break;
        case Interface_For_GetRepayPlanDetail:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlanDetail/list",JNCardSDKDomain];
        }break;
        case Interface_For_CancelCardState:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/SavePlanInfo",JNCardSDKDomain];
        }break;
        case Interface_For_DelPlan:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/DelPlan",JNCardSDKDomain];
        }break;
        case Interface_For_AllPayWay:{
            url = [NSString stringWithFormat:@"%@/api/Pay/AllPayWay",JNCardSDKDomain];
        }break;
        case Interface_For_WeiXinPay:{
            url = [NSString stringWithFormat:@"%@/api/Pay/WxPay",JNCardSDKDomain];
        }break;
        case Interface_For_ResetPlanTime:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/ResetPlanTime",JNCardSDKDomain];
        }break;
        case Interface_For_GetResetPlanDay:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/ResetPlanDay",JNCardSDKDomain];
        }break;
        case Interface_For_GetCardPlanStateList:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard/CardPlanStatelist",JNCardSDKDomain];
        }break;
        case Interface_For_GetCardPlanlist:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard/CardPlanlist",JNCardSDKDomain];
        }break;
        case Interface_For_GetRepayPlanList:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/list",JNCardSDKDomain];
        }break;
        case Interface_For_REPAYPLANTRYCREATE:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/TryCreate",JNCardSDKDomain];
        }break;
        case Interface_For_XINCREATE_REPAYPLAN:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/XinCreate",JNCardSDKDomain];
        }break;
        case Interface_For_GetAllBankList:{
            url = [NSString stringWithFormat:@"%@/api/Bank/GetBankList",JNCardSDKDomain];
        }break;
        case Interface_For_GetUserCardInfo:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard/CardInfo",JNCardSDKDomain];
        }break;
        case Interface_For_ChannelSendSmsConfirm:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/XinOpenChannelSendSmsConfirm",JNCardSDKDomain];
        }break;
        case Interface_For_OpenChannelSendSms:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/XinOpenChannelSendSms",JNCardSDKDomain];
        }break;
        case Interface_For_GetChannelList:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/ChannelList",JNCardSDKDomain];
        }break;
        case Interface_For_FivSavePlanToSQL:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/XinSaveCreate",JNCardSDKDomain];
        }break;
        case Interface_For_FivMakePlanReqChannelReq:{
            url=[NSString stringWithFormat:@"%@/api/UserRepayPlan/XinTryCreate1",JNCardSDKDomain];
        }break;
        case Interface_For_GetPlanBeforeTime:{
            url = [NSString stringWithFormat:@"%@/api/sys/GetPlanDay",JNCardSDKDomain];
        }break;
        case Interface_For_DeleteUserCard:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard/DelCard",JNCardSDKDomain];
        }break;
        case Interface_For_GetPauseInfo:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/GetPausePlanInfo",JNCardSDKDomain];
        }break;
        case Interface_For_ApplyCancelPlan:{
            url = [NSString stringWithFormat:@"%@/api/UserRepayPlan/ApplyCancelPlan",JNCardSDKDomain];
        }break;
        case Interface_For_BindUserCreditCard:{
            url = [NSString stringWithFormat:@"%@/api/UserCreditCard",JNCardSDKDomain];
        }break;
        case Interface_For_GetBill:{
            url = [NSString stringWithFormat:@"%@/api/User/UserMoneyList",JNCardSDKDomain];
        }break;
        case Interface_For_NewCharge:{
            url = [NSString stringWithFormat:@"%@/api/Pay/PayList",JNCardSDKDomain];
        }break;
        case Interface_For_GetCollectRecord:{
            url = [NSString stringWithFormat:@"%@/api/Collect/GetCollectRecords",JNCardSDKDomain];
        }break;
        case Interface_For_GetConsumeSetList:{
            url = [NSString stringWithFormat:@"%@/api/user/getConsumeSetList",JNCardSDKDomain];
        }break;
        case Interface_For_GetConsumeSetInfo:{
            url = [NSString stringWithFormat:@"%@/api/user/getConsumeSetInfo",JNCardSDKDomain];
        }break;
        case Interface_For_PostUpdateConsumeSet:{
            url = [NSString stringWithFormat:@"%@/api/user/updateConsumeSet",JNCardSDKDomain];
        }break;
        case Interface_For_GetArticle:{
            url = [NSString stringWithFormat:@"%@/api/article/getArticle",JNCardSDKDomain];
        }break;
        default:
            break;
    }
    return url;
}
@end

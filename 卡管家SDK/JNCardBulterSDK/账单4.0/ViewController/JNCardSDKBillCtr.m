//
//  JNCardSDKBillCtr.m
//  IDCardManager
//
//  Created by 金牛 on 2017/7/11.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBillCtr.h"
#import "JNCardSDKBillDataModel.h"
#import "JNCardSDKBillDetailCtr.h"
#import "JNCardSDKChargeModel.h"
#import "JNCardSDKShuakaModel.h"
#import "JNCardSDKBillTabCell.h"
@interface JNCardSDKBillCtr ()<UITableViewDelegate,
UITableViewDataSource>
{
    BOOL _isChoseNavRig,_isDownRefresh,_isChoseShuaka;
    NSString * _type;/*选择的筛选类型*/
    NSInteger _page;
    NSMutableArray * _dataArr;
    NSArray * arr,*imgArr;
}
@property(nonatomic,retain)UITableView * billTable;
@property(nonatomic,retain)UIView * shelterView;
@property(nonatomic,retain)UIView * btnBgView;
@end
static NSString * BillTableViewCellID = @"BillTableViewCell";
@implementation JNCardSDKBillCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"账单";
    _dataArr = [NSMutableArray new];
    self.view.backgroundColor = [UIColor whiteColor];
    _isChoseNavRig = NO;
    _isDownRefresh = NO;
    _isChoseShuaka = NO;
    _page = 1;
    _type = @"";
    self.isHideLeftBtn = YES;
    //    arr = @[@"全部",@"还款",@"消费",@"充值",@"提现",@"分红",@"分润",@"回退",@"刷卡"];
    //    imgArr = @[@"全部",@"还款",@"消费",@"充值_new",@"提现",@"分润",@"分润",@"回退",@"消费"];
    arr = @[@"全部",@"还款",@"消费"];
    imgArr = @[@"全部",@"账单还款",@"账单消费"];
    self.rightBtnName = @"筛选";
    self.rightBtnNameColor = [UIColor blackColor];
    self.isHideLeftBtn = YES;
    self.rightBtnFontSize = 14;
    [self.view addSubview:self.billTable];
    [self requestAllData];
}
-(UITableView *)billTable{
    if (!_billTable) {
        _billTable = [[UITableView alloc]initWithFrame:CGRectMake(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT - kTopHeight-kTabbarHeight) style:UITableViewStyleGrouped];
        _billTable.backgroundColor = [UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1];
        _billTable.delegate= self;
        _billTable.dataSource = self;
        _billTable.showsVerticalScrollIndicator = NO;
        _billTable.separatorStyle = NO;
        _billTable.tableFooterView = [UIView new];
        //[_billTable registerClass:[BillTableViewCell class] forCellReuseIdentifier:BillTableViewCellID];
        [_billTable registerNib:[UINib nibWithNibName:@"JNCardSDKBillTabCell" bundle:SDKBundle] forCellReuseIdentifier:@"JNCardSDKBillTabCell"];
        _billTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self->_dataArr removeAllObjects];
            if (self->_isChoseShuaka==YES) {
                [self shuaka];
            }else{
                self->_isDownRefresh = NO;
                if (![self->_type isEqualToString:@"0"]) {
                    [self requestAllData];
                }else{
                    [self newChargeRequest];
                }
            }
        }];
        _billTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self->_page ++ ;
            if (self->_isChoseShuaka==YES) {
                [self shuaka];
            }else{
                if (![self->_type isEqualToString:@"0"]) {
                    self->_isDownRefresh = NO;
                    [self requestAllData];
                }else{
                    self->_isDownRefresh = YES;
                    [self newChargeRequest];
                }
            }
        }];
    }
    return _billTable;
}
-(UIView*)shelterView{
    if (_shelterView ==nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.clipsToBounds = YES;
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
        [_shelterView addGestureRecognizer:tap];
    }
    return _shelterView;
}
-(void)tapClick{
    _isChoseNavRig = !_isChoseNavRig;
    [_shelterView removeFromSuperview];
    _shelterView = nil;
}
-(UIView*)btnBgView{
    if (_btnBgView == nil) {
        _btnBgView = [[UIView alloc]initWithFrame:Frame(IPHONE_WIDTH-AUTO(120), -44*arr.count, AUTO(120), 44*arr.count)];
        _btnBgView.backgroundColor = [UIColor whiteColor];
        
        for (int i=0; i<arr.count; i++) {
            
            UIButton * btn = [[UIButton alloc]initWithFrame:Frame(0, 44*i, AUTO(120), 44)];
            [btn setTitle:arr[i] forState:UIControlStateNormal];
            btn.tag = 200 + i;
            btn.titleLabel.font = Font(15);
            [btn setImage:Image(imgArr[i]) forState:UIControlStateNormal];
            [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [_btnBgView addSubview:btn];
            if (i!=arr.count-1) {
                UIView * line = [[UIView alloc]initWithFrame:Frame(10, CGRectGetMaxY(btn.frame)-1, CGRectGetWidth(btn.frame)-20, 1)];
                line.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.96 alpha:1];
                [_btnBgView addSubview:line];
            }
            
        }
    }
    return _btnBgView;
}
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
-(void)btnClick:(UIButton*)sender{
    switch (sender.tag) {
        case 200:{//全部
            _type = @"";
        }break;
        case 201:{//还款
            _type = @"1";
        }break;
        case 202:{//消费
            _type = @"2";
        }break;
        case 203:{//充值
            _type = @"0";
            _page = 1;
            [self newChargeRequest];
            return;
        }break;
        case 204:{//提现
            _type = @"3";
        }break;
        case 205:{//分红
            _type = @"5";
        }break;
        case 206:{//分润
            _type = @"6";
        }break;
        case 207:{//回退
            _type = @"7";
        }break;
        case 208:{//刷卡
            _page = 1;
            _isChoseNavRig = YES;
            _isChoseShuaka = YES;
            [self rightBtnClick];
            [_dataArr removeAllObjects];
            [self shuaka];
            return;
        }break;
        default:
            break;
    }
    _page = 1;
    _isChoseShuaka = NO;
    _isChoseNavRig = YES;
    [self rightBtnClick];
    [_dataArr removeAllObjects];
    [self requestAllData];
}
-(void)shuaka{//刷卡接口
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:[NSString stringWithFormat:@"%ld",(long)_page] forKey:@"currentpage"];
    [p setObject:@"30" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetCollectRecord];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [self->_billTable.mj_header endRefreshing];
        [self->_billTable.mj_footer endRefreshing];
        if ([responseObject[@"status"]isEqualToString:@"1"]){
            NSArray * dataArr = responseObject[@"data"];
            
            if (dataArr.count!=0) {
                NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKShuakaModel mj_objectArrayWithKeyValuesArray:dataArr]];
                [self->_dataArr addObjectsFromArray:arrM];
            }
            [self NeedResetNoView];
            [self->_billTable reloadData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)newChargeRequest{//新的充值接口筛选请求
    _isChoseNavRig = YES;
    [self rightBtnClick];
    if (_isDownRefresh == NO) {
        [_dataArr removeAllObjects];
    }
    
    [SVProgressHUD show];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:[NSString stringWithFormat:@"%ld",_page] forKey:@"currentpage"];
    [p setObject:@"30" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_NewCharge];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSArray * dataArr = responseObject[@"data"];
            if (dataArr.count!=0) {
                NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKChargeModel mj_objectArrayWithKeyValuesArray:dataArr]];
                [self->_dataArr addObjectsFromArray:arrM];
                
            }
            [self NeedResetNoView];
            [self->_billTable reloadData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
        [self->_billTable.mj_header endRefreshing];
        [self->_billTable.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        
    }];
}
-(void)rightBtnClick{
    if (_isChoseNavRig == NO) {
        [self.view addSubview:self.shelterView];
        [_shelterView addSubview:self.btnBgView];
        [UIView animateWithDuration:0.3 animations:^{
            self->_btnBgView.frame = Frame(IPHONE_WIDTH-AUTO(120), 0, AUTO(120), 44*self->arr.count);
        }];
        self->_isChoseNavRig = YES;
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self->_btnBgView.frame = Frame(IPHONE_WIDTH-AUTO(120), -44*self->arr.count, AUTO(120), 44*self->arr.count);
        }completion:^(BOOL finished) {
            [self->_shelterView removeFromSuperview ];
            self->_shelterView = nil;
            self->_isChoseNavRig = NO;
        }];
    }
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JNCardSDKBillTabCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JNCardSDKBillTabCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_isChoseShuaka) {
        cell.shuaKaModel = _dataArr[indexPath.section];
    }else{
        if (![_type isEqualToString:@"0"]) {
            cell.model = _dataArr[indexPath.section];
        }else{
            cell.chargeModel = _dataArr[indexPath.section];
        }
    }
    return cell;
    /*
     if (_isChoseShuaka==YES) {
     ShuakaCell * cellp = [tableView dequeueReusableCellWithIdentifier:@"cpp"];
     if (!cellp) {
     cellp = [[ShuakaCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cpp"];
     }
     cellp.model = _dataArr[indexPath.row];
     return cellp;
     }else{
     BillTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:BillTableViewCellID];
     if (![_type isEqualToString:@"0"]) {
     cell.model = _dataArr[indexPath.row];
     }else{
     cell.chargeModel = _dataArr[indexPath.row];
     }
     
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     return cell;
     }
     */
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isChoseShuaka==YES) {
        //        JNCardSDKBillDataModel * model = _dataArr[indexPath.row];
        //        return [_billTable cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[BillTableViewCell class] contentViewWidth:IPHONE_WIDTH];
        return 204;
    }else{
        //        ShuakaCell * model = _dataArr[indexPath.row];
        //        return [_billTable cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[ShuakaCell class] contentViewWidth:IPHONE_WIDTH];
        if (![_type isEqualToString:@"0"]) {
            JNCardSDKBillDataModel *model = _dataArr[indexPath.section];
            if ([model.theType isEqualToString:@"0"] || [model.theType isEqualToString:@"5"] || [model.theType isEqualToString:@"6"] || [model.theType isEqualToString:@"11"]) {
                return 124;
            }else if ([model.theType isEqualToString:@"1"]){
                return 154;
            }else if ([model.theType isEqualToString:@"2"]){
                return 184;
            }else if ([model.theType isEqualToString:@"3"]){
                if (model.bankNum.length > 0) {
                    return 154;
                }else{
                    return 124;
                }
            }else if ([model.theType isEqualToString:@"4"]){
                return 94;
            }else if ([model.theType isEqualToString:@"16"] || [model.theType isEqualToString:@"15"]){
                if (model.traOutNo.length > 0) {
                    return 124;
                }else{
                    return 94;
                }
            }else{
                return 124;
            }
        }else{
            JNCardSDKChargeModel *model = _dataArr[indexPath.section];
            if (model.traOutNo.length > 0) {
                return 124;
            }else{
                return 94;
            }
        }
    }
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    JNCardSDKBillDataModel * model = _dataArr[indexPath.row];
//    JNCardSDKBillDetailCtr * vc = [JNCardSDKBillDetailCtr new];
//    vc.billDataModel = model;
//    [self.navigationController pushViewController:vc animated:YES];
//}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == _dataArr.count-1) {
        return 10;
    }else{
        return 0.01;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
-(void)requestAllData{
    [SVProgressHUD show];
    NSMutableDictionary * p = [JNCardSDKReuserFile returnHYKBaseDic];
    [p setObject:_type forKey:@"Thetype"];
    [p setObject:[NSString stringWithFormat:@"%ld",_page] forKey:@"currentpage"];
    [p setObject:@"10" forKey:@"pagelimit"];
    NSString *utf = [JNCardSDKWorkUrl returnURL:Interface_For_GetBill];
    [PGNetworkHelper GET:utf parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"status"]isEqualToString:@"1"]) {
            NSArray * dataArr = responseObject[@"data"];
            if (dataArr.count!=0) {
                NSMutableArray * arrM = [NSMutableArray arrayWithArray:[JNCardSDKBillDataModel mj_objectArrayWithKeyValuesArray:dataArr]];
                [self->_dataArr addObjectsFromArray:arrM];
                
            }
            [self NeedResetNoView];
            [self->_billTable reloadData];
        }else{
            [JNCardSDKMytools warnText:responseObject[@"msg"] status:JNCardSDKError];
        }
        [self->_billTable.mj_header endRefreshing];
        [self->_billTable.mj_footer endRefreshing];
        if ([responseObject[@"count"] integerValue]==self->_dataArr.count) {
            [self->_billTable.mj_footer endRefreshingWithNoMoreData];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [JNCardSDKMytools warnText:@"请求出错" status:JNCardSDKError];
    }];
}
#pragma mark 导航栏创建区

- (void)NeedResetNoView{
    if (_dataArr.count>0) {
        [self.billTable dismissNoView];
    }else{
        [self.billTable dismissNoView];
        [self.billTable showNoView:@"暂无数据" image:nil certer:CGPointZero];
    }
}

@end



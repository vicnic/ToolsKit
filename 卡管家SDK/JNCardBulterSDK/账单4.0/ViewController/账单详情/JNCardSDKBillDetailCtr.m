//
//  JNCardSDKBillDetailCtr.m
//  IDCardManager
//
//  Created by  677676  on 17/7/20.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBillDetailCtr.h"
#define BGCOLOR [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1]
@interface JNCardSDKBillDetailCtr ()
<UITableViewDelegate,
UITableViewDataSource>
@property(nonatomic,retain)UITableView * billDetailTable;
@property(nonatomic,retain)NSArray * textArr;
@property(nonatomic,retain)NSArray * subTextArr;

@end

@implementation JNCardSDKBillDetailCtr

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.view.backgroundColor = [UIColor whiteColor];
    [self createNavBar];
    [self.view addSubview:self.billDetailTable];
    [self creatTableViewHead];
}
-(NSArray *)textArr{
    if (!_textArr) {
        if (self.billDataModel.bankNum.length ==0) {
            _textArr = @[@"交易类型",@"交易时间",@"订单号",@"手续费"];
        }else{
            _textArr = @[@"交易类型",@"交易时间",@"订单号",@"手续费",@"卡号"];
        }
        
    }
    return _textArr;
}
-(NSArray *)subTextArr{
    if (!_subTextArr) {
        if (self.billDataModel.bankNum.length ==0) {
            _subTextArr = @[self.billDataModel.theTypeStr,self.billDataModel.createTime,self.billDataModel.repayPlanId,@"0.00"];
        }else{
            _subTextArr = @[self.billDataModel.theTypeStr,self.billDataModel.createTime,self.billDataModel.repayPlanId,@"0.00",self.billDataModel.bankNum];
        }        
    }
    return _subTextArr;
}
-(UITableView *)billDetailTable{
    if (!_billDetailTable) {
        _billDetailTable = [[UITableView alloc]initWithFrame:CGRectMake(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT - kTopHeight-49) style:UITableViewStylePlain];
        _billDetailTable.delegate= self;
        _billDetailTable.dataSource = self;
        _billDetailTable.backgroundColor = BGCOLOR;
        _billDetailTable.tableFooterView = [UIView new];
        _billDetailTable.rowHeight = 50;
    }
    return _billDetailTable;
}
-(void)creatTableViewHead{
    UIView * headView = [UIView new];
    headView.backgroundColor = [UIColor whiteColor];
    headView.frame = Frame(0, 0, IPHONE_WIDTH, 230);
    
    UIImageView * image = [UIImageView new];
    image.frame = Frame(headView.centerX - 80/2, 230/2-80, 80, 80);
    image.image =[UIImage imageNamed:@"交易成功"];
    [headView addSubview:image];
    
    UILabel * money = [UILabel new];
    money.text = [NSString stringWithFormat:@"%.2f",[self.billDataModel.theMoney floatValue]];
    money.textAlignment = 1;
    money.frame = Frame(headView.centerX-100/2, CGRectGetMaxY(image.frame)+30, 100, 20);
    [headView addSubview:money];
    
    
    UILabel * successText = [UILabel new];
    successText.text = @"交易成功";
    successText.textAlignment = 1;
    successText.textColor = [UIColor grayColor];
    successText.font = [UIFont systemFontOfSize:14];
    successText.frame = Frame(headView.centerX-100/2, CGRectGetMaxY(money.frame)+10, 100, 20);
    [headView addSubview:successText];
    
    UILabel * line = [UILabel new];
    line.backgroundColor = BGCOLOR;
    line.frame = Frame(0, 230-10, IPHONE_WIDTH, 10);
    [headView addSubview:line];
    
    self.billDetailTable.tableHeaderView = headView;
}
-(void)createNavBar{
    UIView * view = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, kTopHeight)];
    view.backgroundColor = [UIColor colorWithHue:0.56 saturation:0.98 brightness:1 alpha:1];
    [self.view addSubview:view];
    
    UILabel * titleLab = [[UILabel alloc]initWithFrame:Frame(IPHONE_WIDTH/2-AUTO(80), 25, AUTO(160), 30)];
    titleLab.text = @"账单详情";
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.textColor = [UIColor whiteColor];
    [view addSubview:titleLab];
    
    UIButton * backBtn = [[UIButton alloc]initWithFrame:Frame(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight)];
    [backBtn setImage:Image(@"左箭头") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backBtn];
}
-(void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
    self.hidesBottomBarWhenPushed=YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.textArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = self.textArr[indexPath.row];
    cell.detailTextLabel.text = self.subTextArr[indexPath.row];
    return cell;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end

//
//  JNCardSDKBillDetailCtr.h
//  IDCardManager
//
//  Created by  677676  on 17/7/20.
//  Copyright © 2017年 zou. All rights reserved.
//

#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKBillDataModel.h"
@interface JNCardSDKBillDetailCtr : JNCardSDKBaseViewCtr
@property(nonatomic,retain)JNCardSDKBillDataModel * billDataModel;
@end

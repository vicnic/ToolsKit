//
//  NewBillTableViewCell.h
//  IDCardManager
//
//  Created by jinniu2 on 2018/8/7.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNCardSDKChargeModel.h"
#import "JNCardSDKShuakaModel.h"
#import "JNCardSDKBillDataModel.h"

@interface JNCardSDKBillTabCell : UITableViewCell

@property(nonatomic,strong)JNCardSDKBillDataModel * model;
@property (nonatomic,strong) JNCardSDKChargeModel *chargeModel; 
@property (nonatomic,strong) JNCardSDKShuakaModel *shuaKaModel;

@end

//
//  NewBillTableViewCell.m
//  IDCardManager
//
//  Created by jinniu2 on 2018/8/7.
//  Copyright © 2018年 zou. All rights reserved.
//

#import "JNCardSDKBillTabCell.h"

@interface JNCardSDKBillTabCell()
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *topNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *topTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *zeroLabel;

@end

@implementation JNCardSDKBillTabCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1];
}
-(void)setChargeModel:(JNCardSDKChargeModel *)chargeModel{
    _moneyLabel.hidden = NO;
    _zeroLabel.hidden = NO;
    _firstLabel.hidden = NO;
    _secondLabel.hidden = NO;
    _thirdLabel.hidden = NO;
    _zeroLabel.textColor = _firstLabel.textColor = _secondLabel.textColor = _thirdLabel.textColor = [UIColor grayColor];
    self.topTimeLabel.text = [NSString stringWithFormat:@"%@",chargeModel.addDateTime];
    if ([chargeModel.payType integerValue]==1) {
        _topNameLabel.text = @"升级费";
    }else if ([chargeModel.payType integerValue]==0){
        _topNameLabel.text = @"手续费";
    }else{
        _topNameLabel.text = @"";
    }
    if (![chargeModel.traOutNo isKindOfClass:[NSNull class]]) {
        self.secondLabel.textColor = [UIColor redColor];
        self.secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([chargeModel.money floatValue])]];
        self.thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",chargeModel.traOutNo];
    }else{
        _thirdLabel.textColor = [UIColor redColor];
        self.thirdLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([chargeModel.money floatValue])]];
        _secondLabel.hidden = YES;
    }
    _moneyLabel.hidden = YES;
    _zeroLabel.hidden = YES;
    _firstLabel.hidden = YES;
}
-(void)setShuaKaModel:(JNCardSDKShuakaModel *)shuaKaModel{
    _moneyLabel.hidden = NO;
    _zeroLabel.hidden = NO;
    _firstLabel.hidden = NO;
    _secondLabel.hidden = NO;
    _thirdLabel.hidden = NO;
    _zeroLabel.textColor = _firstLabel.textColor = _secondLabel.textColor = _thirdLabel.textColor = [UIColor grayColor];
    _leftImageView.image = Image(@"账单消费");
    NSString * monegy =shuaKaModel.PayMoney.length>0?[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%f",[shuaKaModel.PayMoney doubleValue]]]:@"";
    self.moneyLabel.text = [NSString stringWithFormat:@"￥%@",monegy];
    if (shuaKaModel.OrderTime.length>0) {
        self.topTimeLabel.text = shuaKaModel.OrderTime;
    }
    double tmpMoney = [shuaKaModel.PayMoney doubleValue] - [shuaKaModel.ArriveMoney doubleValue];
    _zeroLabel.text = [NSString stringWithFormat:@"手续费：¥%@",[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",tmpMoney]]];
    
    NSString * lastFourNum = shuaKaModel.PayBankNum.length>4?[shuaKaModel.PayBankNum substringWithRange:NSMakeRange(shuaKaModel.PayBankNum.length-4, 4)]:@"";
    _firstLabel.text = [NSString stringWithFormat:@"消费卡：%@(尾号：%@)",shuaKaModel.PayBankName.length>0?shuaKaModel.PayBankName:@"",lastFourNum];
    
    NSString * payFour = [shuaKaModel.BankNum substringWithRange:NSMakeRange(shuaKaModel.BankNum.length-4, 4)];
    _secondLabel.text = [NSString stringWithFormat:@"结算卡：%@(尾号：%@)",shuaKaModel.BankName,payFour];
    
    _topNameLabel.text = @"无卡支付";
    if (![shuaKaModel.RecordId isKindOfClass:[NSNull class]]) {
        self.thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",shuaKaModel.RecordId];
    }else{
        self.thirdLabel.text = @"";
    }
    
}
-(void)setModel:(JNCardSDKBillDataModel *)model{
    _moneyLabel.hidden = NO;
    _zeroLabel.hidden = NO;
    _firstLabel.hidden = NO;
    _secondLabel.hidden = NO;
    _thirdLabel.hidden = NO;
    _zeroLabel.textColor = _firstLabel.textColor = _secondLabel.textColor = _thirdLabel.textColor = [UIColor grayColor];
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    self.topTimeLabel.text = model.createTime;
//    NSString * bannum = model.BankNum;
//    if (bannum.length>0) {
//        self.topTimeLabel.text = [NSString stringWithFormat:@"%@",model.CreateTime];
//        NSString * lastFourNum = [bannum substringWithRange:NSMakeRange(bannum.length-4, 4)];
//        _secondLabel.text = [NSString stringWithFormat:@"卡号：%@(尾号：%@)",model.BankName,lastFourNum];
//    }else{
//        self.topTimeLabel.text = model.CreateTime;
//        _secondLabel.text = @"";
//    }
//
//    if (![model.traOutNo isKindOfClass:[NSNull class]]) {
//        self.thirdLabel.text = [NSString stringWithFormat:@"单号：%@",model.traOutNo];
//    }else{
//        self.thirdLabel.text = @"";
//    }
        if ([model.theType isEqualToString:@"0"]) {//充值
            self.leftImageView.image = Image(@"充值");
            _topNameLabel.text = @"充值";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
            _moneyLabel.hidden = YES;
        }else if ([model.theType isEqualToString:@"1"]){
            self.leftImageView.image = Image(@"账单还款");
            _topNameLabel.text = @"还款";
            _firstLabel.textColor = [UIColor redColor];
            _firstLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            NSString * lastFourNum = [model.bankNum substringWithRange:NSMakeRange(model.bankNum.length-4, 4)];
            _secondLabel.text = [NSString stringWithFormat:@"银行卡：%@(尾号：%@)",model.bankName,lastFourNum];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _zeroLabel.hidden = YES;
            _moneyLabel.hidden = YES;
        }else if ([model.theType isEqualToString:@"2"]){
            self.leftImageView.image = Image(@"账单消费");
            _topNameLabel.text = @"消费";
            _zeroLabel.textColor = [UIColor redColor];
            _zeroLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _firstLabel.text = [NSString stringWithFormat:@"手续费：¥%@",[JNCardSDKMytools returnDecimalNumStr:[NSString stringWithFormat:@"%lf",[model.fee doubleValue]]]];
            NSString * lastFourNum = [model.bankNum substringWithRange:NSMakeRange(model.bankNum.length-4, 4)];
            _secondLabel.text = [NSString stringWithFormat:@"银行卡：%@(尾号：%@)",model.bankName,lastFourNum];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
        }else if ([model.theType isEqualToString:@"3"]){
            self.leftImageView.image = Image(@"提现");
            _topNameLabel.text = @"提现";
            if (model.bankNum.length > 0) {
                _firstLabel.textColor = [UIColor redColor];
                _firstLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _secondLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
                NSString * lastFourNum = [model.bankNum substringWithRange:NSMakeRange(model.bankNum.length-4, 4)];
                _thirdLabel.text = [NSString stringWithFormat:@"银行卡：%@(尾号：%@)",model.bankName,lastFourNum];
                _moneyLabel.hidden = YES;
                _zeroLabel.hidden = YES;
            }else{
                _secondLabel.textColor = [UIColor redColor];
                _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
                _firstLabel.hidden = YES;
                _moneyLabel.hidden = YES;
                _zeroLabel.hidden = YES;
            }
            if ([model.outState isEqualToString:@"0"]) {
                //                    _stateLab.text = @"提现中";
            }else if ([model.outState isEqualToString:@"1"]){
                //                    _stateLab.text = @"提现成功";
            }else{
                //                    _stateLab.text = @"提现失败";
            }
        }else if([model.theType isEqualToString:@"4"]){
            self.leftImageView.image = Image(@"回退");
            _topNameLabel.text = @"回退";
            _thirdLabel.textColor = [UIColor redColor];
            _thirdLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
            _secondLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"5"]){
            self.leftImageView.image = Image(@"分润");//分润和分红图标一样
            _topNameLabel.text = @"分红";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"6"]){
            self.leftImageView.image=Image(@"分润");
            _topNameLabel.text = @"分润";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"7"]){
            self.leftImageView.image=Image(@"提现");//手续费扣费
            _topNameLabel.text = @"手续费扣费";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"8"]){
            self.leftImageView.image=Image(@"提现");//服务费扣费
            _topNameLabel.text = @"服务费扣费";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"9"]){
            self.leftImageView.image=Image(@"提现");//保证金扣费
            _topNameLabel.text = @"保证金扣费";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"10"]){
            self.leftImageView.image=Image(@"提现");//服务费分润
            _topNameLabel.text = @"服务费分润";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"99"]){
            self.leftImageView.image=Image(@"提现");//系统调整
            _topNameLabel.text = @"系统调整";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"11"]){
            self.leftImageView.image=Image(@"提现");//手续费退还
            _topNameLabel.text = @"手续费退还";
            if (model.traOutNo.length > 0) {
                _secondLabel.textColor = [UIColor redColor];
                _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            }else{
                _thirdLabel.textColor = [UIColor redColor];
                _thirdLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _secondLabel.hidden = YES;
            }
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"12"]){
            self.leftImageView.image=Image(@"提现");//手续费
            _topNameLabel.text = @"手续费";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"13"]){
            self.leftImageView.image=Image(@"提现");//刷卡分润
            _topNameLabel.text = @"刷卡分润";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"14"]){
            self.leftImageView.image=Image(@"提现");//收款等级分润
            _topNameLabel.text = @"收款等级分润";
            _secondLabel.textColor = [UIColor redColor];
            _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
            _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"15"]){
            self.leftImageView.image=Image(@"提现");//余额支付
            _topNameLabel.text = @"余额支付";
            if (model.traOutNo.length > 0) {
                _secondLabel.textColor = [UIColor redColor];
                _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            }else{
                _thirdLabel.textColor = [UIColor redColor];
                _thirdLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _secondLabel.hidden = YES;
            }
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else if([model.theType isEqualToString:@"16"]){
            self.leftImageView.image=Image(@"提现");//佣金支付
            _topNameLabel.text = @"佣金支付";
            if (model.traOutNo.length > 0) {
                _secondLabel.textColor = [UIColor redColor];
                _secondLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _thirdLabel.text = [NSString stringWithFormat:@"订单号：%@",model.traOutNo];
            }else{
                _thirdLabel.textColor = [UIColor redColor];
                _thirdLabel.text = [NSString stringWithFormat:@"￥%@",[NSString stringWithFormat:@"%.2f",fabs([model.theMoney floatValue])]];
                _secondLabel.hidden = YES;
            }
            _moneyLabel.hidden = YES;
            _zeroLabel.hidden = YES;
            _firstLabel.hidden = YES;
        }else {
            self.leftImageView.image = Image(@"回退");
        }
    }
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

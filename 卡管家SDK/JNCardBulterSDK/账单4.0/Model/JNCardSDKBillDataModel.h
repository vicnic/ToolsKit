//
//  JNCardSDKBillDataModel.h
//  IDCardManager
//
//  Created by  677676  on 17/7/20.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKBillDataModel : NSObject
@property(nonatomic,copy)NSString * autoID;
@property(nonatomic,copy)NSString * bankName;
@property(nonatomic,copy)NSString * repayPlanId;
@property(nonatomic,copy)NSString * repayPlanDetailId;
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString * theType;
@property(nonatomic,copy)NSString * createTime;
@property(nonatomic,copy)NSString * theMoney;
@property(nonatomic,copy)NSString * remark;
@property(nonatomic,copy)NSString * money;
@property(nonatomic,copy)NSString * bankNum;
@property(nonatomic,copy)NSString * theTypeStr;
@property(nonatomic,copy)NSString * outState;
@property(nonatomic,copy)NSString * traOutNo;
@property(nonatomic,copy)NSString * outType;
@property(nonatomic,copy)NSString * fee;
@property(nonatomic,copy)NSString * payChannelId;
@property(nonatomic,copy)NSString * payFeeRate;
@property(nonatomic,copy)NSString * payName;
@property(nonatomic,copy)NSString * payNum;
@property(nonatomic,copy)NSString * payOrderNo;
@property(nonatomic,copy)NSString * payOutMoney;
@property(nonatomic,copy)NSString * payTime;
@end

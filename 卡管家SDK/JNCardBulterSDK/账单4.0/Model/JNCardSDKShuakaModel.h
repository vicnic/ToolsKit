//
//  JNCardSDKShuakaModel.h
//  IDCardManager
//
//  Created by 金牛 on 2018/1/5.
//  Copyright © 2018年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKShuakaModel : NSObject
@property(nonatomic,copy)NSString * ArriveMoney;
@property(nonatomic,copy)NSString * BankNum;
@property(nonatomic,copy)NSString * PayMoney;
@property(nonatomic,copy)NSString * BankName;
@property(nonatomic,copy)NSString * PayBankNum;
@property(nonatomic,copy)NSString * PayBankName;
@property(nonatomic,copy)NSString * Name;
@property(nonatomic,copy)NSString * TheStateName;
@property(nonatomic,copy)NSString * OrderTime;
@property(nonatomic,copy)NSString * RecordId;
@end

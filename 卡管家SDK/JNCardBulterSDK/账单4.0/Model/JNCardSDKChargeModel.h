//
//  JNCardSDKChargeModel.h
//  IDCardManager
//
//  Created by 金牛 on 2017/10/9.
//  Copyright © 2017年 zou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNCardSDKChargeModel : NSObject
//新充值接口的新增参数
@property(nonatomic,copy)NSString * payChannelId;
@property(nonatomic,copy)NSString * payName;
@property(nonatomic,copy)NSString * qrCode;
@property(nonatomic,copy)NSString * payWay;
@property(nonatomic,copy)NSString * payStateStr;
@property(nonatomic,copy)NSString * userRepayPlayId;
@property(nonatomic,copy)NSString * proName;
@property(nonatomic,copy)NSString * payDateTime;
@property(nonatomic,copy)NSString * addDateTime;
@property(nonatomic,copy)NSString * payContent;
@property(nonatomic,copy)NSString * payOutMoney;
@property(nonatomic,copy)NSString * payType;
@property(nonatomic,copy)NSString * payFeeRate;
@property(nonatomic,copy)NSString * payOrderNo;
@property(nonatomic,copy)NSString * pId;
@property(nonatomic,copy)NSString * payState;
@property(nonatomic,copy)NSString * money;
@property(nonatomic,copy)NSString * traOutNo;
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString * payNum;
@end

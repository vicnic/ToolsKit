//
//  BXExtensions.h
//  BXInsurenceBroker
//
//  Created by JYJ on 16/3/15.
//  Copyright © 2016年 baobeikeji. All rights reserved.
//

#import "UIView+JNExtension.h"
#import "UIView+IBExtension.h"
#import "NSString+BXExtension.h"
#import "NSDate+JNExtension.h"
#import "UITableView+HD_NoList.h"
#import "UIBarButtonItem+Extension.h"
#import "UIImage+JNExtension.h"
#import "UILabel+JNExtension.h"
#import "NSString+PinYin.h"
#import "UIFont+JNExtension.h"
#import "UIColor+SmartHexColor.h"
#import "UITableView+VicExtension.h"

//第三方
#import "SDAutoLayout.h"
#import <MJRefresh/MJRefresh.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <MJExtension/MJExtension.h>
#import "PGNetworkHelper.h"
#import "UIImageView+WebCache.h"
#import "JNCardSDKReuserFile.h"
#import "IQKeyboardManager.h"
#import "FTIndicator.h"
#import "DLPickerView.h"
#import "HDAlertView.h"
//自己的
#import "JNCardSDKWorkUrl.h"
#import "JNCardSDKMytools.h"
#import "JNCardSDKCalculaterBillPayDay.h"
#import "JNCardSDKVicSingleObject.h"
#import "JNCardSDKDLDeviceNameFile.h"
#import "JNCardSDKBaseViewCtr.h"
#import "JNCardSDKNavController.h"


#define ColorByHexStr(hexStr)  ([UIColor byHexString:hexStr])
#define ColorByHexNum(hexNum)  ([UIColor byHexNumber:hexNum])

#define JNCardSDKImageDomain @"http://web.hykpay.com"
#define JNCardSDKDomain [[NSUserDefaults standardUserDefaults] objectForKey:@"JNCardSDKDomain"]
#define MainDomain [[NSUserDefaults standardUserDefaults] objectForKey:@"MainDomain"]

#define LoginOutByOtherLogin @"ISLOGINGOUTBYOTHERLOING"

//版本号
#define VersionNum @"420"

/***************屏幕适配******************/
#define IPHONE_WIDTH     [UIScreen mainScreen].bounds.size.width
#define IPHONE_HEIGHT    [UIScreen mainScreen].bounds.size.height

/*********是否为iPhoneX************/
#define iPhoneX ([UIScreen mainScreen].bounds.size.height >= 812)
#define BXDangerousAreaH 34
#define kStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavBarHeight 44.0
#define kTopHeight (kStatusBarHeight + kNavBarHeight)
#define kTabbarHeight ([UIScreen mainScreen].bounds.size.height >= 812 ? (BXDangerousAreaH + 49) : 49)
#define KTabbarSafeBottomMargin (iPhoneX ? 34.f: 0.f)

#define  Frame(x,y,w,h)         CGRectMake((x), (y), (w), (h))

#define  Image(imageName)       [UIImage imageNamed:(imageName)]

#define SET_FIX_SIZE_WIDTH (IPHONE_WIDTH /375.0)
//获取适配后的数据大小
#define AUTO(num)  num * SET_FIX_SIZE_WIDTH

#define PlaceHolderImg Image(@"holderImg")

//主题颜色
#define ThemeColor [UIColor colorWithRed:0.09 green:0.47 blue:1 alpha:1]
#define  TextColor [UIColor colorWithHue:0.58 saturation:0.13 brightness:0.4 alpha:1]
#define  LineColor [UIColor colorWithHue:0 saturation:0 brightness:0.89 alpha:1]
//唯一标示
#define JNUnitFlag [[[UIDevice currentDevice] identifierForVendor] UUIDString]

//本APP secret
#define MySecret @"FD10D9AC-C337-437A-AB41-D5EC1F2CA767"

#define  Font(font)             [UIFont systemFontOfSize:AUTO(font)]

//判断后台返回的数据是否为空对象
#define  IsEmptyStr(string) [string isKindOfClass:[NSNull class]]? YES : NO

#define SDKBundle [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"JNCardBundleTarget" ofType:@"bundle"]]

#ifdef DEBUG
#define VLog(...) NSLog(@"%s 第%d行 \n %@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define VLog(...)
#endif

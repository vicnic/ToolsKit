//
//  UIImage+JNExtension.h
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/12.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (JNExtension)

/**
 *  返回拉伸图片
 */
+ (UIImage *)resizedImage:(NSString *)name;
/**
 *  用颜色返回一张图片
 */
+ (UIImage *)createImageWithColor:(UIColor*) color;
/**
 *  带边框的图片
 *
 *  @param name        图片名字
 *  @param borderWidth 边框宽度
 *  @param borderColor 边框颜色
 */
+ (instancetype)circleImageWithName:(NSString *)name borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

/**
 *  带边框的图片
 *
 *  @param image        图片
 *  @param borderWidth 边框宽度
 *  @param borderColor 边框颜色
 */
+ (instancetype)circleImageWithImage:(UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;


/**
 *  使用图像名创建图像视图
 *
 *  @param imageName 图像名称
 *
 *  @return UIImageView
 */
+ (instancetype)imageViewWithImageName:(NSString *)imageName;
/**图片压缩*/
- (NSData *)compressQualityWithMaxLength:(NSInteger)maxLength;
/**图片压缩进化版*/
-(UIImage *)compressImageToByte:(NSUInteger)maxLength ;
/**图片压缩进化版返回nsdata*/
-(NSData *)compressImgToData:(NSUInteger)maxLength;
@end

NS_ASSUME_NONNULL_END

//
//  UIFont+JNExtension.m
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/12.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import "UIFont+JNExtension.h"

@implementation UIFont (JNExtension)
+ (UIFont *)fontWithDevice:(CGFloat)fontSize {
    if (IPHONE_WIDTH > 375) {
        fontSize = fontSize + 3;
    }else if (IPHONE_WIDTH == 375){
        fontSize = fontSize + 1.5;
    }else if (IPHONE_WIDTH == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

/**
 *  专门为客户性别，年龄电话写的
 */
+ (UIFont *)fontWithCustomer:(CGFloat)fontSize {
    if (IPHONE_WIDTH > 375) {
        fontSize = fontSize + 2;
    }else if (IPHONE_WIDTH == 375){
        fontSize = fontSize + 1.5;
    }else if (IPHONE_WIDTH == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)navItemFontWithDevice:(CGFloat)fontSize {
    if (IPHONE_WIDTH > 375) {
        fontSize = fontSize + 2;
    }else if (IPHONE_WIDTH == 375){
        fontSize = fontSize + 1;
    }else if (IPHONE_WIDTH == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)fontWithTwoLine:(CGFloat)fontSize {
    if (IPHONE_WIDTH > 375) {
        fontSize = fontSize + 2;
    }else if (IPHONE_WIDTH == 375){
        fontSize = fontSize + 1;
    }else if (IPHONE_WIDTH == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

+ (UIFont *)insuranceCellFont:(CGFloat)fontSize {
    if (IPHONE_WIDTH > 375) {
        fontSize = fontSize + 3.5;
    }else if (IPHONE_WIDTH == 375){
        fontSize = fontSize + 2;
    }else if (IPHONE_WIDTH == 320){
        fontSize = fontSize;
    }
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    return font;
}

@end

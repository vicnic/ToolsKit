//
//  UIView+JNExtension.h
//  CardButlerSDK
//
//  Created by Jinniu on 2019/10/12.
//  Copyright © 2019年 jinniu2. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef UIView *(^ConrnerCorner) (UIRectCorner corner);
typedef UIView *(^ConrnerBounds) (CGRect bounds);
typedef UIView *(^ConrnerRadius) (CGFloat radius);
typedef UIView *(^BorderColor)   (UIColor* color);
typedef UIView *(^BorderWidth)   (CGFloat width);
typedef UIView *(^ShadowColor)   (UIColor* color);
typedef UIView *(^ShadowOffset)  (CGSize size);
typedef UIView *(^ShadowRadius)  (CGFloat radius);
typedef UIView *(^ShadowOpacity) (CGFloat opacity);
typedef UIView *(^ShowVisual) (void);
typedef UIView *(^ClerVisual) (void);

@interface UIView (JNExtension)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;


/**
 *  9.上 < Shortcut for frame.origin.y
 */
@property (nonatomic) CGFloat top;

/**
 *  10.下 < Shortcut for frame.origin.y + frame.size.height
 */
@property (nonatomic) CGFloat bottom;

/**
 *  11.左 < Shortcut for frame.origin.x.
 */
@property (nonatomic) CGFloat left;

/**
 *  12.右 < Shortcut for frame.origin.x + frame.size.width
 */
@property (nonatomic) CGFloat right;

- (void)addTarget:(id)target action:(SEL)action;
- (UIViewController*)getCurrentViewController;


// 圆角
@property(nonatomic, strong, readonly)ConrnerCorner conrnerCorner;  // UIRectCorner 默认UIRectCornerAllCorners
@property(nonatomic, strong, readonly)ConrnerBounds conrnerBounds;  // 在使用约束布局时必传 默认CGSizeZero
@property(nonatomic, strong, readonly)ConrnerRadius conrnerRadius;  // 圆角半径 默认0
// 边框
@property(nonatomic, strong, readonly)BorderColor borderColor;      // 边框颜色 默认black
@property(nonatomic, strong, readonly)BorderWidth borderWidth;      // 边框宽度 默认0
// 阴影
@property(nonatomic, strong, readonly)ShadowColor  shadowColor;     // 阴影颜色 默认black
@property(nonatomic, strong, readonly)ShadowOffset shadowOffset;    // 阴影偏移方向和距离 默认{0，0}
@property(nonatomic, strong, readonly)ShadowRadius shadowRadius;    // 阴影模糊度 默认0
@property(nonatomic, strong, readonly)ShadowOpacity shadowOpacity;  // (0~1] 默认0

@property(nonatomic, strong, readonly)ShowVisual showVisual; // 展示
@property(nonatomic, strong, readonly)ClerVisual clerVisual; // 隐藏

@end

NS_ASSUME_NONNULL_END

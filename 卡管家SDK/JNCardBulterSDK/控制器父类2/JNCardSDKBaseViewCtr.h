//
//  JNCardSDKBaseViewCtr.h
//  7UGame
//
//  Created by 111 on 2017/4/26.
//  Copyright © 2017年 111. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface JNCardSDKBaseViewCtr : UIViewController
@property(nonatomic,copy)NSString * myTitle;
@property(nonatomic,copy)NSString * rightBtnName;
@property(nonatomic,retain)UIButton * rightBtn;
@property(nonatomic,retain)UIButton * backBtn;
@property(nonatomic,copy)NSString * mySubTitle;
@property(nonatomic,copy)NSString * leftBtnImage;
@property(nonatomic,retain)UIColor * rightBtnNameColor;
@property(nonatomic,retain)UIImage * rightBtnImage;
@property(nonatomic,assign)CGFloat rightBtnFontSize;
@property(nonatomic,assign)CGFloat navBarHeight;
@property(nonatomic,assign)BOOL isHideLeftBtn;
@property(nonatomic,assign)CGFloat navTitleFontSize;
@property(nonatomic,retain)UIColor * navBgColor;
@property(nonatomic,assign)BOOL isHideBottomLine;
@property(nonatomic,assign)BOOL isHideNavBar;
@property(nonatomic,retain)UIColor * myTitleColor;
@property(nonatomic,retain)UIView * navView ;
-(void)createNavBar;
-(void)backClick;
-(void)rightBtnClick;
@end

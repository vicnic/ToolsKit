//
//  JNCardSDKJNCardPersonVC.m
//  TwoOneEight
//
//  Created by jinniu2 on 2019/3/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "JNCardSDKJNCardPersonVC.h"
#import "JNCardSDKNewConsumptionPatternsCtr.h"
#import "JNCardSDKRepayPlanVC.h"
@interface JNCardSDKJNCardPersonVC (){
    UIScrollView *_bgScroll;
}
@end

@implementation JNCardSDKJNCardPersonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self createUI];
}
-(void)createUI{
    
    _bgScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT-kTabbarHeight)];
    _bgScroll.alwaysBounceVertical = YES;
    _bgScroll.showsVerticalScrollIndicator = NO;
    _bgScroll.backgroundColor = [UIColor colorWithRed:0.96 green:0.95 blue:0.95 alpha:1];
    [self.view addSubview:_bgScroll];
    
    UIView *topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    [_bgScroll addSubview:topView];
    topView.sd_layout.topEqualToView(_bgScroll).leftEqualToView(_bgScroll).rightEqualToView(_bgScroll);
    
    UILabel *topLabel = [UILabel labelWithTitle:@"个人中心" color:[UIColor blackColor] font:[UIFont boldSystemFontOfSize:18] alignment:NSTextAlignmentCenter];
    [topView addSubview:topLabel];
    topLabel.sd_layout.topSpaceToView(topView, kTopHeight-13).leftEqualToView(topView).rightEqualToView(topView).heightIs(18);
    JNCardSDKVicSingleObject * single = [JNCardSDKVicSingleObject getInstance];
    UIImageView *headerImageView = [UIImageView new];
    if(single.jnAvatar){
        headerImageView.image = single.jnAvatar;
    }else{
        headerImageView.image = PlaceHolderImg;
    }
    headerImageView.cornerRadius = 45;
    [topView addSubview:headerImageView];
    headerImageView.sd_layout.topSpaceToView(topLabel, 45).centerXEqualToView(topView).heightIs(90).widthEqualToHeight();
    
    UILabel *userNameLab = [UILabel labelWithTitle:single.username color:[UIColor blackColor] font:[UIFont boldSystemFontOfSize:18] alignment:NSTextAlignmentCenter];
    [topView addSubview:userNameLab];
    userNameLab.sd_layout.topSpaceToView(headerImageView, 15).leftEqualToView(topView).rightEqualToView(topView).heightIs(16);
    
    UILabel *nameLab = [UILabel labelWithTitle:single.hykRealName color:[UIColor lightGrayColor] fontSize:14 alignment:NSTextAlignmentCenter];
    [topView addSubview:nameLab];
    nameLab.sd_layout.topSpaceToView(userNameLab, 14).leftEqualToView(topView).rightEqualToView(topView).heightIs(13);
    
    [topView setupAutoHeightWithBottomView:nameLab bottomMargin:45];
    
    UIView *lastView = nil;
    for (int i=0; i<2; i++) {
        UIView *view = [UIView new];
        view.tag = 10 + i;
        [view addTarget:self action:@selector(cellClick:)];
        view.backgroundColor = [UIColor whiteColor];
        [_bgScroll addSubview:view];
        view.sd_layout.topSpaceToView(lastView==nil?topView:lastView, lastView==nil?10:0.6).leftEqualToView(_bgScroll).rightEqualToView(_bgScroll).heightIs(55);
        lastView = view;
        [self createBottomViewSubViewsWithSuperView:view withTag:i];
    }
}
-(void)createBottomViewSubViewsWithSuperView:(UIView*)superView withTag:(NSInteger)tag{
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = Image(tag==0?@"个人往期计划":@"个人消费模式");
    [superView addSubview:imageView];
    imageView.sd_layout.centerYEqualToView(superView).leftSpaceToView(superView, 15).heightIs(18).widthEqualToHeight();
    
    UIImageView *rightImageView = [UIImageView new];
    rightImageView.image = Image(@"右箭头");
    [superView addSubview:rightImageView];
    rightImageView.sd_layout.centerYEqualToView(superView).rightSpaceToView(superView, 10).heightIs(16).widthEqualToHeight();
    
    UILabel *label = [UILabel labelWithTitle:tag==0?@"往期计划":@"消费模式" color:[UIColor blackColor] fontSize:16 alignment:NSTextAlignmentLeft];
    [superView addSubview:label];
    label.sd_layout.centerYEqualToView(superView).leftSpaceToView(imageView, 13).rightSpaceToView(rightImageView, 13).heightIs(20);
    
}
-(void)cellClick:(UITapGestureRecognizer*)tap{
    NSInteger tag = tap.view.tag;
    if (tag==10) {
        JNCardSDKRepayPlanVC * vc = [JNCardSDKRepayPlanVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        JNCardSDKNewConsumptionPatternsCtr * vc = [JNCardSDKNewConsumptionPatternsCtr new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end

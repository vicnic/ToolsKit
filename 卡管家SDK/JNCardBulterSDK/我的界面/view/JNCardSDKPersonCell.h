//
//  JNCardSDKPersonCell.h
//  TwoOneEight
//
//  Created by Jinniu on 2018/6/13.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JNCardSDKPersonCell : UITableViewCell
@property(nonatomic,retain)UIImageView * imgView;
@property(nonatomic,retain)UILabel * titleLab;
@property(nonatomic,retain)UILabel * descLab;
@property(nonatomic,retain)UIView * lineView; 
@property(nonatomic,retain)UISwitch * rightSwitch;
@property (nonatomic,retain)UIImageView * arro;
@end

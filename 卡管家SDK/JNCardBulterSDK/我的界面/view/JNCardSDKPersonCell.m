//
//  JNCardSDKPersonCell.m
//  TwoOneEight
//
//  Created by Jinniu on 2018/6/13.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "JNCardSDKPersonCell.h"

@implementation JNCardSDKPersonCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.imgView = [UIImageView new];
    [self.contentView addSubview:self.imgView];
    _imgView.sd_layout.leftSpaceToView(self.contentView, 10).centerYEqualToView(self.contentView).heightIs(30).widthEqualToHeight();
    
    self.titleLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:15];
    [self.contentView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(_imgView, 10).heightIs(16).centerYEqualToView(_imgView);
    [_titleLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.arro = [UIImageView new];
    self.arro.image = Image( @"右箭头");
    [self.contentView addSubview:self.arro];
    self.arro.sd_layout.rightSpaceToView(self.contentView, 10).centerYEqualToView(_titleLab).widthEqualToHeight().heightIs(20);
    
    self.descLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:14];
    [self.contentView addSubview:self.descLab];
    _descLab.sd_layout.rightSpaceToView(self.arro, 5).centerYEqualToView(self.arro).heightIs(16);
    [_descLab setSingleLineAutoResizeWithMaxWidth:0];
    
    self.lineView = [UIView new];
    self.lineView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    self.lineView.hidden = YES;
    [self.contentView addSubview:self.lineView];
    self.lineView.sd_layout.leftSpaceToView(self.contentView, 10).bottomEqualToView(self.contentView).rightEqualToView(self.contentView).heightIs(0.8);
    
    self.rightSwitch = [UISwitch new];
    self.rightSwitch.hidden = YES;
    self.rightSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [self.contentView addSubview:self.rightSwitch];
    self.rightSwitch.sd_layout.centerYEqualToView(self.contentView).rightSpaceToView(self.contentView, 10).widthIs(30).heightIs(20);
    
}
@end

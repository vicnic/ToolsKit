//
//  JKCountDownButton.m
//  JKCountDownButton
//
//  Created by Jakey on 15/3/8.
//  Copyright (c) 2015年 www.skyfox.org. All rights reserved.
//

#import "JKCountDownButton.h"
@interface JKCountDownButton(){
    NSInteger _second;
    NSUInteger _totalSecond;
    
    NSTimer *_timer;
    NSDate *_startDate;
    
    CountDownChanging _countDownChanging;
    CountDownFinished _countDownFinished;
    TouchedCountDownButtonHandler _touchedCountDownButtonHandler;
}
@end

@implementation JKCountDownButton
#pragma -mark touche action
- (void)countDownButtonHandler:(TouchedCountDownButtonHandler)touchedCountDownButtonHandler{
    _touchedCountDownButtonHandler = [touchedCountDownButtonHandler copy];
    [self addTarget:self action:@selector(touched:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)touched:(JKCountDownButton*)sender{
    if (_touchedCountDownButtonHandler) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_touchedCountDownButtonHandler(sender,sender.tag);
        });
    }
}

#pragma -mark count down method
- (void)startCountDownWithSecond:(NSUInteger)totalSecond
{
    _totalSecond = totalSecond;
    _second = totalSecond;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerStart:) userInfo:nil repeats:YES];
    _startDate = [NSDate date];
    _timer.fireDate = [NSDate distantPast];
    [[NSRunLoop currentRunLoop]addTimer:_timer forMode:NSRunLoopCommonModes];
}
- (void)timerStart:(NSTimer *)theTimer {
     double deltaTime = [[NSDate date] timeIntervalSinceDate:_startDate];
    
     _second = _totalSecond - (NSInteger)(deltaTime+0.5) ;

    
    if (_second< 0.0)
    {
        [self stopCountDown];
    }
    else
    {
        if (_countDownChanging)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setTitle:self->_countDownChanging(self,self->_second) forState:UIControlStateNormal];
                [self setTitle:self->_countDownChanging(self,self->_second) forState:UIControlStateDisabled];
            });
        }
        else
        {
            NSString *title = @"";
            if (self.startCountTitle.length) {
                title = [NSString stringWithFormat:@"%@%zd秒",self.startCountTitle,_second];
                [self setTitleColor:self.startCountColor forState:UIControlStateNormal];
            }else{
                title = [NSString stringWithFormat:@"%zd秒",_second];
            }            
            [self setTitle:title forState:UIControlStateNormal];
            [self setTitle:title forState:UIControlStateDisabled];

        }
    }
}
-(void)setStartCountTitle:(NSString *)startCountTitle{
    _startCountTitle = startCountTitle;
}
- (void)setEndCountTitle:(NSString *)endCountTitle{
    _endCountTitle = endCountTitle;
}
- (void)setStartCountColor:(UIColor *)startCountColor{
    _startCountColor = startCountColor;
}
- (void)setEndCountColor:(UIColor *)endCountColor{
    _endCountColor = endCountColor;
}
- (void)stopCountDown{
    if (_timer) {
        if ([_timer respondsToSelector:@selector(isValid)])
        {
            if ([_timer isValid])
            {
                [_timer invalidate];
                _second = _totalSecond;
                if (_countDownFinished)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setTitle:self->_countDownFinished(self,self->_totalSecond)forState:UIControlStateNormal];
                        [self setTitle:self->_countDownFinished(self,self->_totalSecond)forState:UIControlStateDisabled];
                    });
                }
                else
                {
                    if (self.endCountTitle.length) {
                        [self setTitle:self.endCountTitle forState:UIControlStateNormal];
                        [self setTitle:self.endCountTitle forState:UIControlStateDisabled];
                        [self setTitleColor:self.endCountColor forState:UIControlStateNormal];
                    }else{
                        [self setTitle:@"重新获取" forState:UIControlStateNormal];
                        [self setTitle:@"重新获取" forState:UIControlStateDisabled];
                    }
                }
            }
        }
    }
}
#pragma -mark block
- (void)countDownChanging:(CountDownChanging)countDownChanging{
    _countDownChanging = [countDownChanging copy];
}
- (void)countDownFinished:(CountDownFinished)countDownFinished{
    _countDownFinished = [countDownFinished copy];
}
@end
